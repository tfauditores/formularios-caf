<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    /** Include Conection file */
    require_once 'include/conexion.php';
?>
<!DOCTYPE HTML>
<html lang="es_CO">
	<head>
		<title>Transferencias</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
        <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-darkness.css">
        <link rel="stylesheet" type="text/css" href="css/altas.css">
        <link rel="stylesheet" type="text/css" href="css/datosTecnicos.css">
        <link rel="stylesheet" type="text/css" href="css/transferencias.css">
        <link rel="stylesheet" type="text/css" href="css/masive.css">
        
        <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
        
        <script src="vendor/jquery/jquery-1.12.4.js"></script>
        <script src="vendor/jqueryui/jquery-ui.js"></script>
        <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>

        <script src="js/lib/jquery.priceformat.min.js"></script>
        <script src="js/lib/jquery.validate.min.js"></script>
        <script src="js/functions-transferencias.js"></script>
        <script src="js/data-load-transferencias.js"></script>

	</head>
	<body>
        <div id="container">
            <div class="row"  style="display:none;">
                <div class="col-md-6">
                    <img src="img/logoForm.jpg" alt="Ecopetrol">
                </div>
                <div class="col-md-6">
                    <div class="row row-header">GERENCIA DE SERVICIOS COMPARTIDOS</div>
                    <div class="row row-header">DEPARTAMENTO DE SERVICIOS DE INFRAESTRUCTURA Y TIERRAS</div>
                    <div class="row row-header">SOLICITUD DE SERVICIO DE GESTIÓN INMOBILIARIA</div>
                </div>
            </div>

            <form id="formatoTransferencias" name="formatoTransferencias" method="get" action="">
                <div class="row text-left nav-view">
                    <label class="btn btn-styled btn-single fix-margin active">
                        Transferencia sencilla 
                    </label>
                    <label class="btn btn-styled btn-multiple fix-margin">
                        Transferencias multiples
                        <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xls">
                    </label>
                </div>
                <div class="single-view">
                    <p class="red">* Ingrese número de activo o número de inventario para hacer la consulta</p>
                    <div class="form-group row box" id="gcmBox">
                        <div class="form-section col-sm-2">
                            
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="activo">Número de Activo: </label>
                            <input class="form-control" type="text" value="" id="activo" name="activo" autofocus  >
                        </div>
                        <div class="form-section sn col-sm-2">
                            <label for="subn" class="">Subnúmero: </label>
                            <input type="number" id="subn" name="subn" class="form-control valid" placeholder="SUBNÚMERO" value="0" min="0" aria-invalid="false" disabled>
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="placa" class="">NÚMERO DE INVENTARIO (PLACA): </label>
                            <input class="form-control" type="text" value="" id="placa" name="placa"  >
                        </div>
                        <div class="form-section col-sm-2 operacionBox">
                            <label for="operacion" class="">Operación: </label>
                            <input class="form-control" type="text" value="" id="operacion" name="operacion" readonly >
                        </div>
                        
                    </div>  

                    <div class="row show-characteristics">
                        <span>Ver características <i class="ion ion-eye"></i></span>
                    </div>

                    <div class="slide-info row">
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Denominación</label>
                                <span data-name="denominacion"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Denominación 2</label>
                                <span data-name="denominacion2"></span>
                            </div>
                        </div>  

                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Clase</label>
                                <span data-name="clase"></span>
                            </div>
                        </div>                    
                
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Fecha Capitalización</label>
                                <span data-name="fechaCapitalizacion"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Capacidad</label>
                                <span data-name="capacidad"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Unidad de medida</label>
                                <span data-name="unidadMedida"></span>
                            </div>
                        </div>                   
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Fabricante</label>
                                <span data-name="fabricante"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Tag</label>
                                <span data-name="tag"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Serie</label>
                                <span data-name="serie"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Modelo</label>
                                <span data-name="modelo"></span>
                            </div>
                        </div>                    
                        <input type="hidden" data-name="claveProd" id="claveProd" name="claveProd" >                   
                        <input type="hidden" data-name="claveOtros" id="claveOtros" name="claveOtros" >                   
                    </div>

                    <div class="modificaciones row">
                        <p style=" text-align: left; color: #d81919; ">* Edite únicamente los campos que requiere actualizar. </p>

                        <div class="row"> <!-- DMC -->
                            <div class="col-lg-4 col-md-4"> <!-- departamento -->
                                <div class="form-section">
                                    <label>DEPARTAMENTO:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="departamento" name="departamento" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <!-- <input class="form-control user-input" type="text" value="" id="departamentoNew" name="departamentoNew"  >-->
                                            <select class="form-control" name="departamentoNew" id="departamentoNew">
                                                <option value="">Seleccione Departamento</option>
                                                <?php
                                                    $select=mysqli_query($con, "SELECT DISTINCT departamento FROM ubicaciones");
                                                    while($row=mysqli_fetch_array($select))
                                                    {
                                                        $dpto = $row['departamento'];
                                                        echo "<option value=\"".$dpto."\">".$dpto."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <!-- municipio -->
                                <div class="form-section">
                                    <label>MUNICIPIO:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="municipio" name="municipio" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <!--<input class="form-control user-input" type="text" value="" id="municipioNew" name="municipioNew" >-->
                                            <select class="form-control" 
                                                    name="municipioNew" 
                                                    id="municipioNew" 
                                                    data-toggle="tooltip" 
                                                    data-placement="left" 
                                                    title="Para editar este campo seleccione primero Departamento">
                                                <option value="">Seleccione Municipio</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <!-- Campo -->
                                <div class="form-section">
                                    <label>CAMPO PLANTA:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="campo" name="campo" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <!-- <input class="form-control user-input" type="text" value="" id="campoNew" name="campoNew"> -->
                                            <select class="form-control" 
                                                    name="campoNew" 
                                                    id="campoNew" 
                                                    data-toggle="tooltip" 
                                                    data-placement="left" 
                                                    title="Para editar este campo seleccione primero Departamento y Municipio">
                                                <option value="">Seleccione Campo Planta</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> <!-- DMC -->
                            <div class="col-lg-4 col-md-4"> <!-- ubicacion -->
                                <div class="form-section">
                                    <label>UBICACION GEOGRÁFICA:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="ubicacion" name="ubicacion" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <!--<input class="form-control user-input" type="text" value="" id="ubicacionNew" name="ubicacionNew"  >-->
                                            <select class="form-control" 
                                                    name="ubicacionNew" 
                                                    id="ubicacionNew" 
                                                    data-toggle="tooltip" 
                                                    data-placement="left" 
                                                    title="Para editar este campo seleccione primero Departamento y Municipio">    
                                                <option value="">Seleccione Ubicación</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <!-- unidad funcional -->
                                <div class="form-section">
                                    <label>Unidad Funcional:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6 col-lg-6 col-md-6">
                                            <input class="form-control" type="text" value="" id="unidadFuncional" name="gerencia" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6 col-lg-6 col-md-6">
                                            <select class="form-control" name="unidadFuncionalNew" id="unidadFuncionalNew">
                                                <option value="">Seleccione UF</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row"> <!-- AVG -->
                            <div class="col-lg-4 col-md-4"> <!-- area -->
                                <div class="form-section">
                                    <label>Area:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6 col-lg-6 col-md-6">
                                            <input class="form-control" type="text" value="" id="area" name="area" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6 col-lg-6 col-md-6 ">
                                            <!--<input class="form-control user-input" type="text" value="" id="areaNew" name="areaNew"  >-->
                                            <select class="form-control" name="areaNew" id="areaNew">
                                                <option value="">Seleccione Área</option>
                                                <?php
                                                    $select=mysqli_query($con, "SELECT DISTINCT area FROM jerarquizacion WHERE CHAR_LENGTH(area)>1 ORDER BY area ASC");
                                                    while($row=mysqli_fetch_array($select))
                                                    {
                                                        $area = $row['area'];
                                                        echo "<option value=\"".$area."\">".$area."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <!-- vicepresidencia -->
                                <div class="form-section">
                                    <label>Vicepresidencia:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6 col-lg-6 col-md-6">
                                            <input class="form-control" type="text" value="" id="vicepresidencia" name="vicepresidencia" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6 col-lg-6 col-md-6">
                                            <!--<input class="form-control user-input" type="text" value="" id="vicepresidenciaNew" name="vicepresidenciaNew" >-->
                                            <select class="form-control" name="vicepresidenciaNew" id="vicepresidenciaNew">
                                                <option value="">Seleccione Vicepresidencia</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4"> <!-- gerencia -->
                                <div class="form-section">
                                    <label>Gerencia:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="gerencia" name="gerencia" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <select class="form-control" name="gerenciaNew" id="gerenciaNew">
                                                <option value="">Seleccione Gerencia</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                        <div class="row"> <!-- CCC -->
                            <div class="col-lg-4 col-md-4"> <!-- CECO -->
                                <div class="form-section">
                                    <label>CeCo:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="ceco" name="ceco" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6 cecoNew" >
                                            <input class="form-control user-input" type="text" value="" id="cecoNew" name="cecoNew" >
                                            <!--<input class="form-control user-input" type="text" id="cecoSearch" name="cecoSearch" >-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2"> <!-- Clave -->
                                <div class="form-section">
                                    <label>Clave Amortización:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-9">
                                            <input class="form-control" type="text" value="" id="clave" name="clave" readonly  >
                                        </div>
                                        <div class="form-section col-sm-3">
                                            <input class="form-control user-input" type="hidden" value="" id="claveNew" name="claveNew"  >
                                            <!--
                                            <select class="form-control" name="claveNew" id="claveNew">
                                                <option value="">Seleccione clave de amortización</option>
                                                <option value="ZVUR">ZVUR</option>
                                                <option value="UOP">UOP</option>
                                                <option value="NO DEPRECIA">NO DEPRECIA</option>
                                            </select>
                                            -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">                    
                                <div class="row vidaUtil">
                                    <div class="col-lg-12 col-md-12"> <!-- Campo -->
                                        <div class="form-section col-sm-12 col-md-12 col-lg-12" style="padding: 0;">
                                
                                            <div class="row">
                                                <div class="form-section col-sm-4">
                                                    <label>VUT (años):</label>
                                                </div>
                                                <div class="form-section col-sm-4">
                                                    <label>VUT (meses):</label>
                                                </div>
                                                <div class="form-section col-sm-4">
                                                    <label>VUT (total meses):</label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-section col-sm-4">
                                                    <input class="form-control user-input col-sm-3" type="text" id="vuANew" name="vuANew"  >
                                                </div>
                                                <div class="form-section col-sm-4">
                                                    <input class="form-control user-input col-sm-3" type="text" id="vuMNew" name="vuMNew"  >
                                                </div>
                                                <div class="form-section col-sm-4">
                                                    <input class="form-control vumcalc" type="text" name="vummNew" id="vummNew" readonly  >
                                                </div>
                                            </div>
                                        </div>
                                    </div>                      
                                </div>
                            </div> 
                        </div>

                        


                        <div class="form-group row box" id="justificacion">
                            <div class="form-section col-sm-12">
                                <label for="justificacionBox" class="">Observaciones: </label>
                                <textarea class="form-control"  rows="3" cols="70" id="justificacionBox" name="justificacionBox"></textarea>  
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-12 show-list">
                        <label data-modal="transferencias">
                            Ver listado de Solicitudes 
                            <i class="ion ion-android-list" data-modal="transferencias">
                                <sup>0</sup>
                            </i>
                        </label>
                    </div>

                    <div class="form-group row box" id="buttonBox">
                        <div class="col-center col-sm-12"> 
                            <button type="button" id="agregar" class="btn btn-primary button1">Agregar a la solicitud</button>
                            <button type="button" id="generarExcel" class="btn btn-secondary button2">Finalizar solicitud</button>
                        </div>
                    </div>
                </div>
                <div class="multiple-view">
                    <div class="status-block">
                        <nav class="nav-tabs" data-tab="multiple">
                            <ul>
                                <li class="active" data-type="success">Existosos <i>0</i></li>
                                <li data-type="error">Errores <i>0</i></li>
                            </ul>
                        </nav>
                        <div class="tabs" data-tab="multiple">
                            <div class="tab active">
                                <div class="cover-table">
                                    <table class="success-multiple">
                                        <thead>
                                            <tr>
                                                <th>Fila</th>
                                                <th>Código activo fijo</th>
                                                <th>Denominación</th>
                                                <th>No. Inventario</th>
                                                <th>Serie</th>
                                                <th>Cod. Clase</th>
                                                <th>Nuevo Cod. Municipio</th>
                                                <th>Nuevo Municipio</th>
                                                <th>Nuevo Cod. Ubicación Geográfica</th>
                                                <th>Nueva Ubicación Geográfica</th>
                                                <th>Nueva Cod. CeCo</th>
                                                <th>Nueva Cod. Campo Planta</th>
                                                <th>Nueva Campo Planta</th>
                                                <th>Vida Útil Total (en meses)</th>
                                                <th>Nueva Vicepresidencia</th>
                                                <th>Nueva Gerencia</th>
                                                <th>Observaciones</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                    <!-- <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button> -->
                                    <button type="button" id="generarExcel2" class="btn btn-secondary button2">Finalizar solicitud</button>
                                </div>
                            </div>
                            <div class="tab">
                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                </div>
                                <div class="error-multiple">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!--html modalbox subnumbers-->
        <div class="modalbox" data-modal="subnumbers">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="table1" >
            <table class="table table-striped table-responsive table-bajas table-data">
                <thead>
                    <tr>
                        <th>ACCIONES</th>
                        <th>ACTIVO</th>
                        <th>DENOMINACIÓN</th>
                        <th>NUMERO INVENTARIO</th>
                        <th>SERIE</th>
                        <th>CLASE</th>
                        <th>DEPARTAMENTO</th>
                        <th>MUNICIPIO</th>
                        <th>UBICACION GEOGRAFICA</th>
                        <th>VICEPRESIDENCIA</th>
                        <th>GERENCIA</th>
                        <th>CECO</th>
                        <th>CAMPO PLANTA</th>
                        <th>VUmeses</th>
                        <th>OBSERVACIONES</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>

        <div id="tableBox1" class="modalbox" data-modal="transferencias">
        <!--<div id="tableBox" class="modal" >-->
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table">
                            <table class="table table-striped table-responsive table-bajas table-data">
                                <thead>
                                    <tr>    
                                        <th>ACCIONES</th>
                                        <th>ACTIVO</th>
                                        <th>DENOMINACIÓN</th>
                                        <th>NUMERO INVENTARIO</th>
                                        <th>SERIE</th>
                                        <th>CLASE</th>
                                        <th>DEPARTAMENTO</th>
                                        <th>MUNICIPIO</th>
                                        <th>UBICACION GEOGRAFICA</th>
                                        <th>VICEPRESIDENCIA</th>
                                        <th>GERENCIA</th>
                                        <th>CECO</th>
                                        <th>CAMPO PLANTA</th>
                                        <th>VUmeses</th>
                                        <th>OBSERVACIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="wait">
            <div>
                <img src='img/demo_wait.gif' width="64" height="64" />
                <br>Buscando...
            </div>
        </div>

	</body>
</html>