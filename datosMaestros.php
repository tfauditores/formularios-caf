<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    // session_start();
    // var_dump(session_id());
    // echo $_GET['asd'];
?>
<!DOCTYPE html>
<html lang="es_CO">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Actualización de datos maestros auxiliar</title>
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
    <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-darkness.css">
    <link rel="stylesheet" href="css/altas.css">
    <link rel="stylesheet" href="css/generic.css">
    <link rel="stylesheet" type="text/css" href="css/masive.css">
    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <main class="main-content">
        <section class="module">
        <form action="" method="post" class="withdrawal-form" id="formatoDatosMaestros" autocomplete="foo">
            <div class="container big-container">
                <div class="row text-left nav-view">
                    <label class="btn btn-styled btn-single fix-margin active">
                        Actualización sencilla
                    </label>
                    <label class="btn btn-styled2 btn-multiple fix-margin">
                        Actualizaciones multiples
                        <!-- <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xls,.XLS,.XLSX"> -->
                        <input type="file" name="excel-file" class="upload-control" accept="application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">
                    </label>
                    <a href="formats/GFI-F-104-formato-actualizacion-datos-maestros-activo-fijos.xlsx" download>
                        <div class="btn-excel">
                            <span>FORMATO F-104</span>
                            <img src="img/excel.png" alt=""/>
                        </div>
                    </a>
                </div>
                <div class="single-view">
                    <div class="row">
                        <div class="col-lg-3 col-md-3">
                            <label>NÚMERO DE ACTIVO: *</label>
                            <input type="text" name="activo" class="form-control" autofocus autocomplete="foo">
                        </div>
                        <div class="col-lg-1 col-md-1">
                            <label>SUBNÚMERO</label>
                            <input type="number" name="subnumero" class="form-control" placeholder="SUBNÚMERO" value="0" min="0" autocomplete="foo">
                        </div>
                        <div class="col-lg-3 col-md-3">
                            <div class="form-section">
                                <label>NÚMERO DE INVENTARIO (PLACA): *</label>
                                <input type="text" name="inventario" class="form-control" autocomplete="foo">
                            </div>
                        </div>
                    </div>
                    <div class="row show-characteristics">
                        <span>Ver características <i class="ion ion-eye"></i></span>
                    </div>
                    <div class="row slide-info">
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>OPERACIÓN:</label>
                                <span data-name="operacion"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>CLASE:</label>
                                <span data-name="clase"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>CUSTODIO:</label>
                                <span data-name="custodio"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>VICEPRESIDENCIA:</label>
                                <span data-name="vicepresidencia"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>GERENCIA:</label>
                                <span data-name="gerencia"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>FECHA DE CAPITALIZACIÓN:</label>
                                <span data-name="fecha_capitalizacion"></span>
                            </div>
                        </div>
                    </div>
                    <div class="new-data">
                        <div class="ctrl-tabs">
                            <div class="tabs active">General</div>
                            <div class="tabs">Ubicación</div>
                        </div>
                        <div class="block-tabs">
                            <div class="tabs active">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label data-toggle="tooltip" data-html="true" title="- Aplica cuando <u>NO</u> implique cambio de clase <br> - Diligenciar la <u>nueva denominación</u> del activo, según Catálogo si es de operacion <u>directa</u> ">
                                                DENOMINACIÓN:
                                            </label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="denominacion"><i></i></span>
                                                <input type="text" name="denominacion" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>DENOMINACIÓN COMPLEMENTARIA:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="denominacion2"><i></i></span>
                                                <input type="text" name="denominacion2" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>CAPACIDAD:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="capacidad"><i></i></span>
                                                <input type="text" name="capacidad" class="form-control" autocomplete="foo"> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>UNIDAD DE MEDIDA:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="unidadMedida"><i></i></span>
                                                <input type="text" name="unidadMedida" class="form-control" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>NO INVENTARIO:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="noInventario"><i></i></span>
                                                <input type="text" name="noInventario" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>TAG:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="tag"><i></i></span>
                                                <input type="text" name="tag" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>SERIE:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="serie"><i></i></span>
                                                <input type="text" name="serie" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>FABRICANTE:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="fabricante"><i></i></span>
                                                <input type="text" name="fabricante" class="form-control autocomplete" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>MODELO:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="modelo"><i></i></span>
                                                <input type="text" name="modelo" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>TIPO DE OPERACIÓN:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="tipoOperacion"><i></i></span>
                                                <input type="text" name="tipoOperacion" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>MATRÍCULA VEHÍCULO:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="matriculaVehiculo"><i></i></span>
                                                <input type="text" name="matriculaVehiculo" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tabs">
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>DEPARTAMENTO:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="departamento"><i></i></span>
                                                <input type="text" name="departamento" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>MUNICIPIO:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="municipio"><i></i></span>
                                                <input type="text" name="municipio" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>UBICACIÓN GEOGRÁFICA:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="ubicacionGeografica"><i></i></span>
                                                <input type="text" name="ubicacionGeografica" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label  data-toggle="tooltip" title="El cambio de CECO se realiza a principal y subnúmeros">CECO:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="ceco"><i></i></span>
                                                <input type="text" name="ceco" class="form-control autocomplete" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>CAMPO PLANTA:</label>
                                            <div class="double-item">
                                                <!--<i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>-->
                                                <span data-name="campoPlanta"><i></i></span>
                                                <input type="text" name="campoPlanta" class="form-control autocomplete" autocomplete="foo" disabled>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>VIDA ÚTIL REMANENTE:</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="vuRemanente"><i></i></span>
                                                <input type="text" name="vuRemanente" class="form-control" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4">
                                        <div class="form-section">
                                            <label>FECHA DE VIDA ÚTIL REMANENTE (VUD): (AAAA/MM/DD)**</label>
                                            <div class="double-item">
                                                <i class="ion ion-android-create" data-toggle="tooltip" data-placement="top" title="Editar"></i>
                                                <span data-name="fechaVUR"><i></i></span>
                                                <input type="text" name="fechaVUR" class="form-control datepicker" autocomplete="foo">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 informative-vut">
                                        <div class="form-section">
                                            <label>LA NUEVA VIDA ÚTIL TÉCNICA SERÍA: </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-section">
                                    <label>Observaciones:</label>
                                    <textarea name="observaciones" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row disclaimer">
                            <div class="col-lg-12 mv20">
                                <p>* Ingrese número de activo o número de inventario para hacer la consulta. ** Fecha Vida Útil Remanente: es la fecha a partir de la cual se inicia la nueva vida útil remanente</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button>
                            <button class="btn btn-styled2 btn-end" type="button">FINALIZAR SOLICITUD</button>
                        </div>
                    </div>
                </div>
                <div class="multiple-view">
                    <div class="status-block">
                        <nav class="nav-tabs" data-tab="multiple">
                            <ul>
                                <li class="active" data-type="success">Activos agregados <i>0</i></li>
                                <li data-type="error">Activos con errores <i>0</i></li>
                            </ul>
                        </nav>
                        <div class="tabs" data-tab="multiple">
                            <div class="tab active">
                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary button2 btn-end">Finalizar solicitud</button>
                                </div>
                            </div>
                            <div class="tab">
                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                </div>
                                <div class="error-multiple"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="inline-table">
                    <table class="table table-striped table-responsive table-bajas table-data">
                        <thead class="thead-default">
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>No de activo</th>
                                <th>SN</th>
                                <th>Denominación</th>
                                <th>No de inventario</th>
                                <th>Serie</th>
                                <th>Tag</th><!-- ya -->
                                <th>Cod. Clase</th>
                                <th>Nueva denominación</th>
                                <th>Nueva denominación complementaria</th>
                                <th>Nueva serie</th>
                                <th>Nuevo No de inventario</th>
                                <th>Nueva capacidad</th>
                                <th>Nueva unidad de medida</th>
                                <th>Nuevo tag</th>
                                <th>Nueva Matrícula Vehículo</th><!-- ya -->
                                <th>Nuevo fabricante</th>
                                <th>Nuevo modelo</th>
                                <th>Nuevo cod. ceco</th>
                                <th>Nueva vicepresidencia</th>
                                <th>Nueva gerencia</th>
                                <th>Nuevo Registro Custodio</th><!-- no -->
                                <th>Nuevo cod. ubicación geográfica</th>
                                <th>Nueva ubicación geográfica</th>
                                <th>Nuevo cod. campo planta</th>
                                <th>Nuevo campo planta</th>
                                <th>Nuevo Tipo Operación</th>
                                <th>Nuevo cod. municipio</th>
                                <th>Nuevo municipio</th>
                                <th>ceco</th>
                                <th>vicepresidencia</th>
                                <th>gerencia</th>
                                <th>Fecha inicial vida útil remanente</th>
                                <th>Nueva vida útil remanente</th>
                                <th>Observaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        </section>
    </main>
    <footer class="main-footer">
        <div class="col-lg-12 supportedBrowsers">Aplicación probada y soportada en los navegadores:</div>
        <div class="col-lg-12 browserIcons">
            <span title="Descargar Chrome" class="browser">
                <a href="https://www.google.com/chrome/" target="_blank">CHROME</a>
            </span>
            <span title="Descargar Firefox" class="browser firefox">
                <a href="https://www.mozilla.org/es-ES/firefox/" target="_blank">FIREFOX</a>
            </span>
            <span title="Abrir en Edge" class="browser edge">
                <a href="microsoft-edge:https://ecopetrol.sharepoint.com/sites/CAF/SitePages/actualización-datos-maestros.aspx"> EDGE</a>
            </span>
            <span title="Descargar Opera" class="browser opera">
                <a href="https://www.opera.com/es-419/download" target="_blank">OPERA</a>
            </span>
        </div>
    </footer>
    <div class="thankyou-message">
        <div class="table">
            <div class="table-cell">
                <h2 class="title">GRACIAS POR REALIZAR LA SOLICITUD, <br>PRONTO ATENDEREMOS SU REQUERIMIENTO</h2>
            </div>
        </div>
    </div>
    <div id="wait">
        <div>
            <img src='img/demo_wait.gif' width="64" height="64" />
            <br>Cargando...
        </div>
    </div>
    <script src="vendor/jquery/jquery-1.12.4.js"></script>
    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>  -->
    <script src="vendor/jqueryui/jquery-ui.js"></script>
    <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
    <script src="js/lib/jquery.priceformat.min.js"></script>
    <script src="js/lib/jquery.validate.min.js"></script>
    <script src="js/datos-maestros.js"></script>
    <!-- <script src="js/data-load-bajas.js"></script> -->
</body>
</html>
