/*react Components*/
// import React from 'react';
// import { render } from 'react-dom';

// import App from '../react/hello.jsx';
// ReactDOM.render(<App/>, document.getElementById('content'));

// import Home from '../react/views/home.jsx';
//
// if(document.getElementById('content') != null){
//   render(<Home/>, document.getElementById('content'));
// }

// import Calendar from '../react/components/calendar.jsx';

// if( document.getElementById('calendar') != null ){
//   render(<Calendar lang="ES"/>, document.getElementById('calendar'));
// }


/*ES6 Components*/
// import es6 from '../es6/index'
//
// console.log(es6.sqrt(9));
//
// async function asd(){
//   const obj = await es6.api();
//   console.log(obj);
//   console.log('await from async function');
// }
// asd();

/*javascript*/
require('./main.styl');

var $ = require('../../node_modules/jquery');
window.jQuery = $;
window.$ = $;

//require('../../node_modules/slick-carousel/slick/slick.min');

(function($){

	/**Types of validations**/
	var numericSpaceRegex = /^[0-9 ]+$/
  , integerRegex = /^\-?[0-9]+$/
  , decimalRegex = /^\-?[0-9]*\.?[0-9]+$/
  , emailRegex = /^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,6}$/i
  , alphaRegex = /^[a-z]+$/i
  , alphaSpaceRegex = /^[a-z ]+$/i
  , alphaNumericRegex = /^[a-z0-9]+$/i
  , alphaDashRegex = /^[a-z0-9_-]+$/i
  , naturalRegex = /^[0-9]+$/i
  , naturalNoZeroRegex = /^[1-9][0-9]*$/i
  , ipRegex = /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i
  , base64Regex = /[^a-zA-Z0-9\/\+=]/i
  , validateEmail = function( arg ) { return emailRegex.test(arg);}
  , validateAlphaSpace = function( arg ) { return alphaSpaceRegex.test(arg); }
  , validateNumericSpace = function( arg ) { return numericSpaceRegex.test(arg); };

  $('.user-form button').on('click', function(event) {
  	event.preventDefault();

  	for(let i of $('.user-form').find('.form-ctrl')){

  		$('.user-form').find('.form-section').removeClass('error');
  		$('.user-form').find('.error-message,.block-error').remove();

  		let focus = $(i).attr('name'),
  				value = $(i).val().trim();

  		switch(focus){
  			case 'name':
  				if(!alphaSpaceRegex.test(value)){
  					$(i).parents('.form-section').addClass('error');
  					let errorMessage = `<p class="error-message">Se debe escribir un nombre valido</p>`;
  					$(i).parents('.form-section').prepend(errorMessage);
  					$(i).focus();
  					return false;
  				}
  				break;
  			case 'lastname':
  				if(!alphaSpaceRegex.test(value)){
  					$(i).parents('.form-section').addClass('error');
  					let errorMessage = `<p class="error-message">Se debe escribir un apellido valido</p>`;
  					$(i).parents('.form-section').prepend(errorMessage);
  					$(i).focus();
  					return false;
  				}
  				break;
  			case 'email':
  				if(!emailRegex.test(value)){
  					$(i).parents('.form-section').addClass('error');
  					let errorMessage = `<p class="error-message">Se debe escribir un email valido</p>`;
  					$(i).parents('.form-section').prepend(errorMessage);
  					$(i).focus();
  					return false;
  				}
  				break;
  			case 'telephone':
  				if(!integerRegex.test(value)){
  					$(i).parents('.form-section').addClass('error');
  					let errorMessage = `<p class="error-message">Se debe escribir un teléfono valido</p>`;
  					$(i).parents('.form-section').prepend(errorMessage);
  					$(i).focus();
  					return false;
  				}
  				break;
  		}

  	}

  	var serialize = $(this).parents('form').serialize();

  	$.post('/igniter/api/dual', serialize, function(data){
  		if(typeof(data) == 'object'){
  			var json = data;
  		}
  		else{
				var json = JSON.parse(data);
  		}

  		if(json.success){
  			$('.user-form').find('.form-ctrl').val('');
  		}

  		if(json.error){
  			let errorMessage = '';

  			json.error.name != undefined ? errorMessage += json.error.name+' \n' : ''; 
  			json.error.lastname != undefined ? errorMessage += json.error.lastname+' \n' : ''; 
  			json.error.email != undefined ? errorMessage += json.error.email+' \n' : ''; 
  			json.error.telephone != undefined ? errorMessage += json.error.telephone+' \n' : ''; 
  			
  			$('.user-form').prepend('<div class="block-error">'+errorMessage+'</div>')
  		}

  	})
  });
	
})(jQuery)
