(function($){

    /* 
        GLOBAL FUNCTIONS 
    */

    /* 
        GET Salario minimo historico
    */
    function getHistoricSMM(){
        var url = 'include/consultaSMMHistorico.php'
        var obj = "";
        $.ajax({
            url: url,
            dataType: 'json',
            async: false,
            success: function(data) {
                obj = data;
            }
        });
        return obj;
    }

    /* 
        Valor de adquisición - baja parcial
    */
    // var SMMH = getHistoricSMM();
    
    function getVAminusBP(focus){
        var BP = Number( $(focus).unmask() );
        var VA = Number( $('[name="valor_adquisicion"]').unmask() );
        $('.withdrawal-result')
            .text( VA - BP )
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            });
    }

    function getValueByPercentage(value,percent){
        var percentTemp = 0;
        percent > 100 ? percentTemp = 100: percentTemp = percent;
        return Math.round( (percentTemp*value)/100 );
    }
    /* set price format in tables*/
    function priceFormatTables(){
        $.each($('.table-bajas tbody tr'),function(i,e){
            $(e).find('td:eq(21),td:eq(20)').priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            });
        });

        /* clear rows masive low */
        $.each($('.rows_masive_data tbody tr'),function(i,e){
            if(i == 0){
                $(this).find('.form-control').val('');
            }
            else{
                $(this).remove();
            }
        });
    }
    /*
        Consulta masiva de activos
    */
    function requestMultipleBajas(values){
        $('#wait').fadeIn(400);
        var lengthData = values.length;

        $.each(values, function(ind,ele){
            
            var indexData = ind+1;
            var numberActive = ele.active;
            var numberInventory = ele.inventario;
            var subnumberActive = ele.sn;
            var lowType = ele.type;
            var lowValue = ele.low;
            var continueBool = true;

            $.each($($('.table-bajas')[0]).find('tbody tr'), function(i,e){
                if(
                    numberInventory == $(e).find('td:eq(2)').text() &&
                    numberActive == $(e).find('td:eq(3)').text() && 
                    subnumberActive == $(e).find('td:eq(4)').text()
                ){
                    continueBool = false;
                }
            });

            if(continueBool){
                var url = 'include/consultasBajas.php';

                switch(lowType){
                    case 'total':
                        if(ele.inventario != ""){
                            var requestJSON = {
                                'id_referencia': 'noInventario',
                                'id_numero_referencia': numberInventory
                            };
                        }
                        else{
                            var requestJSON = {
                                'id_referencia': 'activo',
                                'id_numero_referencia': numberActive
                            };
                        }

                        $.getJSON(url, requestJSON, function(json){
                            if(json.result == undefined){
                                var data = {
                                    numero_inventario: json.inventario,
                                    numero_activo: json.activo,
                                    subnumero_activo: json.sn,
                                    departamento: json.departamento,
                                    municipio: json.municipio,
                                    ubicacion_geografica: json.ubicacion_geografica,
                                    nombre_ceco: json.nombre_ceco,
                                    numero_ceco: json.numero_ceco,
                                    area: json.area,
                                    vicepresidencia: json.vicepresidencia,
                                    gerencia: json.gerencia,
                                    denominacion: json.denominacion,
                                    denominacion_2: json.denominacion2,
                                    marca_fabricante: json.marca_fabricante,
                                    modelo: json.modelo,
                                    municipio: json.municipio,
                                    serie: json.serie,
                                    tag: json.tag,
                                    motivo_retiro: 'SELECCIONE...',
                                    disposicion_final: 'SELECCIONE...',
                                    valor_adquisicion: json.valor_adquisicion,
                                    valor_baja: json.valor_adquisicion,
                                    cod_municipio: json.cod_municipio,
                                    cod_ubicacion_geografica: json.cod_ubicacion_geografica,
                                    tipo_baja: 'BAJA TOTAL',
                                    has_subnumbers: json.conteo_subnumeros_activo != 0 ? 1 : 0
                                }
                                
                                genericTable(data);
                                showAddInformation();
                            } 
            
                            if(indexData == lengthData){

                                $('.modalbox[data-modal="multiples"]').fadeOut(400);
                                $('#wait').fadeOut(400);
                                $('.show-list').fadeIn(400);
                                $('.show-list sup').text($('.modalbox[data-modal="bajas"] tbody tr').length);
                                $('.inline-table').fadeIn(400);
                                $('.ctrls-table').fadeIn(400);
                                $('[data-modal="bajas"]').fadeIn(400);

                            }
                            priceFormatTables();
                        });
                    break;
                    case 'parcial':
                        if(ele.inventario != ""){
                            var requestJSON = {
                                'id_referencia': 'noInventario',
                                'id_numero_referencia': numberInventory,
                                'id_subnumero_referencia': subnumberActive
                            };
                        }
                        else{
                            var requestJSON = {
                                'id_referencia': 'activo',
                                'id_numero_referencia': numberActive,
                                'id_subnumero_referencia': subnumberActive
                            };
                        }

                        $.getJSON(url, requestJSON, function(json){
                            
                            if(json.result == undefined){
                                if( 
                                    lowValue > Math.round(Number(json.valor_adquisicion)*75/100) ||
                                    lowValue == 0
                                ){
                                    if(subnumberActive == 0){
                                        lowValue = Math.round(Number(json.valor_adquisicion)*75/100);
                                    }
                                    else{
                                        lowValue = Math.round(Number(json.valor_adquisicion));
                                    }
                                }
                                var data = {
                                    numero_inventario: json.inventario,
                                    numero_activo: json.activo,
                                    subnumero_activo: json.sn,
                                    departamento: json.departamento,
                                    municipio: json.municipio,
                                    ubicacion_geografica: json.ubicacion_geografica,
                                    nombre_ceco: json.nombre_ceco,
                                    numero_ceco: json.numero_ceco,
                                    area: json.area,
                                    vicepresidencia: json.vicepresidencia,
                                    gerencia: json.gerencia,
                                    denominacion: json.denominacion,
                                    denominacion_2: json.denominacion2,
                                    marca_fabricante: json.marca_fabricante,
                                    modelo: json.modelo,
                                    municipio: json.municipio,
                                    serie: json.serie,
                                    tag: json.tag,
                                    motivo_retiro: 'SELECCIONE...',
                                    disposicion_final: 'SELECCIONE...',
                                    valor_adquisicion: json.valor_adquisicion,
                                    valor_baja: lowValue,
                                    cod_municipio: json.cod_municipio,
                                    cod_ubicacion_geografica: json.cod_ubicacion_geografica,
                                    tipo_baja: 'BAJA PARCIAL',
                                    has_subnumbers: json.conteo_subnumeros_activo != 0 ? 1 : 0
                                }
                                
                                genericTable(data);
                            } 
            
                            if(indexData == lengthData){

                                $('.modalbox[data-modal="multiples"]').fadeOut(400);
                                $('#wait').fadeOut(400);
                                $('.show-list').fadeIn(400);
                                $('.show-list sup').text($('.modalbox[data-modal="bajas"] tbody tr').length);
                                $('.inline-table').fadeIn(400);
                                $('.ctrls-table').fadeIn(400);
                                $('[data-modal="bajas"]').fadeIn(400);

                            }
                            priceFormatTables();
                            showAddInformation();
                        });
                    break;
                }
            }
            else{
                $('.modalbox[data-modal="multiples"]').fadeOut(400);
                $('#wait').fadeOut(400);
                modalMessage({message: 'El activo ya existe en la tabla', type: 'warning'});
            }

        });
    }

    /* show additional information */
    function showAddInformation(){
        if(!$('.show-characteristics').is(':visible')){
            $('.show-characteristics').fadeIn();
            // $('.slide-info').fadeIn();
        }
    }
    /* 
        Consulta de información para los campos del formulario de bajas 
        recibe como parametros un string o número
    */

    function requestDataBajas(field_value, type_query){
        
        $('.fields-group .block:last-child .ion').remove();
        $('[name="observaciones"]').val('');
        $('.fields-group [name="motivo_retiro"],.fields-group [name="disposicion_final"]').val('SELECCIONE...');
        /* 
            Se limpia los campos cada vez que haya un cambio 
        */
        $('span[data-name]').text('');
        $('.withdrawal-partial input').val('');
        $('.withdrawal-result').text('$ 0');

        if(type_query == 'noInventario'){
            $('[name="id_numero_activo"]').val('');
            $('[name="id_subnumero_activo"]').val('0');
        }
        else if(type_query == 'activo'){
            $('[name="id_numero_inventario"]').val('');
        }
        
        /* 
            Set de valores iniciales, url de consulta , referencia de consulta ya sea inventario activo o subnúmero, valor de subnúmero
        */
        var url = 'include/consultasBajas.php';
        var requestReference = type_query;
        var subNumber = $('[name="id_subnumero_activo"]').val().trim();

        /* 
            Según sea el tipo de request se envia diferentes datos, entre estos están:
            id_referencia:String                Activo||Inventario
            id_numero_referencia:String         Número de consulta de Activo||Inventario
            id_subnumero_referencia:Number      Subnúmero de consulta
        */
        if(requestReference == 'activo' && subNumber != ""){
            var requestJSON = {
                'id_referencia': requestReference,
                'id_numero_referencia': field_value,
                'id_subnumero_referencia': subNumber
            }
        }
        else{
            var requestJSON = {
                'id_referencia': requestReference,
                'id_numero_referencia': field_value
            }
        }

        /* 
            Consulta al php que trae un json con los datos necesarios, se define:
            URL             url de consulta
            requestJSON     son los datos de consulta
            data            retorna el json de información
        */
        $.getJSON(url, requestJSON, function(data){

            var date = new Date();
            if( date.getFullYear() == data.fecha_capitalizacion2){
                modalMessage({message: 'Está consultando un activo capitalizado en el año vigente '+date.getFullYear(), type: 'warning'});
            }
            /* 
                Se asigna a los campos los valores que trae de la consulta
            */
            if(data.result != undefined){
                switch(type_query){
                    case 'noInventario': 
                        modalMessage({message: 'El número inventario no existe, intente con el código SAP del activo, por favor contactarse con CAF por medio de una asesoría, indicando otro dato maestro para su busqueda', type: 'warning'});
                    break;
                    case 'activo': 
                        modalMessage({message: 'El número de activo no existe, intente con el número de inventario, por favor contactarse con CAF por medio de una asesoría, indicando otro dato maestro para su busqueda', type: 'warning'});
                    break;
                }
                $('#wait').fadeOut(400);
                return false;
            }

            if(type_query == 'noInventario'){
                $('[name="id_numero_activo"]').val(data.activo);
                $('[name="id_subnumero_activo"]').val(data.sn);
            }
            else if(type_query == 'activo'){
                $('[name="id_numero_inventario"]').val(data.inventario);
            }
            $('[data-name="departamento"]').text(data.departamento);
            $('[data-name="municipio"]').text(data.municipio);
            $('[data-name="ubicacion_geografica"]').text(data.ubicacion_geografica);
            $('[data-name="nombre_ceco"]').text(data.nombre_ceco);
            $('[data-name="numero_ceco"]').text(data.numero_ceco);
            $('[data-name="area"]').text(data.area);
            $('[data-name="vicepresidencia"]').text(data.vicepresidencia);
            $('[data-name="gerencia"]').text(data.gerencia);
            $('[data-name="denominacion"]').text(data.denominacion);
            $('[data-name="denominacion_2"]').text(data.denominacion2);
            $('[data-name="marca_fabricante"]').text(data.marca_fabricante);
            $('[data-name="modelo"]').text(data.modelo);
            $('[data-name="serie"]').text(data.serie);
            $('[data-name="tag"]').text(data.tag);
            $('[data-name="custodio"]').text(data.custodio);
            $('[data-name="valor_adquisicion"]')
                .text(data.valor_adquisicion)
                .priceFormat({
                    prefix:"$ ",
                    centsSeparator:",",
                    thousandsSeparator:".",
                    clearOnEmpty:!0,
                    centsLimit: 0,
                    allowNegative: false
                });
            $('[data-name="valor_contable_neto"]')
                .text(data.valor_contable_neto)
                .priceFormat({
                    prefix:"$ ",
                    centsSeparator:",",
                    thousandsSeparator:".",
                    clearOnEmpty:!0,
                    centsLimit: 0,
                    allowNegative: false
                });
            $('[name="valor_adquisicion"]').val( Number(data.valor_adquisicion) );

            $('[name="valor_adquisicion"], [name="number_baja_parcial"]').priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            });
            $('[data-name="cod_municipio"]').text(data.cod_municipio);
            $('[data-name="cod_ubicacion_geografica"]').text(data.cod_ubicacion_geografica);
            $('[data-name="fecha_capitalizacion"]').text( (data.fecha_capitalizacion).replace(/-/g,'/').replace(' 0:00:00','') );
            
            $('[data-name="tipo_operacion"]').text( String(data.tipo_operacion).toUpperCase() );
            
            getVAminusBP($('[name="number_baja_parcial"]'));
            
            /* 
                Si se encuentran subnumeros en la consulta muestra el botón 
                para ver la información detallada de los subnúmeros
            */

            $('.show-subnumber .block .ion').remove();
            if(data.conteo_subnumeros_activo != undefined && data.conteo_subnumeros_activo != 0){

                var html = '<span class="ion ion-ios-list" data-modal="subnumbers" data-toggle="tooltip" title="El activo tiene '+data.conteo_subnumeros_activo+' subnúmeros"></span>';
                $('.show-subnumber .block').append(html);
                
                $('[data-toggle="tooltip"]').tooltip(); 
            }
            $('#wait').fadeOut(400);

            showAddInformation();
        });

    }

    /* 
        Función consulta de subnúmeros, está recibe 2 valores:
        field_value:String              Activo
        focus_element:ElementoDom       Modalbox donde se va a mostrar los resultados de los subnúmeros
    */

    function requestSubnumbersBajas(field_value , focus_element){

        /* 
            se remueve la tabla con los ultimos datos agregados
        */
        $(focus_element).find('.table-subnumbers').remove();

        /* 
            url de consulta(url) y parametros de consulta(requestJSON)
        */
        var url = 'include/consultasBajas.php';
        var requestJSON = {
            'id_referencia': 'subnumeros',
            'id_numero_referencia': field_value
        }
        /* 
            Consulta al php que trae un json con los datos necesarios, se define:
            URL             url de consulta
            requestJSON     son los datos de consulta
            data            retorna el json de información
        */
        $.get(url, requestJSON, function(data){
            /* 
                se crea la tabla que muestra la información de subnúmeros
            */
            var html = '<table class="table table-striped table-responsive table-subnumbers"><thead class="thead-default"><tr><th>SN</th><th>Denominación</th><th>Denominación 2</th><th>Valor de adquisición</th><th>Valor de depreciación</th><th>Valor contable neto</th></tr></thead><tbody>';
            
            if(data.length != undefined){
                $.each(data,function(i,e){
                    // console.log(e);
                    html += '<tr><td>'+e.sn+'</td><td>'+e.denominacion+'</td><td>'+e.denominacion2+'</td><td class="format">'+e.valor_adquisicion+'</td><td class="format">'+e.valor_depreciacion+'</td><td class="format">'+e.valor_contable_neto+'</td></tr>';
                });
            }
            else{
                html += '<tr><td>'+data.sn+'</td><td>'+data.denominacion+'</td><td>'+data.denominacion2+'</td><td class="format">'+data.valor_adquisicion+'</td><td class="format">'+data.valor_depreciacion+'</td><td class="format">'+data.valor_contable_neto+'</td></tr>';
            }
            html += '</tbody></table>';
            
            $(focus_element).find('.cover-table').html(html);

            $('.format').priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            });
        });
    }


    /* 
        agregar mensajes de acción modal messages
    */

    function modalMessage(json){
        $('.modalMessage').remove();
        clearTimeout(timeMessage);

        var html = '<div class="modalMessage '+json.type+'"><h3>'+json.message+'</h3><span class="ion ion-android-cancel close-message"></span></div>';
        $('body').append(html);
        var timeMessage = setTimeout(function(){
            $('.modalMessage').fadeOut(400, function(){
                $(this).remove();
            })
        }, 20000);
    }

    /*
        Función de agregar bajas a la tabla
    */

    function genericTable(data){

        var continueBool = false;
        $.each($($('.table-bajas')[0]).find('tbody tr'), function(i,e){
            if(
                data.numero_inventario == $(e).find('td:eq(2)').text() &&
                data.numero_activo == $(e).find('td:eq(3)').text() && 
                data.subnumero_activo == $(e).find('td:eq(4)').text()
            ){
                continueBool = true;
            }
        });

        if(continueBool)
            return false;

        var tableHideColumns = [];
        $.each($('.inline-table thead tr:last-child th'), function(i,e){
            if($(e).is('.hide-column')){
                tableHideColumns.push("hide-column");
            }
            else if($(e).is('.always-hide-column')){
                tableHideColumns.push("always-hide-column");
            }
            else{
                tableHideColumns.push("");
            }
        });

        if($('.ctrls-table .ion').is('.ion-android-more-vertical')){
            var styleCell = "display: table-cell;";
        }
        else{
            var styleCell = "";
        }

        $('.modalbox[data-modal="bajas"], .inline-table').find('.table tbody')
            .prepend( $('<tr class="animated fadeIn">')
                .append( $('<td>',{
                    'html': '<span class="ion ion-trash-b remove-row"></span>',
                    'class': tableHideColumns[0],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': $('.inline-table tbody tr').length+1,
                    'class': tableHideColumns[1],
                    'style': styleCell
                }) )
                .append( $('<td>' ,{
                    'text': data.numero_inventario,
                    'class': tableHideColumns[2],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.numero_activo,
                    'data-subnumbers': data.has_subnumbers,
                    'class': tableHideColumns[3],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.subnumero_activo,
                    'class': tableHideColumns[4],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.departamento,
                    'class': tableHideColumns[5],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.municipio,
                    'class': tableHideColumns[6],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.ubicacion_geografica,
                    'class': tableHideColumns[7],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.nombre_ceco,
                    'class': tableHideColumns[8],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.numero_ceco,
                    'class': tableHideColumns[9],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.area,
                    'class': tableHideColumns[10],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.vicepresidencia,
                    'class': tableHideColumns[11],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.gerencia,
                    'class': tableHideColumns[12],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.denominacion,
                    'class': tableHideColumns[13],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.marca_fabricante,
                    'class': tableHideColumns[14],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.modelo,
                    'class': tableHideColumns[15],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.serie,
                    'class': tableHideColumns[16],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.tag,
                    'class': tableHideColumns[17],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.motivo_retiro,
                    'class': tableHideColumns[18],
                    'style': styleCell,
                    'data-action': 'cambiar_motivo_retiro'
                }) )
                .append( $('<td>',{
                    'text': data.disposicion_final,
                    'class': tableHideColumns[19],
                    'style': styleCell,
                    'data-action': 'cambiar_disposicion_final'
                }) )
                .append( $('<td>',{
                    'text': data.valor_adquisicion,
                    'class': tableHideColumns[20],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.valor_baja,
                    'class': tableHideColumns[21],
                    'style': styleCell
                }) )
                .append( $('<td>',{
                    'text': data.tipo_baja,
                    'class': tableHideColumns[22]
                }) )
                .append( $('<td>',{
                    'text': data.cod_municipio,
                    'class': tableHideColumns[23]
                }) )
                .append( $('<td>',{
                    'text': data.cod_ubicacion_geografica,
                    'class': tableHideColumns[24]
                }) )
                .append( $('<td>',{
                    'text': data.denominacion_2,
                    'class': tableHideColumns[25]
                }) )
                .append( $('<td>',{
                    'text': data.observaciones,
                    'class': tableHideColumns[26],
                    'style': styleCell
                }) )
            );
    }

    function bajasAddToTable(){
        var data = {
            numero_inventario: $('[name="id_numero_inventario"]').val(),
            numero_activo: $('[name="id_numero_activo"]').val(),
            subnumero_activo: $('[name="id_subnumero_activo"]').val(),
            departamento: $('[data-name="departamento"]').text(),
            municipio: $('[data-name="municipio"]').text(),
            ubicacion_geografica: $('[data-name="ubicacion_geografica"]').text(),
            nombre_ceco: $('[data-name="nombre_ceco"]').text(),
            numero_ceco: $('[data-name="numero_ceco"]').text(),
            area: $('[data-name="area"]').text(),
            vicepresidencia: $('[data-name="vicepresidencia"]').text(),
            gerencia: $('[data-name="gerencia"]').text(),
            denominacion: $('[data-name="denominacion"]').text(),
            denominacion_2: $('[data-name="denominacion_2"]').text(),
            marca_fabricante: $('[data-name="marca_fabricante"]').text(),
            modelo: $('[data-name="modelo"]').text(),
            municipio: $('[data-name="municipio"]').text(),
            serie: $('[data-name="serie"]').text(),
            tag: $('[data-name="tag"]').text(),
            motivo_retiro: $('[name="motivo_retiro"]').val(),
            disposicion_final: $('[name="disposicion_final"]').val(),
            valor_adquisicion: $('[data-name="valor_adquisicion"]').text(),
            valor_baja: $('[name="tipo_baja"]:checked').val() != "baja_total" ? $('[name="number_baja_parcial"]').val() : $('[name="valor_adquisicion"]').val(),
            cod_municipio: $('[data-name="cod_municipio"]').text(),
            cod_ubicacion_geografica: $('[data-name="cod_ubicacion_geografica"]').text(),
            tipo_baja: String($('[name="tipo_baja"]:checked').val().replace(/[_]/g," ")).toUpperCase(),
            has_subnumbers: $('span[data-modal="subnumbers"]').length,
            observaciones: $('[name="observaciones"]').val()
        }


        var returnState = false;

        $.each($($('.table-bajas')[0]).find('tbody tr'), function(i,e){
            var comparison = {
                inventario: $(e).find('td:eq(2)').text(),
                activo: $(e).find('td:eq(3)').text(),
                subnumero: $(e).find('td:eq(4)').text()
            }
            
            if(data.numero_inventario == comparison.inventario && data.numero_activo == comparison.activo && data.subnumero_activo == comparison.subnumero){
                returnState =  true;
                return false;
            }

        });
        
        var cleanFields = [];
        $.each($('span[data-name]'),function(i,e){
            if($(e).text().trim() == ""){
                cleanFields.push(true);
            }
            else{
                cleanFields.push(false);
            }
        });
        if(cleanFields.indexOf(false) == -1){
            modalMessage({message: 'Ingresar un activo valido'});
            return false;
        }


        if(returnState){
            modalMessage({message: 'El activo ya está incluido'});
            return false;
        }

        // console.log(data)
        genericTable(data);

        $('.show-list').fadeIn(400);
        $('.show-list sup').text($('.modalbox[data-modal="bajas"] tbody tr').length);
        $('.inline-table').fadeIn(400);
        $('.ctrls-table').fadeIn(400);
    // $('.modalbox[data-modal="bajas"]').fadeIn(400);
    }

    // limpiar campos
        function clearFields(){
            $('.form-control[name="id_numero_activo"]').val('');
            $('.form-control[name="id_numero_inventario"]').val('');
            $('.form-control[name="number_baja_parcial"]').val('');
            $('.form-control[name="observaciones"]').val('');
            $('.form-control[name="motivo_retiro"]').val('SELECCIONE...');
            $('.form-control[name="disposicion_final"]').val('SELECCIONE...');
            $('.show-characteristics').hide();
            $('.modalMessage').remove();
        }
    // limpiar tabla
        function clearTable(){
            $('.table-data tbody tr').remove();
            $('.inline-table').removeAttr('style');
            $('.multiple-view').removeAttr('style');
            $('.single-view').show();
        }

    $(document).on('ready',function(){
    /* 
        Acciones sobre elementos del formulario de bajas
    */
        /*
            capturar valores de smm historico
        */

        // var SMMH = getHistoricSMM();

        /* 
            Selección de baja
        */

        $('.label-selector').on('click',function(){

            $(this).siblings('.label-selector').find('[name="tipo_baja"]').removeAttr('checked');

            var type = $('[name="tipo_baja"]:checked').val();
            
            if(type == 'baja_parcial'){
                $('.withdrawal-partial').stop().fadeIn(300);
                $('.show-subnumber').removeClass('hide-subnumber');
            }
            else{
                $('.show-subnumber').addClass('hide-subnumber');
                $('.withdrawal-partial').stop().fadeOut(300,function(){
                    // $('[name="number_baja_parcial"]')
                    //     .val( Number($('[name="valor_adquisicion"]').unmask()) )
                    //     .priceFormat({
                    //         prefix:"$ ",
                    //         centsSeparator:",",
                    //         thousandsSeparator:".",
                    //         clearOnEmpty:!0,
                    //         centsLimit: 0,
                    //         allowNegative: false
                    //     });
                    // $('.withdrawal-result').text('$ 0');
                });
            }

            if($('[name="id_subnumero_activo"]').val() != 0){
                $('[name="id_subnumero_activo"]').val(0);
                $('[name="id_subnumero_activo"]').change();
            }

            getVAminusBP($('[name="number_baja_parcial"]'));

        });

        /* 
            Cuando se hace un keyup dentro del input del número de referencia se hace la consulta de la información necesaria 
        */

        $('.form-control[name="id_numero_inventario"]').on('change',function(){

            // if($(this).val().trim().length < 3){
            //     return false;
            // }
            $('#wait').fadeIn(400);
            requestDataBajas( $(this).val().trim(), 'noInventario' );

        });

        $('.form-control[name="id_numero_activo"]').on('change',function(){

            // if($(this).val().trim().length < 3){
            //     return false;
            // }
            $('#wait').fadeIn(400);
            $('.form-control[name="id_subnumero_activo"]').val('0');
            requestDataBajas( $(this).val().trim(), 'activo' );

        });

        $('.form-control[name="id_subnumero_activo"]').on('change',function(){
            $('#wait').fadeIn(400);
            requestDataBajas( $('[name="id_numero_activo"]').val().trim(), 'activo' );

        });


        $('[name="number_baja_parcial"]').on('change',function(){
            getVAminusBP(this);
        });
        $('[name="number_baja_parcial"]').on('keyup',function(){
            $('[name="number_baja_parcial"]').validate();
            getVAminusBP(this);
        });
        /* 
            Acción sobre visualización de subnúmeros
        */
        $(document).on('click','[data-modal]',function(e){

            var focus = $(e.target).attr('data-modal');
            switch(focus){
                case "subnumbers":
                    requestSubnumbersBajas( $('[name="id_numero_activo"]').val().trim() , $('.modalbox[data-modal="'+focus+'"]') );
                    $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                    break;
                default:
                    $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                    break;
            }

        });

        $('.modalbox,.close-modal').on('click',function(e){
            if($(e.target).is('.table-cell') || $(e.target).is('.close-modal')){
                $(this).closest('.modalbox').stop().fadeOut(400);
            }
        });

        /*
            Agregar solicitud
        */

        $('#formatoBajas .btn-add').on('click',function(e){
            e.preventDefault(),
                validator = $('#formatoBajas').validate();
            validator.form('#formatoBajas');

            if(validator.numberOfInvalids() == 0){
                bajasAddToTable();
            }
        });

        /*
            Crear excel
        */

        function createJSON(){

            $('.cloned').remove();

            var arrayJson = [],
                arrayJsonChilds = [];

            for(var i=$($('.table-bajas tbody')[0]).find('tr').length - 1; i>= 0; i-- ){
                var focus = $($('.table-bajas tbody')[0]).find('tr:eq('+i+')');
                var tempJson = {
                    numero_inventario: $(focus).find('td:eq(2)').text(),
                    numero_activo: $(focus).find('td:eq(3)').text(),
                    sn: $(focus).find('td:eq(4)').text(),
                    departamento: $(focus).find('td:eq(5)').text(),
                    municipio: $(focus).find('td:eq(6)').text(),
                    ubicacion_geografica: $(focus).find('td:eq(7)').text(),
                    ceco: $(focus).find('td:eq(8)').text(),
                    numero_ceco: $(focus).find('td:eq(9)').text(),
                    area: $(focus).find('td:eq(10)').text(),
                    vicepresidencia: $(focus).find('td:eq(11)').text(),
                    gerencia: $(focus).find('td:eq(12)').text(),
                    denominacion: $(focus).find('td:eq(13)').text(),
                    marca_fabricante: $(focus).find('td:eq(14)').text(),
                    modelo: $(focus).find('td:eq(15)').text(),
                    serie: $(focus).find('td:eq(16)').text(),
                    tag: $(focus).find('td:eq(17)').text(),
                    motivo_retiro: $(focus).find('td:eq(18)').text(),
                    disposicion_final: $(focus).find('td:eq(19)').text(),
                    valor_adquisicion: $(focus).find('td:eq(20)').text().replace(/[$. ]/g,""),
                    valor_baja: $(focus).find('td:eq(21)').text().replace(/[$. ]/g,""),
                    tipo_baja: $(focus).find('td:eq(22)').text(),
                    cod_municipio: $(focus).find('td:eq(23)').text(),
                    cod_ubicacion_geografica: $(focus).find('td:eq(24)').text(),
                    denominacion_2: $(focus).find('td:eq(25)').text(),
                    observaciones: $(focus).find('td:eq(26)').text()
                };
                if(tempJson.motivo_retiro != 'SELECCIONE...' && tempJson.disposicion_final != 'SELECCIONE...'){

                    arrayJson.push(tempJson);
    
                    if(
                        Number($(focus).find('td:eq(3)').attr('data-subnumbers')) >= 1 && 
                        $(focus).find('td:eq(22)').text() == 'BAJA TOTAL'
                    ){
                        $(focus).find('.cloned').remove();
                        var url = 'include/consultasBajas.php';
                        var requestJSON = {
                            'id_referencia': 'subnumeros',
                            'id_numero_referencia': $(focus).find('td:eq(3)').text().trim()
                        }
                        $.ajax({
                            url: url,
                            type: 'get',
                            async: false,
                            data: requestJSON,
                            contentType: 'application/json; charset=utf-8',
                            success: function(data){
                                $.each(data, function(i,e){
                                    if(i != 0){
                                        var tempChildJson = {
                                            numero_inventario: e.inventario,
                                            numero_activo: e.activo,
                                            sn: e.sn,
                                            departamento: e.departamento,
                                            municipio: e.municipio,
                                            ubicacion_geografica: e.ubicacion_geografica,
                                            ceco: e.nombre_ceco,
                                            numero_ceco: e.numero_ceco,
                                            area: e.area,
                                            vicepresidencia: e.vicepresidencia,
                                            gerencia: e.gerencia,
                                            denominacion: e.denominacion,
                                            marca_fabricante: e.marca_fabricante,
                                            modelo: e.modelo,
                                            serie: e.serie,
                                            tag: e.tag,
                                            motivo_retiro: $(focus).find('td:eq(18)').text(),
                                            disposicion_final: $(focus).find('td:eq(19)').text(),
                                            valor_adquisicion: e.valor_adquisicion,
                                            valor_baja: e.valor_adquisicion,
                                            tipo_baja: 'BAJA TOTAL',
                                            cod_municipio: e.cod_municipio,
                                            cod_ubicacion_geografica: e.cod_ubicacion_geografica,
                                            denominacion_2: e.denominacion2,
                                            observaciones: $(focus).find('td:eq(26)').text()
                                        };
                                        arrayJson.push(tempChildJson);
                                    }
                                });
                            }
                        });
    
                    }

                }
            }
            return { 
                'DarthVader': JSON.stringify(arrayJson)
            };
        }

        $('#formatoBajas .btn-end').on('click',function(e){
            e.preventDefault();
            
            $('table.table [data-action]').find('select').remove();

            if($('table.table td:contains("SELECCIONE...")').length){
                modalMessage({message: 'Modifique el valor en los campos donde aparece "SELECCIONE..."'})
                return false;
            }

            if($('.table-bajas tbody tr').length){
                var url = 'include/generarXlsBajas.php';
                var arrayJson = createJSON();
                // console.log(arrayJson);
                // return false;
                $('#wait').fadeIn(400);
                $.post(url, {jsonExcel: arrayJson.DarthVader }, function(data){
                    var d = JSON.parse(data);

                    var $a = $("<a>");
                    $a.attr("href","include/"+d.route);
                    $("body").append($a);
                    $a[0].setAttribute("download",d.name);
                    $a[0].click();
                    $a.remove();
                    $('#wait').fadeOut(400);
                    $('.tankyou-message').fadeIn(400);
                    clearFields();
                    clearTable();
                    setTimeout(function(){
                        $('.tankyou-message').fadeOut(400);
                    }, 5000);
                });
            }
            else{
                modalMessage({message: 'No existe solicitudes de bajas'})
            }
        });

        /* 
            Remove row table-bajas
        */
        
        $(document).on('click', '.remove-row',function(){
            var rowInd = $(this).parents('tr').index();
            $.each($('.table-bajas tbody'),function(i,e){
                $(e).find('tr:eq('+rowInd+')').fadeOut(400,function(){
                    $(this).remove();
                });
            });

            setTimeout(function(){
                $.each($('.table-bajas tbody tr'),function(i,e){
                    $(e).find('td:eq(1)').text( $(e).parents('tbody').find('tr').length - $(e).index() );
                });
            }, 500);

            $('.show-list sup').text($('.modalbox[data-modal="bajas"] tbody tr').length -1 );

            if($($('.table-bajas tbody')[0]).find('tr').length -1 <= 0){
                $('.show-list').fadeOut(400);
                $('.inline-table').fadeOut(400);
                $('.ctrls-table').fadeOut(400);
            }

            
        })
        
        /* Formulario Validate*/
        jQuery.validator.addMethod('partial', function(value, element, params){
                var type = $('[name="tipo_baja"]:checked').val();
                var val = $('[name="number_baja_parcial"]').unmask();
                var sn = $('[name="id_subnumero_activo"]').val();
                var valorAdquisicion = Number( $('[name="valor_adquisicion"]').unmask() );

                switch(type){
                    case 'baja_parcial':
                        if(sn == 0){
                            var limit = Math.floor( (valorAdquisicion*75)/100 );
                            if(val >= limit){
                                $('[name="number_baja_parcial"]').val(limit).priceFormat({
                                    prefix:"$ ",
                                    centsSeparator:",",
                                    thousandsSeparator:".",
                                    clearOnEmpty:!0,
                                    centsLimit: 0,
                                    allowNegative: false
                                }).change();
                            }
                        }
                        else{
                            if(val >= valorAdquisicion){
                                $('[name="number_baja_parcial"]').val(valorAdquisicion).priceFormat({
                                    prefix:"$ ",
                                    centsSeparator:",",
                                    thousandsSeparator:".",
                                    clearOnEmpty:!0,
                                    centsLimit: 0,
                                    allowNegative: false
                                }).change();
                            }
                        }
                    break;
                }
                if(val <= 0){
                    return false;
                }
                return true;
            }, 'Ingresar un valor'
            //     var type = $('.form-control[name="tipo_creacion"]').val();
            //     var val = Number(value);
            //     switch(type){
            //         case 'principal':
            //             if(val < 12){
            //                 params = '12 meses';
            //                 return false;
            //             }
            //         break;
            //         case 'subnumero':
            //             if(val < 1){
            //                 params = '1 mes';
            //                 return false;
            //             }
            //         break;
            //     }
            //     return true;
            // }, function(params){
            //     var type = $('.form-control[name="tipo_creacion"]').val();
            //     switch(type){
            //         case 'principal':
            //             return 'La vida útil debe ser mayor a 12 meses ';
            //         break;
            //         case 'subnumero':
            //             return 'La vida útil debe ser mayor a 1 mes ';
            //         break;
            //     }
            // }
        );
        jQuery.validator.addMethod('seleccion', function(value, element, params){
                if(value == "" || value == "SELECCIONE..."){
                    return false;
                }
                return true;
            }, 'Seleccione alguna opción'
        );

        $('#formatoBajas').validate({
            debug: true,
            rules: {
                motivo_retiro: {
                    required: true,
                    seleccion: true
                },
                disposicion_final: {
                    required: true,
                    seleccion: true
                },
                id_numero_activo: {
                    required: true
                },
                number_baja_parcial: {
                    required: true,
                    partial: true
                }
            },
            messages: {
                motivo_retiro: {
                    required: "Se debe seleccionar alguna opción"
                },
                disposicion_final: {
                    required: "Se debe seleccionar alguna opción"
                },
                id_numero_activo: {
                    required: "El número debe conicidir con un número de activo"
                },
                number_baja_parcial: {
                    required: "Ingresar un valor de baja"
                }
            }
        })

        /* close modalmessage*/

        $(document).on('click', '.modalMessage',function(e){
            $(this).fadeOut(400, function(){
                $(this).remove();
            })
        });

        
        var boolTextCharacteristics = true;
        $('.show-characteristics span').on('click', function(){
            var html = $(this).html();
            if($(this).find('i').is('.ion-eye')){
                $(this).html( html.replace('Ver', 'Ocultar') );
            }
            if($(this).find('i').is('.ion-eye-disabled')){
                $(this).html( html.replace('Ocultar', 'Ver') );
            }
            $(this).find('.ion').toggleClass('ion-eye ion-eye-disabled');
            $('.slide-info').stop().slideToggle(400);
        });

        $('[data-toggle="tooltip"]').tooltip();


        /* mostrar columnas en inline-table*/

        $('.ctrls-table .ion').on('click', function(){
            $(this).parents('.ctrls-table').siblings('div').find('.hide-column').stop().fadeToggle(300);
            $(this).toggleClass('ion-android-more-vertical ion-android-apps', 300);
        });

        /* seleccionar subnumero desde el panel de información*/

        $(document).on('click', '.table-subnumbers tbody tr', function(){
            if($('[name="tipo_baja"]:checked').val() == "baja_parcial"){
                var subnumber = $(this).find('td:eq(0)').text();
                $(this).parents('.modalbox').fadeOut(400);
                $('[name="id_subnumero_activo"]').val(subnumber);
                $('[name="id_subnumero_activo"]').change();
            }
        });

        /* cambiar disposición final y motivo de retiro dentro de la tabla*/
        
        $(document).on('mouseenter', '[data-action]', function(event){
            event.preventDefault();
            if($(this).find('.cloned').length){
                return false;
            }
            var action = $(this).attr('data-action');
            var selected_text = $(this).text();
            switch(action){
                case 'cambiar_motivo_retiro':
                    var clone = $('[name="motivo_retiro"]').not('.cloned').clone();
                    // $(clone).find('option:eq(0)').remove();
                    $(clone).find('option[value="'+selected_text+'"]').prop('selected',true);
                    $(this).append( $(clone).addClass('cloned') );
                break;
                case 'cambiar_disposicion_final':
                    var clone = $('[name="disposicion_final"]').not('.cloned').clone();
                    // $(clone).find('option:eq(0)').remove();
                    $(clone).find('option[value="'+selected_text+'"]').prop('selected',true);
                    $(this).append( $(clone).addClass('cloned') );
                break;
            };
        });

        $(document).on('change', '[data-action] .cloned', function(){
            var newText = $(this).val();
            var rowId = $(this).parents('tr').index();
            var columnId = $(this).parents('td').index();
            $.each($('.table-bajas'),function(i,e){
                $(e).find('tbody tr:eq('+rowId+')').find('td:eq('+columnId+')').html(newText);
            });
        });

        /* UPPERCASE & REGEX REPLACE */
        function regexReplace(obj, regex){
            obj.val( obj.val().toUpperCase() );
            var sanitized = obj.val().replace(regex, '');
            obj.val(sanitized);
        }
        $('[name="id_numero_activo"]').on('change keyup', function(){
            regexReplace($(this), /[^0-9]/g );
        });
        $('[name="id_numero_inventario"]').on('change keyup', function(){
            regexReplace($(this), /[^a-zA-Z0-9-]/g );
        });
        $('[name="observaciones"]').on('keyup',function(){
            $(this).val( $(this).val().toUpperCase() );
        });
        
        /*masive query*/
        $('.newRow-multiple').on('click',function(e){
            e.preventDefault();
            var lowType = $('[name="tipo_baja_multiple"]:checked').val();

            $('.rows_masive_data tbody').append(
                $('<tr>')
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleInventario',
                                'class': 'form-control'
                            })
                        )
                    )
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleActive',
                                'class': 'form-control'
                            })
                        )
                    )
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleSN',
                                'class': 'form-control',
                                'data-low': lowType
                            })
                        )
                    )
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleLow',
                                'class': 'form-control',
                                'data-low': lowType
                            })
                        )
                    )
                    .append(
                        $('<td>').append('<i class="ion ion-trash-b"></i>')
                    )
            )
        });

        $('.rows_masive_data').on('click','.ion-trash-b',function(e){
            $(this).parents('tr').fadeOut(300,function(){
                $(this).remove();
            });
        });

        function addFields(obj, lowType){
            $('.rows_masive_data tbody').append(
                $('<tr>')
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleInventario',
                                'class': 'form-control'
                            }).val(obj.inventario)
                        )
                    )
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleActive',
                                'class': 'form-control'
                            }).val(obj.active)
                        )
                    )
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleSN',
                                'class': 'form-control',
                                'data-low': lowType
                            }).val( lowType == 'total' ? 0 : obj.sn )
                        )
                    )
                    .append(
                        $('<td>').append(
                            $('<textarea>', {
                                'type': 'text',
                                'name': 'multipleLow',
                                'class': 'form-control',
                                'data-low': lowType
                            }).val(obj.low)
                        )
                    )
                    .append(
                        $('<td>').append('<i class="ion ion-trash-b"></i>')
                    )
            )
        }

        $('.rows_masive_data').on('change','.form-control[name="multipleInventario"]',function(e){
            var rowsOfMultiple = $(this).val().trim().split(/[\n;]/g);
            var lowType = $('[name="tipo_baja_multiple"]:checked').val();
            $(this).val('');

            for(var i = 0; i< rowsOfMultiple.length; i++){
                var tempRow = rowsOfMultiple[i].split(/[\t]/g);

                switch(lowType){
                    case 'total':
                        var obj = {
                            'inventario': tempRow[0],
                            'active': tempRow[1] ? Number(tempRow[1]): '',
                            'sn': 0,
                            'low': 'total'
                        }
                    break;
                    case 'parcial':
                        var obj = {
                            'inventario': tempRow[0],
                            'active':  tempRow[1] ? Number(tempRow[1]): '',
                            'sn':  tempRow[2] ? Number(tempRow[2]) : 0,
                            'low': tempRow[3] ? tempRow[3] : lowType
                        }
                    break;
                }

                var exist = false;
                $.each($('.rows_masive_data tbody tr'),function(i,e){
                    var inventario = $(e).find('td:eq(0) .form-control').val();
                    var active = $(e).find('td:eq(1) .form-control').val();
                    var sn = $(e).find('td:eq(2) .form-control').val();
                    if(inventario == obj.inventario && active == obj.active && sn == obj.sn && exist == false){
                        sn == "" ? '' : exist = true;
                    }
                });

                if(!exist){
                    if(i == 0){
                        var focus = $(this).parents('tr');
                        focus.find('td:eq(0) .form-control').val(obj.inventario);
                        focus.find('td:eq(1) .form-control').val(obj.active);
                        focus.find('td:eq(2) .form-control').val( lowType == 'total' ? 0 : obj.sn ).attr('data-low',lowType);
                        focus.find('td:eq(3) .form-control').val(obj.low).attr('data-low',lowType);
                    }
                    else{
                        addFields(obj, lowType);
                    }
                }
            }

        });

        $('.rows_masive_data').on('change','.form-control[name="multipleActive"]',function(e){
            var rowsOfMultiple = $(this).val().trim().split(/[\n;]/g);
            var lowType = $('[name="tipo_baja_multiple"]:checked').val();
            $(this).val('');
            
            for(var i = 0; i< rowsOfMultiple.length; i++){

                var tempRow = rowsOfMultiple[i].split(/[\t]/g);
                
                switch(lowType){
                    case 'total':
                        var obj = {
                            'inventario': '',
                            'active':  Number(tempRow[0]),
                            'sn': 0,
                            'low': 'total'
                        }
                    break;
                    case 'parcial':
                        var obj = {
                            'inventario': '',
                            'active':  Number(tempRow[0]),
                            'sn':  tempRow[1] ? Number(tempRow[1]) : 0,
                            'low': tempRow[2] ? tempRow[2] : lowType
                        }
                    break;
                }

                var exist = false;
                $.each($('.rows_masive_data tbody tr'),function(i,e){
                    var active = $(e).find('td:eq(1) .form-control').val();
                    var sn = $(e).find('td:eq(2) .form-control').val();
                    if(active == obj.active && sn == obj.sn && exist == false){
                        sn == "" ? '' : exist = true;
                    }
                });

                if(!exist){
                    if(i == 0){
                        var focus = $(this).parents('tr');
                        focus.find('td:eq(1) .form-control').val(obj.active);
                        focus.find('td:eq(2) .form-control').val( lowType == 'total' ? 0 : obj.sn ).attr('data-low',lowType);
                        focus.find('td:eq(3) .form-control').val(obj.low).attr('data-low',lowType);
                    }
                    else{
                        addFields(obj, lowType);
                    }
                }

                
            }
        });

        $('.rows_masive_data').on('change','.form-control[name="multipleSN"]',function(e){
            if($(this).val() != 0 && $(this).attr('data-low') == 'total'){
                $(this).val(0);
            }
        });

        $('.btn-multiple').on('click',function(e){
            $('[data-modal="multiples"]').fadeIn(400);
        });

        $('.btn-add-multiple').on('click',function(e){
            e.preventDefault();
            // var data = $('.masive_data [name="multiple_actives"]').val().trim().split(/[\n,;]/g);
            
            var data = [];
            $.each($('.rows_masive_data tbody tr'),function(i,e){
                
                var lowReference = $(e).find('td:eq(3) .form-control').val(),
                lowVal = 0,
                lowType = 0;

                switch(lowReference){
                    case 'total':
                        lowVal = 0;
                        lowType = 'total';
                    break;
                    case 'parcial':
                        lowVal = 0;
                        lowType = 'parcial';
                    break;
                    default:
                        lowVal = parseFloat(lowReference.replace(/[$.\s]/g, ''));
                        lowType = 'parcial';
                    break;
                }
                
                var obj = {
                    'inventario': $(e).find('td:eq(0) .form-control').val(),
                    'active': $(e).find('td:eq(1) .form-control').val() != "" ? Number( $(e).find('td:eq(1) .form-control').val() ) : '',
                    'sn': Number( $(e).find('td:eq(2) .form-control').val() ),
                    'low': lowVal,
                    'type': lowType
                }

                data.push(obj);

            });

            requestMultipleBajas(data);
        });


        /**Konami code**/
        var allowedKeys = {
            37: 'left',
            38: 'up',
            39: 'right',
            40: 'down',
            65: 'a',
            66: 'b'
        };
        var konamiCode = ['up', 'up', 'down', 'down', 'left', 'right', 'left', 'right', 'b', 'a'];
        var konamiCodePosition = 0;

        document.addEventListener('keydown', function(e) {
            var key = allowedKeys[e.keyCode];
            var requiredKey = konamiCode[konamiCodePosition];
          
            if (key == requiredKey) {

                konamiCodePosition++;

                if (konamiCodePosition == konamiCode.length) {
                    konamiEvent();
                    konamiCodePosition = 0;
                }
            }
            else {
                konamiCodePosition = 0;
            }
        });
        function konamiEvent(){
            console.log('asd');
        }
    });
    
    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);