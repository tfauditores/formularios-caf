(function($){
    $(document).on('ready',function(){
        // console.log('ready');
        // automatic slide login
            $('.login .back-slide .slide:last-child').addClass('animated');
            setInterval(function(){
                var $focus = $('.login .back-slide .slide:last-child');
                $focus.prev().addClass('animated');
                $focus.fadeOut(500,function(){
                    $(this).parent().prepend($(this).removeAttr('style').removeClass('animated'));
                });
            },5000);
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);