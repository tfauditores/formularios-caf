(function($){
    
    /* 
        GLOBAL FUNCTIONS 
    */
    /* 
        Función consulta de subnúmeros, está recibe 2 valores:
        field_value:String              Activo
        focus_element:ElementoDom       Modalbox donde se va a mostrar los resultados de los subnúmeros
    */
    
    var countActiveValue = [];
    function requestSubnumbersBajas(field_value , focus_element){
        
        /* 
            se remueve la tabla con los ultimos datos agregados
        */
        $(focus_element).find('.table-subnumbers').remove();

        /* 
            url de consulta(url) y parametros de consulta(requestJSON)
        */
        var url = 'include/consultasBajas.php';
        var requestJSON = {
            'id_referencia': 'subnumeros',
            'id_numero_referencia': field_value
        }
        /* 
            Consulta al php que trae un json con los datos necesarios, se define:
            URL             url de consulta
            requestJSON     son los datos de consulta
            data            retorna el json de información
        */
        $.get(url, requestJSON, function(data){
            /* 
                se crea la tabla que muestra la información de subnúmeros
            */
            var html = '<table class="table table-striped table-responsive table-subnumbers"><thead class="thead-default"><tr><th>SN</th><th>Descripción</th><th>Descripción 2</th><th>Clase</th></tr></thead><tbody>';
            
            if(data.length != undefined){
                $.each(data,function(i,e){
                    // console.log(e);
                    html += '<tr><td>'+e.sn+'</td><td>'+e.denominacion+'</td><td>'+e.denominacion2+'</td><td>'+e.clase+'</td></tr>';
                });
            }
            else{
                html += '<tr><td>'+data.sn+'</td><td>'+data.denominacion+'</td><td>'+data.denominacion2+'</td><td>'+data.clase+'</td></tr>';
            }
            html += '</tbody></table>';
            
            $(focus_element).find('.cover-table').html(html);
        });
    }
    /* show additional information */
    function showAddInformation(){
        if(!$('.show-characteristics').is(':visible')){
            $('.show-characteristics').fadeIn();
            $('.slide-info').fadeIn();
        }
    }
    /** Consulta de activos */
    function queryActive(type){
        var url = 'include/consultasTraslados.php';
        if(type == "origen"){
            var params = {
                id_numero_activo: $('.activo_'+type).find('[name="id_numero_activo"]').val().trim(),
                id_subnumero_activo: $('.activo_'+type).find('[name="id_subnumero_activo"]').val().trim()
            }
            $('.activo_origen .form-control:disabled').val('');
            
            $('.activo_origen .fields-group .subnumber-block .ion').remove();
        }
        if(type == "destino"){
            var params = {
                id_numero_activo: $('.activo_'+type).find('[name="id_numero_activo_destino"]').val().trim(),
                id_subnumero_activo: $('.activo_'+type).find('[name="id_subnumero_activo_destino"]').val().trim()
            }
            $('.activo_destino .form-control:disabled').val('');
            $('.activo_destino .form-control[name="fecha_capitalizacion_destino"]').val('');

            $('.activo_destino .fields-group .subnumber-block .ion').remove();
        }
        
        $.getJSON(url, params, function(data){
            // console.log(data);
            if(data.result != undefined){
                $('#wait').fadeOut(400);
                modalMessage({message: "El activo no existe, por favor contactarse con CAF por medio de una asesoría, indicando otro dato maestro para su busqueda"});
                return false;
            }
            if(data.clave_amortizacion == 0){
                var clave = "NO DEPRECIA";
            }
            else if(data.clave_amortizacion == 'ZVUR'){
                var clave = "LÍNEA RECTA";
            }
            else{
                var clave = "UNIDADES DE PRODUCCIÓN, (CAMPO  "+data.clave_amortizacion+")";
            }
            switch(type){
                case 'origen':
                    var $focus = $('.activo_origen');
                    $focus.find('[name="valor_adquisicion"]')
                        .val(data.valor_adquisicion)
                        .priceFormat({
                            prefix:"$ ",
                            centsSeparator:",",
                            thousandsSeparator:".",
                            clearOnEmpty:!0,
                            centsLimit: 0,
                            allowNegative: false
                        });
                    $focus.find('[data-name="fecha_capitalizacion"]').text(data.fecha_capitalizacion);
                    $focus.find('[data-name="denominacion"]').text(data.denominacion);
                    $focus.find('[data-name="vida_util_a"]').text(data.vida_util_anios);
                    $focus.find('[data-name="vida_util_m"]').text(data.vida_util_meses);
                    $focus.find('[data-name="vida_util_t"]').text(data.vida_util_total+' (MESES)');
                    $focus.find('[data-name="clave_amortizacion"]').text(clave).attr('data-clave', data.clave_amortizacion);
                    $focus.find('[data-name="ceco"]').text(data.ceco);
                    $focus.find('[data-name="vida_util_remanente"]').text(data.vida_util_remanente);
                    $focus.find('[data-name="valor_contable_neto"]').text(data.valor_contable_neto);
                    $focus.find('[data-name="numero_inventario"]').text(data.numero_inventario);

                    if(data.subnumeros != 0){
                        $focus.find('.fields-group .subnumber-block .ion').remove();    
                        
                        var html = '<span class="ion ion-ios-list" data-modal="subnumbers" data-toggle="tooltip" title="El activo tiene '+data.subnumeros+' subnúmeros"></span>';
                        $focus.find('.fields-group .subnumber-block').append(html);
                        
                        $focus.find('[data-toggle="tooltip"]').tooltip(); 
                    } 

                break;
                case 'destino':
                    var $focus = $('.activo_destino');
                    $focus.find('[name="valor_traslado"]')
                        .priceFormat({
                            prefix:"$ ",
                            centsSeparator:",",
                            thousandsSeparator:".",
                            clearOnEmpty:!0,
                            centsLimit: 0,
                            allowNegative: false
                        }).val(0);
                    $focus.find('[data-name="fecha_capitalizacion_destino"]').text(data.fecha_capitalizacion);
                    $focus.find('[data-name="denominacion_destino"]').text(data.denominacion);
                    $focus.find('[data-name="vida_util_a_destino"]').text(data.vida_util_anios);
                    $focus.find('[data-name="vida_util_m_destino"]').text(data.vida_util_meses);
                    $focus.find('[data-name="vida_util_t_destino"]').text(data.vida_util_total + ' (MESES)');
                    $focus.find('[data-name="clave_amortizacion_destino"]').text(clave).attr('data-clave', data.clave_amortizacion);
                    $focus.find('[data-name="ceco_destino"]').text(data.ceco);
                    $focus.find('[data-name="vida_util_remanente_destino"]').text(data.vida_util_remanente);
                    $focus.find('[data-name="valor_adquisicion_destino"]')
                        .text(data.valor_adquisicion)
                        .priceFormat({
                            prefix:"$ ",
                            centsSeparator:",",
                            thousandsSeparator:".",
                            clearOnEmpty:!0,
                            centsLimit: 0,
                            allowNegative: false
                        });
                    $focus.find('[data-name="valor_contable_neto_destino"]').text(data.valor_contable_neto);
                    $focus.find('[data-name="bloqueado_destino"]').text(data.bloqueado);
                    $focus.find('[data-name="numero_inventario_destino"]').text(data.numero_inventario);
                    
                    if(data.subnumeros != 0 ){
                        $focus.find('.fields-group .subnumber-block .ion').remove();    
                        
                        var html = '<span class="ion ion-ios-list" data-modal="subnumbers" data-toggle="tooltip" title="El activo tiene '+data.subnumeros+' subnúmeros"></span>';
                        $focus.find('.fields-group .subnumber-block').append(html);
                        
                        $focus.find('[data-toggle="tooltip"]').tooltip(); 
                    } 
                break;
            }

            var idActivo = $('[name="id_numero_activo"]').val()+$('[name="id_subnumero_activo"]').val();

            // console.log(idActivo);
            // console.log(valuesActiveJSON);

            if(valuesActiveJSON[idActivo] != undefined){
                $('[name="valor_adquisicion"]').attr('data-limit', valuesActiveJSON[idActivo].available_value);
                $('[data-name="valor_disponible"]').text(valuesActiveJSON[idActivo].available_value).priceFormat({
                    prefix:"$ ",
                    centsSeparator:",",
                    thousandsSeparator:".",
                    clearOnEmpty:!0,
                    centsLimit: 0,
                    allowNegative: false
                });
            }
            else if(type == 'origen'){
                $('[name="valor_adquisicion"]').attr( 'data-limit',  data.valor_adquisicion );
                $('[data-name="valor_disponible"]').text(data.valor_adquisicion).priceFormat({
                    prefix:"$ ",
                    centsSeparator:",",
                    thousandsSeparator:".",
                    clearOnEmpty:!0,
                    centsLimit: 0,
                    allowNegative: false
                });
            }
            $('#wait').fadeOut(400);
            showAddInformation();
        });
    }
    /* modalmessages */
    function modalMessage(json){
        $('.modalMessage').remove();
        clearTimeout(timeMessage);

        var html = '<div class="modalMessage"><h3>'+json.message+'</h3><span class="ion ion-android-cancel close-message"></span></div>';
        $('body').append(html);
        var timeMessage = setTimeout(function(){
            $('.modalMessage').fadeOut(400, function(){
                $(this).remove();
            })
        }, 20000);
    }
    /** Agregar o actualizar datos en json de la comparación de valores de activos **/
    var valuesActiveJSON = {};
    function addNewActiveObj(data){
        var valor_adquisicion = Number( String(data.origen_valor_adquisicion).replace(/[$. ]/g,'') );
        var valor_traslado = Number( String(data.destino_valor_traslado).replace(/[$. ]/g,'') );

        if(valuesActiveJSON[data.origen_numero_activo+data.origen_subnumero_activo] != undefined){
            valuesActiveJSON[data.origen_numero_activo+data.origen_subnumero_activo].available_value -= valor_traslado;
        }
        else{
            valuesActiveJSON[data.origen_numero_activo+data.origen_subnumero_activo] = {
                acquisition_value: valor_adquisicion,
                available_value: valor_adquisicion - valor_traslado
            }
        }

        $('[name="valor_adquisicion"]').attr(
            'data-limit', 
            valuesActiveJSON[data.origen_numero_activo+data.origen_subnumero_activo].available_value
        );
        
        $('[data-name="valor_disponible"]').text(valuesActiveJSON[data.origen_numero_activo+data.origen_subnumero_activo].available_value).priceFormat({
            prefix:"$ ",
            centsSeparator:",",
            thousandsSeparator:".",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        });
    }
    /** Years to milliseconds **/
    function years2milliseconds(years){
        milliseconds = 1000;
        seconds = 60;
        minutes = 60;
        hours = 24;
        days = 365.25;
        return milliseconds*seconds*minutes*hours*days*years;
    }
    /** sumatoria de valor contable neto origen en las tablas */
    function validateSigmaVCN(table){
        var objActives = {};
        $.each($(table).find('tbody tr') , function(i,e){
            var active = $(e).find('td:eq(12)').text();
            if( objActives[active] != undefined ){
                objActives[active] += Number( $(e).find('td:eq(24)').text() );
            }
            else{
                objActives[active] = Number( $(e).find('td:eq(24)').text() );
            }
        });
        return objActives;
    }
    /** agregar datos a la tabla*/
    function trasladosAddToTable(){
        var tableHideColumns = [];
        $.each($('.inline-table thead tr:last-child th'), function(i,e){
            if($(e).is('.hide-column')){
                tableHideColumns.push("hide-column");
            }
            else{
                tableHideColumns.push("");
            }
        });

        var data = {
            origen_numero_activo: $('[name="id_numero_activo"]').val(),
            origen_subnumero_activo: $('[name="id_subnumero_activo"]').val(),
            origen_numero_inventario: $('[data-name="numero_inventario"]').text(),
            origen_valor_adquisicion: $('[name="valor_adquisicion"]').val(),
            origen_fecha_capitalizacion: $('[data-name="fecha_capitalizacion"]').text(),
            origen_denominacion: $('[data-name="denominacion"]').text(),
            origen_vida_util_anios: $('[data-name="vida_util_a"]').text(),
            origen_vida_util_meses: $('[data-name="vida_util_m"]').text(),
            origen_clave_amortizacion: $('[data-name="clave_amortizacion"]').text(),
            origen_clave: $('[data-name="clave_amortizacion"]').attr('data-clave'),
            origen_ceco: $('[data-name="ceco"]').text(),
            origen_valor_contable_neto: Number($('[data-name="valor_contable_neto"]').text()),
            origen_vida_util_remanente: Number($('[data-name="vida_util_remanente"]').text()),
            destino_numero_activo: $('[name="id_numero_activo_destino"]').val(),
            destino_subnumero_activo: $('[name="id_subnumero_activo_destino"]').val(),
            destino_numero_inventario: $('[data-name="numero_inventario_destino"]').text(),
            destino_valor_traslado: $('[name="valor_traslado"]').val(),
            destino_fecha_capitalizacion: $('[data-name="fecha_capitalizacion_destino"]').text(),
            destino_denominacion: $('[data-name="denominacion_destino"]').text(),
            destino_vida_util_anios: $('[data-name="vida_util_a_destino"]').text(),
            destino_vida_util_meses: $('[data-name="vida_util_m_destino"]').text(),
            destino_clave_amortizacion: $('[data-name="clave_amortizacion_destino"]').text(),
            destino_clave: $('[data-name="clave_amortizacion_destino"]').attr('data-clave'),
            destino_ceco: $('[data-name="ceco_destino"]').text(),
            destino_vida_util_remanente: Number( $('[data-name="vida_util_remanente_destino"]').text() ),
            destino_valor_contable_neto: Number( $('[data-name="valor_contable_neto_destino"]').text() ),
            destino_valor_adquisicion: Number( $('[data-name="valor_adquisicion_destino"]').unmask() ),
            destino_bloqueo: $('[data-name="bloqueado_destino"]').text(),
            motivo_traslado: $('[name="motivo_traslado"]').val(),
            observaciones: $('[name="observaciones"]').val(),
        }
        // console.log(data);

        /** Validaciones requeridas**/
            /** 
             *      AD bloqueo == SI
             * **/
            // if(data.destino_bloqueo == "SI" ){
            //     modalMessage({message: "No se puede hacer un traslado al activo destino porque actualmente se encuentra bloqueado"});
            //     return false;
            // }
            /**
            *           AO                                                        AD
            *           up(unidades de produccion)      !=         000 (no deprecia)
            *           zvur(línea recta)               !=         000 (no deprecia)
            * **/
            if( data.origen_clave != "0" || data.origen_clave == 'ZVUR' ){
                if(data.destino_clave == "0"){
                    modalMessage({message: "No se puede hacer un traslado de unidades de producción o línea recta a un activo que no deprecia"});
                    return false;
                }
            }
            /**
             *          AO(UoP) -> AD(LR) => AD(VUT>0)
             * 
             */
            if( data.origen_clave > 0 && data.destino_clave == 'ZVUR'){
                if( Number($('[data-name="vida_util_t_destino"]').text().replace('(MESES)','')) <= 0 ){
                    modalMessage({message: "La vida útil técnica del activo destino debe ser mayor a 0"});
                    return false;
                }
            }
            /**
             *          AD Vadquisición es = 0
             */
            if( data.destino_valor_adquisicion > 0 ){
                modalMessage({message: "El valor de adquisición del activo destino debe ser igual a 0"});
                return false;
            }

            /**
             *          AD VUR > 0
             */
            if( Number($('[data-name="vida_util_remanente_destino"]').text()) <= 0 ){
                modalMessage({message: "La vida útil remanente del activo destino debe ser mayor a 0"});
                return false;
            }
            /**
             *            AO                                 AD
             *  si     VCN = 0             <=>             VUR >= 0
             *  si     VCN  > 0            <=>             VUR > 0
             * 
             **/
            // if(
            //     data.origen_valor_contable_neto == 0 &&
            //     data.destino_vida_util_remanente < 0
            // ){
            //     modalMessage({message: "El valor contable neto del activo origen debe ser igual a 0 y la vida útil remanente del activo destino debe ser mayor o igual a 0"});
            //     return false;
            // }
            // if( 
            //     data.origen_valor_contable_neto > 0 &&
            //     data.destino_vida_util_remanente <= 0
            // ){
            //     modalMessage({message: "El valor contable neto del activo origen debe ser mayor a 0 y la vida útil remanente del activo destino debe ser mayor a 0"});
            //     return false;
            // }

            

            

            /**
             *          |fecha capitalizacion AO - Fcap AD| <= 4 años
             */

            var OrigenFechaCap = String(data.origen_fecha_capitalizacion).split('/');
            var DestinoFechaCap = String(data.destino_fecha_capitalizacion).split('/');
            
            OrigenFechaCap = new Date( Number(OrigenFechaCap[2]), Number(OrigenFechaCap[1]) - 1, Number(OrigenFechaCap[0]) );
            DestinoFechaCap = new Date( Number(DestinoFechaCap[2]), Number(DestinoFechaCap[1]) - 1, Number(DestinoFechaCap[0]) );

            var differenceDate = Math.abs( OrigenFechaCap.getTime() - DestinoFechaCap.getTime() );
            
            // var differenceYears = Math.abs( differenceDate.getUTCFullYear() - 1970);
            
            // console.log(Math.abs( differenceDate.getUTCFullYear() - 1970) );

            // if(differenceDate > years2milliseconds(4) ){
            //     modalMessage({message: "El activo origen y el activo destino deben tener una diferencia máxima de 4 años en la fecha de capitalización"});
            //     return false;
            // }

        /** Validaciones necesarias**/
        if(
            data.origen_fecha_capitalizacion == "" && 
            data.origen_denominacion == "" && 
            data.origen_valor_adquisicion == ""
        ){
            modalMessage({message: "Ingresar un activo origen válido o intente contactarse con CAF por medio de una asesoría, indicando otro dato maestro para su busqueda"});
            return false;
        }
        if(
            data.destino_fecha_capitalizacion == "" && 
            data.destino_denominacion == ""
        ){
            modalMessage({message: "Ingresar un activo destino válido o intente contactarse con CAF por medio de una asesoría, indicando otro dato maestro para su busqueda"});
            return false;
        }

        var statusBlock = false;
        $.each($($('.table-traslados')[0]).find('tbody tr'), function(i,e){
            if(
                data.origen_numero_activo == $(e).find('td:eq(2)').text() &&
                data.origen_subnumero_activo == $(e).find('td:eq(3)').text() &&
                data.destino_numero_activo == $(e).find('td:eq(12)').text() &&
                data.destino_subnumero_activo == $(e).find('td:eq(13)').text() 
            ){
                modalMessage({message: "El traslado ya está incluido"});
                statusBlock = true;
            }
        });

        if(statusBlock) 
            return false;

        if($('.ctrls-table .ion').is('.ion-android-more-vertical')){
            var styleCell = "display: table-cell;";
        }
        else{
            var styleCell = "";
        }

        /* validar Sigma valor traslado con respecto valor de adquisición*/
            addNewActiveObj(data);

        data.destino_valor_traslado == "" ? data.destino_valor_traslado = 0 : '';

        $('.modalbox[data-modal="traslados"], .inline-table').find('.table tbody')
            .prepend( $('<tr class="animated fadeIn">')
                .append( $('<td>',{
                    'html': '<span class="ion ion-trash-b remove-row"></span>',
                    'class': tableHideColumns[0],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': $('.inline-table tbody tr').length+1,
                    'class': tableHideColumns[1],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_numero_activo,
                    'class': tableHideColumns[2],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_subnumero_activo,
                    'class': tableHideColumns[3],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_fecha_capitalizacion,
                    'class': tableHideColumns[4],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_numero_inventario,
                    'class': tableHideColumns[5],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_denominacion,
                    'class': tableHideColumns[6],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_vida_util_anios,
                    'class': tableHideColumns[7],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_vida_util_meses,
                    'class': tableHideColumns[8],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_clave_amortizacion,
                    'class': tableHideColumns[9],
                    'style': styleCell,
                    'data-clave': data.origen_clave
                }) )
                .append( $('<td>', {
                    'text': data.origen_ceco,
                    'class': tableHideColumns[10],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_valor_adquisicion,
                    'class': tableHideColumns[11]+' border-right',
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_numero_activo,
                    'class': tableHideColumns[12],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_subnumero_activo,
                    'class': tableHideColumns[13],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_fecha_capitalizacion,
                    'class': tableHideColumns[14],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_numero_inventario,
                    'class': tableHideColumns[15],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_denominacion,
                    'class': tableHideColumns[16],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_vida_util_anios,
                    'class': tableHideColumns[17],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_vida_util_meses,
                    'class': tableHideColumns[18],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_clave_amortizacion,
                    'class': tableHideColumns[19],
                    'style': styleCell,
                    'data-clave': data.destino_clave
                }) )
                .append( $('<td>', {
                    'text': data.destino_ceco,
                    'class': tableHideColumns[20],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.destino_valor_traslado,
                    'class': tableHideColumns[21],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.motivo_traslado,
                    'class': tableHideColumns[22],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.observaciones,
                    'class': tableHideColumns[23],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': data.origen_valor_contable_neto,
                    'class': 'always-hide-column',
                    'style': styleCell
                }) )
            );
            
        $('.show-list').fadeIn(400);
        $('.show-list sup').text($('.modalbox[data-modal="traslados"] tbody tr').length);
        $('.inline-table').fadeIn(400);
        $('.ctrls-table').fadeIn(400);
        
        $('.inline-table td').removeAttr('style');
        $('[name="observaciones"]').val('');
    }

    $(document).on('ready',function(){
        /* consulta activo origen*/
            $('.activo_origen [name="id_numero_activo"]').on('change',function(){
                $('#wait').fadeIn(400);
                $('.activo_origen [name="id_subnumero_activo"]').val(0);
                $('.activo_origen [data-name]').text('');
                $('[name="valor_traslado"]').val(0);
                queryActive('origen');
            });
            $('.activo_origen [name="id_subnumero_activo"]').on('change',function(){
                $('#wait').fadeIn(400);
                $('.activo_origen [data-name]').text('');
                $('[name="valor_traslado"]').val(0);
                queryActive('origen');
            });
        /* consulta activo destino*/
            $('.activo_destino [name="id_numero_activo_destino"]').on('change',function(){
                $('#wait').fadeIn(400);
                $('.activo_destino [name="id_subnumero_activo_destino"]').val(0);
                $('.activo_destino [data-name]').text('');
                $('[name="valor_traslado"]').val(0);
                queryActive('destino');
            });
            $('.activo_destino [name="id_subnumero_activo_destino"]').on('change',function(){
                $('#wait').fadeIn(400);
                $('.activo_destino [data-name]').text('');
                $('[name="valor_traslado"]').val(0);
                queryActive('destino');
            });
        /* datepicker */
            $( '.datepicker' ).datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
                maxDate: 0,
                yearRange: "-100:+0"
            });
        /* limitar campo valor traslado*/
            $('[name="valor_traslado"]').on('keyup',function(){
                var trasladoVal = Number($(this).unmask());
                var adquisicionVal = Number($('[name="valor_adquisicion"]').attr('data-limit'));
                if(trasladoVal > adquisicionVal){
                    $(this).val(adquisicionVal).priceFormat({
                        prefix:"$ ",
                        centsSeparator:",",
                        thousandsSeparator:".",
                        clearOnEmpty:!0,
                        centsLimit: 0,
                        allowNegative: false
                    });
                    adquisicionVal == 0 ? $(this).val('$ 0') : '';
                }
            }); 
        /* modalbox*/
            /* 
                Acción sobre visualización de subnúmeros
            */
            
            var activeParent = '';

            $(document).on('click','[data-modal]',function(e){
                
                var focus = $(e.target).attr('data-modal');
                switch(focus){
                    case "subnumbers":
                        var numeroActivo = $(this).parents('.form-section').find('[name^="id_numero_activo"]').val().trim();
                        var focusModal = $('.modalbox[data-modal="'+focus+'"]');

                        requestSubnumbersBajas( numeroActivo , focusModal  );
                        focusModal.stop().fadeIn(400);

                        if($(this).parents('.activo_origen').length)
                            activeParent = 'Origen';
                        if($(this).parents('.activo_destino').length)
                            activeParent = 'Destiny';
                        break;
                    default:
                        $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                        break;
                }

            });

            $('.modalbox,.close-modal').on('click',function(e){
                if($(e.target).is('.table-cell') || $(e.target).is('.close-modal')){
                    $(this).closest('.modalbox').stop().fadeOut(400);
                }
            });
        /* agregar traslado*/
            $('#formatoTraslados .btn-add').on('click',function(e){
                e.preventDefault();
                var validator = $('#formatoTraslados').validate();
                validator.form('#formatoTraslados');

                if(validator.numberOfInvalids() == 0){
                    trasladosAddToTable();
                }
            });

            $('#formatoTraslados').validate({
                debug: true,
                rules: {
                    id_numero_activo: {required:true},
                    id_numero_activo_destino: {required: true},
                    motivo_traslado: {required: true}
                },
                messages: {
                    id_numero_activo: "Ingresar un activo",
                    id_numero_activo_destino: 'Ingresar un activo',
                    motivo_traslado: 'Seleccionar una opción'
                }
            })

        /* remove row */
            $(document).on('click', '.remove-row',function(){
                var rowInd = $(this).parents('tr').index();
                $.each($('.table-traslados tbody'),function(i,e){
                    $(e).find('tr:eq('+rowInd+')').fadeOut(400,function(){
                        $(this).remove();
                    });
                });

                setTimeout(function(){
                    $.each($('.table-traslados tbody tr'),function(i,e){
                        $(e).find('td:eq(1)').text( $(e).parents('tbody').find('tr').length - $(e).index() );
                    });
                }, 500);

                $('.show-list sup').text($('.modalbox[data-modal="traslados"] tbody tr').length -1 );

                if($($('.table-traslados tbody')[0]).find('tr').length -1 <= 0){
                    $('.show-list').fadeOut(400);
                    $('.inline-table').fadeOut(400);
                    $('.ctrls-table').fadeOut(400);
                }

                /** Ajuste del valor limite de traslados en el JSON de activos  **/
                var idActivo = $(this).parents('tr').find('td:eq(2)').text().trim()+$(this).parents('tr').find('td:eq(3)').text().trim();
                var valor_traslado = Number($(this).parents('tr').find('td:eq(19)').text().replace(/[$ .]/g,''));

                valuesActiveJSON[idActivo].available_value += valor_traslado;

                if($('[name="id_numero_activo"]').val()+$('[name="id_subnumero_activo"]').val() == idActivo){
                    $('[name="valor_adquisicion"]').attr('data-limit',valuesActiveJSON[idActivo].available_value);
                }
            })
        /* control de campos de tabla */
            $('.table-traslados .green').attr('data-colspan', $('.table-traslados .green').attr('colspan'));

            $('.ctrls-table .ion').on('click', function(){
                var lenHideColumn = Number($(this).parents('.ctrls-table').siblings('div').find('tr:eq(1) .hide-column.separate-column').length)/2;
                var $focus = $(this).parents('.ctrls-table').siblings('div').find('.green');
                var colspanVal = Number( $focus.attr('data-colspan') );
                
                $(this).parents('.ctrls-table').siblings('div').find('.hide-column').stop().fadeToggle(0);
                $(this).toggleClass('ion-android-more-vertical ion-android-apps', 0);

                if($(this).is('.ion-android-more-vertical')){
                    $focus.attr('colspan', lenHideColumn+colspanVal)
                }
                if($(this).is('.ion-android-apps')){
                    $focus.attr('colspan', colspanVal)
                }
                

            });
        /* seleccionar subnumero desde el panel de información*/

            $(document).on('click', '.table-subnumbers tbody tr', function(){
                if(activeParent == "Origen"){
                    var subnumber = $(this).find('td:eq(0)').text();
                    $(this).parents('.modalbox').fadeOut(400);
                    $('.activo_origen [name="id_subnumero_activo"]').val(subnumber);
                    $('.activo_origen [name="id_subnumero_activo"]').change();
                }
                if(activeParent == "Destiny"){
                    var subnumber = $(this).find('td:eq(0)').text();
                    $(this).parents('.modalbox').fadeOut(400);
                    $('.activo_destino [name="id_subnumero_activo_destino"]').val(subnumber);
                    $('.activo_destino [name="id_subnumero_activo_destino"]').change();
                }
            });
        /* close modalmessage*/

            $(document).on('click', '.modalMessage',function(e){
                $(this).fadeOut(400, function(){
                    $(this).remove();
                })
            });
        /* show/hide characteristics*/
            var boolTextCharacteristics = true;
            $('.show-characteristics span').on('click', function(){
                var html = $(this).html();
                if(boolTextCharacteristics){
                    $(this).html( html.replace('Ocultar', 'Ver') );
                    boolTextCharacteristics = !boolTextCharacteristics;
                }
                else{
                    $(this).html( html.replace('Ver', 'Ocultar') );
                    boolTextCharacteristics = !boolTextCharacteristics;
                }
                $(this).find('.ion').toggleClass('ion-eye ion-eye-disabled');
                $('.slide-info').stop().slideToggle(400);
            });

        /* UPPERCASE & REGEX REPLACE */
            function regexReplace(obj, regex){
                obj.val( obj.val().toUpperCase() );
                var sanitized = obj.val().replace(regex, '');
                obj.val(sanitized);
            }
            $('[name="id_numero_activo"],[name="id_numero_activo_destino"]').on('change keyup', function(){
                regexReplace($(this), /[^0-9]/g );
            });
        /* Crear excel */

            function createJSON(){
                var arrayJson = [];
                var validateVCN = validateSigmaVCN('.inline-table');
                // console.log(validateVCN);

                for(var i= $($('.table-traslados tbody')[0]).find('tr').length - 1; i>= 0; i-- ){
                    var focus = $($('.table-traslados tbody')[0]).find('tr:eq('+i+')');
                    if(validateVCN[$(focus).find('td:eq(12)').text()] >= 0){
                        var tempJson = {
                            origen_numero_activo: $(focus).find('td:eq(2)').text(),
                            origen_subnumero_activo: $(focus).find('td:eq(3)').text(),
                            origen_fecha_capitalizacion: $(focus).find('td:eq(4)').text(),
                            origen_numero_inventario: $(focus).find('td:eq(5)').text(),
                            origen_denominacion: $(focus).find('td:eq(6)').text(),
                            origen_vida_util_anios: $(focus).find('td:eq(7)').text(),
                            origen_vida_util_meses: $(focus).find('td:eq(8)').text(),
                            origen_clave_amortizacion: $(focus).find('td:eq(9)').text(),
                            origen_clave: $(focus).find('td:eq(9)').attr('data-clave'),
                            origen_ceco: $(focus).find('td:eq(10)').text(),
                            origen_valor_adquisicion: $(focus).find('td:eq(11)').text().replace(/[$.]/g,''),
                            destino_numero_activo: $(focus).find('td:eq(12)').text(),
                            destino_subnumero_activo: $(focus).find('td:eq(13)').text(),
                            destino_fecha_capitalizacion: $(focus).find('td:eq(14)').text(),
                            destino_numero_inventario: $(focus).find('td:eq(15)').text(),
                            destino_denominacion: $(focus).find('td:eq(16)').text(),
                            destino_vida_util_anios: $(focus).find('td:eq(17)').text(),
                            destino_vida_util_meses: $(focus).find('td:eq(18)').text(),
                            destino_clave_amortizacion: $(focus).find('td:eq(19)').text(),
                            destino_clave: $(focus).find('td:eq(19)').attr('data-clave'),
                            destino_ceco: $(focus).find('td:eq(20)').text(),
                            destino_valor_traslado: $(focus).find('td:eq(21)').text().replace(/[$.]/g,''),
                            destino_motivo_traslado: $(focus).find('td:eq(22)').text(),
                            observaciones: $(focus).find('td:eq(23)').text()
                        };
                        arrayJson.push(tempJson);
                    }
                    else{
                        modalMessage({message: "El activo destino "+$(focus).find('td:eq(12)').text()+" no se incluirá en el formato GFI-F-049 puesto que su valor contable neto es igual a 0"});
                    }
                }
                return { 
                    'DarthVader': JSON.stringify(arrayJson)
                };
            }

            $('#formatoTraslados .btn-end').on('click',function(e){
                e.preventDefault();
                if($('.table-traslados tbody tr').length){
                    var url = 'include/generarXlsTraslados.php';
                    var arrayJson = createJSON();
                    // console.log(arrayJson);
                    $('#wait').fadeIn(400);
                    $.post(url, {jsonExcel: arrayJson.DarthVader }, function(data){
                        var d = JSON.parse(data);

                        var $a = $("<a>");
                        $a.attr("href","include/"+d.route);
                        $("body").append($a);
                        $a[0].setAttribute("download",d.name);
                        $a[0].click();
                        $a.remove();
                        $('#wait').fadeOut(400);
                    });
                }
                else{
                    modalMessage({message: 'No existe solicitudes de bajas'})
                }
            });
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);