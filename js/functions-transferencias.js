var solicitudes = new Array(); //Variable para items agregados a la solicitud
var meses = 0 ; //Variable para calculo de la Vida Util

$(function(){
    $.getJSON("js/json/cecos.json", function(data){
        var cecos = data.items;
        $( "#cecoNew" ).autocomplete({
            source: cecos,
            minLength: 3,
            focus: function( event, ui ) {
                $( "#cecoNew" ).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                //resetForm();
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append(  item.label + " - " + item.nombre )
            .appendTo( ul );
        };
    });
    
// VALIDATOR *****************************************************/
    /**
     * Custom Validation Method Called "regex"
     * This custom validation method receibe as input an regular expresion 
     * The error message by default is "El campo contiene caracteres no permitidos"
     */
    jQuery.validator.addMethod(
        "regex", 
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }, 
        "El campo contiene caracteres no permitidos"
    );
    /**
     * Form validation rules and error messages
     */
    $("#formatoDatosTecnicos").validate({
        debug: true,
        rules: {
            activo: {
                number: true,
                required:true
            },
            placa:{
                required: true
            },
            departamentoNew:{},
            municipioNew:{},
            ubicacionNew:{},
            areaNew:{},
            vicepresidenciaNew:{},
            gerenciaNew:{},
            cecoNew:{
                maxlength: 6,
                regex: "^[A-Za-z]{2}[0-9]{4}$"
            },
            claveNew:{},
            campoNew:{},
            vuANew:{
                required: true,
                max: 999,
                regex:  "^[0-9]{1,3}$" 
            },
            vuMNew:{
                required: true,
                max: 12,
                regex:  "^(0?[0-9]?[0-9]|1[0-2])$"
            },
            justificacionBox:{
                maxlength: 255,
                regex:  "^[a-zA-Z0-9,.:;() ]*$"
            }
        },
        messages: {
            activo: {
                number: "Solamente se permiten caracteres numéricos",
                required: "Este campo es requerido"
            },
            placa:{
                required: "Este campo es requerido"
            },
            cecoNew:{
                maxlength: "La longitud máxima son 6 caracteres",
                regex: "El campo contiene caracteres no permitidos o no sigue el patrón"
            },
            vuANew:{
                max: "el valor máximo a resgistrar es 999 meses",
                regex:  "El campo contiene caracteres no permitidos o no sigue el patrón" 
            },
            vuMNew:{
                max: "El valor máximo es 12 meses",
                regex:  "El campo contiene caracteres no permitidos o no sigue el patrón"
            },
            justificacionBox:{
                maxlength: "El texto supera el límite de 255 caracteres",
                regex:  "El campo contiene caracteres no permitidos o no sigue el patrón"
            }
        }
    });
// END VALIDATOR *****************************************************/

/*  REQUEST DATA */
    // De acuerdo al número de activo+sn o placa, se consultan en la base de datos auxiliar, 
    // las principales características del activo fijo encontrado
    function requestData(valor, tipo){
        // Reset user input fields
        $('[data-toggle="tooltip"]').tooltip('enable'); //Se rehabilitan los tooltip en caso de que hayan sido desactivados
        $('input.user-input').val('');
        //$('select').val("");
        // End Reset
    
        if(tipo == 'noInventario'){
            // Inputs reset
            $('[name="activo"]').val('');
            $('[name="subn"]').val('0');
            // Datos de consulta para el JSON
            var requestJSON = {
                'id_referencia': tipo,  
                'id_numero_referencia': valor
            }
        }
        else if(tipo == 'activo'){
            // Input reset
            $('[name="placa"]').val('');
            // Datos de consulta para el JSON 
            var subNumber = $('#subn').val().trim();
            var requestJSON = {
                'id_referencia': tipo,
                'id_numero_referencia': valor,
                'id_subnumero_referencia': subNumber
            }
        }
        var url = 'include/consultasTransferencias.php';

        //console.log(requestJSON);
        $.getJSON(url, requestJSON, function(data){
            console.log(data);
            //Se muestran los valores de la respuesta JSON
            if(data.result == "invalid active"){
                $(".modificaciones").fadeOut(400);
                $(".show-characteristics").fadeOut(400);
                $(".slide-info").fadeOut(400);
                $("#justificacion").fadeOut(400);
                localStorage.clear();
                if(tipo == 'activo'){
                    modalMessage({
                        message:"El número de activo que buscó no está registrado, intente buscar por número de inventario(placa) o contáctese con CAF por medio de una asesoría.",
                        type:'warning'
                    });
                }
                if(tipo == 'noInventario'){
                    modalMessage({
                        message:"El número de placa que buscó no está registrado, intente buscar por número de activo o contáctese con CAF por medio de una asesoría.",
                        type:'warning'
                    });
                }

                $('#activo').val('');
                $('#placa').val('');
                $('.operacionBox').fadeOut(400);
            }
            else{
                // MOSTRAR VALORES COMPLEMENTARIOS DE BUSQUEDA
                if(tipo == 'noInventario' || tipo == 'activo'){
                    $('[name="activo"]').val(data.activo);
                    $('[name="subn"]').val(data.sn);
                    $('[name="placa"]').val(data.inventario);
                }

                // MOSTRAR TIPO DE OPERACION
                $('#operacion').val(data.operacion);
                $('.operacionBox').fadeIn(400);

                // MOSTRAR TEXTO "VER CARACTERISTICAS CON ICONO OJO"
                $('.row.show-characteristics').fadeIn(400);
                $('#justificacion').fadeIn(400);

                // MOSTRAR CAJA DE PARAMETROS A MODIFICAR
                $(".modificaciones").fadeIn(400);

                // ACTIVAR LISTA DE SUBNUMEROS
                if(data.subnumeros != 0 ){
                    $('.sn .ion').remove();
                    var html = '<span class="ion ion-ios-list" data-modal="subnumbers" data-toggle="tooltip" data-placement="down" title="El activo tiene '+data.subnumeros+' subnúmeros"></span>';
                    $('.sn').append(html);
                    $('.sn .ion').tooltip(); 
                }

                //INFORMACION  DEL ACTIVO
                $('#departamento').val(data.departamento).attr('title', data.departamento);
                $('#municipio').val(data.nombreMunicipio).attr('title', data.nombreMunicipio);
                $('#campo').val(data.nombreCampoPlanta).attr('title', data.nombreCampoPlanta);
                $('#ubicacion').val(data.nombreUbicacionGeografica).attr('title', data.nombreUbicacionGeografica);

                $('#area').val(data.area).attr('title', data.area);
                $('#vicepresidencia').val(data.vicepresidencia).attr('title', data.vicepresidencia);
                $('#gerencia').val(data.gerencia).attr('title', data.gerencia);

                $('#ceco').val(data.ceco).attr('title', data.ceco);

                var nombreClave;
                if( Number.isInteger(parseInt(data.claveAr01)) && parseInt(data.claveAr01) >= 0){
                    if(data.claveAr01==0)
                        nombreClave = "no deprecia";
                    else
                        nombreClave = "Unidades de Producción";
                }
                else
                    nombreClave = "Línea Recta";

                $('#clave').val(nombreClave).attr('title', data.claveAr01);
                //INFORMACION ADICIONAL
                $('[data-name="clase"]').text("("+data.clase+") "+ data.nombreClase);
                $('[data-name="fechaCapitalizacion"]').text(data.fechaCapitalizacion);
                $('[data-name="denominacion"]').text(data.denominacion);
                $('[data-name="denominacion2"]').text(data.denominacion2);
                $('[data-name="fabricante"]').text(data.fabricante);
                $('[data-name="tag"]').text(data.tag);
                $('[data-name="serie"]').text($.trim(data.serie));
                $('[data-name="modelo"]').text(data.modelo);
                $('[data-name="capacidad"]').text(data.capacidad);
                $('[data-name="unidadMedida"]').text(data.unidadMedida);
                
                $('[data-name="claveProd"]').val(data.claveProd);
                $('[data-name="claveOtros"]').val(data.claveOtros);

            }
            $('#wait').fadeOut(400);
        });
        
    }

    /* 
    Función consulta de subnúmeros, está recibe 2 valores:
    field_value:String Activo
    focus_element:ElementoDom Modalbox donde se va a mostrar los resultados de los subnúmeros
    */
    function requestSubnumbers(field_value , focus_element){
        // Se remueve la tabla con los ultimos datos agregados
        $(focus_element).find('.table-subnumbers').remove();
        // url de consulta(url) y parametros de consulta(requestJSON)
        var url = 'include/consultasTransferencias.php';
        var requestJSON = {
            'id_referencia': 'subnumeros',
            'id_numero_referencia': field_value
        }
        // Consulta al php que trae un json con los datos necesarios, se define:
        $.get(url, requestJSON, function(data){
            console.log(data);
            // Se crea la tabla que muestra la información de subnúmeros
            var html = '<table class="table table-striped table-responsive table-subnumbers"><thead class="thead-default"><tr><th>SN</th><th>Descripción</th><th>Descripción 2</th><th>CeCo</th></tr></thead><tbody>';
            if(data.length != undefined){
                $.each(data,function(i,e){
                    html += '<tr><td>'+e.sn+'</td><td>'+e.denominacion+'</td><td>'+e.denominacion2+'</td><td>'+e.numero_ceco+'</td></tr>';
                });
            }
            else{
                html += '<tr><td>'+data.sn+'</td><td>'+data.denominacion+'</td><td>'+data.denominacion2+'</td><td>'+data.numero_ceco+'</td></tr>';
            }
            html += '</tbody></table>';
            $(focus_element).find('.cover-table').html(html);
        });
    }



    $(document).ready(function(){
        localStorage.clear();

        // Se aplica formato de número o moneda
        $("#vuANew").priceFormat({
            prefix:"",
            suffix: "",
            centsSeparator:"",
            thousandsSeparator:"",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        });
        $("#vuMNew").priceFormat({
            prefix:"",
            suffix: "",
            centsSeparator:"",
            thousandsSeparator:"",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        });
        //PLACEHOLDER FOR INPUT FIELDS
        $('#denominacion2New').attr('placeholder', 'Nueva denominación 2');
        $('#inventarioNew').attr('placeholder', 'Nuevo #inventario');
        $('#modeloNew').attr('placeholder', 'Nuevo modelo');
        $('#serieNew').attr('placeholder', 'Nueva serie');
        $('#tagNew').attr('placeholder', 'Nuevo Tag');
        $('#capacidadNew').attr('placeholder', 'Nueva capacidad');
        $('#vuANew').attr('placeholder', 'Años');
        $('#vuMNew').attr('placeholder', 'Meses');
        $('#vurmNew').attr('placeholder', 'Nueva VUR');
        $('#capitalizacionNew').attr('placeholder', 'Nueva Fecha cap.');
        $('#justificacionBox').attr('placeholder', 'Digite observaciones para este activo');

/**
 * *****************************************
 * *****************************************
 *              EVENT LISTENERS 
 * *****************************************
 * *****************************************
 */
        /* PRECARGA AJAX */  
            // Muestra div #wait con imagen de precarga al iniciar el método ajax de jquery 
            // Al completar el método ajax de jquery, esconde el div #wait 
            $(document)
            .ajaxStart(function(){
                $("#wait").css("display", "block");
            })
            .ajaxComplete(function(){
                function hide(){
                    $("#wait").css("display", "none");
                }
                setTimeout(hide, 500);
            });

        // tooltips
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
        }); 

        // Event Listener para agregar valores del formulario a tabla virtual
        $("#agregar").click(function(){
            var validator = $( "#formatoTransferencias" ).validate();
            validator.form( "#formatoTransferencias" );
            console.log("Validation Result: "+ validator); 
            console.log("Number of invalids: "+validator.numberOfInvalids());
            if(validator.numberOfInvalids() == 0){
                duplicates = verifyDuplicates();
                if(duplicates == true){
                    modalMessage({
                        message: "¡Ya hay una solicitud para ese criterio del Activo Fijo!",
                        type: "warning"
                    });
                }
                else{
                    modalMessage({
                        message: "¡Solicitud de Datos Técnicos recibida!",
                        type: "success"
                    });

                    cambios = countChanges();
                    console.log("cambios detectados:" + cambios);
                    if(cambios>0){
                        addToTable("table1");
                        addToTable("tableBox1");
                        $("#table1").show();
                        resetForm();
                        $('.show-list').fadeIn(400);
                        $('.show-list sup').text($('#tableBox1 tbody tr').length);
                    }
                    else{
                        modalMessage({
                            type: "warning",
                            message: "No se han modificado parámetros..."
                        });
                    }
                }
            }
        });
        /* BUSCAR POR DEPARTAMENTO */
        $( "#departamentoNew" ).change(function() {
            buscarMunicipios( $( this ).val() );
        });
        function buscarMunicipios(val){
            $.ajax({
                type: 'post',
                url: 'include/buscarMunicipios.php',
                data: {
                    get_option:val
                },
                success: function (response) {
                    $("#municipioNew").html("<option value=''>Seleccione Municipio</option>"+response).prop('selectedIndex',0);
                    $("#ubicacionNew").html("<option value=''>Seleccione Ubicación</option>").prop('selectedIndex',0);
                    $('#municipioNew').tooltip('disable'); //desactiva el toooltip en este campo en caso de  
                }
            });
        }

        /* BUSCAR POR MUNICIPIO */
        $( "#municipioNew" ).change(function() {
            buscarUbicaciones(  $("#municipioNew option:selected").text() );
            buscarCampos( $("#municipioNew option:selected").text() );
        });
        function buscarUbicaciones(val){
            $.ajax({
                type: 'post',
                url: 'include/buscarUbicaciones.php',
                data: {
                    get_option:val
                },
                success: function (response) {
                    $("#ubicacionNew").html("<option value=''>Seleccione Ubicación</option>"+response).prop('selectedIndex',0);
                    $('#ubicacionNew').tooltip('disable'); //desactiva el toooltip en este campo en caso de   
                }
            });
        }
        function buscarCampos(val){
            $.ajax({
                type: 'post',
                url: 'include/buscarCampos.php',
                data: {
                    get_option:val
                },
                success: function (response) {
                    $("#campoNew").html("<option value=''>Seleccione Campo Planta</option>"+response).prop('selectedIndex',0);
                    $('#campoNew').tooltip('disable'); //desactiva el toooltip en este campo en caso de                     
                }
            });
        }

        /* BUSCAR POR AREA - JERARQUIZACION */
        $( "#areaNew" ).change(function() {
            buscarVicepresidencias(  $("#areaNew option:selected").text() );
        });
        function buscarVicepresidencias(val){
            $.ajax({
                type: 'post',
                url: 'include/buscarVicepresidencias.php',
                data: {
                    get_option:val
                },
                success: function (response) {
                    $("#vicepresidenciaNew").html("<option value=''>Seleccione Vicepresidencia</option>"+response).prop('selectedIndex',0);
                    $("#gerenciaNew").html("<option value=''>Seleccione Gerencia</option>").prop('selectedIndex',0);
                }
            });
        }

        /* BUSCAR POR VICEPRESIDENCIA - JERARQUIZACION */
        $( "#vicepresidenciaNew" ).change(function() {
            buscarGerencias(  $("#vicepresidenciaNew option:selected").val() );
        });
        function buscarGerencias(val){
            $.ajax({
                type: 'post',
                url: 'include/buscarGerencias.php',
                data: {
                    get_option:val
                },
                success: function (response) {
                    console.log(response);
                    $("#gerenciaNew").html("<option value=''>Seleccione Gerencia</option>"+response).prop('selectedIndex',0);
                }
            });
        }

        // Click en elemento con atributo data-modal
        $(document).on('click','[data-modal]',function(e){
            var focus = $(e.target).attr('data-modal');
            switch(focus){
                case "subnumbers":
                    requestSubnumbers( $('[name="activo"]').val().trim() , $('.modalbox[data-modal="'+focus+'"]') );
                    //$(".show-characteristics, .slide-info, .modificaciones").fadeOut(400);
                    $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                break;
                default:
                    $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                break;
            }
        });

        // Click en elemento con clase close-modal
        $('.modalbox,.close-modal').on('click',function(e){
            if($(e.target).is('.table-cell') || $(e.target).is('.close-modal')){
            $(this).closest('.modalbox').stop().fadeOut(400);
            }
        });
        //cambio en el input de nombre "activo"
        $('.form-control[name="activo"]').on('change',function(){
            $('#wait').fadeIn(400);
            requestData( $(this).val().trim(), 'activo' );
        });
        //cambio en el input de "subnumero" 
        $('.form-control[name="subn"]').on('change',function(){
            $('#wait').fadeIn(400);
            requestData( $('[name="activo"]').val().trim(), 'activo' );
        });
        //cambio en el input de placa
        $('.form-control[name="placa"]').on('change',function(){
            $('#wait').fadeIn(400);
            requestData( $(this).val().trim(), 'noInventario' );
        });

        // Change en el input CECO
        $("#cecoNew").on('change', function(){
            var val = $("#cecoNew").val();
            buscarCeco(val);
        });
        function buscarCeco(val){
            $.ajax({
                type: 'post',
                url: 'include/buscarPorCeco.php',
                data: {
                    get_option: val
                },
                success: function (response){
                    console.log(response);
                    if(response=="" || response==null){
                        modalMessage({
                            message: "El CECO ("+ val +") no fue encontrado. Verifique y vuelva a intentar.",
                            type: "warning"
                        });
                        $("#cecoNew").val('');
                    }
                    else{
                        console.log(response);
                        clearTimeout(timeMessage);
                        
                        var html = '<span class="ion ion-checkmark-circled"></span>';
                        $('#cecoNew').after(html);
                        
                        var timeMessage = setTimeout(function(){
                            $('.ion-checkmark-circled').animate({
                                    opacity: 0.1,
                                    left: "80%",
                                    top: 0
                                }, 500, function() {
                                    $(this).remove();
                                });
                        }, 1500);
                        
                        // VALIDACIONES DE CAMBIO DE CECO
                        var val1 = $("#ceco").val().substring(0,2);
                        var val2 = $("#cecoNew").val().substring(0,2);

                        if( $('#claveProd').val() != $('#claveOtros').val() ){
                            if(val2 == "PR"){
                                $('#claveNew').val( $('#claveProd').val() );
                                console.log( $('#claveNew').val() );
                            }
                            if(val2 != "PR"){
                                $('#claveNew').val( $('#claveOtros').val() );
                                console.log( $('#claveNew').val() );
                            }
    
                            if( $('#claveNew').val() == "ZVUR"){
                                $('.vidaUtil').fadeIn(400);
                            }
                            else
                                $('.vidaUtil').fadeOut(400);
                        }
                        // fin validaciones de cambio de ceco
                    }
                }
            });
        }

        // Cambio den vida útil
        $('#vuANew, #vuMNew').on('change', function(e){
            if($('#vuANew').val()>=999){
                $('#vuANew').val(999);
                $('#vuMNew').val(0);
            }
            if($('#vuMNew').val()>12){
                $('#vuMNew').val(12);
            }
            meses = $('#vuANew').val()*12 + $('#vuMNew').val()*1;
            $('.vumcalc').val(meses);
        });

        //Click en el ojo de ver caracteristicas
        var boolTextCharacteristics = true;
        $('.show-characteristics span').on('click', function(){
            var html = $(this).html();
            if(boolTextCharacteristics){
                $(this).html( html.replace('Ver', 'Ocultar') );
                boolTextCharacteristics = !boolTextCharacteristics;
            }
            else{
                $(this).html( html.replace('Ocultar', 'Ver') );
                boolTextCharacteristics = !boolTextCharacteristics;
            }
            $(this).find('.ion').toggleClass('ion-eye ion-eye-disabled');
            $('.slide-info').stop().slideToggle(400);
        });

        //Click en las filas de la tabla de subnúmeros
        $(document).on('click', '.table-subnumbers tbody tr', function(){
            var subnumber = $(this).find('td:eq(0)').text();
            $(this).parents('.modalbox').fadeOut(400);
            //$('[name="subn"]').val(subnumber);
            //$('[name="subn"]').change();
        });

    });
 
/* RESET FORM */
    $("#reset").click(function(){
        resetForm();
    });
    function resetForm(){
        $('.vidaUtil').fadeOut(400);

        $('#activo').val("");
        //$('#subn').val("");
        $('#placa').val("");

        $('#departamentoNew').val("");
        $('#municipioNew').html('<option value>Seleccione Municipio</option>');
        $('#ubicacionNew').html('<option value>Seleccione Ubicación</option>');        
        $('#campoNew').html('<option value>Seleccione Campo Planta</option>');        
        $('#unidadFuncionalNew').html('<option value>Seleccione UF</option>');        
        $('#areaNew').val("");
        $('#vicepresidenciaNew').html('<option value>Seleccione Vicepresidencia</option>');
        $('#gerenciaNew').html('<option value>Seleccione Gerencia</option>');

        $('#cecoNew').val("");
        $("#clave").val("");
        $('#claveNew').val("");
        
        $('#vuANew').val("");
        $('#vuMNew').val("");
        $('#vummNew').val("");
             
        $('.operacionBox').fadeOut(400);
        $("label.error").hide();    
        $(".matriculaVehiculo").fadeOut(400);
        $('#justificacionBox').val("");

        $(".show-characteristics, .modificaciones").fadeOut(400);
    }

/* VERIFYDUPLICATES */
    function verifyDuplicates(){
        if(countChanges()>0){
            v = getCurrentValues();
            nuevaSolicitud = v.activo+"-"+v.inventario ;
            if( solicitudes.indexOf(nuevaSolicitud) >= 0 ){
                return true;
            }else{
                solicitudes.push(nuevaSolicitud);
                return false;
            }
        }
    }
/* GET CURRENT VALUES IN THE ENTIRE FORM*/
    function getCurrentValues(){
        currentValues = {
            "activo" :    $("#activo").val(),
            "subn" :    $("#subn").val(),
            "denominacion": $('[data-name="denominacion"]').text(),
            "serie": $('[data-name="serie"]').text(),
            "clase": $('[data-name="clase"]').text(),
            "inventario" :    $("#placa").val(),
            "departamento" :    $("#departamentoNew").val(),
                "old_departamento" :    $("#departamento").val(),
            "municipio" :    $("#municipioNew").val(),
                "old_municipio" :    $("#municipio").val(),
                "name_municipio" :    $("#municipioNew").find('option:selected').index() == 0 ? '': $("#municipioNew").find('option:selected').text(),
            "ubicacion" :    $("#ubicacionNew").val(),
                "old_ubicacion" :    $("#ubicacion").val(),
                "name_ubicacion" :   $("#ubicacionNew").find('option:selected').index() == 0 ? '': $("#ubicacionNew").find('option:selected').text(),
            "area" :    $("#areaNew").val(),
                "old_area" :    $("#area").val(),
            "vicepresidencia" :    $("#vicepresidenciaNew").val(),
                "old_vicepresidencia" :    $("#vicepresidencia").val(),
            "gerencia" :    $("#gerenciaNew").val(),
                "old_gerencia" :    $("#gerencia").val(),
            "ceco" :    $("#cecoNew").val(),
                "old_ceco" :    $("#ceco").val(),
            "clave" :    $("#claveNew").val(),
                "old_clave" :    $("#clave").val(),
            "campo" :    $("#campoNew").val(),
                "old_campo" :    $("#campo").val(),
                "name_campo" :    $("#campoNew").find('option:selected').index() == 0 ? '':$("#campoNew").find('option:selected').text(),
            "vum" :    $("#vummNew").val(),

            "observaciones": $("#justificacionBox").val()
        };
        return currentValues;
    }    

/* REMOVE FROM TABLE */
$(document).on('click', '.remove-row',function(){
    var rowInd = $(this).parents('tr').index();
    $.each($('.table-bajas tbody'),function(i,e){
        $(e).find('tr:eq('+rowInd+')').fadeOut(400,function(){
            var a = $(e).find('tr:eq('+rowInd+')>td:eq(1)').text();
            var b = $(e).find('tr:eq('+rowInd+')>td:eq(2)').text();
            var rem = a+"-"+b;
            if(solicitudes.indexOf(rem) >= 0)
                solicitudes.splice(solicitudes.indexOf(rem), 1);
            //console.log(solicitudes);
            $(this).remove();
        });
    });

    $('.show-list sup').text($('#tableBox1 tbody tr').length -1 );
    if( ($($('#tableBox1 tbody')[0]).find('tr').length) -1 <= 0){
        $('.show-list').fadeOut(400);
        $('#table1').fadeOut(400);
        $('#tableBox1').fadeOut(400);
    }
});

/* COUNT CHANGES IN INPUT FIELDS */
    function countChanges(){
        var matches = 0;
        $("input.user-input").each(function(i, val) {
            if ($(this).val() != '' && $(this).val() != null) {
            matches++;
            }
        });
        $("select").each(function(i, val) {
            if ($(this).val() != '' && $(this).val() != null) {
                matches++;
            }
        });
        return matches;
    }

/* ADD TO TABLE */
    function addToTable(id){
        variables = getCurrentValues();

        // console.log(variables);

        $('#'+id).find('tbody')
        .prepend($('<tr>')
            .append('<td><span class="ion ion-trash-b remove-row"></span></td')
            .append( $('<td>').text( variables.activo ) )
            .append( $('<td>').text( variables.denominacion ) )
            .append( $('<td>').text( variables.inventario ) )
            .append( $('<td>').text( variables.serie ) )
            .append( $('<td>').text( variables.clase ) )
            .append( $('<td>').text( variables.departamento ).attr({'data-old': variables.old_departamento}) )
            .append( $('<td>').text( variables.municipio ).attr({'data-old': variables.old_municipio, 'data-name': variables.name_municipio}) )
            .append( $('<td>').text( variables.ubicacion ).attr({'data-old': variables.old_ubicacion, 'data-name': variables.name_ubicacion}) )
            .append( $('<td>').text( variables.vicepresidencia ).attr('data-old', variables.old_vicepresidencia) )
            .append( $('<td>').text( variables.gerencia ).attr('data-old', variables.old_gerencia) )
            .append( $('<td>').text( variables.ceco ).attr('data-old', variables.old_ceco) )
            .append( $('<td>').text( variables.campo ).attr({'data-old': variables.old_campo, 'data-name': variables.name_campo}) )
            .append( $('<td>').text( variables.vum ) )
            .append( $('<td>').text( variables.observaciones ) )
         ); 
    }
/* MODAL MESSAGE */ 
    function modalMessage(json){
        $('.modalMessage').remove();
        clearTimeout(timeMessage);
        
        var html = '<div class="modalMessage '+json.type+'"><h3>'+json.message+'</h3><span class="ion ion-android-cancel close-message"></span></div>';
        $('body').append(html);
        var timeMessage = setTimeout(function(){
            $('.modalMessage').animate({
                    opacity: 0.1,
                    left: "100%",
                    top: 0
                }, 500, function() {
                    $(this).remove();
                });
                //$(this).remove();
        }, 10000);

    }
/* CLOSE MODALMESSAGE*/
$(document).on('click', '.modalMessage',function(e){
    $(this).fadeOut(400, function(){
        $(this).remove();
    })
});

/* UPPERCASE & REGEX REPLACE */
    function regexReplace(obj, regex){
        obj.val( obj.val().toUpperCase() );
        var sanitized = obj.val().replace(regex, '');
        obj.val(sanitized);
    }
    $('[name="activo"]').on('change keyup', function(){
        regexReplace($(this), /[^0-9]/g );
    });
    $('[name="placa"]').on('change keyup', function(){
        regexReplace($(this), /[^A-Z0-9\-]/g );
    });

/* CREATE EXCEL */
function createJSON(){
    var arrayJson = [];

    for(var i= $($('table.table tbody')[0]).find('tr').length - 1; i>= 0; i-- ){
        var focus = $($('table.table tbody')[0]).find('tr:eq('+i+')');
    
        var tempJson = {
            activo:                 $(focus).find('td:eq(1)').text(),
            denominacion:           $(focus).find('td:eq(2)').text(),
            inventario:             $(focus).find('td:eq(3)').text(),
            serie:                  $(focus).find('td:eq(4)').text(),
            clase:                  $(focus).find('td:eq(5)').text(),

            new_departamento:       $(focus).find('td:eq(6)').text(),
            old_departamento:       $(focus).find('td:eq(6)').attr('data-old'),
            new_municipio:          $(focus).find('td:eq(7)').text(),
            old_municipio:          $(focus).find('td:eq(7)').attr('data-old'),
            nombre_municipio:       $(focus).find('td:eq(7)').attr('data-name'),
            new_ubicacion:          $(focus).find('td:eq(8)').text(),
            old_ubicacion:          $(focus).find('td:eq(8)').attr('data-old'),
            nombre_ubicacion:       $(focus).find('td:eq(8)').attr('data-name'),

            new_vicepresidencia:    $(focus).find('td:eq(9)').text(),
            old_vicepresidencia:    $(focus).find('td:eq(9)').attr('data-old'),
            new_gerencia:           $(focus).find('td:eq(10)').text(),
            old_gerencia:           $(focus).find('td:eq(10)').attr('data-old'),
            new_ceco:               $(focus).find('td:eq(11)').text(),
            old_ceco:               $(focus).find('td:eq(11)').attr('data-old'),

            new_campo:              $(focus).find('td:eq(12)').text(),
            old_campo:              $(focus).find('td:eq(12)').attr('data-old'),
            nombre_campo:           $(focus).find('td:eq(12)').attr('data-name'),
            vu_meses:               $(focus).find('td:eq(13)').text(),
            observaciones:          $(focus).find('td:eq(14)').text()
        };
        arrayJson.push(tempJson);
        console.log(arrayJson);
    }
    return { 
        'DarthVader': JSON.stringify(arrayJson)
    };
}

$('#generarExcel,#generarExcel2').on('click',function(e){
    e.preventDefault();
    if($('table.table tbody tr').length){
        var url = 'include/generarXlsTransferencias.php';
        var arrayJson = createJSON();
        $('#wait').fadeIn(400);
        $.post(url, {jsonExcel: arrayJson.DarthVader }, function(data){
            console.log(data);
            var d = JSON.parse(data);
            console.log(d);
            var $a = $("<a>");
            $a.attr("href","include/"+d.route);
            $("body").append($a);
            $a[0].setAttribute("download",d.name);
            $a[0].click();
            $a.remove();
            $('#wait').fadeOut(400);
        });
    }
    else{
        modalMessage({message: 'No existen solicitudes de transferencias'})
    }
});    
});
