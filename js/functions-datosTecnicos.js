var solicitudes = new Array();
var meses = 0 ;
$(function(){
    // datepicker
        $( ".datepicker" ).datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'dd/mm/yy',
            minDate: - ((new Date).getDate() - 1),
            closeText: 'Cerrar',
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm'
        });
        $('.datepicker').next('.input-group-addon').on('click', function(){
            $(this).prev('.datepicker').focus();
        });
    // Event Listener para agregar valores del formulario a tabla virtual
    $("#agregar").click(function(){
        var validator = $( "#formatoDatosTecnicos" ).validate();
        validator.form( "#formatoDatosTecnicos" );
        // console.log(validator); 
        // console.log(validator.numberOfInvalids());
        if(validator.numberOfInvalids() == 0){
            duplicates = verifyDuplicates();
            if(duplicates == true){
                modalMessage({
                    message: "¡Ya hay una solicitud para ese Activo Fijo!",
                    type: "warning"
                });
            }
            else{
                modalMessage({
                    message: "¡Solicitud de Datos Técnicos recibida!",
                    type: "success"
                });

                cambios = countChanges();
                // console.log("cambios detectados:" + cambios);
                if(cambios>0){
                    addToTable("table1");
                    addToTable("tableBox");
                    $("#table1").show();
        
                    $('.show-list').fadeIn(400);
                    $('.show-list sup').text($('#tableBox tbody tr').length);
                }
                else{
                    modalMessage({
                        type: "warning",
                        message: "No se han modificado parámetros..."
                    });
                }
                
            }
        }
    });

// VALIDATOR *****************************************************/
    // Método de validación personalizado que recibe como parámetro una expresión regular (regexp)ç
    // El mensaje de error por defecto es "El campo contiene caracteres no permitidos" 
    jQuery.validator.addMethod(
        "regex", 
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }, 
        "El campo contiene caracteres no permitidos"
    );
    $("#formatoDatosTecnicos").validate({
        debug: true,
        rules: {
            activo: {
                number: true
            },
            subn: {},
            placa: {},
            denominacionNew:{
                required: true,
                regex:  "^[a-zA-Z0-9 ]*$"
            },
            denominacion2New:{                
                minlength: 5,
                maxlength: 50,
                regex:  "^[a-zA-Z0-9 ]*$",
            },
            inventarioNew:{
                maxlength: 7,
                regex: "^[0-9]*$"
            },
            fabricanteNew:{
            },
            modeloNew:{
                maxlength: 15,
                regex: "^[a-zA-Z0-9]*$"
            },
            serieNew:{
                maxlength: 18,
                regex: "^[a-zA-Z0-9-_]*$"
            },
            tagNew:{
                maxlength: 15,
                regex: "^[a-zA-Z0-9 -_\+]*$"
            },
            capacidadNew:{
                maxlength:18,
                regex: "^[0-9]+(\,[0-9]+)?$"
            },
            unidadNew:{},
            vuANew:{
                max: 999,
                regex:  "^[0-9]{1,3}$" 
            },
            vuMNew:{
                max: 12,
                regex:  "^[0-9]{1,2}$" 
            },
            curmNew:{
                maxlength: 8,
                regex:  "^[0-9]{1,2} $" 
            },
            vurmNew:{
                max: 999,
                regex:  "^[0-9]{1,3}$" 
            },
            capitalizacionNew:{},
            matriculaVNew:{},
            justificacionBox:{
                maxlength: 255,
                regex:  "^[a-zA-Z0-9,.:;() ]*$"
            }
        },
        messages: {
            activo: {required: "Este campo es requerido"},
            subn: {required: "Este campo es requerido"},
            placa: {required: "Este campo es requerido"},
            capacidadNew: {
                required: "Este campo es requerido",
                maxlength: "La longitud máxima permitida es 18 caracteres alfanuméricos",
                regex: "El campo contiene caracteres no permitidos. Para decimales utilice coma ( , ) "
            },
            custodioNew: {
                required: "Este campo es requerido",
                regex: "Verifique el registro del nuevo custodio"
            },
            vuANew:{
                maxlength: "Valor máximo permitido 999",
                regex:  "Contiene caracteres invalidos" 
            },
            vurmNew:{
                maxlength: "Valor máximo permitido 999",
                regex:  "Contiene caracteres invalidos" 
            },
            denominacion2New: {required: "Este campo es requerido"},
            fabricanteNew: {required: "Este campo es requerido"},
            matriculaNew: {required: "Este campo es requerido"},
            modeloNew: {required: "Este campo es requerido"},
            inventarioNew: {required: "Este campo es requerido"},
            serieNew: {required: "Este campo es requerido"},
            tagNew: {required: "Este campo es requerido"},
            justificacionBox: {
                required:  "Este campo es requerido",
                maxlength: "Longitud máxima excedida",
                regex:  "El campo contiene caracteres no permitidos"
            }
        }
    });
    // END VALIDATOR *****************************************************/

/*  REQUEST DATA */
    function claveAmortizacion(data){
        if(data == 'ZVUR'){
            return 'Línea recta';
        }
        if(Number(data) > 0){
            return 'Unidades de producción';
        }
        if(Number(data) == 0){
            return 'No deprecia';
        }
        return 'No definido';
    }
    // De acuerdo al número de activo+sn o placa, se consultan en la base de datos auxiliar, 
    // las principales características del activo fijo encontrado
    function requestData(valor, tipo){
        // Reset data in info box
        $.each($('.modificaciones .form-control'),function(i,e){
            if(!$(e).attr('readonly')){
                $(e).val('');
            }
        });
        $('.matriculaVehiculo').hide();

        if(tipo == 'noInventario'){
            // Inputs reset
            $('[name="activo"]').val('');
            $('[name="subn"]').val('0');
            // Datos de consulta para el JSON
            var requestJSON = {
                'id_referencia': tipo,  
                'id_numero_referencia': valor
            }
        }
        else if(tipo == 'activo'){
            // Input reset
            $('[name="placa"]').val('');
            // Datos de consulta para el JSON 
            var subNumber = $('#subn').val().trim();
            var requestJSON = {
                'id_referencia': tipo,
                'id_numero_referencia': valor,
                'id_subnumero_referencia': subNumber
            }
        }
        var url = 'include/consultasDatosTecnicos.php';

        // console.log(requestJSON);
        $.getJSON(url, requestJSON, function(data){
            // console.log(data);
            // agregar fecha actual en fecha inicial vida útil remanente
            var date = new Date();
            $('#fecha_VURM').val( date.getDate()+'/'+(date.getMonth()+1)+'/'+date.getFullYear() );
            //Se muestran los valores de la respuesta JSON
            if(data.result == "invalid active"){
                $(".show-characteristics").fadeOut(400);
                $(".slide-info").fadeOut(400);
                $(".modificaciones").fadeOut(400);
                $("#justificacion").fadeOut(400);
                localStorage.clear();
                if(tipo == 'activo'){
                    modalMessage({
                        message:"El número de activo que buscó no está registrado, intente buscar por número de inventario(placa) o contáctese con CAF por medio de una asesoría.",
                        type:'warning'
                    });
                }
                if(tipo == 'noInventario'){
                    modalMessage({
                        message:"El número de placa que buscó no está registrado, intente buscar por número de activo o contáctese con CAF por medio de una asesoría.",
                        type:'warning'
                    });
                }
            }
            else{
                //OPERACION - UNIDADES
                if(data.operacion == "Directa")
                    var urlUnidades = "js/json/unidadesCatalogo.json";
                else if(data.operacion == "Asociadas")
                    var urlUnidades = "js/json/unidadesSAP.json";
                $('#operacion').val(data.operacion);
                $('.operacionBox').fadeIn(400);
                //UNIDADES
                
                $.ajax({
                    type: 'post',
                    url: 'include/buscarUnidades.php',
                    data: {
                        operacion: data.operacion.toLowerCase(),
                        clase: data.clase
                    },
                    success: function (response) {
                        $("#unidadNew").html("<option value=''>Seleccione Unidad</option>"+response).prop('selectedIndex',0);
                    }
                });
        
                //DENOMINACION
                $('#denominacionNew').remove();
                if(data.sn != "0")
                {
                    $('.denominacionNew').append(
                        $('<select>', {
                            class: 'form-control',
                            value: '',
                            id: 'denominacionNew',
                            name: 'denominacionNew'
                        }) 
                        .append($('<option>',{
                            selected: true,
                            value: "",
                            text: "Seleccione denonimación...",
                            })
                        )
                        .append($('<option>').val("MAYOR VALOR SUBREPARTO").text( "MAYOR VALOR SUBREPARTO"))
                        .append($('<option>').val("MAYOR VALOR COSTOS FINANCIEROS").text( "MAYOR VALOR COSTOS FINANCIEROS"))
                        .append($('<option>').val("MAYOR VALOR COSTO DE ADQUISICION").text( "MAYOR VALOR COSTO DE ADQUISICION"))
                        .append($('<option>').val("MAYOR VALOR SERVICIO DE PERFORACION").text( "MAYOR VALOR SERVICIO DE PERFORACION"))
                        .append($('<option>').val("MAYOR VALOR SERVICIO DE WORKOVER").text( "MAYOR VALOR SERVICIO DE WORKOVER"))
                        .append($('<option>').val("MAYOR VALOR OBRAS CIVILES").text( "MAYOR VALOR OBRAS CIVILES"))
                        .append($('<option>').val("ADICION ACCESORIOS").text( "ADICION + ACCESORIOS"))
                        .append($('<option>').val("ADICION CAMPO").text( "ADICION + CAMPO"))
                        .append($('<option>').val("ADICION GESTION CONTRATO").text( "ADICION + GESTION CONTRATO"))
                        .append($('<option>').val("ADICION GESTORIA").text( "ADICION + GESTORIA"))
                        .append($('<option>').val("ADICION AMPLIACION").text( "ADICION + AMPLIACION"))
                        .append($('<option>').val("ADICION EJECUCION CONTRATO").text( "ADICION + EJECUCION CONTRATO"))
                        .append($('<option>').val("ADICION MATERIALES").text( "ADICION + MATERIALES"))
                        .append($('<option>').val("ADICION LINEA DE FLUJO").text( "ADICION + LINEA DE FLUJO"))
                        .append($('<option>').val("AUMENTO DE VIDA UTIL").text( "AUMENTO DE VIDA UTIL"))
                        .append($('<option>').val("AMPLIACION PRODUCTIVIDAD").text( "AMPLIACION PRODUCTIVIDAD"))
                        .append($('<option>').val("REDUCCION COSTOS DE OPERACION").text( "REDUCCION COSTOS DE OPERACION"))
                        .append($('<option>').val("REEMPLAZO DE PARTE O COMPONENTE").text( "REEMPLAZO DE PARTE O COMPONENTE"))
                    );
                    $('#denominacionNew').val("");
                }
                // CLASE DE VEHICULOS
                var clasesVehiculos = ["23001", "23002"];
                // console.log(data.clase);
                // if( jQuery.inArray( data.clase, clasesVehiculos) >= 0 ){
                //     $(".matriculaVehiculo").fadeIn(400);
                // }

                $('.row.show-characteristics').fadeIn(400);
                // $('.slide-info').fadeIn(400);
                $('.modificaciones').fadeIn(400);
                $('#justificacion').fadeIn(400);

                if(tipo == 'noInventario'){
                    $('[name="activo"]').val(data.activo);
                    $('[name="subn"]').val(data.sn);
                    $('[name="placa"]').val(data.inventario);
                }
                else if(tipo == 'activo'){
                    $('[name="activo"]').val(data.activo);
                    $('[name="subn"]').val(data.sn);
                    $('[name="placa"]').val(data.inventario);
                }

                // ACTIVAR LISTA DE SUBNUMEROS
                if(data.subnumeros != 0 ){
                    $('.sn .ion').remove();
                    var html = '<span class="ion ion-ios-list" data-modal="subnumbers" data-toggle="tooltip" data-placement="down" title="El activo tiene '+data.subnumeros+' subnúmeros"></span>';
                    $('.sn').append(html);
                    $('.sn .ion').tooltip(); 
                } 
                //INFORMACION ADICIONAL DEL ACTIVO
                    /*Información del slide info*/
                        $('[data-name="departamento"]').text(data.departamento);
                        $('[data-name="municipio"]').text(data.municipio);
                        $('[data-name="ubicacion_geografica"]').text(data.ubicacion_geografica);
                        $('[data-name="area"]').text(data.area);
                        $('[data-name="vicepresidencia"]').text(data.vicepresidencia).attr('data-sigla',data.siglaVicepresidencia);
                        $('[data-name="gerencia"]').text(data.gerencia).attr('data-sigla',data.siglaGerencia);
                        $('[data-name="ceco"]').text(data.ceco);
                        $('[data-name="clave_amortizacion"]').text( claveAmortizacion(data.clave_amortizacion) );
                        $('[data-name="campo_planta"]').text(data.campo_planta);
                        $('[data-name="custodio"]').text(data.custodio);
                        $('[data-name="clase"]').text(data.clase);
                        $('[data-name="nombre_clase"]').text(data.nombre_clase);
                        $('[data-name="tipo_operacion"]').text(data.tipo_operacion);
                    
                    // switch tag/sig
                        var claseValidationTagSig = [11001,11002,14001,28001,33006];
                        if( claseValidationTagSig.indexOf( Number(data.clase) ) != -1 ){
                            $('#tagNew').parents('.form-section').find('label').text('SIG:');
                        }
                        else{
                            $('#tagNew').parents('.form-section').find('label').text('TAG:');
                        }
                    // validación campo nuevo de inventario
                        var claseValidatorArray = [11001,11002,14001,28001];
                        
                        if(String(data.tipo_operacion).toLowerCase() == 'asociadas' || claseValidatorArray.indexOf( Number(data.clase) ) != -1 ){
                            $("#inventarioNew").rules('add', {
                                maxlength: 7,
                                regex: "^[a-zA-Z0-9]*$",
                                messages: {
                                    required:  "Este campo es requerido",
                                    maxlength: "Longitud máxima excedida",
                                    regex:  "El campo solo recibe carácteres alfanúmericos"
                                }
                            });
                        }
                        else{
                            $("#inventarioNew").rules('add', {
                                maxlength: 7,
                                regex: "^[0-9]*$",
                                messages: {
                                    required:  "Este campo es requerido",
                                    maxlength: "Longitud máxima excedida",
                                    regex:  "El campo solo recibe carácteres númericos"
                                }
                            });
                        }

                    $('#denominacion').val(data.denominacion);
                        //$('#denominacionNew').val(data.denominacion);
                    $('#denominacion2').val(data.denominacion2);
                        //$('#denominacion2New').val(data.denominacion2);
                    $('#inventario').val(data.inventario);
                        //$('#inventarioNew').val(data.inventario);

                    $('#fabricante').val(data.fabricante);
                        //$('#fabricanteNew').val(data.fabricante);
                    $('#modelo').val(data.modelo);
                        //$('#modeloNew').val(data.modelo);
                    $('#serie').val(data.serie);
                        //$('#serieNew').val(data.serie);

                    $('#tag').val(data.tag);
                        //$('#tagNew').val(data.tag);
                    $('#capacidad').val(data.capacidad);
                        var s = data.capacidad.replace(/\./g, '');
                        //$('#capacidadNew').val(s);
                    $('#unidad').val(data.unidad);
                        //$('#unidadNew').val(data.unidad);
                    $('#vuAnos').val(data.vuAnos);
                       // $('#vumNew').val(data.vum);
                    $('#vuPeriodos').val(data.vuPeriodos);
                       // $('#vumNew').val(data.vum);
                       
                    $('#vurm').val(data.vurm);
                        //$('#vurmNew').val(data.vurm);
                    $('#capitalizacion').val(data.capitalizacion);
                        //$('#capitalizacionNew').val(data.capitalizacion);
                    $('#matriculaV').val(data.matriculaVehiculo);
                        //$('#matriculaVNew').val(data.matriculaVehiculo);    
            }

            $('#wait').fadeOut(400);
        });
        
    }

    /* 
    Función consulta de subnúmeros, está recibe 2 valores:
    field_value:String Activo
    focus_element:ElementoDom Modalbox donde se va a mostrar los resultados de los subnúmeros
    */
    function requestSubnumbers(field_value , focus_element){
        // Se remueve la tabla con los ultimos datos agregados
        $(focus_element).find('.table-subnumbers').remove();
        // url de consulta(url) y parametros de consulta(requestJSON)
        var url = 'include/consultasDatosTecnicos.php';
        var requestJSON = {
            'id_referencia': 'subnumeros',
            'id_numero_referencia': field_value
        }
        // Consulta al php que trae un json con los datos necesarios, se define:
        $.get(url, requestJSON, function(data){
            console.log(data);
            // Se crea la tabla que muestra la información de subnúmeros
            var html = '<table class="table table-striped table-responsive table-subnumbers"><thead class="thead-default"><tr><th>SN</th><th>Descripción</th><th>Descripción 2</th><th>CeCo</th></tr></thead><tbody>';
            if(data.length != undefined){
                $.each(data,function(i,e){
                    html += '<tr><td>'+e.sn+'</td><td>'+e.denominacion+'</td><td>'+e.denominacion2+'</td><td>'+e.numero_ceco+'</td></tr>';
                });
            }
            else{
                html += '<tr><td>'+data.sn+'</td><td>'+data.denominacion+'</td><td>'+data.denominacion2+'</td><td>'+data.numero_ceco+'</td></tr>';
            }
            html += '</tbody></table>';
            $(focus_element).find('.cover-table').html(html);
        });
    }

    $(document).ready(function(){
        localStorage.clear();

        // Se aplica formato de número o moneda
        $("#vuANew").priceFormat({
            prefix:"",
            suffix: "",
            centsSeparator:"",
            thousandsSeparator:"",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        });
        $("#vuMNew").priceFormat({
            prefix:"",
            suffix: "",
            centsSeparator:"",
            thousandsSeparator:"",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        });
        //PLACEHOLDER FOR INPUT FIELDS
        $('#denominacion2New').attr('placeholder', 'Nueva denominación 2');
        $('#inventarioNew').attr('placeholder', 'Nuevo #inventario');
        $('#modeloNew').attr('placeholder', 'Nuevo modelo');
        $('#serieNew').attr('placeholder', 'Nueva serie');
        $('#tagNew').attr('placeholder', 'Nuevo Tag');
        $('#capacidadNew').attr('placeholder', 'Nueva capacidad');
        $('#vuANew').attr('placeholder', 'Años');
        $('#vuMNew').attr('placeholder', 'Meses');
        $('#vurmNew').attr('placeholder', 'Nueva VUR');
        $('#capitalizacionNew').attr('placeholder', 'Nueva Fecha cap.');
        $('#justificacionBox').attr('placeholder', 'Digite observaciones para este activo');

        // EVENT LISTENERS

        // Cambio den vida útil
        $('#vuANew, #vuMNew').on('change', function(e){
            if($('#vuANew').val()>=999){
                $('#vuANew').val(999);
                $('#vuMNew').val(0);
            }
            if($('#vuMNew').val()>12){
                $('#vuMNew').val(12);
            }
            meses = $('#vuANew').val()*12 + $('#vuMNew').val()*1;
            $('.vumcalc').val(meses);
        });

        // Click en elemento con atributo data-modal
        $(document).on('click','[data-modal]',function(e){
            var focus = $(e.target).attr('data-modal');
            switch(focus){
                case "subnumbers":
                    requestSubnumbers( $('[name="activo"]').val().trim() , $('.modalbox[data-modal="'+focus+'"]') );
                    $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                break;
                default:
                    $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
                break;
            }
        });

        // Click en elemento con clase close-modal
        $('.modalbox,.close-modal').on('click',function(e){
            if($(e.target).is('.table-cell') || $(e.target).is('.close-modal')){
            $(this).closest('.modalbox').stop().fadeOut(400);
            }
        });
        //cambio en el input de nombre "activo"
        $('.form-control[name="activo"]').on('change',function(){
            $('#wait').fadeIn(400);
            $('#subn').val(0);
            requestData( $(this).val().trim(), 'activo' );
        });
        //cambio en el input de "subnumero"
        $('.form-control[name="subn"]').on('change',function(){
            $('#wait').fadeIn(400);
            requestData( $('[name="activo"]').val().trim(), 'activo' );
            $sub = $('[name="subn"]').val().trim();
            if ($sub != 0){
                $('option[value="custodio"]').hide();
            }
            else{
                $('option[value="custodio"]').show();
            }
        });
        //cambio en el input de placa
        $('.form-control[name="placa"]').on('change',function(){
            $('#wait').fadeIn(400);
            requestData( $(this).val().trim(), 'noInventario' );
        });
        //Click en el ojo de ver caracteristicas
        var boolTextCharacteristics = true;
        $('.show-characteristics span').on('click', function(){
            var html = $(this).html();
            if(boolTextCharacteristics){
                $(this).html( html.replace('Ver', 'Ocultar') );
                boolTextCharacteristics = !boolTextCharacteristics;
            }
            else{
                $(this).html( html.replace('Ocultar', 'Ver') );
                boolTextCharacteristics = !boolTextCharacteristics;
            }
            $(this).find('.ion').toggleClass('ion-eye ion-eye-disabled');
            $('.slide-info').stop().slideToggle(400);
        });

        //Click en las filas de la tabla de subnúmeros
        $(document).on('click', '.table-subnumbers tbody tr', function(){
            var subnumber = $(this).find('td:eq(0)').text();
            $(this).parents('.modalbox').fadeOut(400);
            $('[name="subn"]').val(subnumber);
            $('[name="subn"]').change();
        });

    });
 
/* RESET FORM */
    $("#reset").click(function(){
        resetForm();
    });
    function resetForm(){
        $('#activo').val("");
        $('#subn').val("");
        $('#placa').val("");
        $('#datoActual').val("");
        $('.datoNuevo').val("");
        $('#justificacionBox').val("");
        $('#operacion').val("");
        $('#vuANew').val("");
        $('#vuMNew').val("");
        $('.operacionBox').fadeOut(400);
        $("label.error").hide();    
        $(".matriculaVehiculo").fadeOut(400);

    }

/* VERIFYDUPLICATES */
    function verifyDuplicates(){
        if(countChanges()>0){
            v = getCurrentValues();
            nuevaSolicitud = v.activo + v.subn + v.inventario + v.criterio ;
            if( solicitudes.indexOf(nuevaSolicitud) >= 0 ){
                return true;
            }else{
                solicitudes.push(nuevaSolicitud);
                return false;
            }
        }
    }
/* GET CURRENT VALUES IN THE ENTIRE FORM*/
    function getCurrentValues(){
        currentValues = {
            "activo" :    $("#activo").val(),
            "subnumero" :    $("#subn").val(),
            "denominacion" :  $('#subn').val()==0 ?  $("#denominacion").val() : $("#denominacionNew").val(),
            "denominacion2":    $("#denominacion2New").val() ,
            "inventario":       $("#inventarioNew").val(),
            "fabricante":         $("#fabricanteNew").val(),
            "modelo":       $("#modeloNew").val(),
            "serie":        $("#serieNew").val(),
            "tag":          $("#tagNew").val(),
            "capacidad":    $("#capacidadNew").val(),
            "unidad":           $("#unidadNew").val(),
            "vum":              $("#vumNew").val(),
            "vurm":             $("#vurmNew").val(),
            "ceco":             $('[data-name="ceco"]').text(),
            "vicepresidencia":  $('[data-name="vicepresidencia"]').attr('data-sigla'),
            "gerencia":         $('[data-name="gerencia"]').attr('data-sigla'),
            "fecha_vurm":       $("#fecha_VURM_New").val(),
            "capitalizacion":       $("#capitalizacionNew").val(),
            "observaciones": $("#justificacionBox").val(),
            "matriculaV": $("#matriculaVNew").val(),
            "old_denominacion": $('#denominacion').val(),
            "old_denominacion2": $('#denominacion2').val(),
            "old_inventario": $('#inventario').val(),
            "old_fabricante": $('#fabricante').val(),
            "old_modelo": $('#modelo').val(),
            "old_serie": $('#serie').val(),
            "old_tag": $('#tag').val(),
            "old_capacidad": $('#capacidad').val(),
            "old_unidad": $('#unidad').val(),
            "old_vum": Number($('#vuAnos').val())*12 + Number($('#vuPeriodos').val()),
            "old_vurm": $('#vurm').val(),
            "old_fecha_vurm":       $("#fecha_VURM").val(),
            "old_capitalizacion": $('#capitalizacion').val()
        };
        return currentValues;
    }    
/* COUNT CHANGES IN INPUT FIELDS */
    function countChanges(){
        var matches = 0;
        $("input.user-input").each(function(i, val) {
            if ($(this).val() != '') {
            matches++;
            }
        });
        $("select").each(function(i, val) {
            if ($(this).val() != '') {
                matches++;
            }
        });
        return matches;
    }

/* ADD TO TABLE */
    function addToTable(id){
        var variables = getCurrentValues();
        // console.log(variables);
        $('#'+id).find('tbody')
        .prepend($('<tr>')
            .append('<td><span class="ion ion-trash-b remove-row"></span></td')
            .append( $('<td>').text( variables.activo ) )
            .append( $('<td>').text( variables.subnumero ) )
            .append( $('<td>').text( variables.denominacion ).attr('data-old', variables.old_denominacion) )
            .append( $('<td>').text( variables.denominacion2 ).attr('data-old', variables.old_denominacion2) )
            .append( $('<td>').text( variables.inventario ).attr('data-old', variables.old_inventario) )
            .append( $('<td>').text( variables.fabricante ).attr('data-old', variables.old_fabricante) )
            .append( $('<td>').text( variables.modelo ).attr('data-old', variables.old_modelo) )
            .append( $('<td>').text( variables.serie ).attr('data-old', variables.old_serie) )
            .append( $('<td>').text( variables.tag ).attr('data-old', variables.old_tag) )
            .append( $('<td>').text( variables.capacidad ).attr('data-old', variables.old_capacidad) )
            .append( $('<td>').text( variables.unidad ).attr('data-old', variables.old_unidad) )
            // .append( $('<td>').text( variables.vum ).attr('data-old', variables.old_vum) )
            .append( $('<td>').text( variables.vurm ).attr({
                'data-old': variables.old_vurm,
                'data-fecha': (String(variables.fecha_vurm).trim() != undefined && String(variables.fecha_vurm).trim() != "") ? variables.fecha_vurm : variables.old_fecha_vurm,
                'data-ceco': variables.ceco,
                'data-vicepresidencia': variables.vicepresidencia,
                'data-gerencia': variables.gerencia
            }) )
            // .append( $('<td>').text( variables.capitalizacion ).attr('data-old', variables.old_capitalizacion) )
            // .append( $('<td>').text( variables.matriculaV ) )
            .append( $('<td>').text( variables.observaciones ) )
        ); 
    }
/* MODAL MESSAGE */ 
    function modalMessage(json){
        $('.modalMessage').remove();
        clearTimeout(timeMessage);
        
        var html = '<div class="modalMessage '+json.type+'"><h3>'+json.message+'</h3><span class="ion ion-android-cancel close-message"></span></div>';
        $('body').append(html);
        var timeMessage = setTimeout(function(){
            $('.modalMessage').animate({
                    opacity: 0.1,
                    left: "100%",
                    top: 0
                }, 500, function() {
                    $(this).remove();
                });
                //$(this).remove();
        }, 10000);

    }
/* CLOSE MODALMESSAGE*/

    $(document).on('click', '.modalMessage',function(e){
        $(this).fadeOut(400, function(){
            $(this).remove();
        })
    });

/* UPPERCASE & REGEX REPLACE */
    function regexReplace(obj, regex){
        obj.val( obj.val().toUpperCase() );
        var sanitized = obj.val().replace(regex, '');
        obj.val(sanitized);
    }
    $('[name="custodioNew"]').on('change keyup', function(){
        regexReplace($(this), /[^a-zA-Z0-9]/g );
    });
    $('[name="vurmNew"]').on('change keyup', function(){
        regexReplace($(this), /[^0-9]/g );
    });
    $('[name="activo"]').on('change keyup', function(){
        regexReplace($(this), /[^0-9]/g );
    });
    $('[name="placa"]').on('change keyup', function(){
        regexReplace($(this), /[^A-Z0-9\-]/g );
    });
    
/* OPEN MODAL*/
    $(document).on('click','[data-modal]',function(e){
    
        var focus = $(e.target).attr('data-modal');
        switch(focus){
            case "datosTecnicos":
                $('#tableBox.modal').stop().fadeIn(400);
                break;
        }

    });

    $('.modal .close').on('click',function(){
        $(this).parents('.modal').stop().fadeOut(400);
    });
/* REMOVE ROW TABLES */
    $(document).on('click','.remove-row',function(){
        var ind = $(this).parents('tr').index();
        solicitudes.splice(ind, 1);
        $.each($('table.table'),function(i,e){
            $(e).find('tbody tr:eq('+ind+')').remove();
        });
    });
/* CREATE EXCEL */
    function createJSON(){
        var arrayJson = [];

        for(var i= $($('table.table tbody')[0]).find('tr').length - 1; i>= 0; i-- ){
            var focus = $($('table.table tbody')[0]).find('tr:eq('+i+')');
        
            var tempJson = {
                activo: $(focus).find('td:eq(1)').text(),
                subnumero: $(focus).find('td:eq(2)').text(),
                denominacion: $(focus).find('td:eq(3)').attr('data-old'),
                numero_inventario: $(focus).find('td:eq(5)').attr('data-old'),
                serie: $(focus).find('td:eq(8)').attr('data-old'),
                nueva_denominacion: $(focus).find('td:eq(3)').text(),
                nueva_denominacion2: $(focus).find('td:eq(4)').text(),
                nueva_capacidad: $(focus).find('td:eq(10)').text(),
                nueva_unidad_medida: $(focus).find('td:eq(11)').text(),
                nuevo_numero_inventario: $(focus).find('td:eq(5)').text(),
                nuevo_tag: $(focus).find('td:eq(9)').text(),
                nueva_serie: $(focus).find('td:eq(8)').text(),
                nuevo_fabricante: $(focus).find('td:eq(6)').text(),
                nuevo_modelo: $(focus).find('td:eq(7)').text(),
                nuevo_ceco: $(focus).find('td:eq(12)').text().trim() != "" ? $(focus).find('td:eq(12)').data('ceco'): '',
                nueva_vicepresidencia: $(focus).find('td:eq(12)').text().trim() != "" ? $(focus).find('td:eq(12)').data('vicepresidencia'): '',
                nueva_gerencia: $(focus).find('td:eq(12)').text().trim() != "" ? $(focus).find('td:eq(12)').data('gerencia'): '',
                fecha_inicial_vida_util_remanente: $(focus).find('td:eq(12)').text().trim() != "" ? $(focus).find('td:eq(12)').data('fecha'): '',
                nueva_vida_util_remanente: $(focus).find('td:eq(12)').text().trim(),
                observaciones: $(focus).find('td:eq(13)').text()
            };
            // activo: $(focus).find('td:eq(1)').text(),
            // new_denominacion: $(focus).find('td:eq(2)').text(),
            // old_denominacion: $(focus).find('td:eq(2)').attr('data-old'),
            // new_denominacion2: $(focus).find('td:eq(3)').text(),
            // old_denominacion2: $(focus).find('td:eq(3)').attr('data-old'),
            // new_inventario: $(focus).find('td:eq(4)').text(),
            // old_inventario: $(focus).find('td:eq(4)').attr('data-old'),
            // new_fabricante: $(focus).find('td:eq(5)').text(),
            // old_fabricante: $(focus).find('td:eq(5)').attr('data-old'),
            // new_modelo: $(focus).find('td:eq(6)').text(),
            // old_modelo: $(focus).find('td:eq(6)').attr('data-old'),
            // new_serie: $(focus).find('td:eq(7)').text(),
            // old_serie: $(focus).find('td:eq(7)').attr('data-old'),
            // new_tag: $(focus).find('td:eq(8)').text(),
            // old_tag: $(focus).find('td:eq(8)').attr('data-old'),
            // new_capacidad: $(focus).find('td:eq(9)').text(),
            // old_capacidad: $(focus).find('td:eq(9)').attr('data-old'),
            // new_unidad: $(focus).find('td:eq(10)').text(),
            // old_unidad: $(focus).find('td:eq(10)').attr('data-old'),
            // new_vum: $(focus).find('td:eq(11)').text(),
            // old_vum: $(focus).find('td:eq(11)').attr('data-old'),
            // new_vurm: $(focus).find('td:eq(12)').text(),
            // old_vurm: $(focus).find('td:eq(12)').attr('data-old'),
            // new_capitalizacion: $(focus).find('td:eq(13)').text(),
            // old_capitalizacion: $(focus).find('td:eq(13)').attr('data-old'),
            // new_matriculaVehiculo: $(focus).find('td:eq(14)').text(),
            // old_matriculaVehiculo: $(focus).find('td:eq(14)').text(),
            // observaciones: $(focus).find('td:eq(15)').text()
            arrayJson.push(tempJson);
        }
        return { 
            'DarthVader': JSON.stringify(arrayJson)
        };
    }

    $('#generarExcel,#generarExcel2').on('click',function(e){
        e.preventDefault();
        if($('table.table tbody tr').length){
            var url = 'include/generarXlsDatosTecnicos.php';
            var arrayJson = createJSON();
            $('#wait').fadeIn(400);
            $.post(url, {jsonExcel: arrayJson.DarthVader }, function(data){
                var d = JSON.parse(data);

                var $a = $("<a>");
                $a.attr("href","include/"+d.route);
                $("body").append($a);
                $a[0].setAttribute("download",d.name);
                $a[0].click();
                $a.remove();
                $('#wait').fadeOut(400);
            });
        }
        else{
            modalMessage({message: 'No existen solicitudes de bajas'})
        }
    });
});
