(function($){
    // GLOBAL FUNCTIONS
    // datepicker
        $( ".datepicker" ).datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            dateFormat: 'dd/mm/yy',
            closeText: 'Cerrar',
            changeMonth: true,
            changeYear: true,
            prevText: '< Ant',
            nextText: 'Sig >',
            currentText: 'Hoy',
            monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
            dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
            dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
            weekHeader: 'Sm',
            yearRange: "-100:+0"
        });
        $('.datepicker').next('.input-group-addon').on('click', function(){
            $(this).prev('.datepicker').focus();
        });
    // formato a costo del activo
        $('.format').priceFormat({
            prefix:"$ ",
            centsSeparator:",",
            thousandsSeparator:".",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        });
    // get catalogo
        var catalogoJSON = [];
        $.getJSON("include/buscarCatalogo.php", function(data){
            catalogoJSON = data;
        });
    // get clases asociadas
        var clasesAsociadas = 0;
        $.get('include/buscarClases.php', { 'option': 'asociadas' }, function(data) {
            clasesAsociadas = data;
        });
    // get clases
        var clases = 0;
        $.get('include/buscarClases.php', { 'type': 'json' }, function(data) {
            clases = data;
        });
    // get fabricantes
        var fabricantesJSON = [];
        $.getJSON("include/buscarFabricantes.php", function(data){
            fabricantesJSON = data;
        });
    // get cecos
        var cecosJSON = [];
        $.getJSON("include/buscarCecos.php", function(data){
            cecosJSON = data;
        });
    // get campos
        var camposJSON = [];
        $.getJSON("include/buscarCampos.php", function(data){
            camposJSON = data;
        });
    // get departamentos
        var departamentosHTML = '';
        $.get('include/buscarDepartamentos.php', function(data){
            $('.form-control[name="departamento"]').append(data);
            departamentosHTML = '<option value="">Seleccione...</option>'+data;
        });
    // get municipios
        $('.form-control[name="departamento"]').change(function(){
            $('#wait').stop().fadeIn(400);
            var html = '<option value="">Seleccione...</option>';
            $('.form-control[name="municipio"]').html(html);
            $('.form-control[name="ubicacion_geografica"]').html(html);

            var query = {
                "get_option": $(this).val()
            };
            $.post('include/buscarMunicipios.php', query, function(data){
                $('.form-control[name="municipio"]').append(data);
                $('#wait').stop().fadeOut(400);
            });
        });
    // get ubicacion geografica
        $('.form-control[name="municipio"]').change(function(){
            $('#wait').stop().fadeIn(400);
            var html = '<option value="">Seleccione...</option>';
            $('.form-control[name="ubicacion_geografica"]').html(html);

            var query = {
                "get_option": $(this).find('option:selected').text()
            };
            $.post('include/buscarUbicaciones.php', query, function(data){
                $('.form-control[name="ubicacion_geografica"]').append(data);
                $('#wait').stop().fadeOut(400);
            });
        });
    // get activo
        function getActivo(element, sn){
            clearFields();
            var sn = sn || 0;
            $('#wait').fadeIn(400);
            var url = 'include/buscarPorActivo.php';
            var query = {
                get_option: element,
                subnum: sn
            }
            $.post( url, query, function(data){
                // asignar valores
                // console.log(data.vidaUtilRemanente);
                if(data.vidaUtilRemanente <= 0){
                    $('#wait').fadeOut(400);
                    modalMessage({message: "El activo "+data.activo+" no tiene vida útil remanente, para hacer creación de subnúmeros debe primero solicitar una actualización a la vida útil del padre o comunicarse con control de activos fijos"});
                    return false;
                }

                $('.form-control[name="vida_util"]').removeAttr('data-limit');

                $('.form-control[name="clase"]').val(data.clase);
                $('.form-control[name="inventario"]').val(data.inventario);
                $('.form-control[name="vida_util"]').attr('data-limit', data.vidaUtilRemanente).val('');
                $('.form-control[name="tag"]').val(data.tag);
                $('.form-control[name="ceco"]').val(data.ceco);
                $('.form-control[name="campo_planta"]').val(data.nombreCampoPlanta).attr('data-id',data.campoPlanta);

                if(data.tipoOperacion == 'Asociadas'){
                    $('.form-control[name="tipo_operacion"]').val('asociada');
                }
                if(data.tipoOperacion == 'Directa'){
                    $('.form-control[name="tipo_operacion"]').val('directa');
                }

                if(String(data.unidadMedida).trim() != ""){
                    $('.form-control[name="unidad_medida"]').append(
                        $('<option>',{
                            value: data.unidadMedida,
                            text: data.unidadMedida
                        })
                    );
                }

                if( data.departamento != ""){
                    $('.form-control[name="departamento"]').html('<option>Seleccione...</option>');
                    $('.form-control[name="departamento"]').append($('<option>', {
                        value: data.departamento,
                        text: data.departamento,
                        selected: true
                    }));

                    $('.form-control[name="municipio"]').html('<option>Seleccione...</option>');
                    $('.form-control[name="municipio"]').append($('<option>', {
                        value: data.municipio,
                        text: data.nombreMunicipio,
                        selected: true
                    }));

                    $('.form-control[name="ubicacion_geografica"]').html('<option>Seleccione...</option>');
                    $('.form-control[name="ubicacion_geografica"]').append($('<option>', {
                        value: data.ubicacionGeografica,
                        text: data.nombreUbicacionGeografica,
                        selected: true
                    }));
                }
                else{
                    $('.form-control[name="departamento"]').removeAttr('readonly').html(departamentosHTML);
                    $('.form-control[name="municipio"]').removeAttr('readonly');
                    $('.form-control[name="ubicacion_geografica"]').removeAttr('readonly');
                }

                if(data=="" || data==null){
                    modalMessage({message: "No se encontró el número de activo en el libro Auxiliar. Verifíquelo y vuelva a intentar."});
                }
                $('#wait').fadeOut(400);
            });
        }
    // get inventario
        function getInventario(element){
            $('.form-control[name="inventario"]').removeAttr('data-exist');
            var url = 'include/buscarPorInventario.php';
            var query = {
                get_option: element
            }
            $.getJSON(url, query, function(data){
                // console.log(data.count);
                if(Number(data.count) > 0){
                    $('.form-control[name="inventario"]').attr('data-exist',1);
                    $('.form-control[name="inventario"]').valid();
                }
                else{
                    $('.form-control[name="inventario"]').attr('data-exist',0);
                    $('.form-control[name="inventario"]').valid();
                }
            })
        }
    // get activo traslados
        var activosTraslados = 0;
        function getActivoTraslado(element, sn){
            var sn = sn || 0;
            $('#wait').fadeIn(400);
            var url = 'include/consultasTraslados.php';
            var query = {
                'id_numero_activo': element,
                'id_subnumero_activo': sn
            }
            $('.form-control[name="sn_traslado"]').html('<option value="">Seleccione...</option>');
            $('.form-control[name="fecha_traslado"]').val('');
            $('.form-control[name="valor_traslado"]').val('');
            $('.form-control[name="motivo_traslado"]').val('');

            $.getJSON(url, query, function(data) {
                if(data.result){
                    modalMessage({message: "No se encontró el número de activo en el libro Auxiliar. Verifíquelo y vuelva a intentar."});
                }
                else{
                    $.each(data, function(i, e) {
                        $('.form-control[name="sn_traslado"]').append('<option value="'+e.sn+'">'+e.sn+'</option>');
                    });
                    activosTraslados = data;
                }
                $('#wait').fadeOut(400);
            });
        }
    // get unidades asociadas
        var unidadesAsociadas = 0;
        $.post('include/buscarUnidades.php', {'operacion': 'sap'}, function(data) {
            unidadesAsociadas = data;
        });
        var unidadesCatalogo = 0;
        $.post('include/buscarUnidades.php', {'operacion': 'directa'}, function(data) {
            unidadesCatalogo = data;
        });
    // get salario minimo
        var salary = 0;
        $.getJSON('include/buscarSalario.php', {'ano': new Date().getFullYear() }, function(data) {
            if(data.length){
                salary = Number(data[0].salario)/2;
                $('.form-control[name="costo"]').rules('remove', 'min');
                $('.form-control[name="costo"]').attr('data-min', salary);
                $('.form-control[name="costo"]').rules('add', {
                    costo: 1,
                    messages: {
                        costo: 'El costo del activo no debe ser menor a '+ salary
                    }
                });
            }
        })
    // validaciones de forma
        jQuery.validator.addMethod( "regex", function(value, element, regexp) {
                var re = new RegExp(regexp);
                return this.optional(element) || re.test(value);
            },
            "El campo contiene caracteres no permitidos"
        );
        jQuery.validator.addMethod('inventory', function(value, element){
                var validate = Number($(element).attr('data-exist'));

                if( validate == 0 || isNaN(validate) ){
                    return true;
                }
                else{
                    return false;
                }
            },
                'El número de inventario ya existe'
        );
        jQuery.validator.addMethod('costo', function(value, element){
                var min = Number($(element).data('min'));
                var val = Number(value.trim().replace(/[\.$ ]/g,''));
                if(val < min){
                    return false;
                }
                else{
                    return true;
                }
            },
                'El costo del activo no debe ser menor a '
        );
        jQuery.validator.addMethod('vidaUtil', function(value, element, params){
                var type = $('.form-control[name="tipo_creacion"]').val();
                var val = Number(value);
                switch(type){
                    case 'principal':
                        if(val < 12){
                            params = '12 meses';
                            return false;
                        }
                    break;
                    case 'subnumero':
                        if(val < 1){
                            params = '1 mes';
                            return false;
                        }
                    break;
                }
                return true;
            }, function(params){
                var type = $('.form-control[name="tipo_creacion"]').val();
                switch(type){
                    case 'principal':
                        return 'La vida útil debe ser mayor a 12 meses ';
                    break;
                    case 'subnumero':
                        return 'La vida útil debe ser mayor a 1 mes ';
                    break;
                }
            }
        );
        jQuery.validator.addMethod('clase', function(value, element, params){
                var val = value;
                var operation = $('[name="tipo_operacion"]').val();
                var existClass = false;

                // $.each(clases, function(i,e){
                //     if(val == e.clase){
                //         existClass = true;
                //     }
                // })
                switch(operation){
                    case 'directa':
                        $.each(clases, function(i,e){
                            if(val == e.clase){
                                existClass = true;
                            }
                        })
                    break;
                    case 'asociada':
                        $.each(clases, function(i,e){
                            //if(val == e.clase && e.asociadas == '1'){
                            if(val == e.clase){
                                existClass = true;
                            }
                        })
                    break;
                }
                
                if(existClass){
                    return true;
                }
                else{
                    return false;
                }

            }, 
            function(params){
                var operation = $('[name="tipo_operacion"]').val();
                //return 'La clase no se encuentra dentro del catálogo';
                switch(operation){
                    case 'directa':
                        return 'La clase no se encuentra dentro del catálogo';
                    break;
                    case 'asociada':
                        return 'La clase no se encuentra dentro del catálogo para asociadas';
                    break;
                }
            }
        );

        function fieldFormFormat(){
            $('#formatoAltas').validate().destroy();

            var operacion = $('.form-control[name="tipo_operacion"]').val();
            var creacion = $('.form-control[name="tipo_creacion"]').val();

            $('#formatoAltas').validate({
                debug: true,
                rules: {
                    denominacion: {
                        required: true
                    },
                    denominacion_complementaria: {
                        required: true,
                        regex:  "^[a-zA-Z0-9#-. ]*$",
                    },
                    denominacion2: {
                        minlength: 2,
                        maxlength: 50,
                        regex:  "^[a-zA-Z0-9_ ]*$",
                        required: true
                    },
                    clase: {
                        required: true,
                        clase: true
                    },
                    inventario: {
                        required: true,
                        maxlength: 7,
                        regex: "^[a-zA-Z0-9]*$",
                        inventory: 1
                    },
                    fabricante: {
                        maxlength: 30,
                        required: true
                    },
                    modelo: {
                        regex: "^[a-zA-Z0-9ñÑ +-_]*$"
                    },
                    serie: {
                        maxlength: 25,
                        regex: "^[a-zA-Z0-9ñÑ +-_]*$"
                    },
                    tag: {
                        maxlength: 25,
                        regex: "^[a-zA-Z0-9ñÑ +-_]*$"
                    },
                    vida_util: {
                        required: true,
                        maxlength: 3,
                        number: true,
                        vidaUtil: true
                    },
                    unidad_medida: {
                        required: true
                    },
                    capacidad: {
                        required: true,
                        maxlength: 18,
                        regex: "^[0-9]+([\,\.][0-9]+)?$"
                    },
                    departamento: {
                        required:true
                    },
                    municipio: {
                        required:true
                    },
                    ubicacion_geografica: {
                        required:true
                    },
                    unidad_funcional: {
                        required:true
                    },
                    costo: {
                        required:true
                    },
                    ceco:{
                        required: true,
                        maxlength: 6,
                        regex: "^[A-Za-z]{2}[0-9]{4}$"
                    },
                    campo_planta: {
                        regex:  "^[a-zA-Z0-9À-ÿ ]*$"
                    },
                    activo_traslado: {
                        required: true
                    },
                    sn_traslado: {
                        required: true
                    },
                    fecha_traslado:{
                        required: true
                    },
                    valor_traslado:{
                        required: true
                    },
                    motivo_traslado:{
                        required: true
                    },
                    matricula:{
                        required: true,
                        maxlength: 7,
                        regex: "^[a-zA-Z0-9]*$"
                    },
                    anio:{
                        required: true,
                        number: true,
                        maxlength: 4,
                        minlength: 4,
                        max: (new Date()).getFullYear() + 1,
                        min: 1980
                    },
                },
                messages: {
                    denominacion: {
                        required: "Este campo es requerido"
                    },
                    denominacion_complementaria: {
                        required: "Este campo es requerido"
                    },
                    denominacion2: {
                        minlength: "La denominación 2 es muy corta",
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    clase: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    inventario: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    fabricante: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    modelo: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    serie: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    tag: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    vida_util: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido",
                        min: "La vida útil mínima debe ser 12 meses"
                    },
                    unidad_medida: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    capacidad: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    departamento: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    municipio: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    ubicacion_geografica:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    unidad_funcional: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    costo:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido",
                        min: "El valor no debe ser menor a "+salary
                    },
                    ceco:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    campo_planta:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    activo_traslado: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    sn_traslado: {
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    fecha_traslado:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    valor_traslado:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    motivo_traslado:{
                        maxlength: "Excede el límite de caracteres",
                        required: "Este campo es requerido"
                    },
                    matricula: {
                        required: "Este campo es requerido para vehículos",
                        maxlength: "Máximo 7 caracteres Alfanuméricos",
                        regex: "El campo contiene caracteres no permitidos, se admiten números y letras"
                    },
                    anio: {
                        required: "Este campo es requerido para vehículos",
                        number: "Este campo solamente acepta números",
                        maxlength: "Máximo 4 caracteres de tipo numérico",
                        minlength: "Mínimo 4 caracteres de tipo numérico",
                        max: "El modelo no puede ser superior a 2019",
                        min: "El modelo no puede ser menor a 1980"
                    }
                }
            });

            if(creacion == 'principal'){
                // inventario para operacion asociada
                if(operacion == 'asociada'){
                    $('.form-control[name="inventario"]').rules('remove', 'regex maxlength');
                    var regex = '^[a-zA-Z0-9\\/\\- _]*$';
                    $('.form-control[name="inventario"]').rules('add', {'regex': regex, 'maxlength': 25});
                }
            }
            if(creacion == 'subnumero'){
                $('.form-control[name="unidad_funcional"]').rules('remove','required');
            }
        }
        fieldFormFormat();
    // cambiar vista
        function switchCreacion(type, operation){
            switch(type){
                case 'principal':
                // cambio de vista
                    $('.row-creacion .col-md-6').removeClass('active');
                    $('.row-creacion .form-control[name="busqueda_denominacion"]')
                        .parents('.col-md-6')
                        .fadeIn(400, function(){
                            $(this).removeAttr('style').addClass('active');
                        })
                // cambio de denominacion
                    var html = '<input type="text" name="denominacion" class="form-control" placeholder="Denominación..." readonly>';
                    $('.form-control[name="denominacion"]').replaceWith(html);
                // deshabilitar o habilitar campos
                    $('.form-control[name="inventario"]').removeAttr('readonly disabled');
                    $('.form-control[name="tag"]').removeAttr('readonly disabled');
                    $('.form-control[name="departamento"]').removeAttr('readonly disabled');
                    $('.form-control[name="municipio"]').removeAttr('readonly disabled');
                    $('.form-control[name="ubicacion_geografica"]').removeAttr('readonly disabled');
                    $('.form-control[name="unidad_funcional"]').removeAttr('readonly disabled');
                    $('.form-control[name="ceco"]').removeAttr('readonly disabled');
                    $('.form-control[name="campo_planta"]').removeAttr('readonly disabled');
                    // $('.form-control[name="vida_util"]').removeAttr('readonly');
                    $('.form-control[name="tipo_operacion"]').removeAttr('readonly disabled');
                // directa
                    if(operation == 'directa'){
                        // cambio unidad de medida
                            var html = '<input type="text" name="unidad_medida" class="form-control" readonly>';
                            $('.form-control[name="unidad_medida"]').replaceWith(html);
                        // cambio clase
                            var html = '<input type="text" name="clase" class="form-control" readonly>';
                            $('.form-control[name="clase"]').replaceWith(html);
                        // deshabilitar o habilitar campos
                            $('.form-control[name="unidad_medida"]').attr('readonly',true);
                            $('.form-control[name="denominacion"]').attr('readonly',true);
                    }
                // asociada
                    if(operation == 'asociada'){
                        // cambio unidad de medida
                            var html = '<select name="unidad_medida" class="form-control"><option value="">Seleccione...</option>'+unidadesAsociadas;
                            $('.form-control[name="unidad_medida"]').replaceWith(html);
                        // cambio clase
                            var html = '<select name="clase" class="form-control"><option value="">Seleccione...</option>'+clasesAsociadas;
                            $('.form-control[name="clase"]').replaceWith(html);
                        // deshabilitar o habilitar campos
                            $('.form-control[name="unidad_medida"]').removeAttr('disabled readonly');
                            $('.form-control[name="capacidad"]').removeAttr('disabled readonly');
                            $('.form-control[name="denominacion"]').removeAttr('disabled readonly');
                    }
                // agregar opciones a departamento
                    $('.form-control[name="departamento"]').html(departamentosHTML);
                break;
                case 'subnumero':
                // cambio de vista
                    $('.block-complement').removeClass('show');
                    $('.row-creacion .col-md-6').removeClass('active');
                    $('.row-creacion .form-control[name="id_numero_activo"]').parents('.col-md-6').fadeIn(400, function(){
                        $(this).removeAttr('style').addClass('active');
                    })
                // cambio de denominacion
                    var options = [
                        'Seleccione...',
                        'MAYOR VALOR SUBREPARTO',
                        'MAYOR VALOR COSTOS FINANCIEROS',
                        'MAYOR VALOR COSTO DE ADQUISICION',
                        'MAYOR VALOR SERVICIO DE PERFORACION',
                        'MAYOR VALOR SERVICIO DE WORKOVER',
                        'MAYOR VALOR OBRAS CIVÍLES',
                        'ADICION + ACCESORIOS',
                        'ADICION + CAMPO',
                        'ADICION + GESTION CONTRATO',
                        'ADICION + GESTORIA + TIPO DE GESTORIA (TECNICA, INTERVENTORIA, ETC)',
                        'ADICION + AMPLIACION',
                        'ADICION + EJECUCION CONTRATO',
                        'ADICION + MATERIALES',
                        'ADICION + LINEA DE FLUJO (EL VALOR DE MAS SE DEBE SUMAR A LA CAPACIDAD DEL ACTIVO PRINCIPAL)',
                        'AUMENTO DE LA VIDA ÚTIL',
                        'AMPLIACIÓN DE LA PRODUCTIVIDAD',
                        'REDUCCIÓN SIGNIFICATICA DE COSTOS DE OPERACIÓN',
                        'REEMPLAZO DE PARTE O COMPONENTE',
                    ];
                    var html = '<select name="denominacion" class="form-control">';
                    for(var i = 0; i < options.length; i++){
                        if(options[i] != 'Seleccione...'){
                            html += '<option value="'+options[i]+'">'+options[i]+'</option>'
                        }
                        else{
                            html += '<option value="">'+options[i]+'</option>'
                        }
                    }
                    html += '</select>';
                    $('.form-control[name="denominacion"]').replaceWith(html);
                // cambio unidad de medida
                    var html = '<select name="unidad_medida" class="form-control"><option value="">Seleccione...</option><option value="UN">Unidad (UN)</option></select>';
                    $('.form-control[name="unidad_medida"]').replaceWith(html);
                // cambio de clase
                    if($('[name="traslado"]').is(':checked')){
                        var html = '<input type="text" name="clase" class="form-control">';
                    }
                    else{
                        var html = '<input type="text" name="clase" class="form-control" readonly>';
                    }
                    $('.form-control[name="clase"]').replaceWith(html);
                // si es traslado asignar unidades de medida
                    if($('[name="traslado"]').is(':checked')){
                        if(operation == 'directa'){
                            var html = '<select name="unidad_medida" class="form-control"><option value="">Seleccione...</option>'+unidadesCatalogo;
                            $('.form-control[name="unidad_medida"]').replaceWith(html);
                        }
                        if(operation == 'asociada'){
                            var html = '<select name="unidad_medida" class="form-control"><option value="">Seleccione...</option>'+unidadesAsociadas;
                            $('.form-control[name="unidad_medida"]').replaceWith(html);
                        }
                    }
                // deshabilitar o habilitar campos
                    if(!$('[name="traslado"]').is(':checked')){
                        $('.form-control[name="unidad_medida"]').removeAttr('disabled readonly');
                        $('.form-control[name="capacidad"]').removeAttr('disabled readonly');
                        $('.form-control[name="inventario"]').attr('disabled', true);
                        $('.form-control[name="tag"]').attr('disabled', true);
                        $('.form-control[name="departamento"]').attr('readonly', true);
                        $('.form-control[name="municipio"]').attr('readonly', true);
                        $('.form-control[name="ubicacion_geografica"]').attr('readonly', true);
                        $('.form-control[name="unidad_funcional"]').attr('readonly', true);
                        $('.form-control[name="ceco"]').attr('disabled', true);
                        $('.form-control[name="campo_planta"]').attr('disabled', true);
                        // $('.form-control[name="vida_util"]').attr('readonly', true);
                        $('.form-control[name="clase"]').attr('disabled', true);
                        $('.form-control[name="tipo_operacion"]').attr('readonly', true);
                    }
                break;
            }
            fieldFormFormat();
        }
    // modalmessage
        function modalMessage(json){
            $('.modalMessage').remove();
            clearTimeout(timeMessage);

            var html = '<div class="modalMessage '+json.type+'"><h3>'+json.message+'</h3><span class="ion ion-android-cancel close-message"></span></div>';
            $('body').append(html);
            var timeMessage = setTimeout(function(){
                $('.modalMessage').fadeOut(400, function(){
                    $(this).remove();
                })
            }, 50000);
        }
    // limpiar campos
        function clearFields(){
            $('.row-campo,.block-complement').removeClass('show');

            $('.row').each(function(i,e){
                if(!$(e).is('.fields-group') && !$(e).is('.row-creacion')){
                    $(e).find('.form-control').val('');
                }
            });

            var html = '<option value="">Seleccione...</option>';
            $('.form-control[name="municipio"]').html(html);
            $('.form-control[name="ubicacion_geografica"]').html(html);
            $('.form-control[name="sn_traslado"]').html(html);

            $('.form-control[name="denominacion2"]').attr('placeholder', 'Denominación complementaria');
            $('.form-control[name="campo_planta"]').removeAttr('data-id');
            $('.form-control[name="vida_util"]').removeAttr('data-limit');

            if($('.form-control[name="tipo_creacion"]').val() == 'principal'){
                $('.form-control[name="id_numero_activo"]').val('');
            }

            showAditionalFieldsByClass($('.form-control[name="clase"]'))
        }
    // limpiar tabla
        function clearTable(){
            $('.table-data tbody tr').remove();
            $('.inline-table').removeAttr('style');
            $('.multiple-view').removeAttr('style');
            $('.single-view').show();
        }
    // autocomplete
        function autocomplete(val, json, searchFields, type){
            var regExp = new RegExp('^'+val, 'i');
            var arr = [];

            switch(type){
                case 'denominacion':
                    for(var j = 0; j < json.length; j++){
                        for(var f = 0; f < searchFields.length; f++){
                            var fieldValue = String(json[j][searchFields[f]]).trim();
                            if( regExp.test(fieldValue) ){
                                var tempJSON = {
                                    'name': fieldValue,
                                    'clase': json[j]['clase']
                                }
                                arr.push(tempJSON);
                            }
                        }
                    }
                break;
                case 'fabricante':
                    for(var j = 0; j < json.length; j++){
                        for(var f = 0; f < searchFields.length; f++){
                            var fieldValue = String(json[j][searchFields[f]]).trim();
                            if( regExp.test(fieldValue) ){
                                var tempJSON = {
                                    'name': fieldValue
                                }
                                arr.push(tempJSON);
                            }
                        }
                    }
                break;
                case 'ceco':
                    for(var j = 0; j < json.length; j++){
                        for(var f = 0; f < searchFields.length; f++){
                            var fieldValue = String(json[j][searchFields[f]]).trim();
                            if( regExp.test(fieldValue) ){
                                var tempJSON = {
                                    'name': fieldValue,
                                    'ceco_name': json[j]['nombre']
                                }
                                arr.push(tempJSON);
                            }
                        }
                    }
                break;
                case 'campos':
                    for(var j = 0; j < json.length; j++){
                        for(var f = 0; f < searchFields.length; f++){
                            var fieldValue = String(json[j][searchFields[f]]).trim();
                            if( regExp.test(fieldValue) ){
                                var tempJSON = {
                                    'name': fieldValue,
                                    'id': json[j]['codigo']
                                }
                                arr.push(tempJSON);
                            }
                        }
                    }
                break;
            }

            return arr;

        }
    // autocomplete denominacion selector
        function autocompleteDenominacionSelector(val, json){
            clearFields();
            $('.block-complement').removeClass('show');
            $('.form-control[name="capacidad"]').removeAttr('readonly');

            var element = 0;
            for(var i = 0; i< json.length; i++){
                if(val.name == json[i].label && val.class == json[i].clase){
                    element = json[i];
                }
            }

            $('.autocomplete').val('');
            var regex = /\+/g;
            if(regex.test(element.label)){
                var label = String(element.label).trim().split(' + ');
                $('.block-complement').addClass('show');
                $('.form-control[name="denominacion"]').val(label[0]);
                $('.form-control[name="denominacion_complementaria"]').attr('placeholder',label[1]);
                $('.form-control[name="clase"]').val(element.clase).change();
                $('.form-control[name="unidad_medida"]').val(element.unidad);
            }
            else{
                $('.form-control[name="denominacion"]').val(element.label);
                $('.form-control[name="clase"]').val(element.clase).change();
                $('.form-control[name="unidad_medida"]').val(element.unidad);
            }

            var observaciones = String(element.observaciones).trim().split('\n');
            var regex = /DESCRIPCI+[O|Ó]+N ADICIONAL:/gi;

            var denominacion2Placeholder = 'Denominación complementaria'
            for(var i = 0; i< observaciones.length; i++){
                if(regex.test(observaciones[i])){
                    denominacion2Placeholder = observaciones[i].replace(regex,'').trim();
                }
            }
            $('.form-control[name="denominacion2"]').attr('placeholder', denominacion2Placeholder);

            if(element.unidad == 'UN'){
                $('.form-control[name="capacidad"]').val(1).attr('readonly',true);
            }
        }
    // mostrar campos adicionales
        function validateAditionalFields(element){
            var name = $(element).attr('name');

            switch(name){
                case 'ceco':
                    // var val = $(element).val().trim();
                    // var regex = /^PR/i;

                    // if(regex.test(val)){
                    //     $('.row-campo').fadeIn(400, function(){
                    //         $(this).addClass('show').removeAttr('style');
                    //     });
                    // }
                    // else{
                    //     $('.row-campo').removeClass('show');
                    // }
                break;
            }
        }
    // mostrar campos adicionales por clase
        function showAditionalFieldsByClass(element){
            var classCode = $(element).val();
            // agregar validaciones adicionales

                // para 11001, 11002, 33006 - terrenos debe tener al final _
                // if( classCode == 11001 || classCode == 11002 || classCode == 33006 )
                // {
                //     $('.form-control[name="inventario"]').rules('remove', 'regex maxlength');
                //     var regex = '^[a-zA-Z0-9\/_]*$';
                //     $('.form-control[name="inventario"]').rules('add', {'regex': regex, 'maxlength': 25});
                // }
                // para clase 13001 recibe n letras y números sin espacio
                if( classCode == 13001 )
                {
                    $('.form-control[name="inventario"]').rules('remove', 'regex maxlength');
                    var regex = '^[a-zA-Z0-9_]*$';
                    $('.form-control[name="inventario"]').rules('add', {'regex': regex, 'maxlength': 25});
                }
                // 33001 - 33005 n longitud de digitos numericos con _ al final // patentes y licencias
                else if( classCode == 33001 || classCode == 33002 || classCode == 33003 || classCode == 33004 || classCode == 33005 )
                {
                    $('.form-control[name="inventario"]').rules('remove', 'regex maxlength required');
                    // var regex = '^[a-zA-Z0-9 _]*$';
                    // $('.form-control[name="inventario"]').rules('add', {'regex': regex, 'maxlength': 25});
                }
            // reset fields
                $('.row-vehiculos').removeClass('show');

                $('.form-control[name="inventario"]').removeAttr('data-type').prev('label').text('No INVENTARIO:');

                $('.form-control[name="tag"]').removeAttr('data-type').prev('label').text('TAG:');

                $('.form-control[name="fabricante"]').removeAttr('data-type').addClass('autocomplete').prev('label').text('FABRICANTE:');

                $('.form-control[name="modelo"]').removeAttr('data-type').prev('label').text('MODELO:');

            var clasesInmuebles = ['33006', '11001', '11002', '28001', '28002', '15001', '15002', '15003', '15004', '15005', '15009', '15015'];
            var clasesVehiculos = ['23001'];
            var clasesVehiculosMaritimos = ['23002'];

            if( clasesInmuebles.indexOf(classCode) != -1 ){

                $('.form-control[name="inventario"]')
                    .attr('data-type', 'inmueble')
                    .prev('label')
                    .text('No. MATRÍCULA INMOBILIARIA:');

                $('.form-control[name="inventario"]').rules('remove', 'regex maxlength');
                var regex = '^[a-zA-Z0-9\\/\\- _]*$';
                $('.form-control[name="inventario"]').rules('add', {'regex': regex, 'maxlength': 25});

                $('.form-control[name="tag"]')
                    .attr('data-type', 'inmueble')
                    .prev('label')
                    .text('CÓDIGO SIG:');

                $('.form-control[name="fabricante"]')
                    .attr('data-type', 'inmueble')
                    .removeClass('autocomplete')
                    .prev('label')
                    .text('No. ESCRITURA:');

                $('.form-control[name="modelo"]')
                    .attr('data-type', 'inmueble')
                    .prev('label')
                    .text('NOTARIA DE REGISTRO:');
            }
            if( clasesVehiculos.indexOf(classCode) != -1){
                $('.row-vehiculos').fadeIn(400, function(){
                    $(this).addClass('show').removeAttr('style');
                });
            }
            if( clasesVehiculosMaritimos.indexOf(classCode) != -1){

            }
        }
    // agregar valores de los campos a la tabla
        function genericTable(data){
            // console.log(data);
            var continueBool = false;
            var row = 0;
            var itemsPositions = [
                'operacion',
                'creacion',
                'activo',
                'sn',
                'codClase',
                'denominacion',
                'denominacion2',
                'inventario',
                'fabricante',
                'modelo',
                'capacidad',
                'unidadMedida',
                'serie',
                'tag',
                'codCustodio',
                'nombreCustodio',
                'codMunicipio',
                'nombreMunicipio',
                'codUbicacion',
                'nombreUbicacion',
                'codCeco',
                'vicepresidencia',
                'gerencia',
                'vidaUtil',
                'costoActivo',
                'codCampoPlanta',
                'nombreCampoPlanta',
                'matriculaVehiculos',
                'anioModeloVehiculo',
                'activoOrigen',
                'snOrigen',
                'fechaCapitalizacion',
                'valorAdquisicion',
                'motivoTraslado',
                'observaciones'
            ]

            $.each($('.table-data').find('tbody tr'), function(i,e){
                if(
                    String(data.inventario).toUpperCase() == $(e).find('td:eq("9")').text() &&
                    String(data.activoOrigen).toUpperCase() == $(e).find('td:eq("31")').text() &&
                    String(data.snOrigen).toUpperCase() == $(e).find('td:eq("32")').text()
                ){
                    continueBool = true;
                    row = i;
                }
            });

            if(continueBool){
                $.each( $('.table-data').find('tbody tr:eq('+row+') td'), function(i,e){
                    if( i > 1){
                        if( $(e).text() != data[itemsPositions[i-2]] ){
                            $(e).text( String(data[itemsPositions[i-2]]).toUpperCase() );
                        }
                    }
                });
            }
            else{
                var itemsAppend = '';

                for(var i = 0; i < (itemsPositions.length+2); i++){
                    itemsAppend += '<td>';
                    if(i == 0){
                        itemsAppend += '<span class="ion ion-trash-b remove-row"></span>';
                    }
                    else if(i == 1){
                        itemsAppend += $('.table-data tbody tr').length+1;
                    }
                    else{
                        itemsAppend += String(data[itemsPositions[i-2]]).toUpperCase();
                    }
                    itemsAppend += '</td>';
                }

                $('.inline-table').find('.table tbody').prepend( $('<tr class="animated fadeIn">').append( itemsAppend ));
            }

            // limpiar campos cuando se agrega la linea
                clearFields();
                $('.form-control[name="id_numero_activo"]').val('');

        }

        function addToTable(){
            var cecoJSON = '';

            // verificar si la información se puede agregar
                var positionValidation = {
                    inventario:     9,
                    activoOrigen:   31,
                    snOrigen:       32,
                    costo:          26,
                    valorTraslado:  34
                }

            // verifica si no es traslado y se agrega otro activo por el mismo inventario
                var state = false;
                $.each($('.table-data tbody tr'), function(i,e){
                    // console.log($(e).find('td:eq('+positionValidation.inventario+')').text());
                    if(
                        $(e).find('td:eq('+positionValidation.inventario+')').text() == $('.form-control[name="inventario"]').val() &&
                        (
                            $('.form-control[name="activo_traslado"]').val().trim() == "" ||
                            $('.form-control[name="sn_traslado"]').val().trim() == "" ||
                            $('.form-control[name="fecha_traslado"]').val().trim() == "" ||
                            $('.form-control[name="valor_traslado"]').val().trim() == "" ||
                            $('.form-control[name="motivo_traslado"]').val().trim() == ""
                        )
                    ){
                        state = true;
                    }
                });

                if(state){
                    modalMessage({message: "El número de inventario ya se encuentra asignado a otro activo."});
                    return false;
                }
            // verifica si el valor de traslado es mayor que el valor del costoActivo
               if($('input[name="traslado"]').is(':checked')){
                    var cost = Number($('.form-control[name="costo"]').unmask());
                    var translate = Number($('.form-control[name="valor_traslado"]').unmask());
                    if(translate > cost){
                        modalMessage({message: "El valor del traslado no debe ser mayor al valor del costo."});
                        return false;
                    }
               }
            // verifica si los valores de traslado son acordes al costo del numero de inventario
               if($('input[name="traslado"]').is(':checked')){
                    var translatesActives = {};
                    $.each($('.table-data tbody tr'), function(i,e){
                        var translateValue = Number($(e).find('td:eq('+positionValidation.valorTraslado+')').text().replace(/[$\.]/g,''));
                        if( translateValue > 0){
                            var inventario = $(e).find('td:eq('+positionValidation.inventario+')').text().trim();
                            var costo = $(e).find('td:eq('+positionValidation.costo+')').text().replace(/[$\.]/g,'');
                            var traslado = $(e).find('td:eq('+positionValidation.valorTraslado+')').text().replace(/[$\.]/g,'');

                            var json = {
                                costo: Number(costo),
                                valorTraslado: Number(traslado)
                            }

                            translatesActives[inventario] == undefined ? translatesActives[inventario] = [] : '';
                            translatesActives[inventario].push(json);
                        }
                    });
                    
                    var countTranslate = Number($('.form-control[name="valor_traslado"]').unmask());

                    $.each(translatesActives[$('.form-control[name="inventario"]').val().trim()], function(i,e){
                        countTranslate += e.valorTraslado;
                    });

                    var cost = Number($('.form-control[name="costo"]').unmask());
                    if(countTranslate > cost){
                        modalMessage({message: "La suma de los traslados no debe superar el valor del costo ("+cost+") del nuevo activo."});
                        return false;
                    }
               }

            $.ajax({
                url: 'include/buscarPorCeco.php',
                type: 'GET',
                data: {
                    get_option: $('.form-control[name="ceco"]').val()
                },
                dataType: 'JSON',
                async: false,
                success: function(data){
                    cecoJSON = data;
                }
            });
            var json = {
                operacion:          $('.form-control[name="tipo_operacion"]').val(),
                creacion:           $('.form-control[name="tipo_creacion"]').val(),
                activo:             $('.form-control[name="id_numero_activo"]').val(),
                sn:                 '',
                codClase:           $('.form-control[name="clase"]').val(),
                denominacion:       $('.form-control[name="denominacion"]').val() +' '+ $('.form-control[name="denominacion_complementaria"]').val(),
                denominacion2:      $('.form-control[name="denominacion2"]').val(),
                inventario:         $('.form-control[name="inventario"]').val() == "" ? "NO APLICA" : $('.form-control[name="inventario"]').val(),
                fabricante:         $('.form-control[name="fabricante"]').val(),
                modelo:             $('.form-control[name="modelo"]').val().trim() == "" ? "NO REGISTRA" : $('.form-control[name="modelo"]').val(),
                capacidad:          $('.form-control[name="capacidad"]').val(),
                unidadMedida:       $('.form-control[name="unidad_medida"]').val(),
                serie:              $('.form-control[name="serie"]').val().trim() == "" ? "NO REGISTRA" : $('.form-control[name="serie"]').val(),
                tag:                $('.form-control[name="tag"]').val().trim() == "" ? "NO REGISTRA" : $('.form-control[name="tag"]').val(),
                codCustodio:        '',
                nombreCustodio:     '',
                codMunicipio:       $('.form-control[name="municipio"]').val(),
                nombreMunicipio:    $('.form-control[name="municipio"]').find('option:selected').text(),
                codUbicacion:       $('.form-control[name="ubicacion_geografica"]').val(),
                nombreUbicacion:    $('.form-control[name="ubicacion_geografica"]').find('option:selected').text(),
                codCeco:            cecoJSON.ceco,
                vicepresidencia:    cecoJSON.vicepresidencia,
                gerencia:           cecoJSON.gerencia,
                vidaUtil:           $('.form-control[name="vida_util"]').val(),
                costoActivo:        $('.form-control[name="costo"]').val(),
                codCampoPlanta:     $('.form-control[name="campo_planta"]').data('id'),
                nombreCampoPlanta:  $('.form-control[name="campo_planta"]').val(),
                matriculaVehiculos: $('.form-control[name="matricula"]').val(),
                anioModeloVehiculo: $('.form-control[name="anio"]').val(),
                activoOrigen:       $('.form-control[name="activo_traslado"]').val(),
                snOrigen:           $('.form-control[name="sn_traslado"]').val(),
                fechaCapitalizacion:$('.form-control[name="fecha_traslado"]').val(),
                valorAdquisicion:   $('.form-control[name="valor_traslado"]').val(),
                motivoTraslado:     $('.form-control[name="motivo_traslado"]').val(),
                observaciones:      $('.form-control[name="observaciones"]').val()
            }

            genericTable(json);

            $('.inline-table').fadeIn(400);
            $('.ctrls-table').fadeIn(400);
        }

    $(document).on('ready',function(){
    // tooltip
        $('[data-toggle="tooltip"]').tooltip({
            trigger: "hover"
        });
    // seleccion de tipo de operación - reación
        $('select[name="tipo_operacion"], select[name="tipo_creacion"]').change(function(){
            clearFields();
            var creation = $('select[name="tipo_creacion"]').val();
            var operation = $('select[name="tipo_operacion"]').val();
            switchCreacion(creation, operation);
        });
    // change all fields in uppercase
        $(document).on('keyup', '.form-control', function(e){
            $(this).val($(this).val().toUpperCase());
        });
    // autocomplete
        $(document).on('keyup', '.autocomplete', function(e){
            $(this).parent().addClass('autocomplete-wrapper');
            $(this).siblings('.error').remove();
            switch(e.keyCode){
                case 13:
                    if($(this).siblings('.autocomplete-box').find('.selector.hover').length){
                        var $focus = $(this).siblings('.autocomplete-box').find('.selector.hover');
                        $(this).siblings('.autocomplete-box').remove();
                        $(this).val( $focus.text() );
                        $(this).blur();
                        $(this).siblings('.error').remove();

                        var nameField = $(this).attr('name');

                        if(nameField == 'busqueda_denominacion'){
                            var clase = $focus.data('clase');
                            var text = $focus.text();

                            autocompleteDenominacionSelector({'name': text, 'class': clase}, catalogoJSON);
                        }
                        if(nameField == 'ceco'){
                            $(this).val( $focus.data('ceco') );
                        }
                        if(nameField == 'campo_planta'){
                            $(this).attr('data-id', $focus.data('id'));
                        }
                    }
                break;
                case 38:
                    var $focus = $(this).next('.autocomplete-box').find('.selector.hover');
                    var exist = $(this).siblings('.autocomplete-box').length;
                    var nameField = $(this).attr('name');

                    if($focus.length && exist){
                        if($focus.prev().length){
                            $focus.removeClass('hover');
                            $focus.prev().addClass('hover');
                            if(nameField == 'ceco'){
                                $(this).val($focus.prev().data('ceco'));
                            }
                            else if(nameField == 'campo_planta'){
                                $(this).val($focus.prev().text());
                                $(this).attr('data-id', $focus.prev().data('id'));
                            }
                            else{
                                $(this).val($focus.prev().text());
                            }
                        }
                    }
                    else if(exist){
                        var $focus = $(this).next('.autocomplete-box').find('.selector:eq(0)');
                        $focus.addClass('hover');
                        if(nameField == 'ceco'){
                            $(this).val($focus.data('ceco'));
                        }
                        else if(nameField == 'campo_planta'){
                            $(this).val($focus.text());
                            $(this).attr('data-id', $focus.data('id'));
                        }
                        else{
                            $(this).val($focus.text());
                        }
                    }

                    var offTop = $(this).next('.autocomplete-box').find('.selector.hover').offset().top;
                    var parentOffTop = $(this).next('.autocomplete-box').offset().top;
                    var scroll = $(this).next('.autocomplete-box').scrollTop();
                    $(this).next('.autocomplete-box').scrollTop( offTop-parentOffTop+scroll );
                break;
                case 40:
                    var $focus = $(this).next('.autocomplete-box').find('.selector.hover');
                    var exist = $(this).siblings('.autocomplete-box').length;
                    var nameField = $(this).attr('name');

                    if($focus.length && exist){
                        if($focus.next().length){
                            $focus.removeClass('hover');
                            $focus.next().addClass('hover');
                            if(nameField == 'ceco'){
                                $(this).val($focus.next().data('ceco'));
                            }
                            else if(nameField == 'campo_planta'){
                                $(this).val($focus.next().text());
                                $(this).attr('data-id', $focus.next().data('id'));
                            }
                            else{
                                $(this).val($focus.next().text());
                            }
                        }
                    }
                    else if(exist){
                        var $focus = $(this).next('.autocomplete-box').find('.selector:eq(0)');
                        $focus.addClass('hover');
                        if(nameField == 'ceco'){
                            $(this).val($focus.data('ceco'));
                        }
                        else if(nameField == 'campo_planta'){
                            $(this).val($focus.text());
                            $(this).attr('data-id', $focus.data('id'));
                        }
                        else{
                            $(this).val($focus.text());
                        }
                    }
                    var offTop = $(this).next('.autocomplete-box').find('.selector.hover').offset().top;
                    var parentOffTop = $(this).next('.autocomplete-box').offset().top;
                    var scroll = $(this).next('.autocomplete-box').scrollTop();
                    $(this).next('.autocomplete-box').scrollTop( offTop-parentOffTop+scroll );
                break;
                default:
                    if($(this).val().trim().length > 0){
                        $(this).siblings('.autocomplete-box').remove();

                        var value = $(this).val();
                        var nameField = $(this).attr('name');
                        var searched = [];

                        if(nameField == 'busqueda_denominacion'){
                            searched = autocomplete(value, catalogoJSON, ['label'], 'denominacion');
                        }
                        if(nameField == 'fabricante'){
                            searched = autocomplete(value, fabricantesJSON, ['fabricante'], 'fabricante');
                        }
                        if(nameField == 'ceco'){
                            var regex = /^[^p]/i;

                            if(regex.test(value)){
                                searched = autocomplete(value, cecosJSON, ['ceco'], 'ceco');
                            }
                            else if(value.length > 2){
                                searched = autocomplete(value, cecosJSON, ['ceco'], 'ceco');
                            }
                        }
                        if(nameField == 'campo_planta'){
                            $(this).removeAttr('data-id');
                            searched = autocomplete(value, camposJSON, ['nombre'], 'campos');
                        }


                        var html = '';
                        !$(this).siblings('.autocomplete-box').length ? html += '<div class="autocomplete-box">' : '';
                        for(var i = 0; i < searched.length; i++){
                            if(nameField == 'busqueda_denominacion'){
                                html += '<div class="selector" data-clase="'+searched[i].clase+'">'+searched[i].name+'</div>';
                            }
                            if(nameField == 'fabricante'){
                                html += '<div class="selector">'+searched[i].name+'</div>';
                            }
                            if(nameField == 'ceco'){
                                var searched = autocomplete(value, cecosJSON, ['ceco'], 'ceco');
                                html += '<div class="selector" data-ceco="'+searched[i].name+'">'+searched[i].name+' - '+searched[i].ceco_name+'</div>';
                            }
                            if(nameField == 'campo_planta'){
                                html += '<div class="selector" data-id="'+searched[i].id+'">'+searched[i].name+'</div>';
                            }

                        }
                        !$(this).siblings('.autocomplete-box').length ? html += '</div>' : '';

                        if($(this).siblings('.autocomplete-box').length){
                            $(this).siblings('.autocomplete-box').html(html);
                        }
                        else{
                            $(this).parent().append(html)
                        }
                    }
                    else{
                        $(this).parents('.autocomplete-wrapper').removeClass('autocomplete-wrapper');
                        $(this).parent().find('.autocomplete-box').remove();
                    }
                break;
            }
            //validateAditionalFields($(this));
        });
        $(document).on('click', '.autocomplete-box .selector', function(e){
            e.preventDefault();
            var $field = $(this).parents('.autocomplete-wrapper').find('.form-control');
            var nameField = $field.attr('name');
            var text = $(this).text();

            $field.siblings('.error').remove();

            if(nameField == 'ceco'){
                text = $(this).attr('data-ceco');
            }
            if(nameField == 'campo_planta'){
                $field.attr('data-id', $(this).data('id'));
            }

            $field.val(text);
            $field.change();

            var nameField = $field.attr('name');

            switch(nameField){
                case 'busqueda_denominacion':
                    var clase = $(this).attr('data-clase');

                    autocompleteDenominacionSelector({'name': text, 'class': clase}, catalogoJSON);
                break;
            }

            $(this).parents('.autocomplete-wrapper').removeClass('autocomplete-wrapper');
            $(this).parent().remove();
        });
        $(document).on('click', function(e){
            if($(e.target).closest('.autocomplete-wrapper').length == 0){
                $('.autocomplete-wrapper').find('.autocomplete-box').remove();
                $('.autocomplete-wrapper').removeClass('autocomplete-wrapper');
            }
        });
    // cambio de clase
        $('.form-control[name="clase"]').change(function(){
            showAditionalFieldsByClass($(this));
        });
    // buscar activo
        $('.form-control[name="id_numero_activo"]').change(function(){
            getActivo($(this).val());
        });
        $('.form-control[name="activo_traslado"]').change(function(){
            getActivoTraslado($(this).val());
        });
    // cerrar modalmessage
        $(document).on('click', '.modalMessage',function(e){
            $(this).fadeOut(400, function(){
                $(this).remove();
            })
        });
    // cambio de unidad de medida
        $(document).on('change', 'select[name="unidad_medida"]', function(){
            if($(this).val() == 'UN'){
                $('.form-control[name="capacidad"]').val(1).attr('readonly', true);
            }
            else{
                $('.form-control[name="capacidad"]').val('').removeAttr('readonly');
            }
        });
    // mostrar elementos para traslado
        $('.label-selector [name="traslado"]').change( function(e){
            // clearFields();
            var creacion = $('[name="tipo_creacion"]').val();
            if($(this).is(':checked')){
                $('.row-traslados').stop().fadeIn(400, function(){
                    $(this).addClass('show').removeAttr('style');
                });
                $('[name="tipo_creacion"]').change();

                if(creacion == 'subnumero'){
                    $('.form-control[name="tipo_operacion"]').removeAttr('disabled readonly');
                    $('.form-control[name="clase"]').removeAttr('disabled readonly');
                    $('.form-control[name="inventario"]').removeAttr('disabled readonly');
                    $('.form-control[name="vida_util"]').removeAttr('disabled readonly');
                    $('.form-control[name="tag"]').removeAttr('disabled readonly');
                    $('.form-control[name="departamento"]').removeAttr('disabled readonly');
                    $('.form-control[name="municipio"]').removeAttr('disabled readonly');
                    $('.form-control[name="ubicacion_geografica"]').removeAttr('disabled readonly');
                    $('.form-control[name="unidad_funcional"]').removeAttr('disabled readonly');
                    $('.form-control[name="ceco"]').removeAttr('disabled readonly');
                    $('.form-control[name="campo_planta"]').removeAttr('disabled readonly');
                }
            }
            else{
                $('.row-traslados').stop().fadeOut(400, function(){
                    $(this).removeClass('show').removeAttr('style');
                });

                $('.form-control[name="tipo_traslado"]').val('');
                $('.form-control[name="activo_traslado"]').val('');
                $('.form-control[name="sn_traslado"]').val('');
                $('.form-control[name="fecha_traslado"]').val('');
                $('.form-control[name="valor_traslado"]').val('');
                $('.form-control[name="motivo_traslado"]').val('');

                if(creacion == 'subnumero'){
                    $('.form-control[name="tipo_operacion"]').attr('disabled', true);
                    $('.form-control[name="clase"]').attr('readonly', true);
                    $('.form-control[name="inventario"]').attr('readonly', true);
                    // $('.form-control[name="vida_util"]').attr('readonly', true);
                    $('.form-control[name="tag"]').attr('readonly', true);
                    $('.form-control[name="departamento"]').attr('readonly', true);
                    $('.form-control[name="municipio"]').attr('readonly', true);
                    $('.form-control[name="ubicacion_geografica"]').attr('readonly', true);
                    $('.form-control[name="unidad_funcional"]').attr('readonly', true);
                    $('.form-control[name="ceco"]').attr('readonly', true);
                    $('.form-control[name="campo_planta"]').attr('readonly', true);
                       
                }
            }
            if($('[name="tipo_creacion"]').val() == 'subnumero'){
                clearFields();
            }
        })
    // cambio de subnumero para traslados
        $('.form-control[name="sn_traslado"]').change( function() {
            $('.form-control[name="valor_traslado"]').val('');
            var value = $(this).val();

            if( value != ""){
                $.each(activosTraslados, function(i,e) {
                    if(value == e.sn){
                        $('.form-control[name="fecha_traslado"]').val(e.fecha_capitalizacion);
                        $('.form-control[name="valor_traslado"]').attr('data-limit', e.valor_adquisicion);
                    }
                })
            }
            else{
                $('.form-control[name="fecha_traslado"]').val('');
                $('.form-control[name="valor_traslado"]').val('').removeAttr('data-limit');
            }

        });
    // limitación de valor de traslado
        $('.form-control[name="valor_traslado"]').on('keyup',function() {
            var value = Number($(this).unmask());
            var limit = $(this).attr('data-limit');
            if(value > limit){
                $(this).val(limit);
                $(this).blur();
            }
        });
    // limitación vida útil total
        $('.form-control[name="vida_util"]').on('keyup', function(){
            $(this).val( $(this).val().replace(/[a-zA-Z]/g,'') );
            var limit = $(this).attr('data-limit') != undefined ? $(this).attr('data-limit') : 999;

            var val = Number($(this).val());
            if(val > limit){
                $(this).val(limit);
            }
        })
    // validación campo planta para cecos de producción
        $('.form-control[name="ceco"]').change(function(){
            var regex = /^PR/g;
            var value = $(this).val();
            if(regex.test(value)){
                $('.form-control[name="campo_planta"]').rules('add', {'required': true});
            }
            else{
                $('.form-control[name="campo_planta"]').rules('remove', 'required');
            }
        });
    // validación inventario repetidos
        $('.form-control[name="inventario"]').change(function(e){
            var value = $(this).val();
            var status = $(this).data('type');
            if(status == undefined){
                getInventario(value);
            }
        });
    // agregar a la tabla
        $('.btn-add').on('click',function(){
            var validator = $('#formatoAltas').validate();
            validator.form('#formatoAltas');

            if(validator.numberOfInvalids() == 0){
                addToTable();
            }
        });
    // eliminar row tabla
        $(document).on('click', '.remove-row',function(){
            var rowInd = $(this).parents('tr').index();
            $.each($('.table-data tbody'),function(i,e){
                $(e).find('tr:eq('+rowInd+')').fadeOut(400,function(){
                    $(this).remove();
                    var successCount = $('.table-data tbody tr').length;
                    $('.nav-tabs li[data-type="success"] i').text(Number(successCount));
                });
            });


            setTimeout(function(){
                $.each($('.table-data tbody tr'),function(i,e){
                    $(e).find('td:eq(1)').text( $(e).parents('tbody').find('tr').length - $(e).index() );
                });
            }, 500);

            if($($('.table-data tbody')[0]).find('tr').length -1 <= 0){
                $('.inline-table').fadeOut(400);
            }


        });
    // generar excel
        function createJSON(){

            var arrayJson = [];

            for(var i=$($('.table-data tbody')[0]).find('tr').length - 1; i>= 0; i-- ){
                var focus = $($('.table-data tbody')[0]).find('tr:eq('+i+')');
                var tempJson = {
                    operacion:          $(focus).find('td:eq(2)').text(),
                    creacion:           $(focus).find('td:eq(3)').text(),
                    activo:             $(focus).find('td:eq(4)').text(),
                    sn:                 $(focus).find('td:eq(5)').text(),
                    codClase:           $(focus).find('td:eq(6)').text(),
                    denominacion:       $(focus).find('td:eq(7)').text(),
                    denominacion2:      $(focus).find('td:eq(8)').text(),
                    inventario:         $(focus).find('td:eq(9)').text(),
                    fabricante:         $(focus).find('td:eq(10)').text(),
                    modelo:             $(focus).find('td:eq(11)').text(),
                    capacidad:          $(focus).find('td:eq(12)').text(),
                    unidadMedida:       $(focus).find('td:eq(13)').text(),
                    serie:              $(focus).find('td:eq(14)').text(),
                    tag:                $(focus).find('td:eq(15)').text(),
                    regCustodio:        $(focus).find('td:eq(16)').text(),
                    custodio:           $(focus).find('td:eq(17)').text(),
                    codMunicipio:       $(focus).find('td:eq(18)').text(),
                    municipio:          $(focus).find('td:eq(19)').text(),
                    codUbicacion:       $(focus).find('td:eq(20)').text(),
                    ubicacion:          $(focus).find('td:eq(21)').text(),
                    codCeco:            $(focus).find('td:eq(22)').text(),
                    vicepresidencia:    $(focus).find('td:eq(23)').text(),
                    gerencia:           $(focus).find('td:eq(24)').text(),
                    vidaUtilTotal:      $(focus).find('td:eq(25)').text(),
                    costo:              $(focus).find('td:eq(26)').text(),
                    codCampoPlanta:     $(focus).find('td:eq(27)').text(),
                    campoPlanta:        $(focus).find('td:eq(28)').text(),
                    matriculaVehiculo:  $(focus).find('td:eq(29)').text(),
                    modeloVehiculo:     $(focus).find('td:eq(30)').text(),
                    activoOrigen:       $(focus).find('td:eq(31)').text(),
                    snOrigen:           $(focus).find('td:eq(32)').text(),
                    fechaCapOrigen:     $(focus).find('td:eq(33)').text(),
                    valorTraslado:      $(focus).find('td:eq(34)').text(),
                    motivoTraslado:     $(focus).find('td:eq(35)').text(),
                    observaciones:      $(focus).find('td:eq(36)').text()
                };

                arrayJson.push(tempJson);
            }
            return {
                'DarthVader': JSON.stringify(arrayJson)
            };
        }
        $('.btn-end').on('click',function(e){
            if($('.table-data tbody tr').length){
                var url = 'include/generarXls.php';
                var arrayJson = createJSON();
                // console.log(arrayJson);
                // return false;
                $('#wait').fadeIn(400);
                $.post(url, {jsonExcel: arrayJson.DarthVader }, function(data){
                    var d = JSON.parse(data);

                    var $a = $("<a>");
                    $a.attr("href","include/"+d.route);
                    $("body").append($a);
                    $a[0].setAttribute("download",d.name);
                    $a[0].click();
                    $a.remove();
                    $('#wait').fadeOut(400);
                    $('.tankyou-message').fadeIn(400);
                    clearFields();
                    clearTable();
                    setTimeout(function(){
                        $('.tankyou-message').fadeOut(400);
                    }, 5000);
                });
            }
            else{
                modalMessage({message: 'No existe solicitudes de creaciones'})
            }
        });
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);
