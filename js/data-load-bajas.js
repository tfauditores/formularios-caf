(function($){
    var fileLocation = '';
    /* success */
    function print_success(index, excelRow, positions, data=''){
        var appendBool = false;
        
        $.each($($('.table.table-data')[0]).find('tbody tr'), function(i,e){
            if(
                $(e).find('td:eq(3)').text() == excelRow[positions.activo] &&
                $(e).find('td:eq(4)').text() == excelRow[positions.subnumero] &&
                $(e).find('td:eq(2)').text() == excelRow[positions.inventario]
            ){
                appendBool = true;
            }
        });

        if(appendBool)
            return false;
        
        var successCount = $('.nav-tabs li[data-type="success"] i').text();
        $('.nav-tabs li[data-type="success"] i').text(Number(successCount) + 1);
        $('.ctrls-table').show();

        // add directly into tables
        // console.log(excelRow);
        // console.log(positions);
        var tableHideColumns = [];
        $.each($('.inline-table thead tr:last-child th'), function(i,e){
            if($(e).is('.hide-column')){
                tableHideColumns.push("hide-column");
            }
            else if($(e).is('.always-hide-column')){
                tableHideColumns.push("always-hide-column");
            }
            else{
                tableHideColumns.push("");
            }
        });

        if($('.ctrls-table .ion').is('.ion-android-more-vertical')){
            var styleCell = "display: table-cell;";
        }
        else{
            var styleCell = "";
        }

        $('table.table-data tbody').each(function(i,e){
            var len = $(e).find('tr').length + 1;
            $('.show-list').fadeIn(400).find('sup').text(len);

            $(e).prepend(
                $('<tr class="animated fadeIn">')
                .append( $('<td>', {
                    'html': '<span class="ion ion-trash-b remove-row"></span>',
                    'class': tableHideColumns[0],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': len,
                    'class': tableHideColumns[1],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': excelRow[positions.inventario],
                    'class': tableHideColumns[2],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': excelRow[positions.activo],
                    'data-subnumbers': data.countSubnumbers != 0 ? 1 : 0,
                    'class': tableHideColumns[3],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': excelRow[positions.subnumero],
                    'class': tableHideColumns[4],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[5],
                    'style': styleCell
                }) ) // departamento
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[6],
                    'style': styleCell
                }) ) // municipio
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[7],
                    'style': styleCell
                }) ) // ubicación geográfica
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[8],
                    'style': styleCell
                }) ) // ceco nombre
                .append( $('<td>', {
                    'text': excelRow[positions.ceco],
                    'class': tableHideColumns[9],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[10],
                    'style': styleCell
                }) ) // area
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[11],
                    'style': styleCell
                }) ) // vicepresidencia
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[12],
                    'style': styleCell
                }) ) // gerencia
                .append( $('<td>', {
                    'text': excelRow[positions.denominacion],
                    'class': tableHideColumns[13],
                    'style': styleCell
                }) ) 
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[14],
                    'style': styleCell
                }) ) // marca del fabricante
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[15],
                    'style': styleCell
                }) ) // modelo
                .append( $('<td>', {
                    'text': excelRow[positions.serie],
                    'class': tableHideColumns[16],
                    'style': styleCell
                }) ) // serie
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[17],
                    'style': styleCell
                }) ) // tag
                .append( $('<td>', {
                    'text': excelRow[positions.motivo_retiro],
                    'class': tableHideColumns[18],
                    'style': styleCell,
                    'data-action': 'cambiar_motivo_retiro'
                }) )
                .append( $('<td>', {
                    'text': excelRow[positions.disposicion_final],
                    'class': tableHideColumns[19],
                    'style': styleCell,
                    'data-action': 'cambiar_disposicion_final'
                }) )
                .append( $('<td>', {
                    'text': data.valorAdquisicion,
                    'class': tableHideColumns[20],
                    'style': styleCell
                }).priceFormat({
                    prefix:"$ ",
                    centsSeparator:",",
                    thousandsSeparator:".",
                    clearOnEmpty:!0,
                    centsLimit: 0,
                    allowNegative: false
                }) ) // valor adquisición
                .append( $('<td>', {
                    'text': excelRow[positions.valor_baja],
                    'class': tableHideColumns[21],
                    'style': styleCell
                }).priceFormat({
                    prefix:"$ ",
                    centsSeparator:",",
                    thousandsSeparator:".",
                    clearOnEmpty:!0,
                    centsLimit: 0,
                    allowNegative: false
                }) )
                .append( $('<td>', {
                    'text': excelRow[positions.tipo_baja],
                    'class': tableHideColumns[22],
                    'style': styleCell
                }) )
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[23],
                    'style': styleCell
                }) ) // código de municipio
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[24],
                    'style': styleCell
                }) ) // código de ubicación geográfica
                .append( $('<td>', {
                    'text': '',
                    'class': tableHideColumns[25],
                    'style': styleCell
                }) ) // denominación 2
                .append( $('<td>', {
                    'text': excelRow[positions.observaciones],
                    'class': tableHideColumns[26],
                    'style': styleCell
                }) ) // observaciones
            )
        });

        $('.inline-table').fadeIn(300);
    }
    /* error */
    function print_error(index,err){
        var errorCount = $('.nav-tabs li[data-type="error"] i').text();
        $('.nav-tabs li[data-type="error"] i').text(Number(errorCount) + 1);
        


        var html = '<div class="item" data-index="'+index+'"><h6 class="line">Fila '+index+'<span><i class="ion ion-bug"></i>'+(err.length > 1 ? err.length+' errores': err.length+' error')+'</span></h6><ul>';
        for( i = 0; i < err.length; i++ ){
            // console.log(err[i]);

            var options = '';

            if(err[i][0].match(/%%(.*)%%;/g)){
                options = err[i][0].match(/%%(.*)%%;/g)[0];
                options =  options.match(/[^%%|^%%;]*/g).filter(function(n){ return n != "" }) ;
            }

            var column = '';
            for( j = 1; j < err[i].length; j++ ){
                column += err[i][j]+(j == err[i].length - 1 ? '': ';' );
            }

            if(err.length == 1){
                html += '<li data-column="'+column+'">'+err[0][0].replace(/%%(.*)%%;/g, '')+'</li>';
            }
            else{
                html += '<li data-column="'+column+'">'+err[i][0].replace(/%%(.*)%%;/g, '')+'</li>';
            }

            if(options != '' && options.length){
                html += '<ol>';
                for(var j = 0; j < options.length; j++){
                    html += '<li>'+options[j]+'</li>';
                }
                html += '</ol>';
            }
        }
        html += '</ul></div>';
        $('.error-multiple').append(html);
    }
    /* validate individual Data*/
    function validateIndividual(url, params){
        params['individual'] = true;
        var json = 0;
        $.ajax({
            method: "GET",
            url: url,
            data: params,
            async: false,
            cache: false,
            dataType: 'json'
        })
        .done( function( data ) {
            json = data;
        });
        return json;
    }
    /* validate individual by column*/
    function validateIndividualPro(url, params){
        params['individualPro'] = true;
        var json = 0;
        $.ajax({
            method: "GET",
            url: url,
            data: params,
            async: false,
            cache: false,
            dataType: 'json'
        })
        .done( function( data ) {
            json = data;
        });
        return json;
    }
    /* validate */
    function validate(excelRow, rowLength, line){
        // reset null fields into array
        for(var i=0; i<excelRow.length; i++){
            if(excelRow[i] == null){
                excelRow[i] = '';
            }
        }
        // console.log(excelRow);
        var positions = {
            tipo_baja: 1,
            activo: 2,
            subnumero: 3,
            denominacion: 4,
            inventario: 5,
            serie: 6,
            ceco: 7,
            valor_baja: 8,
            motivo_retiro: 9,
            disposicion_final: 10,
            observaciones: 11
        }
        // retorna si es baja total y subnúmero
        if(
            String(excelRow[positions.tipo_baja]).trim().toLowerCase() == 'baja total' &&
            Number(excelRow[positions.subnumero]) > 0
        ){
            if(line == rowLength){
                $('#wait').fadeOut(400);
            }
            return false;
        }
        // no hace más si no encuentra un valor de activo y subnúmero
        if(excelRow[positions.activo] == '' && excelRow[positions.subnumero] == ''){
            $('#wait').fadeOut(400);
            return false;
        }

        var request = {
            activo: excelRow[positions.activo],
            subnumero: excelRow[positions.subnumero],
            inventario: excelRow[positions.inventario]
        }

        var url = 'include/cargaMasivaBajas.php';
        $.getJSON(url, request, function(query){
            console.log(query[0]);
            // console.log(request);
            
            var error_array = [];

            if(query.result != undefined){
                switch(query.result){
                    case 'invalid active':
                        error_array.push(['El número de activo o número de inventario no existe en el auxiliar', positions.activo, positions.inventario]);
                    break;
                    case 'invalid format':
                        error_array.push(['El formato de la fila es inválido, verificar campos obligatorios']);
                    break;
                    default:
                        error_array.push(['unknow error, please contact with the administrator']);
                }
            }
            else{
                

                // Verificar #activo y #invetario
                if( String(excelRow[positions.activo]).trim() != "" ){
                    if( String(excelRow[positions.activo]).trim() != String(query[0].activo).trim() ){
                        error_array.push(['El número de activo no coincide con el auxiliar', positions.activo]);
                    }
                }
                else{
                    excelRow[positions.activo] = query[0].activo
                }

                if( String(excelRow[positions.inventario]).trim() != "" ){
                    if(String(excelRow[positions.inventario]).trim() != String(query[0].inventario).trim()){
                        error_array.push(['El número de inventario no coincide con el auxiliar', positions.inventario]);
                    }
                }
                else{
                    excelRow[positions.inventario] = query[0].inventario
                }

                if( excelRow[positions.subnumero] == ""){
                    excelRow[positions.subnumero] = query[0].sn
                }
                // if( Number(excelRow[positions.subnumero]) < 0 ){
                //     error_array.push(['El subnúmero no debe ser menor a 0', positions.subnumero]);
                // }

                // denominación
                if( String(excelRow[positions.denominacion]).trim() != query[0].denominacion ){
                    error_array.push(['Ingrese la denominación correcta para este activo.', positions.denominacion]);
                }

                // serie
                if( String(excelRow[positions.serie]).trim() != query[0].serie ){
                    error_array.push(['Ingrese la serie correcta para este activo, la serie del activo es '+query[0], positions.serie]);
                }
                
                // código de ceco
                if( String(excelRow[positions.ceco]).trim() != query[0].ceco ){
                    error_array.push(['Ingrese código de centro de costos correcto para este activo.', positions.ceco]);
                }

                // valor de baja y tipo de baja
                switch(String(excelRow[positions.tipo_baja]) ){
                    case 'BAJA TOTAL':
                        if( Number(excelRow[positions.valor_baja]) != Number(query[0].valorAdquisicion)){
                            error_array.push(['El valor de la baja total es '+Number(query[0].valorAdquisicion), positions.valor_baja]);
                        }
                    break;
                    case 'BAJA PARCIAL':
                        if(Number(excelRow[positions.subnumero]) == 0){

                            if( Number(excelRow[positions.valor_baja]) > Math.round(Number(query[0].valorAdquisicion)*75/100) ){
                                error_array.push(['El valor de la baja no debe superar el 75% del valor de adquisición del activo', positions.valor_baja]);
                            }

                        }
                        else if(Number(excelRow[positions.subnumero]) > 0){

                            if( Number(excelRow[positions.valor_baja]) > Number(query[0].valorAdquisicion)){
                                error_array.push(['El valor de la baja no debe superar el valor de adquisición del activo', positions.valor_baja]);
                            
                            }
                        }

                        if( Number(excelRow[positions.valor_baja]) < 1){
                            error_array.push(['El valor de la baja debe tener un valor coherente, no puede ser menor o igual a 0', positions.valor_baja]);
                        }
                    break;
                    default:
                        error_array.push(['El campo de tipo de baja debe estar diligenciado con BAJA TOTAL o BAJA PARCIAL', positions.tipo_baja]);
                    break;
                }

                // motivo de retiro
                if( String(excelRow[positions.motivo_retiro]).trim() != "" ){
                    var regexArray = [
                        "OBSOLESCENCIA O DESUSO",
                        "DEMOLICIÓN DE CONSTRUCCIÓN",
                        "DESMANTELAMIENTO DE FACILIDADES",
                        "DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO",
                        "PÉRDIDA O HURTO",
                        "PÉRDIDA O HURTO EN PODER DE TERCEROS",
                        "ESCISIÓN",
                        "PERMUTA O CAMBIO",
                        "DACIÓN EN PAGO",
                        "NO SE REQUIERE PARA LA OPERACIÓN"
                    ];

                    var regexBool = false;

                    for(var i = 0; i<regexArray.length; i++){
                        var regex = new RegExp('^\\b'+regexArray[i]+'\\b' , 'g');
                        var value = String(excelRow[positions.motivo_retiro]).trim();

                        regex.test( value ) ? regexBool = true: '';
                    }

                    var options = '';
                    for(var i = 0; i<regexArray.length; i++){
                        options += '%%'+regexArray[i]+'%%;';
                    }

                    !regexBool ? error_array.push(['El motivo de retiro debe coincidir con alguna de las siguientes opciones: '+options, positions.motivo_retiro]) : '';
                }
                else{
                    error_array.push(['Ingrese un motivo de retiro.', positions.motivo_retiro]);
                }

                // disposición final
                if( String(excelRow[positions.disposicion_final]).trim() != "" ){

                    var regexArray = [
                        "CESIÓN SIN COSTO",
                        "VENTA ACTIVO FUNCIONAL",
                        "VENTA POR CHATARRA"
                    ];

                    var regexBool = false;

                    for(var i = 0; i<regexArray.length; i++){
                        var regex = new RegExp('^\\b'+regexArray[i]+'\\b' , 'g');
                        var value = String(excelRow[positions.disposicion_final]).trim();

                        regex.test( value ) ? regexBool = true: '';
                    }

                    var options = '';
                    for(var i = 0; i<regexArray.length; i++){
                        options += '%%'+regexArray[i]+'%%;';
                    }

                    !regexBool ? error_array.push(['El motivo de retiro debe coincidir con alguna de las siguientes opciones: '+options, positions.disposicion_final]) : '';
                }
                else{
                    error_array.push(['Ingrese una disposición final.', positions.disposicion_final]);
                }
                
            }

            if(error_array.length){
                print_error(line, error_array);
            }
            else{
                print_success(line, excelRow , positions, query[0]);
            }

            if(line == rowLength){
                $('#wait').fadeOut(400);
            }
        });
    }
    /* add to table */
    function add2Table(element){
        var variables = {
            activo: $(element).find('td:eq(1)').text(),
            denominacion: $(element).find('td:eq(2)').text(),
            inventario: $(element).find('td:eq(3)').text(),
            serie: $(element).find('td:eq(4)').text(),
            clase: $(element).find('td:eq(5)').text(),
            cod_municipio: $(element).find('td:eq(6)').text(),
            municipio: $(element).find('td:eq(7)').text(),
            cod_ubicacion_geografica: $(element).find('td:eq(8)').text(),
            ubicacion_geografica: $(element).find('td:eq(9)').text(),
            ceco: $(element).find('td:eq(10)').text(),
            cod_campo_planta: $(element).find('td:eq(11)').text(),
            campo_planta: $(element).find('td:eq(12)').text(),
            vida_util_total: $(element).find('td:eq(13)').text(),
            vicepresidencia: $(element).find('td:eq(14)').text(),
            gerencia: $(element).find('td:eq(15)').text(),
            observaciones: $(element).find('td:eq(16)').text()
        }
        $('table.table-data tbody').each(function(i,e){
            $(e).append(
                $('<tr>')
                .append('<td><span class="ion ion-trash-b remove-row"></span></td')
                .append( $('<td>').text( variables.activo ) )
                .append( $('<td>').text( variables.denominacion ) )
                .append( $('<td>').text( variables.inventario ) )
                .append( $('<td>').text( variables.serie ) )
                .append( $('<td>').text( variables.clase ) )
                .append( $('<td>').text( '' ) )
                .append( $('<td>').text( variables.cod_municipio ) )
                .append( $('<td>').text( variables.cod_ubicacion_geografica ) )
                .append( $('<td>').text( variables.vicepresidencia ) )
                .append( $('<td>').text( variables.gerencia ) )
                .append( $('<td>').text( variables.ceco ) )
                .append( $('<td>').text( variables.cod_campo_planta ) )
                .append( $('<td>').text( variables.vida_util_total ) )
                .append( $('<td>').text( variables.observaciones ) )
            )
        });
    }
    $(document).on('ready',function(){
        // nav view selector
            $('.nav-view .btn').on('click',function(e){
                $(this).siblings('.btn').removeClass('active');
                $(this).addClass('active');

                if($(this).is('.btn-single')){
                    $('.multiple-view').stop().hide();
                    $('.single-view').stop().fadeIn(400);
                }
                if($('.inline-table tbody tr').length)
                    $('.inline-table,.show-list').stop().fadeIn(400);
            });
        // excel file
            $('.upload-control').change(function(e){
                if(!$(this)[0].files.length){
                    return false;
                }

                if($('.nav-tabs li[data-type="error"]').is('.active')){
                    $('.inline-table,.show-list').hide();
                }

                var files = $(this)[0].files;
                var formData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    formData.append('excel', files[i]);
                }

                $('#wait').fadeIn(400);
                $('.multiple-view').fadeIn(300);
                $('.nav-tabs li[data-type="error"] i').text('0');
                $('.nav-tabs li[data-type="success"] i').text('0');
                $('.success-multiple tbody').html('');
                $('.error-multiple').html('');
                $('.single-view').hide();
                
                var url = 'include/cargaMasivaBajas.php';
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        
                        fileLocation = data[data.length - 1];
                        data.splice(data.length - 1, 1);

                        var minRow = 7;
                        var maxRow = data.length - 2;

                        $.each(data,function(i,e){
                            if(i == 3 && String(e[0][3]).trim() != 'GFI-F-055'){
                                $('#wait').fadeOut(400);
                                print_error(1, ['formato inválido, utilice el formato GFI-F-055']);
                                return false;
                            }

                            if(i >= minRow && i < maxRow){
                                // console.log(e[0]);
                                // $('#wait').fadeOut(400);
                                validate(e[0] , maxRow, i+1);
                            }
                        });

                        $('.upload-control').val('');
                    }
                });
            
            });
        // tabs
            $('.nav-tabs').on('click', 'li', function(e){
                e.preventDefault();
                if($(this).is('.active')){
                    return false;
                }
                var focus = $(this).parents('.nav-tabs').attr('data-tab');
                var ind = $(this).index();
                $(this).addClass('active');
                $(this).siblings('li').removeClass('active');

                $('.tabs[data-tab="'+focus+'"]').find('.tab').removeClass('active');
                $('.tabs[data-tab="'+focus+'"]').find('.tab:eq('+ind+')').stop().fadeIn(300,function(){
                    $(this).addClass('active').removeAttr('style');
                });

                // console.log($(this).attr('data-type'));
                if($(this).attr('data-type') == 'error'){
                    $('.inline-table,.show-list').hide();
                }
                else{
                    $('.inline-table,.show-list').fadeIn(300);
                }
            });
        // error acordion
            $('.error-multiple').on('click','.line',function(){
                $(this).toggleClass('active');
                $(this).next('ul').stop().slideToggle(400);
            });
        // download errors
            $('.btn-error').on('click',function(event){
                event.preventDefault();
                if($('.error-multiple .item').length){
                    $('#wait').fadeIn(400);
                    var errJSON = {};
                    $('.error-multiple .item').each(function(i,e){
                        var row = $(e).data('index');
                        errJSON[row] = [];
                        $(e).find('> ul > li').each(function(j,c){
                            var regex = /[;]/g;
                            var columnData = regex.test($(c).data('column')) ? String($(c).data('column')).split(';') : $(c).data('column');
                            var errorText = $(c).text();
                            if($(c).next('ol').length){
                                for(var i = 0; i < $(c).next('ol').find('li').length; i++){
                                    errorText += '\n'+$(c).next('ol').find('li:eq('+i+')').text();
                                }
                            }
                            if(columnData.length){
                                for(var i=0; i<columnData.length; i++){
                                    var obj = {
                                        column: Number(columnData[i]),
                                        error: errorText
                                    }
                                    errJSON[row].push(obj);
                                }
                            }
                            else{
                                var obj = {
                                    column: columnData,
                                    error: errorText
                                }
                                errJSON[row].push(obj);
                            }
                        });
                    });
                    
                    // console.log(errJSON);
                    
                    var url = 'include/downloadErrors.php';
                    var postQuery = {
                        path: fileLocation,
                        errors: errJSON,
                        format: 'bajas'
                    }

                    $.post(url, postQuery, function(data){
                        // $('#wait').fadeOut(400);
                        // return false;
                        var fileName = 'ERROR-GFI-F-055-FORMATO DE AUTORIZACIÓN PARA RETIRO DE ACTIVOS FIJOS DE LA OPERACIÓN.'+data.ext;
                        var $a = $("<a>");
                        $a.attr("href","include/"+data.file);
                        $("body").append($a);
                        $a[0].setAttribute("download",fileName);
                        $a[0].click();
                        $a.remove();
                        $('#wait').fadeOut(400);
                    });
                    // $.each(errJSON, function(i,e){
                    //     console.log(i);
                    //     console.log(e);
                    // })
                    // console.log(fileLocation);
                }
            });
        // add to table
            // $('.tab .btn-add').on('click', function(e){
            //     e.preventDefault();
            //     $('.success-multiple tbody tr').each(function(i,e){
            //         add2Table(e);
            //     });
            // });
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);