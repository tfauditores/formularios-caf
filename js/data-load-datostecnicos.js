(function($){
    var fileLocation = '';
    /* success */
    function print_success(index, excelRow, positions){
        var appendBool = false;
        
        $.each($($('.table.table-data')[0]).find('tbody tr'), function(i,e){
            if(
                $(e).find('td:eq(1)').text() == excelRow[positions.activo] &&
                $(e).find('td:eq(2)').text() == excelRow[positions.subnumero] &&
                $(e).find('td:eq(5)').attr('data-old') == excelRow[positions.inventario]
            ){
                appendBool = true;
            }
        });

        if(appendBool)
            return false;
        
        var successCount = $('.nav-tabs li[data-type="success"] i').text();
        $('.nav-tabs li[data-type="success"] i').text(Number(successCount) + 1);

        // add directly into tables
        // console.log(excelRow);
        // console.log(positions);
        $('table.table-data tbody').each(function(i,e){
            var vurmBool = false;
            if( String(excelRow[positions.nueva_vida_util_remanente]).trim() != ""){
                vurmBool = true;
            }
            $(e).append(
                $('<tr class="animated fadeIn">')
                .append('<td><span class="ion ion-trash-b remove-row"></span></td')
                .append( $('<td>').text( excelRow[positions.activo] ) )
                .append( $('<td>').text( excelRow[positions.subnumero] ) )
                .append( $('<td>').text( excelRow[positions.nueva_denominacion] ).attr('data-old', excelRow[positions.denominacion] ) )
                .append( $('<td>').text( excelRow[positions.nueva_denominacion2] ).attr('data-old', '') )
                .append( $('<td>').text( excelRow[positions.nuevo_inventario] ).attr('data-old', excelRow[positions.inventario] ) )
                .append( $('<td>').text( excelRow[positions.nuevo_fabricante] ).attr('data-old', '') )
                .append( $('<td>').text( excelRow[positions.nuevo_modelo] ).attr('data-old', '') )
                .append( $('<td>').text( excelRow[positions.nueva_serie] ).attr('data-old', excelRow[positions.serie] ) )
                .append( $('<td>').text( excelRow[positions.nuevo_tag] ).attr('data-old', '') )
                .append( $('<td>').text( excelRow[positions.nueva_capacidad] ).attr('data-old', '') )
                .append( $('<td>').text( excelRow[positions.nueva_unidad_medida] ).attr('data-old', '') )
                .append( $('<td>').text( excelRow[positions.nueva_vida_util_remanente] )
                        .attr({
                            'data-old': '', 
                            'data-fecha': excelRow[positions.fecha_vida_util_remanente], 
                            'data-ceco': excelRow[positions.ceco],
                            'data-vicepresidencia': excelRow[positions.vicepresidencia],
                            'data-gerencia': excelRow[positions.gerencia]
                        }) 
                    )
                .append( $('<td>').text( excelRow[positions.observaciones] ).attr('data-old', '') )
            )
        });

        $('#table1').fadeIn(300);
    }
    /* error */
    function print_error(index,err){
        // console.log(err);
        var errorCount = $('.nav-tabs li[data-type="error"] i').text();
        $('.nav-tabs li[data-type="error"] i').text(Number(errorCount) + 1);

        var html = '<div class="item" data-index="'+index+'"><h6 class="line">Fila '+index+'<span><i class="ion ion-bug"></i>'+(err.length > 1 ? err.length+' errores': err.length+' error')+'</span></h6><ul>';
        for( i = 0; i < err.length; i++ ){
            // console.log(err[i]);

            var options = '';

            if(err[i][0].match(/%%(.*)%%;/g)){
                options = err[i][0].match(/%%(.*)%%;/g)[0];
                options =  options.match(/[^%%|^%%;]*/g).filter(function(n){ return n != "" }) ;
            }

            var column = '';
            for( j = 1; j < err[i].length; j++ ){
                column += err[i][j]+(j == err[i].length - 1 ? '': ';' );
            }

            if(err.length == 1){
                html += '<li data-column="'+column+'">'+err[0][0].replace(/%%(.*)%%;/g, '')+'</li>';
            }
            else{
                html += '<li data-column="'+column+'">'+err[i][0].replace(/%%(.*)%%;/g, '')+'</li>';
            }

            if(options != '' && options.length){
                html += '<ol>';
                for(var j = 0; j < options.length; j++){
                    html += '<li>'+options[j]+'</li>';
                }
                html += '</ol>';
            }
        }
        html += '</ul></div>';
        $('.error-multiple').append(html);
    }
    /* validate individual Data*/
    function validateIndividual(url, params){
        params['individual'] = true;
        var json = 0;
        $.ajax({
            method: "GET",
            url: url,
            data: params,
            async: false,
            cache: false,
            dataType: 'json'
        })
        .done( function( data ) {
            json = data;
        });
        return json;
    }
    /* validate individual by column*/
    function validateIndividualPro(url, params){
        params['individualPro'] = true;
        var json = 0;
        $.ajax({
            method: "GET",
            url: url,
            data: params,
            async: false,
            cache: false,
            dataType: 'json'
        })
        .done( function( data ) {
            json = data;
        });
        return json;
    }
    /* validate */
    function validate(excelRow, rowLength, line){

        // console.log(excelRow);
        var positions = {
            activo: 1,
            subnumero: 2,
            denominacion: 3,
            inventario: 4,
            serie: 5,
            nueva_denominacion: 6,
            nueva_denominacion2: 7,
            nueva_capacidad: 8,
            nueva_unidad_medida: 9,
            nuevo_inventario: 10,
            nuevo_tag: 11,
            nueva_serie: 12,
            nuevo_fabricante: 13,
            nuevo_modelo: 14,
            ceco: 15,
            vicepresidencia: 16,
            gerencia: 17,
            fecha_vida_util_remanente: 18,
            nueva_vida_util_remanente: 19,
            observaciones: 20
        }

        var request = {
            activo: excelRow[positions.activo],
            subnumero: excelRow[positions.subnumero],
            inventario: excelRow[positions.inventario]
        }

        var url = 'include/cargaMasivaDatosTecnicos.php';
        $.getJSON(url, request, function(query){
            
            // console.log(request);
            
            var error_array = [];
            // console.log(query[0]);

            if(query.result != undefined){
                switch(query.result){
                    case 'invalid active':
                        error_array.push(['El número de activo o número de inventario no existe en el auxiliar', positions.activo, positions.inventario]);
                    break;
                    case 'invalid format':
                        error_array.push(['El formato de la fila es inválido, verificar campos obligatorios']);
                    break;
                    default:
                        error_array.push(['unknow error, please contact with the administrator']);
                }
            }
            else{
                // reset null fields into array
                for(var i=0; i<excelRow.length; i++){
                    if(excelRow[i] == null){
                        excelRow[i] = '';
                    }
                }

                // Verificar #activo y #invetario
                if(String(excelRow[positions.activo]).trim() != "")
                    if(String(excelRow[positions.activo]).trim() != String(query[0].activo).trim())
                        error_array.push(['El número de activo no coincide con el auxiliar', positions.activo]);

                if(String(excelRow[positions.inventario]).trim() != "")
                    if(String(excelRow[positions.inventario]).trim() != String(query[0].inventario).trim())
                        error_array.push(['El número de inventario no coincide con el auxiliar', positions.inventario]);

                if(Number(excelRow[positions.subnumero]) < 0)
                    error_array.push(['El subnúmero no debe ser menor a 0', positions.subnumero]);

                // Nueva denominación exista en el catalogo
                if(String(excelRow[positions.nueva_denominacion]).trim() != ""){
                    if(Number(excelRow[positions.subnumero]) > 0){
                        // console.log(excelRow);
                        var regexArray = [
                            "MAYOR VALOR SUBREPARTO",
                            "MAYOR VALOR COSTOS FINANCIEROS",
                            "MAYOR VALOR COSTO DE ADQUISICION",
                            "MAYOR VALOR SERVICIO DE PERFORACION",
                            "MAYOR VALOR SERVICIO DE WORKOVER",
                            "MAYOR VALOR OBRAS CIVILES",
                            "MAYOR VALOR",
                            "ADICION ACCESORIO",
                            "ADICION CAMPO",
                            "ADICION GESTION CONTRATO",
                            "ADICION GESTORIA",
                            "ADICION AMPLIACION",
                            "ADICION EJECUCION CONTRATO",
                            "ADICION",
                            "ADICION LINEA DE FLUJO (EL VALOR DE MAS DEBE SUMAR A LA CAPACIDAD DEL ACTIVO PRINCIPAL)"
                        ];

                        var regexBool = false;

                        for(var i = 0; i<regexArray.length; i++){
                            var regex = new RegExp('^\\b'+regexArray[i]+'\\b' , 'g');
                            regex.test( String(excelRow[positions.nueva_denominacion]).trim() ) ? regexBool = true: '';
                        }

                        var options = '';
                        for(var i = 0; i<regexArray.length; i++){
                            options += '%%'+regexArray[i]+'%%;';
                        }

                        !regexBool ? error_array.push(['La nueva denominación para un subnúmero debe empezar con alguna de las siguientes opciones: '+options, positions.nueva_denominacion]) : '';


                    }
                    else if(Number(excelRow[positions.subnumero]) == 0){
                        switch(query[0].tipoOperacion){
                            case 'Asociadas': 
                                var regex = /^[a-zA-Z0-9 \r\n]*$/g;
                                !regex.test(excelRow[positions.nueva_denominacion]) ? error_array.push(['La denominación tiene caracteres no permitidos, solo se permiten alfanumericos y espacios.', positions.nueva_denominacion]) : '';
                            break;
                            case 'Directa': 
                                var params = {
                                    table: 'catalogo',
                                    column: 'descripcion_clase',
                                    value: excelRow[positions.nueva_denominacion]
                                }
                                Number( validateIndividual(url, params).count ) < 1 ? error_array.push(['La nueva denominación no existe dentro del catálogo, verifique el catálogo o contacte con control de activos fijos', positions.nueva_denominacion]) : '';
                            break;
                        }
                    }
                }
                // Nueva denominación complementaria que sea texto
                if(String(excelRow[positions.nueva_denominacion2]).trim() != ""){
                    var regex = /^[a-zA-Z0-9_ \r\n]*$/g;
                    
                    !regex.test(String(excelRow[positions.nueva_denominacion2])) ? error_array.push(['La denominación complementaria tiene caracteres no permitidos, solo se permiten alfanumericos y espacios.', positions.nueva_denominacion2]) : '';
                }
                // Nueva Capacidad numerica
                if(String(excelRow[positions.nueva_capacidad]).trim() != ""){
                    var regex = /^\d+(\.\d{1,3})?/g;

                    !regex.test(excelRow[positions.nueva_capacidad]) ? error_array.push(['La capacidad tiene caracteres no permitidos, solo se permiten números o maximo 3 decimales.', positions.nueva_capacidad]) : '';
                }
                // Nueva unidad de medida según la tabla de catalogo para operación directa o unidades para asociadas
                if(String(excelRow[positions.nueva_unidad_medida]).trim() != ""){
                    
                    switch(query[0].tipoOperacion){
                        case 'Directa':
                            var params = {
                                table: 'catalogo',
                                column: 'clase',
                                value: query[0].clase,
                                queryColumns: 'unidad'
                            }
                            var boolCheck = true;
                            var queryArray = validateIndividualPro(url, params);
                            for(var i = 0; i< queryArray.length; i++){
                                if(queryArray[i].unidad == String(excelRow[positions.nueva_unidad_medida]).trim()){
                                    boolCheck = false;
                                }
                            }

                            boolCheck ? error_array.push(['La unidad de medida no es permitida para la clase, por favor confirme las unidades que sirven para la clase dentro del catálogo o comuniquese con control de activos', positions.nueva_unidad_medida]) : '';
                        break;
                        case 'Asociadas':
                            var params = {
                                table: 'unidades',
                                column: 'tipo',
                                value: 'sap',
                                queryColumns: 'unidad'
                            }
                            var boolCheck = true;
                            var queryArray = validateIndividualPro(url, params);
                            for(var i = 0; i< queryArray.length; i++){
                                if(queryArray[i].unidad == String(excelRow[positions.nueva_unidad_medida]).trim()){
                                    boolCheck = false;
                                }
                            }

                            boolCheck ? error_array.push(['La unidad de medida no es permitida para sap, por favor confirme las unidades que sirven para sap o comuniquese con control de activos', positions.nueva_unidad_medida]) : '';
                        break;
                    }

                }
                // Nuevo inventario 7 cifras
                if(String(excelRow[positions.nuevo_inventario]).trim() != ""){
                    // clases de terrenos para recibir # de matricula (FMI):
                    // 11001, 11002, 33006 - terrenos debe tener al final _
                    if(
                        query[0].clase == 11001 ||
                        query[0].clase == 11002 ||
                        query[0].clase == 33006 
                    )
                    {
                        var regex = /^[a-zA-Z0-9\/]{7,25}+_/g;
                        var value = String(excelRow[positions.nuevo_inventario]).trim();
    
                        !(regex.test(value) && value.length == 7) ? error_array.push(['El nuevo número de inventario para una clase de terrenos debe finalizar con _ al final y no tener espacios.', positions.nuevo_inventario]) : '';
                    }
                    // para clase 13001 recibe n letras y números sin espacio 
                    else if(
                        query[0].clase == 13001
                    )
                    {
                        var regex = /^[a-zA-Z0-9]/g;
                        var value = String(excelRow[positions.nuevo_inventario]).trim();
    
                        !(regex.test(value) && value.length == 7) ? error_array.push(['El nuevo número de inventario para la clase 13001 debe ser alfanumerico y no tener espacios.', positions.nuevo_inventario]) : '';
                    }
                    // 33001 - 33005 n longitud de digitos numericos con _ al final
                    else if(
                        query[0].clase == 33001 ||
                        query[0].clase == 33002 ||
                        query[0].clase == 33003 ||
                        query[0].clase == 33004 ||
                        query[0].clase == 33005 
                    ){
                        var regex = /^[0-9 ]+_/g;
                        var value = String(excelRow[positions.nuevo_inventario]).trim();
    
                        !(regex.test(value) && value.length == 7) ? error_array.push(['El nuevo número de inventario debe ser numérico y finalizar con _.', positions.nuevo_inventario]) : '';
                    }
                    // directa 7 digitos numericos
                    else if(
                        query[0].tipoOperacion == 'Directa'
                    ){
                        var regex = /^\d{7}/g;
                        var value = String(excelRow[positions.nuevo_inventario]).trim();
    
                        !(regex.test(value) && value.length == 7) ? error_array.push(['El nuevo número de inventario para operación directa debe ser numérico y tener 7 caracteres.', positions.nuevo_inventario]) : '';
                    }
                    // asociadas n digitos alfanumericos
                    else if(
                        query[0].tipoOperacion == 'Asociadas'
                    ){
                        var regex = /^[a-zA-Z0-9 _-]/g;
                        var value = String(excelRow[positions.nuevo_inventario]).trim();
    
                        !(regex.test(value) && value.length == 7) ? error_array.push(['El nuevo número de inventario para operación asociada debe ser alfanumérico.', positions.nuevo_inventario]) : '';
                    }
                }
                // Nuevo tag alfanumerico (oscar) _ - 
                if(String(excelRow[positions.nuevo_tag]).trim() != ""){
                    var regex = /^[a-zA-Z0-9_\-\/ ]*$/g;
                    var value = String(excelRow[positions.nuevo_tag]).trim();

                    !regex.test(value) ? error_array.push(['El nuevo tag debe contener caracteres alfanumericos.', positions.nuevo_tag]) : '';
                }
                // Nuevo serie alfanumerico (oscar) _ 
                if(String(excelRow[positions.nueva_serie]).trim() != ""){
                    var regex = /^[a-zA-Z0-9 _\/]*$/g;
                    var value = String(excelRow[positions.nueva_serie]).trim();

                    !regex.test(value) ? error_array.push(['La nueva serie debe contener caracteres alfanumericos.', positions.nueva_serie]) : '';
                }
                // Nuevo fabricante tabla de fabricantes
                if(String(excelRow[positions.nuevo_fabricante]).trim() != ""){
                    var params = {
                        table: 'fabricantes',
                        column: 'fabricante',
                        value: excelRow[positions.nuevo_fabricante]
                    }
                    Number( validateIndividual(url, params).count ) < 1 ? error_array.push(['El fabricante no existe dentro de la data maestra de fabricantes, verifique el nombre del fabricante o contacte con control de activos fijos ', positions.nuevo_fabricante]) : '';
                }
                // Nuevo modelo alfanumerico (oscar) _
                if(String(excelRow[positions.nuevo_modelo]).trim() != ""){
                    var regex = /^[a-zA-Z0-9 _]*$/g;
                    var value = String(excelRow[positions.nuevo_modelo]).trim();

                    !regex.test(value) ? error_array.push(['El nuevo modelo debe contener caracteres alfanumericos.', positions.nuevo_modelo]) : '';
                }
                // Actualización vidas útiles
                // validar con Erika
                // console.log(excelRow);
                if(String(excelRow[positions.fecha_vida_util_remanente]).trim() != ""){

                    if(String(excelRow[positions.nueva_vida_util_remanente]).trim() == ""){
                        error_array.push(['Debe ingresar una nueva vida útil remanente.', positions.nueva_vida_util_remanente]);
                    }

                    var date = new Date();
                    var regexDate = /^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/g;

                    if(regexDate.test(excelRow[positions.fecha_vida_util_remanente]) == false){
                        error_array.push(['La fecha debe ser de la siguiente forma dd/mm/yyyy.', positions.fecha_vida_util_remanente]);
                    }
                    else{
                        var dataDate = excelRow[positions.fecha_vida_util_remanente].split("/");
                        dataDate = new Date(dataDate[2],Number(dataDate[1]) - 1,dataDate[0]);
    
                        if(date.getMonth() != dataDate.getMonth()){
                            error_array.push(['La fecha debe estar en el mes actual.', positions.fecha_vida_util_remanente]);
                        }
                    }
                    
                }
                if(String(excelRow[positions.nueva_vida_util_remanente]).trim() != ""){
                    var regex = /^\d*$/g;
                    var value = String(excelRow[positions.nueva_vida_util_remanente]).trim();

                    if(regex.test(value) == false){
                        error_array.push(['La nueva vida útil remanente debe ser un valor numérico.', positions.nueva_vida_util_remanente]);
                    }
                    else{
                        if(Number(excelRow[positions.subnumero]) == 0){
                            if( Number(excelRow[positions.nueva_vida_util_remanente]) == query[0].vidaUtilRemanente){
                                error_array.push(['La nueva vida útil remanente debe ser diferente al vida actual y mayor a 0 ', positions.nueva_vida_util_remanente]);
                            }
                        }
                        else if(Number(excelRow[positions.subnumero]) > 0){
                            var params = {
                                table: 'auxiliar',
                                column: 'activo',
                                value: excelRow[positions.activo],
                                queryColumns: 'vuRemanenteAr01, activo, sn',
                                complement: 'AND sn = 0'
                            }
                            var queryTemp = validateIndividualPro(url, params);
                            // console.log(queryTemp[0].vuMesesAr01);
                            
                            if( Number(excelRow[positions.nueva_vida_util_remanente]) > queryTemp[0].vuRemanenteAr01 ){
                                var bool_return = false;
                                $('#table1 tbody tr').each(function(i,e){
                                    var active = $(e).find('td:eq(1)').text();
                                    var subnumero = $(e).find('td:eq(2)').text();
                                    var vidautil = $(e).find('td:eq(12)').text().trim();
                                    if( active == excelRow[positions.active] && subnumero == 0 && vidautil != "" ){
                                        bool_return = true;
                                    }
                                });

                                if(!bool_return){
                                    error_array.push(['La nueva vida útil remanente de un subnúmero no debe ser mayor que la vida útil remanente del activo principal.', positions.nueva_vida_util_remanente]);
                                }
                            }  
                        }
                    }
                    
                }
            }

            if(error_array.length){
                print_error(line, error_array);
            }
            else{
                // reasignar valores necesarios del auxiliar
                excelRow[positions.vicepresidencia] = query[0].siglaVicepresidencia;
                excelRow[positions.gerencia] = query[0].siglaGerencia;
                excelRow[positions.ceco] = query[0].ceco;

                print_success(line, excelRow , positions);
            }

            if(line == rowLength){
                $('#wait').fadeOut(400);
            }
        });
    }
    /* add to table */
    function add2Table(element){
        var variables = {
            activo: $(element).find('td:eq(1)').text(),
            denominacion: $(element).find('td:eq(2)').text(),
            inventario: $(element).find('td:eq(3)').text(),
            serie: $(element).find('td:eq(4)').text(),
            clase: $(element).find('td:eq(5)').text(),
            cod_municipio: $(element).find('td:eq(6)').text(),
            municipio: $(element).find('td:eq(7)').text(),
            cod_ubicacion_geografica: $(element).find('td:eq(8)').text(),
            ubicacion_geografica: $(element).find('td:eq(9)').text(),
            ceco: $(element).find('td:eq(10)').text(),
            cod_campo_planta: $(element).find('td:eq(11)').text(),
            campo_planta: $(element).find('td:eq(12)').text(),
            vida_util_total: $(element).find('td:eq(13)').text(),
            vicepresidencia: $(element).find('td:eq(14)').text(),
            gerencia: $(element).find('td:eq(15)').text(),
            observaciones: $(element).find('td:eq(16)').text()
        }
        $('table.table-data tbody').each(function(i,e){
            $(e).append(
                $('<tr>')
                .append('<td><span class="ion ion-trash-b remove-row"></span></td')
                .append( $('<td>').text( variables.activo ) )
                .append( $('<td>').text( variables.denominacion ) )
                .append( $('<td>').text( variables.inventario ) )
                .append( $('<td>').text( variables.serie ) )
                .append( $('<td>').text( variables.clase ) )
                .append( $('<td>').text( '' ) )
                .append( $('<td>').text( variables.cod_municipio ) )
                .append( $('<td>').text( variables.cod_ubicacion_geografica ) )
                .append( $('<td>').text( variables.vicepresidencia ) )
                .append( $('<td>').text( variables.gerencia ) )
                .append( $('<td>').text( variables.ceco ) )
                .append( $('<td>').text( variables.cod_campo_planta ) )
                .append( $('<td>').text( variables.vida_util_total ) )
                .append( $('<td>').text( variables.observaciones ) )
            )
        });
    }
    $(document).on('ready',function(){
        // nav view selector
            $('.nav-view .btn').on('click',function(e){
                $(this).siblings('.btn').removeClass('active');
                $(this).addClass('active');

                if($(this).is('.btn-single')){
                    $('.multiple-view').stop().hide();
                    $('.single-view').stop().fadeIn(400);
                }
                if($('.inline-table tbody tr').length)
                    $('.inline-table,.show-list').stop().fadeIn(400);
            });
        // excel file
            $('.upload-control').change(function(e){
                if(!$(this)[0].files.length){
                    return false;
                }
                if($('.nav-tabs li[data-type="error"]').is('.active')){
                    $('.inline-table,.show-list').hide();
                }

                var files = $(this)[0].files;
                var formData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    formData.append('excel', files[i]);
                }

                $('#wait').fadeIn(400);
                $('.multiple-view').fadeIn(300);
                $('.nav-tabs li[data-type="error"] i').text('0');
                $('.nav-tabs li[data-type="success"] i').text('0');
                $('.success-multiple tbody').html('');
                $('.error-multiple').html('');
                $('.single-view').hide();
                
                var url = 'include/cargaMasivaDatosTecnicos.php';
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        
                        fileLocation = data[data.length - 1];
                        data.splice(data.length - 1, 1);

                        var minRow = 7;
                        var maxRow = data.length - 4;

                        $.each(data,function(i,e){
                            // console.log(e);
                            // $('#wait').fadeOut(400);
                            if(i == 3 && String(e[0][3]).trim() != 'GFI-F-104'){
                                $('#wait').fadeOut(400);
                                print_error(1, ['formato inválido, utilice el formato GFI-F-104']);
                                return false;
                            }

                            if(i >= minRow && i < maxRow){
                                // console.log(e[0]);
                                validate(e[0] , maxRow, i+1);
                            }
                        });

                        $('.upload-control').val('');
                    }
                });
            
            });
        // tabs
            $('.nav-tabs').on('click', 'li', function(e){
                e.preventDefault();
                if($(this).is('.active')){
                    return false;
                }
                var focus = $(this).parents('.nav-tabs').attr('data-tab');
                var ind = $(this).index();
                $(this).addClass('active');
                $(this).siblings('li').removeClass('active');

                $('.tabs[data-tab="'+focus+'"]').find('.tab').removeClass('active');
                $('.tabs[data-tab="'+focus+'"]').find('.tab:eq('+ind+')').stop().fadeIn(300,function(){
                    $(this).addClass('active').removeAttr('style');
                });

                // console.log($(this).attr('data-type'));
                if($(this).attr('data-type') == 'error'){
                    $('#table1').hide();
                }
                else{
                    $('#table1').fadeIn(300);
                }
            });
        // error acordion
            $('.error-multiple').on('click','.line',function(){
                $(this).toggleClass('active');
                $(this).next('ul').stop().slideToggle(400);
            });
        // download errors
            $('.btn-error').on('click',function(event){
                event.preventDefault();
                if($('.error-multiple .item').length){
                    $('#wait').fadeIn(400);
                    var errJSON = {};
                    $('.error-multiple .item').each(function(i,e){
                        var row = $(e).data('index');
                        errJSON[row] = [];
                        $(e).find('> ul > li').each(function(j,c){
                            var regex = /[;]/g;
                            var columnData = regex.test($(c).data('column')) ? String($(c).data('column')).split(';') : $(c).data('column');
                            var errorText = $(c).text();
                            if($(c).next('ol').length){
                                for(var i = 0; i < $(c).next('ol').find('li').length; i++){
                                    errorText += '\n'+$(c).next('ol').find('li:eq('+i+')').text();
                                }
                            }
                            if(columnData.length){
                                for(var i=0; i<columnData.length; i++){
                                    var obj = {
                                        column: Number(columnData[i]),
                                        error: errorText
                                    }
                                    errJSON[row].push(obj);
                                }
                            }
                            else{
                                var obj = {
                                    column: columnData,
                                    error: errorText
                                }
                                errJSON[row].push(obj);
                            }
                        });
                    });
                    
                    // console.log(errJSON);
                    
                    var url = 'include/downloadErrors.php';
                    var postQuery = {
                        path: fileLocation,
                        errors: errJSON,
                        format: 'datosTecnicos'
                    }

                    $.post(url, postQuery, function(data){
                        // $('#wait').fadeOut(400);
                        // return false;
                        var fileName = 'GFI-F-104-FORMATO DE ACTUALIZACIÓN DE DATOS TÉCNICOS DE ACTIVOS FIJOS.'+data.ext;
                        var $a = $("<a>");
                        $a.attr("href","include/"+data.file);
                        $("body").append($a);
                        $a[0].setAttribute("download",fileName);
                        $a[0].click();
                        $a.remove();
                        $('#wait').fadeOut(400);
                    });
                    // $.each(errJSON, function(i,e){
                    //     console.log(i);
                    //     console.log(e);
                    // })
                    // console.log(fileLocation);
                }
            });
        // add to table
            // $('.tab .btn-add').on('click', function(e){
            //     e.preventDefault();
            //     $('.success-multiple tbody tr').each(function(i,e){
            //         add2Table(e);
            //     });
            // });
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);