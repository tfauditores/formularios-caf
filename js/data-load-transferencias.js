(function($){
    var fileLocation = '';
    /* success */
    function print_success(index, excelRow, positions){
        var appendBool = false;
        
        $.each($($('.table.table-data')[0]).find('tbody tr'), function(i,e){
            if(
                $(e).find('td:eq('+positions.activo+')').text() == excelRow[positions.activo] &&
                $(e).find('td:eq('+positions.inventario+')').text() == excelRow[positions.inventario]

            ){
                appendBool = true;
            }
        });

        if(appendBool)
            return false;
        
        var successCount = $('.nav-tabs li[data-type="success"] i').text();
        $('.nav-tabs li[data-type="success"] i').text(Number(successCount) + 1);

        $('.success-multiple tbody').append(
            $('<tr class="animated fadeIn">')
                .append( $('<td>',{
                    'html': index
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.activo]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.denominacion]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.inventario]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.serie]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.cod_clase]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nuevo_cod_municipio]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nuevo_municipio]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nuevo_cod_ubicacion_geografica]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nueva_ubicacion_geografica]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nuevo_cod_ceco]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nuevo_cod_campo_planta]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nuevo_campo_planta]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.vida_util_total]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nueva_vicepresidencia]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.nueva_gerencia]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.observaciones]
                }) )
        );

        // add directly into table
        // console.log(excelRow);
        // console.log(positions);
        $('table.table-data tbody').each(function(i,e){
            $(e).append(
                $('<tr class="animated fadeIn">')
                .append('<td><span class="ion ion-trash-b remove-row"></span></td')
                .append( $('<td>').text( excelRow[positions.activo] ) )
                .append( $('<td>').text( excelRow[positions.denominacion] ) )
                .append( $('<td>').text( excelRow[positions.inventario] ) )
                .append( $('<td>').text( excelRow[positions.serie] ) )
                .append( $('<td>').text( excelRow[positions.cod_clase] ) )
                .append( $('<td>').text( '' ) )
                .append( $('<td>').text( excelRow[positions.nuevo_cod_municipio] ).attr( 'data-name', excelRow[positions.nuevo_municipio] ) )
                .append( $('<td>').text( excelRow[positions.nuevo_cod_ubicacion_geografica] ).attr( 'data-name', excelRow[positions.nueva_ubicacion_geografica] ) )
                .append( $('<td>').text( excelRow[positions.nueva_vicepresidencia] ) )
                .append( $('<td>').text( excelRow[positions.nueva_gerencia] ) )
                .append( $('<td>').text( excelRow[positions.nuevo_cod_ceco] ) )
                .append( $('<td>').text( excelRow[positions.nuevo_cod_campo_planta] ).attr( 'data-name', excelRow[positions.nuevo_campo_planta] ) )
                .append( $('<td>').text( excelRow[positions.vida_util_total] ) )
                .append( $('<td>').text( excelRow[positions.observaciones] ) )
            )
        });

        $('#table1').fadeIn(300);
    }
    /* error */
    function print_error(index,err){
        console.log(err);
        var errorCount = $('.nav-tabs li[data-type="error"] i').text();
        $('.nav-tabs li[data-type="error"] i').text(Number(errorCount) + 1);

        var html = '<div class="item" data-index="'+index+'"><h6 class="line">Fila '+index+'<span><i class="ion ion-bug"></i>'+(err.length > 1 ? err.length+' errores': err.length+' error')+'</span></h6><ul>';
        for( i = 0; i < err.length; i++ ){
            // console.log(err[i]);

            var options = '';

            if(err[i][0].match(/%%(.*)%%;/g)){
                options = err[i][0].match(/%%(.*)%%;/g)[0];
                options =  options.match(/[^%%|^%%;]*/g).filter(function(n){ return n != "" }) ;
            }
            
            var column = '';
            for( j = 1; j < err[i].length; j++ ){
                column += err[i][j]+(j == err[i].length - 1 ? '': ';' );
            }
            
            if(err.length == 1){
                html += '<li data-column="'+column+'">'+err[0][0].replace(/%%(.*)%%;/g, '')+'</li>';
            }
            else{
                html += '<li data-column="'+column+'">'+err[i][0].replace(/%%(.*)%%;/g, '')+'</li>';
            }

            if(options != '' && options.length){
                html += '<ol>';
                for(var j = 0; j < options.length; j++){
                    html += '<li>'+options[j]+'</li>';
                }
                html += '</ol>';
            }
        }
        html += '</ul></div>';
        $('.error-multiple').append(html);
    }
    /* validate sync fields*/
    function validateCongruence(url, params){
        var json = 0;
        $.ajax({
            method: "GET",
            url: url,
            data: params,
            async: false,
            cache: false,
            dataType: 'json'
        })
        .done( function( data ) {
            json = data;
        });
        return json;
    }
    /* validate individual Data*/
    function validateIndividual(url, params){
        params['individual'] = true;
        var json = 0;
        $.ajax({
            method: "GET",
            url: url,
            data: params,
            async: false,
            cache: false,
            dataType: 'json'
        })
        .done( function( data ) {
            json = data;
        });
        return json;
    }
    /* validate individual by column*/
    function validateIndividualByColumn(url, params){
        params['individualByColumn'] = true;
        var json = 0;
        // $.ajax({
        //     method: "GET",
        //     url: url,
        //     data: params,
        //     async: false,
        //     cache: false,
        //     dataType: 'json'
        // })
        // .done( function( data ) {
        //     json = data;
        // });
        return json;
    }
    /* validate */
    function validate(excelRow, rowLength, line){

        // console.log(excelRow);
        var positions = {
            activo: 1,
            denominacion: 2,
            inventario: 3,
            serie: 4,
            cod_clase: 5,
            nuevo_cod_municipio: 6,
            nuevo_municipio: 7,
            nuevo_cod_ubicacion_geografica: 8,
            nueva_ubicacion_geografica: 9,
            nuevo_cod_ceco: 10,
            nuevo_cod_campo_planta: 11,
            nuevo_campo_planta: 12,
            vida_util_total: 13,
            nueva_vicepresidencia: 14,
            nueva_gerencia: 15,
            observaciones: 16
        }

        var request = {
            activo: excelRow[positions.activo],
            inventario: excelRow[positions.inventario]
        }

        var url = 'include/cargaMasivaTransferencias.php';
        $.getJSON(url, request, function(query){
            
            // console.log(request);
            // console.log(query[0]);

            var error_array = [];

            if(query.result != undefined){
                switch(query.result){
                    case 'invalid active':
                        error_array.push(['El número de activo no existe en el auxiliar', positions.activo]);
                    break;
                    case 'invalid format':
                        error_array.push(['El formato de la fila es invalido, verificar campos obligatorios']);
                    break;
                    default:
                        error_array.push(['unknow error, please contact with the administrator']);
                }
            }
            else{
                for(var i=0; i<excelRow.length; i++){
                    if(excelRow[i] == null){
                        excelRow[i] = '';
                    }
                }
                // Todos los valores nuevos tienen validaciones de forma

                if(String(excelRow[positions.activo]).trim() != "")
                    if(String(excelRow[positions.activo]).trim() != String(query[0].activo).trim())
                        error_array.push(['El número de activo no coincide con el auxiliar',positions.activo]);

                if(String(excelRow[positions.inventario]).trim() != "")
                    if(String(excelRow[positions.inventario]).trim() != String(query[0].inventario).trim())
                        error_array.push(['El número de inventario no coincide con el auxiliar', positions.inventario]);

                // if(String(excelRow[positions.denominacion]).trim() != String(query[0].denominacion).trim())
                //     error_array.push('La denominacion no coincide con el auxiliar');
                
                // if(String(excelRow[positions.serie]).trim() != String(query[0].serie).trim())
                //     error_array.push('La serie no coincide con el auxiliar');
                
                if( String(excelRow[positions.cod_clase]).trim().indexOf(String(query[0].clase).trim()) == -1 )
                    error_array.push(['El código de clase no coincide con el auxiliar', positions.cod_clase]);

                var congruence_data = {};
                // Nuevo Cod Municipio 
                if(excelRow[positions.nuevo_cod_municipio] != ''){
                    congruence_data['cod_municipio'] = excelRow[positions.nuevo_cod_municipio];
                }
                else{
                    congruence_data['cod_municipio'] = query[0].cod_municipio;
                }
                // Nuevo Cod Ubicación Geográfica 
                if(excelRow[positions.nuevo_cod_ubicacion_geografica] != ''){
                    congruence_data['cod_ubicacion_geografica'] = excelRow[positions.nuevo_cod_ubicacion_geografica];
                }
                else{
                    congruence_data['cod_ubicacion_geografica'] = query[0].cod_ubicacion_geografica;
                }
                // Nuevo Cod CeCo
                if(excelRow[positions.nuevo_cod_ceco] != ''){
                    congruence_data['cod_ceco'] = excelRow[positions.nuevo_cod_ceco];
                }
                else{
                    congruence_data['cod_ceco'] = query[0].ceco;
                }
                // Nuevo Cod CeCo
                if(excelRow[positions.nuevo_cod_campo_planta] != ''){
                    congruence_data['cod_campo_planta'] = excelRow[positions.nuevo_cod_campo_planta];
                }
                else{
                    congruence_data['cod_campo_planta'] = query[0].cod_campo_planta;
                }

                congruence_data['congruence'] = true;
                var verify_congruence = validateCongruence(url, congruence_data);

                // console.log(verify_congruence);

                if(verify_congruence.value == "Incongruence"){

                    // error_array.push(['No existe congruencia de ubicación, por favor verifique los siguientes datos:']);

                    if(excelRow[positions.nuevo_cod_municipio] == ''){
                        error_array.push(['No hay congruencia en el código de municipio', positions.nuevo_cod_municipio]);
                    }
                    else{
                        var params = {
                            table: 'ubicaciones',
                            column: 'codMun',
                            value: excelRow[positions.nuevo_cod_municipio]
                        }
                        Number( validateIndividual(url, params).count ) < 1 ? error_array.push(['El código de municipio no existe en la base de datos, verifique el código o contacte con control de activos fijos ', positions.nuevo_cod_municipio]) : '';
                    }

                    if(excelRow[positions.nuevo_cod_ubicacion_geografica] == ''){
                        error_array.push(['No hay congruencia en el código de ubicación geográfica', positions.nuevo_cod_ubicacion_geografica]);
                    }
                    else{
                        var params = {
                            table: 'ubicaciones',
                            column: 'codUbi',
                            value: excelRow[positions.nuevo_cod_ubicacion_geografica]
                        }
                        Number( validateIndividual(url, params).count ) < 1 ? error_array.push(['El código de ubicación geográfica no existe en la base de datos, verifique el código o contacte con control de activos fijos', positions.nuevo_cod_ubicacion_geografica]) : '';
                    }

                    if(excelRow[positions.nuevo_cod_ceco] == ''){
                        error_array.push(['No hay congruencia en el código de CeCo', positions.nuevo_cod_ceco]);
                    }
                    else{
                        var params = {
                            table: 'cecos',
                            column: 'ceco',
                            value: excelRow[positions.nuevo_cod_ceco]
                        }
                        Number( validateIndividual(url, params).count ) < 1 ? error_array.push(['El código de CeCo no existe en la base de datos, verifique el código o contacte con control de activos fijos', positions.nuevo_cod_ceco]) : '';
                    }

                    if(excelRow[positions.nuevo_cod_campo_planta] == ''){
                        error_array.push(['No hay congruencia en el código de campo planta', positions.nuevo_cod_campo_planta]);
                    }
                    else{
                        var params = {
                            table: 'ubicaciones',
                            column: 'codCam',
                            value: excelRow[positions.nuevo_cod_campo_planta]
                        }
                        Number( validateIndividual(url, params).count ) < 1 ? error_array.push(['El código de campo planta no existe en la base de datos, verifique el código o contacte con control de activos fijos', positions.nuevo_cod_campo_planta]) : '';
                    }

                }


                /* verificacion de cambio de clave */
                if( excelRow[positions.nuevo_cod_ceco] != '' ){
                    
                    if(
                        (String(query[0].ceco).indexOf('PR') >= 0 &&
                        String(excelRow[positions.nuevo_cod_ceco]).indexOf('PR') < 0) 
                        ||
                        (String(query[0].ceco).indexOf('PR') < 0 &&
                        String(excelRow[positions.nuevo_cod_ceco]).indexOf('PR') >= 0)
                    ){

                        if( String(excelRow[positions.nuevo_cod_ceco]).indexOf('PR') >= 0 ){
                            var newKey = query[0].clave_produccion;
                        }
                        else{
                            var newKey = query[0].clave_otros;
                        }

                        if(newKey == 'ZVUR'){
                            if(String(excelRow[positions.vida_util_total]).trim() == '')
                                error_array.push(['Para el nuevo CeCo se debe asignar una vida útil total en meses', positions.vida_util_total]);
                        }
                        else if(newKey == 'UoP'){
                            if(String(excelRow[positions.nuevo_cod_campo_planta]).trim() == '')
                                error_array.push(['Para el nuevo CeCo se debe asignar un campo planta', positions.nuevo_cod_campo_planta]);
                        }

                    }

                }

                
                // Vida Útil Total (en meses) => > 0 && limite maximo de 11988 meses (999*12)
                if(String(excelRow[positions.vida_util_total]) != '')
                {
                    if(
                        Number(excelRow[positions.vida_util_total]) < 0 ||
                        Number(excelRow[positions.vida_util_total]) > 11988
                    )
                    {
                        error_array.push(['La vida útil total debe ser mayor a 0 y debe ser menor a 11988 es decir 999 años por 12 meses', positions.vida_util_total]);
                    }
                }

            }

            // add information from auxiliar into some cells
            if(excelRow[positions.nuevo_cod_municipio] != '')
                excelRow[positions.nuevo_municipio] = verify_congruence.new_municipio != undefined ? verify_congruence.new_municipio : query[0].nombre_municipio;
            
            if(excelRow[positions.nuevo_cod_ubicacion_geografica] != '')
                excelRow[positions.nueva_ubicacion_geografica] = verify_congruence.new_ubicacion != undefined ? verify_congruence.new_ubicacion : query[0].ubicacion_geografica;
            
            if(excelRow[positions.nuevo_cod_campo_planta] != '')
                excelRow[positions.nuevo_campo_planta] = verify_congruence.new_campo_planta != undefined ? verify_congruence.new_campo_planta : query[0].campo_planta;

            // console.log(query[0]);
            // console.log(excelRow);
            // console.log(error_array);

            if(error_array.length){
                print_error(line, error_array);
            }
            else{
                print_success(line, excelRow , positions);
            }

            if(line == rowLength){
                $('#wait').fadeOut(400);
            }
        });
    }
    /* add to table */
    function add2Table(element){
        var variables = {
            activo: $(element).find('td:eq(1)').text(),
            denominacion: $(element).find('td:eq(2)').text(),
            inventario: $(element).find('td:eq(3)').text(),
            serie: $(element).find('td:eq(4)').text(),
            clase: $(element).find('td:eq(5)').text(),
            cod_municipio: $(element).find('td:eq(6)').text(),
            municipio: $(element).find('td:eq(7)').text(),
            cod_ubicacion_geografica: $(element).find('td:eq(8)').text(),
            ubicacion_geografica: $(element).find('td:eq(9)').text(),
            ceco: $(element).find('td:eq(10)').text(),
            cod_campo_planta: $(element).find('td:eq(11)').text(),
            campo_planta: $(element).find('td:eq(12)').text(),
            vida_util_total: $(element).find('td:eq(13)').text(),
            vicepresidencia: $(element).find('td:eq(14)').text(),
            gerencia: $(element).find('td:eq(15)').text(),
            observaciones: $(element).find('td:eq(16)').text()
        }
        $('table.table-data tbody').each(function(i,e){
            $(e).append(
                $('<tr>')
                .append('<td><span class="ion ion-trash-b remove-row"></span></td')
                .append( $('<td>').text( variables.activo ) )
                .append( $('<td>').text( variables.denominacion ) )
                .append( $('<td>').text( variables.inventario ) )
                .append( $('<td>').text( variables.serie ) )
                .append( $('<td>').text( variables.clase ) )
                .append( $('<td>').text( '' ) )
                .append( $('<td>').text( variables.cod_municipio ) )
                .append( $('<td>').text( variables.cod_ubicacion_geografica ) )
                .append( $('<td>').text( variables.vicepresidencia ) )
                .append( $('<td>').text( variables.gerencia ) )
                .append( $('<td>').text( variables.ceco ) )
                .append( $('<td>').text( variables.cod_campo_planta ) )
                .append( $('<td>').text( variables.vida_util_total ) )
                .append( $('<td>').text( variables.observaciones ) )
            )
        });
    }
    $(document).on('ready',function(){
        // nav view selector
            $('.nav-view .btn').on('click',function(e){
                $(this).siblings('.btn').removeClass('active');
                $(this).addClass('active');

                if($(this).is('.btn-single')){
                    $('.multiple-view').stop().hide();
                    $('.single-view').stop().fadeIn(400);
                }
                if($('.inline-table tbody tr').length)
                    $('.inline-table,.show-list').stop().fadeIn(400);
            });
        // excel file
            $('.upload-control').change(function(e){
                if(!$(this)[0].files.length){
                    return false;
                }
                if($('.nav-tabs li[data-type="error"]').is('.active')){
                    $('.inline-table,.show-list').hide();
                }

                var files = $(this)[0].files;
                var formData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    formData.append('excel', files[i]);
                }

                $('#wait').fadeIn(400);
                $('.multiple-view').fadeIn(300);
                $('.nav-tabs li[data-type="error"] i').text('0');
                $('.nav-tabs li[data-type="success"] i').text('0');
                $('.success-multiple tbody').html('');
                $('.error-multiple').html('');
                $('.single-view').hide();
                
                var url = 'include/cargaMasivaTransferencias.php';
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        fileLocation = data[data.length - 1];
                        data.splice(data.length - 1, 1);
                        
                        var minRow = 6;
                        var maxRow = data.length - 6;

                        $.each(data,function(i,e){
                            // console.log(e);
                            // $('#wait').fadeOut(400);
                            if(i == 3 && String(e[0][3]).trim() != 'GFI-F-088'){
                                $('#wait').fadeOut(400);
                                print_error(1, ['formato inválido, utilice el formato GFI-F-088']);
                                return false;
                            }

                            if(i >= minRow && i < maxRow){
                                // console.log(e[0]);
                                validate(e[0] , maxRow, i+1);
                            }
                        });

                        $('.upload-control').val('');
                    }
                });
            
            });
        // tabs
            $('.nav-tabs').on('click', 'li', function(e){
                e.preventDefault();
                if($(this).is('.active')){
                    return false;
                }
                var focus = $(this).parents('.nav-tabs').attr('data-tab');
                var ind = $(this).index();
                $(this).addClass('active');
                $(this).siblings('li').removeClass('active');

                $('.tabs[data-tab="'+focus+'"]').find('.tab').removeClass('active');
                $('.tabs[data-tab="'+focus+'"]').find('.tab:eq('+ind+')').stop().fadeIn(300,function(){
                    $(this).addClass('active').removeAttr('style');
                });

                // console.log($(this).attr('data-type'));
                if($(this).attr('data-type') == 'error'){
                    $('#table1').hide();
                }
                else{
                    $('#table1').fadeIn(300);
                }
            });
        // error acordion
            $('.error-multiple').on('click','.line',function(){
                $(this).toggleClass('active');
                $(this).next('ul').stop().slideToggle(400);
            });
        // download errors
            $('.btn-error').on('click',function(event){
                event.preventDefault();
                if($('.error-multiple .item').length){
                    $('#wait').fadeIn(400);
                    var errJSON = {};
                    $('.error-multiple .item').each(function(i,e){
                        var row = $(e).data('index');
                        errJSON[row] = [];
                        $(e).find('> ul > li').each(function(j,c){
                            var regex = /[;]/g;
                            var columnData = regex.test($(c).data('column')) ? String($(c).data('column')).split(';') : $(c).data('column');
                            var errorText = $(c).text();
                            if($(c).next('ol').length){
                                for(var i = 0; i < $(c).next('ol').find('li').length; i++){
                                    errorText += '\n'+$(c).next('ol').find('li:eq('+i+')').text();
                                }
                            }
                            if(columnData.length){
                                for(var i=0; i<columnData.length; i++){
                                    var obj = {
                                        column: Number(columnData[i]),
                                        error: errorText
                                    }
                                    errJSON[row].push(obj);
                                }
                            }
                            else{
                                var obj = {
                                    column: columnData,
                                    error: errorText
                                }
                                errJSON[row].push(obj);
                            }
                        });
                    });
                    
                    // console.log(errJSON);
                    
                    var url = 'include/downloadErrors.php';
                    var postQuery = {
                        path: fileLocation,
                        errors: errJSON,
                        format: 'transferencias'
                    }

                    $.post(url, postQuery, function(data){
                        // $('#wait').fadeOut(400);
                        // return false;
                        var fileName = 'GFI-F-088-FORMATO DE TRANSFERENCIA DE ACTIVOS FIJOS.'+data.ext;
                        var $a = $("<a>");
                        $a.attr("href","include/"+data.file);
                        $("body").append($a);
                        $a[0].setAttribute("download",fileName);
                        $a[0].click();
                        $a.remove();
                        $('#wait').fadeOut(400);
                    });
                    // $.each(errJSON, function(i,e){
                    //     console.log(i);
                    //     console.log(e);
                    // })
                    // console.log(fileLocation);
                }
            });
        // add to table
            // $('.tab .btn-add').on('click', function(e){
            //     e.preventDefault();
            //     $('.success-multiple tbody tr').each(function(i,e){
            //         add2Table(e);
            //     });
            // });
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);