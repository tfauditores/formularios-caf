var solicitudes = new Array();
$(function() {
/*
    Se aplica formato de moneda colombiana al campo "valor" del activo a agregar
*/    
    $("#costo").priceFormat({
        prefix:"$ ",
        centsSeparator:",",
        thousandsSeparator:".",
        clearOnEmpty:!0,
        centsLimit: 0,
        allowNegative: false
    });

/* 
    Al cargar este script comienza una carga asincrónica del archivo catalogo.json
    el cual es almacenado en una variable y luego pasado como parámetro al
    método autocomplete de jquery ui, para que el usuario pueda escribir una denominación
    y el sistema le sugiera las opciones válidas de acuerdo al catálogo de activos.
*/
    $.getJSON("js/json/catalogo.json", cargaCatalogo);
    function cargaCatalogo(data){
        var denominaciones = data.items;
        $( "#denomSearch" ).autocomplete({
            source: denominaciones,
            focus: function( event, ui ) {
                $( "#denomSearch" ).val( ui.item.label );
                return false;
            },
            select: function( event, ui ) {
                resetForm();
                //Prueba de local storage
                myObj = { "denominacion":ui.item.label, "grupo":ui.item.grupo, "clase":ui.item.clase, "medida":ui.item.medida, "vida":ui.item.vida };
                myJSON = JSON.stringify(myObj);
                localStorage.setItem("tipoActivo", myJSON);
                $( "#denom" ).val( ui.item.label );

                //Valida si la denominación contiene el caracter "+" para crear un nuevo input
                var str = $('#denom').val().indexOf('+');
                if (str > -1) {
                    $('.extra').remove();
                    var res = $('#denom').val().substring(str+2, );
                    $('#d1').append($("<input>").prop('placeholder',res).addClass('form-control valid extra'));
                }
                    

                $( "#grupo" ).val( ui.item.grupo );
                $( "#clase" ).val( ui.item.clase );
                $( "#medida" ).val( ui.item.medida );
                $( "#vida" ).val( ui.item.vida );
                if( $( "#medida" ).val() == "UN"){
                    $('#capacidad').val( 1 ).attr('readonly', true);
                }
                else{
                    $('#capacidad').removeAttr('readonly').val("");
                }
                /* DETERMINAR SI EL ACTIVO ES VEHICULO O INMUEBLE */
                var clasesInmuebles = [11001, 11002, 15001, 15002, 15003, 15004, 15005, 15009, 15010, 15014, 15015];
                var clasesVehiculos = [23001, 23002];
                var valorclase = parseInt( $("#clase").val() );
                if( jQuery.inArray( valorclase, clasesVehiculos) >= 0 ){
                    $("#inmuebles").hide(); 
                    $('#sig').val("");
                    $('#cedula').val("");
                    $('#matricula').val("");
                    $('#escritura').val("");

                    $("#vehiculos").show(500, "easeInExpo");
                }
                else if( jQuery.inArray( valorclase, clasesInmuebles) >= 0 ){
                    $("#vehiculos").hide(); 
                    $('#placavehiculo').val("");
                    $('#modelovehiculo').val("");

                    $("#inmuebles").show(500, "easeInExpo"); 
                }
                else{
                    $("#inmuebles").hide();
                    $("#vehiculos").hide(); 
                }
                return false;
            }
        })
        .autocomplete( "instance" )._renderItem = function( ul, item ) {
        return $( "<li>" )
            .append(  item.label )
            .appendTo( ul );
        };
    }
/*
    Este Event listener, verifica si el usuario selecciona la opción
    de agregar un subnúmero. De ser así entonces muestra una opción
    adicional en el formulario para digitar el número de activo principal.
*/
    $('input[name="tipoActivo"]').click(function(){

        /* Verifica si da click en el radio ya seleccionado */
        var valChecked = $('input[name=tipoActivo]:checked').val();
        var numAct = $("#numActivo").is(':visible');
        var isSameP = (valChecked=="principal" && numAct==false) ? true : false ;
        var isSameS = (valChecked=="subnumero" && numAct==true) ? true : false ;
        var isSame = isSameP || isSameS;

        /* Si da click en un radio diferente muestra el dialogo de confirmación*/
        if(!isSame){
            $( "#dialog-confirm" ).dialog({
                resizable: false,
                height: "auto",
                width: 400,
                modal: true,
                buttons: {
                    /* si el usuario confirma, procede con los cambios de campos */
                    "Confirmar": function() {
                        $( this ).dialog( "close" );
                        $("label.error").hide();
                        
                        if(valChecked == 'subnumero'){
                            $('#numActivoBox').show(500, "easeInExpo").attr('required', true);
                            $('#denomSearchBox').hide(500, "easeInExpo");  
                            $('#d1').hide(500, "easeInExpo");  
                            $('#d2').show(500, "easeInExpo");  
                            $('#d3').show(500, "easeInExpo");  
                            $('#d3 #denom2').attr('readonly', true).attr('placeholder',"");
                            $('#capacidad').attr('readonly', true);
                            resetForm();        
                        }
                        else {
                            $('#numActivoBox').hide(500, "easeInExpo").removeAttr('required');
                            $('#denomSearchBox').show(500, "easeInExpo");   
                            $('#d1').show(500, "easeInExpo");   
                            $('#d2').hide(500, "easeInExpo");   
                            $('#d3').show(500, "easeInExpo");   
                            $('#d3 #denom2').removeAttr('readonly').attr('placeholder',"Describa la denominación 2 si aplica...");
                            $('#capacidad').removeAttr('readonly');
                            resetForm();
                        }        
                    },
                    /* si el usuario cancela, cierra el dialogo y restaura el radio */
                    "Cancelar": function() {
                        $( this ).dialog( "close" );
                        var valChecked = $('input[name=tipoActivo]:checked').val();
                        if(valChecked == 'subnumero'){
                            $("input[name=tipoActivo][value='principal']").prop("checked",true);
                        }
                        else{
                            $("input[name=tipoActivo][value='subnumero']").prop("checked",true);
                        }
                    }
                }
            });    
        }
        


    });
/*
    Event Listener para buscar si el número de activo digitado por el usuario
    se encuentra en el auxiliar de activos fijos
*/
    $("#numActivo").on('change', function(){
        var val = $("#numActivo").val();
        buscarActivo(val);
    });
/*
    Inicializar   "nombre":s de Bootstrap
*/
    $('[data-toggle="tooltip"]').tooltip({
        trigger: "hover"
    }); 

/* VERIFICAR Y AGREGAR */
    // Event Listener para agregar valores del formulario a tabla virtual
    $("#agregar").click(function(){
        var validator = $( "#formatoAltas" ).validate();
        validator.form( "#formatoAltas" );
        console.log(validator); 
        console.log(validator.numberOfInvalids());
        if(validator.numberOfInvalids() == 0){
            duplicates = verifyDuplicates();
            if(duplicates == true){
                test = {
                    message: "Ya agregó un activo con esos datos, verifique y vuelva a intentar",
                    type: "warning"
                };
                modalMessage(test);
            }
            if (duplicates == false){
                console.log(solicitudes);
                test = {
                    message: "Activo agregado correctamente",
                    type: "success"
                };
                modalMessage(test);
                addToTable("table1");
                $("#table1").show();
                addToTable("tableBox");

                $('.show-list').fadeIn(400);
                $('.show-list sup').text($('#tableBox tbody tr').length);

             }
        }
    });
/* EVENT LISTENER verifica si CECO es de Produccion*/    
    $("#ceco").on('keyup', function(){
        var val = $("#ceco").val().substring(0,2);
        if(val == "PR"){
            $("#vpr").show(400);
        }
        else{
            $("#vpr").hide(400);
        }
    });
    $("#ceco").on('change', function(){
        var val = $("#ceco").val();
        buscarCeco(val);
    });

/********************************************************************************************************* */
/**
 * Función consulta de subnúmeros, está recibe 2 valores:
 * field_value:String Activo
 * focus_element:ElementoDom Modalbox donde se va a mostrar los resultados de los subnúmeros
 */
function requestSubnumbers(field_value , focus_element){
    // Se remueve la tabla con los ultimos datos agregados
    $(focus_element).find('.table-subnumbers').remove();
    // url de consulta(url) y parametros de consulta(requestJSON)
    var url = 'include/consultasTransferencias.php';
    var requestJSON = {
        'id_referencia': 'subnumeros',
        'id_numero_referencia': field_value
    }
    // Consulta al php que trae un json con los datos necesarios, se define:
    $.get(url, requestJSON, function(data){
        console.log(data);
        // Se crea la tabla que muestra la información de subnúmeros
        var html = '<table class="table table-striped table-responsive table-subnumbers"><thead class="thead-default"><tr><th>SN</th><th>Descripción</th><th>Descripción 2</th><th>CeCo</th></tr></thead><tbody>';
        if(data.length != undefined){
            $.each(data,function(i,e){
                html += '<tr><td>'+e.sn+'</td><td>'+e.denominacion+'</td><td>'+e.denominacion2+'</td><td>'+e.numero_ceco+'</td></tr>';
            });
        }
        else{
            html += '<tr><td>'+data.sn+'</td><td>'+data.denominacion+'</td><td>'+data.denominacion2+'</td><td>'+data.numero_ceco+'</td></tr>';
        }
        html += '</tbody></table>';
        $(focus_element).find('.cover-table').html(html);
    });
}

/* EVENT LISTENER si es altas para TRASLADO*/    
$('#esTraslado').change(function() {
    if(this.checked) {
        $("#TrasladoBox").show(400);
    }
    else{
        $("#TrasladoBox").hide(400);
    }
});

/* EVENT LISTENER EN EL NUMERO DE ACTIVO PARA TRASLADOS */
$('input[name="id_numero_activo_destino"]').change(function(){
    console.log("buscando el activo origen");
    val = $(this).val();
    $.ajax({
        type: 'post',
        url: 'include/buscarPorActivo.php',
        data: {
            get_option:val
        },
        success: function (response) {
            if(response != ""){
                console.log(response);
                // ACTIVAR LISTA DE SUBNUMEROS
                if(response.subnumeros != 0 ){
                    $('.sn .ion').remove();
                    var html = '<span class="ion ion-ios-list" data-modal="subnumbers" data-toggle="tooltip" data-placement="down" title="El activo tiene '+response.subnumeros+' subnúmeros"></span>';
                    $('.sn').append(html);
                    $('.sn .ion').tooltip(); 
                }
            }
            else{
                modalMessage({
                    message: "No se encuentra el activo fijo con el número "+val,
                    type: "warning"
                });
                $('input[name="id_numero_activo_destino"]').val("");     
                $('input[name="id_subnumero_activo_destino"]').val("");   
                $('select[name="motivo_traslado"]').val("").prop('selectedIndex',0);
                $('input[name="valor_traslado"]').val("");
                $('.sn .ion').remove();           
            }
        }        
    });
});
// Click en elemento con atributo data-modal
$(document).on('click','[data-modal]',function(e){
    var focus = $(e.target).attr('data-modal');
    switch(focus){
        case "subnumbers":
            requestSubnumbers( $('[name="id_numero_activo_destino"]').val().trim() , $('.modalbox[data-modal="'+focus+'"]') );
            //$(".show-characteristics, .slide-info, .modificaciones").fadeOut(400);
            $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
        break;
        default:
            $('.modalbox[data-modal="'+focus+'"]').stop().fadeIn(400);
        break;
    }
});
// Click en elemento con clase close-modal
$('.modalbox,.close-modal').on('click',function(e){
    if($(e.target).is('.table-cell') || $(e.target).is('.close-modal')){
    $(this).closest('.modalbox').stop().fadeOut(400);
    }
});
 //Click en las filas de la tabla de subnúmeros
 $(document).on('click', '.table-subnumbers tbody tr', function(){
    var subnumber = $(this).find('td:eq(0)').text();
    $(this).parents('.modalbox').fadeOut(400);
    $('[name="id_subnumero_activo_destino"]').val(subnumber);
    $('[name="id_subnumero_activo_destino"]').change();
});

//cambio en el input de "subnumero" 
$('.form-control[name="id_subnumero_activo_destino"]').on('change',function(){
    $('#wait').fadeIn(400);

    var mainVal = $('[name="id_numero_activo_destino"]').val().trim();
    var subVal = $('[name="id_subnumero_activo_destino"]').val().trim();

    $.ajax({
        type:'post',
        url: 'include/buscarPorActivo.php',
        data: {
            get_option: mainVal,
            subnum: subVal
        },
        success: function(response){
            console.log(response);
        }
    });
});

/********************************************************************************************************* */
/* VALIDATE */
    // Método de validación personalizado que recibe como parámetro una expresión regular (regexp)ç
    // El mensaje de error por defecto es "El campo contiene caracteres no permitidos" 
    jQuery.validator.addMethod(
        "regex", 
        function(value, element, regexp) {
            var re = new RegExp(regexp);
            return this.optional(element) || re.test(value);
        }, 
        "El campo contiene caracteres no permitidos"
    );

    $("#formatoAltas").validate({
        debug: true,
        rules: {
            numActivo: {
                required: true,
                number: true
            },
            denom: {required:true},
            grupo: {required:true},
            clase: {required:true},
            medida: {required:true},
            denom2: {
                minlength: 5,
                maxlength: 50,
                regex:  "^[a-zA-Z0-9 ]*$",
                required: true
            },
            subdenom: {
                required: true,
            },
            capacidad: {
                required:true,
                maxlength:18,
                regex: "^[0-9]+(\,[0-9]+)?$"
            },
            tag: {
                maxlength: 15,
                regex: "^[a-zA-Z0-9]*$"
            },
            modelo: {
                maxlength: 15,
                regex: "^[a-zA-Z0-9]*$"
            },
            serie: {
                maxlength: 18,
                regex: "^[a-zA-Z0-9_-]*$"
            },
            vida: {
                required:true,
                maxlength: 3,
                number: true            
            },
            costo: {required:true},
            departamento: {required:true},
            municipio: {required:true},
            ubicacion: {required:true},
            fabricante: {required: true},
            inventario: {
                required: true,
                maxlength: 7,
                regex: "^[a-zA-Z0-9]*$"
            },
            ceco:{
                required: true,
                maxlength: 6,
                regex: "^[A-Za-z]{2}[0-9]{4}$"
            },
            placaVehiculo:{
                required: true,
                maxlength: 7,
                regex: "^[a-zA-Z0-9]*$"
            },
            modeloVehiculo:{
                required: true,
                number: true,
                maxlength: 4,
                minlength: 4,
                max: 2019,
                min: 1980
            },
            sig:{
                required: true,
                maxlength: 10,
                regex: "^[a-zA-Z0-9]*$"
            },
            cedula:{
                required: true,
                regex: "^[a-zA-Z0-9]*$"
            },
            matricula:{
                required: true,
                regex: "^[a-zA-Z0-9]*$"
            },
            escritura:{
                required: true,
                regex: "^[a-zA-Z0-9]*$"
            },
            nombreCampo: {
                required: true,
                regex:  "^[a-zA-Z0-9 ]*$"
            },
            tipoPozo: {
                required: true,
                regex:  "^[a-zA-Z0-9 ]*$"
            }
        },
        messages: {
            numActivo: {
                required: "Este campo es requerido"
            },
            denom: "Este campo es requerido",
            subdenom: "Este campo es requerido",
            grupo: "Este campo es requerido",
            clase: "Este campo es requerido",
            medida: "Este campo es requerido",
            modelo: {
                required: "Este campo es requerido",
                maxlength: "La longitud máxima permitida es 15 caracteres alfanuméricos",
                regex: "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            denom2: {
                minlength: "La denominación2 es muy corta",
                maxlength: "La denominación2 excede el límite de caracteres",
                required: "Este campo es requerido"
            },
            capacidad: {
                required: "La capacidad no puede ser cero o estar vacía" 
            },
            tag: {
                required: "Este campo es requerido",
                maxlength: "La longitud mínima es 15 caracteres alfanuméricos",
                regex: "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            serie: {
                required: "Este campo es requerido",
                minlength: "La longitud mínima es 5 caracteres alfanuméricos",
                maxlength: "La longitud máxima permitida es 18 caracteres alfanuméricos",
                regex: "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            vida: {
                required: "Este campo es requerido",
                maxlength: "La longitud mínima es 3 caracteres numéricos",
                number: "Solamente se admiten números" 
            },
            costo: {
                required: "Este campo es requerido"
            },
            departamento: {
                required: "Este campo es requerido"
            },
            municipio: {
                required: "Este campo es requerido"
            },
            ubicacion: {
                required: "Este campo es requerido"
            },
            fabricante: {
                required: "Este campo es requerido"
            },
            inventario: {
                required: "Este campo es requerido",
                maxlength: "La longitud máxima permitida es 7 caracteres alfanuméricos. Si el número es muy largo digite solamente los últimos 18",
                regex: "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            ceco:{
                required: "Este campo es requerido",
                maxlength: "La longitud máxima permitida es 6 caracteres alfanuméricos",
                regex: "La estructura del CeCo consiste de 2 letras y 4 números, sin espacios ni caracteres adicionales. Ejemplo: PR1234"
            },
            placaVehiculo:{
                required: "Este campo es requerido para vehículos",
                maxlength: "Máximo 7 caracteres Alfanuméricos",
                regex: "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            modeloVehiculo:{
                required: "Este campo es requerido para vehículos",
                number: "Este campo solamente acepta números",
                maxlength: "Máximo 4 caracteres de tipo numérico",
                minlength: "Mínimo 4 caracteres de tipo numérico",
                max: "El modelo no puede ser superior a 2018",
                min: "El modelo no puede ser menor a 1980"
            },
            sig:{
                required: "Este campo es requerido para inmuebles y edificaciones",
                maxlength: "Longitud máxima de 10 caracteres alfanuméricos",
                regex: "El campo contiene caracteres no permitidos, se admiten números y letras"    
            },
            cedula:{
                required: "Este campo es requerido para inmuebles y edificaciones",
                regex:  "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            matricula:{
                required: "Este campo es requerido para inmuebles y edificaciones",
                regex:  "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            escritura:{
                required: "Este campo es requerido para inmuebles y edificaciones",
                regex:  "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            nombreCampo: {
                required: "Este campo es requerido para activos de Producción",
                regex:  "El campo contiene caracteres no permitidos, se admiten números y letras"
            },
            tipoPozo: {
                required: "Este campo es requerido para activos de Producción",
                regex:  "El campo contiene caracteres no permitidos, se admiten números y letras"
            }
        }
    });

/* GET CURRENT VALUES IN THE ENTIRE FORM*/
    function getCurrentValues(){
        tipoAct = $(".tipoActivo:checked").val();
        currentValues = {
            "numActivo" : tipoAct=="principal" ? "nuevo" : $("#activoOK").val(),
            "denom": tipoAct=="principal" ? $("#denom").val() : $("#subdenom").val() ,
            "denom2": $("#denom2").val(),
            "grupo": $("#grupo").val(),
            "clase": $("#clase").val(),
            "medida": $("#medida").val(),
            "capacidad": $("#capacidad").val(),
            "vida": $("#vida").val(),
            "fabricante": $("#fabricante").val(),
            "modelo": $("#modelo").val()=="" ? "NO REGISTRA" : $("#modelo").val(),
            "serie": $("#serie").val()=="" ? "NO REGISTRA" : $("#serie").val(),
            "tag": $("#tag").val()=="" ? "NO REGISTRA" : $("#tag").val(),
            "costo": $("#costo").unmask(),    
            "departamento": $("#departamento").val(),    
            "municipio": $("#municipio").val(),    
            "codMunicipio": $("#municipio option:selected").text(),    
            "ubicacion": $("#ubicacion").val(),
            "codUbicacion": $("#ubicacion option:selected").text(),    
            "ceco": $("#ceco").val(),
            "inventario": $("#inventario").val(),

            "nombreCampo":  $("#nombreCampo").val(),
            "tipoPozo": $("#tipoPozo").val(),
            "sig": $("#sig").val(),
            "cedula": $("#cedula").val(),
            "matricula": $("#matricula").val(),
            "escritura": $("#escritura").val(),

            "placaVehiculo": $("#placaVehiculo").val() == "" ? "N/A" : $("#placaVehiculo").val() ,
            "modeloVehiculo": $("#modeloVehiculo").val() == "" ? "N/A" :  $("#modeloVehiculo").val()
        };
        return currentValues;
    }

/* VERIFYDUPLICATES */
    function verifyDuplicates(){
        v = getCurrentValues();
        nuevaSolicitud = v.grupo + v.numActivo + v.denom + v.serie + v.inventario + v.capacidad + v.medida + v.tag + v.ceco + v.ubicacion + v.codMunicipio + v.municipio + v.fabricante + v.modelo + v.vida + v.costo;
        if( solicitudes.indexOf(nuevaSolicitud) >= 0 ){
            return true;
        }else{
            solicitudes.push(nuevaSolicitud);
            return false;
        }
    }

/* ADD TO TABLE */
    function addToTable(id){
        variables = getCurrentValues();
        $('#'+id).find('tbody')
        .prepend($('<tr>')
            .append('<td><span class="ion ion-trash-b remove-row"></span></td')
            .append( $('<td>').text( variables.grupo ) )
            .append( $('<td>').text( variables.numActivo ) )
            .append( $('<td>').text( variables.denom ) )
            .append( $('<td>').text( variables.denom2 ) )
            .append( $('<td>').text( variables.serie ) )
            .append( $('<td>').text( variables.inventario ) )
            .append( $('<td>').text( variables.capacidad ).attr("contenteditable","true") )
            .append( $('<td>').text( variables.medida ) )
            .append( $('<td>').text( variables.tag ).attr("contenteditable","true") )
            .append( $('<td>').text( variables.ceco ).attr("contenteditable","true") )
            .append( $('<td>').text( variables.ubicacion ) )
            .append( $('<td>').text( variables.codMunicipio ) )
            .append( $('<td>').text( variables.codMunicipio ) )
            .append( $('<td>').text( variables.fabricante ) )
            .append( $('<td>').text( variables.modelo ).attr("contenteditable","true") )
            .append( $('<td>').text( variables.vida ).attr("contenteditable","true") )
            .append( $('<td>').text( variables.costo ).attr("contenteditable","true") )
            .append( $('<td>').text( variables.nombreCampo ) )
            .append( $('<td>').text( variables.tipoPozo ) )
            .append( $('<td>').text( variables.sig ) )
            .append( $('<td>').text( variables.cedula ) )
            .append( $('<td>').text( variables.matricula ) )
            .append( $('<td>').text( variables.escritura ) )
            .append( $('<td>').text( variables.placaVehiculo ) )
            .append( $('<td>').text( variables.modeloVehiculo ) )
        ); 

    }
/* REMOVE FROM TABLE */
    $(document).on('click', '.remove-row',function(){
        var rowInd = $(this).parents('tr').index();
        $.each($('.table-bajas tbody'),function(i,e){
            $(e).find('tr:eq('+rowInd+')').fadeOut(400,function(){
                $(this).remove();
            });
        });

        $('.show-list sup').text($('#tableBox tbody tr').length -1 );
        if($($('#tableBox tbody')[0]).find('tr').length -1 <= 0){
            $('.show-list').fadeOut(400);
            $('#table1').fadeOut(400);
            $('#tableBox').fadeOut(400);
        }
        
    });

/* RESET FORM */
    $("#reset").click(function(){
        resetForm();
    });
    function resetForm(){
        $('#numActivo').val("");
        $('#denomSearch').val("");
        $('#denom').val("");
        $('#denom2').val("");
        $('#subdenom').val("");
        $('#grupo').val("");
        $('#clase').val("");
        $('#medida').val("");
        $('#capacidad').val("");
        $('#vida').val("");
        $('#activoOK').val("");
        $('#fabricante').val("");
        $('#modelo').val("");
        $('#serie').val("");
        $('#tag').val("");
        $('#costo').val("");

        $('#ceco').val("").removeAttr('readonly');

        $.ajax({
            type: 'post',
            url: 'include/buscarDepartamentos.php',
            data: {
                get_option:"test"
            },
            success: function (response) {
                $("#departamento").html("<option value=''>Seleccione Departamento</option>"+response).removeAttr('disabled').prop('selectedIndex',0);
                $("#municipio").html("<option value=''>Seleccione Municipio</option>").removeAttr('disabled').prop('selectedIndex',0);
                $("#ubicacion").html("<option value=''>Seleccione Ubicación</option>").removeAttr('disabled').prop('selectedIndex',0);
            }
        });

        $('#placaVehiculo').val("");
        $('#modeloVehiculo').val("");
        $('#sig').val("");
        $('#cedula').val("");
        $('#matricula').val("");
        $('#escritura').val("");

        $("#vehiculos").hide(); 
        $("#inmuebles").hide(); 
        $("#vpr").hide();

        $('#esTraslado').prop( "checked", false );
        $("#TrasladoBox").hide(400);

        $("label.error").hide();    
    }

/* BUSCAR POR CECO */
    function buscarCeco(val){
        $.ajax({
            type: 'post',
            url: 'include/buscarPorCeco.php',
            data: {
                get_option: val
            },
            success: function (response){
                if(response=="" || response==null){
                    console.log("no se encuentra ese CeCo");
                    $("#ceco-info").fadeOut(400);
                }
                else{
                    console.log(response);

                    $( "#descripcionCeco" ).val( response.descripcionCeco );
                    $( "#area" ).val( response.area );
                    $( "#vicepresidencia" ).val( response.vicepresidencia );
                    $( "#gerencia" ).val( response.gerencia );
                    $("#ceco-info").fadeIn(400);
                }
            }
        });
    }

/* BUSCAR POR #ACTIVO */    
    // BUSCAR POR ACTIVO , ejecuta una consulta ajax que busca en un archivo PHP
    // y de acuerdo a la respuesta, asigna valores a algunos campos de entrada
    //aplica solo para subnumeros
    function buscarActivo(val, subn=0){
        $.ajax({
            type: 'post',
            url: 'include/buscarPorActivo.php',
            data: {
                get_option:val,
                subnum: subn
            },
            success: function (response) {
                if(response=="" || response==null){
                    var message1 = "No se encontró el número de activo en el libro Auxiliar. Verifíquelo y vuelva a intentar.";
                    $("#numActivoBox .alert-danger").html(message1);
                    $("#numActivoBox .alert-danger").show(500, "easeInExpo");
                    resetForm();
                }
                else{
                    $("#numActivoBox .alert-danger").hide(500, "easeInExpo");
                    $("#activoOK").val( $("#numActivo").val() );
                    $( "#denom2" ).val( response.denominacion );
                    $( "#grupo" ).val( response.grupo );
                    $( "#clase" ).val( response.clase );
                    $( "#medida" ).val( "UN" );
                    $( "#capacidad" ).val( 1 );
                    if(response.departamento != ""){
                        console.log("response: " + response.departamento);
                        $("#departamento").html("<option>Seleccione Departamento</option>"); 
                        $('#departamento').append($('<option>', {
                            value: response.departamento,
                            text: response.departamento,
                            selected: true
                        })).attr('disabled', true);
                        $("#departamento").val(response.departamento).attr('disabled', true);;
                        $("#municipio").html("<option>Seleccione Municipio</option>"); 
                        $('#municipio').append($('<option>', {
                            value: response.municipio,
                            text: response.nombreMunicipio,
                            selected: true
                        })).attr('disabled', true);
                        $("#ubicacion").html("<option>Seleccione Ubicación</option>");
                        $('#ubicacion').append($('<option>', {
                            value: response.ubicacionGeografica,
                            text: response.nombreUbicacionGeografica,
                            selected: true
                        })).attr('disabled', true);
                    }
                    else{
                        $('#departamento').removeAttr('disabled');
                        $('#municipio').removeAttr('disabled');
                        $('#ubicacion').removeAttr('disabled');
                    }
                    if(response.ceco != ""){
                        $( "#ceco" ).val( response.ceco ).attr('readonly', true);
                    }
                    else{
                        $('#ceco').removeAttr('disabled');
                    }
                }
            }
        });
    }

/* SHOW AND HIDE TABLE BOX */

    $(".slider-arrow").click(function(){
        $('#tableBox').fadeIn(400);
        //$("#tableBox").css("display", "block");
    });
    var modal = document.getElementById('tableBox');
    var span = document.getElementsByClassName("close")[0];
    window.onclick = function(event) {
        if (event.target == modal) {
            $('#tableBox').fadeOut(400);
        }
    }
    span.onclick = function() {
        $('#tableBox').fadeOut(400);
    }    

/* PRECARGA AJAX */  
    // Muestra div #wait con imagen de precarga al iniciar el método ajax de jquery 
    // Al completar el método ajax de jquery, esconde el div #wait 
    $(document)
    .ajaxStart(function(){
        $("#wait").css("display", "block");
    })
    .ajaxComplete(function(){
        function hide(){
            $("#wait").css("display", "none");
        }
        setTimeout(hide, 500);
    });

/* BUSCAR POR DEPARTAMENTO */
    $( "#departamento" ).change(function() {
        buscarMunicipios( $( this ).val() );
    });
    function buscarMunicipios(val){
        $.ajax({
            type: 'post',
            url: 'include/buscarMunicipios.php',
            data: {
                get_option:val
            },
            success: function (response) {
                $("#municipio").html("<option value=''>Seleccione Municipio</option>"+response).prop('selectedIndex',0);
                $("#ubicacion").html("<option value=''>Seleccione Ubicación</option>").prop('selectedIndex',0);
            }
        });
    }

/* BUSCAR POR MUNICIPIO */
    $( "#municipio" ).change(function() {
        buscarUbicaciones(  $("#municipio option:selected").text() );
    });
    function buscarUbicaciones(val){
        $.ajax({
            type: 'post',
            url: 'include/buscarUbicaciones.php',
            data: {
                get_option:val
            },
            success: function (response) {
                $("#ubicacion").html("<option value=''>Seleccione Ubicación</option>"+response).prop('selectedIndex',0);
            }
        });
    }
/* CREARJSON */ 
    function crearJson(){
        var $table = $("#tableBox"),
        rows = [],
        header = [];
        //Encontramos los nombre de columna
        $table.find("thead th").each(function () {
            header.push($(this).html());
        });
        //Recorremos las filas
        $table.find("tbody tr").each(function () {
            var row = {};
            $(this).find("td").each(function (i) {
                var key     = header[i],
                    value   = $(this).html();
            
                row[key] = value;
            });
            rows.push(row);
        });
        var myJsonString = JSON.stringify(rows);
        return myJsonString;
    }
/* GENERAR EXCEL */ 
    $( "#generarExcel" ).click(function() {
        var myJson = crearJson();
        console.log(myJson);
        
        $.ajax({
            type: 'POST',
            url: 'include/generarXls.php',
            data: { get_option: myJson },
            success: function(response){
                console.log("Respuesta: \n" + response );
                var $a = $("<a>");
                $a.attr("href","include/xls/dataMaestra-"+response);
                $("body").append($a);
                $a.attr("download","dataMaestra-"+response+".xls");
                $a[0].click();
                $a.remove();
            }
        });
        
    });

/* MODAL MESSAGE */ 
    function modalMessage(json){
        $('.modalMessage').remove();
        clearTimeout(timeMessage);

        var html = '<div class="modalMessage '+json.type+'"><h3>'+json.message+'</h3><span class="ion ion-android-cancel close-message"></span></div>';
        $('body').append(html);
        var timeMessage = setTimeout(function(){
            $('.modalMessage').animate({
                    opacity: 0.1,
                    left: "100%",
                    top: 0
                }, 500, function() {
                    $(this).remove();
                });
                //$(this).remove();
        }, 3000);

    }
/* UPPERCASE & REGEX REPLACE */
    function regexReplace(obj, regex){
        obj.val( obj.val().toUpperCase() );
        var sanitized = obj.val().replace(regex, '');
        obj.val(sanitized);
    }

    $("#serie").on('change keyup', function(){
        regexReplace($(this), /[^a-zA-Z0-9]/g );
    });
    $("#modelo").on('change keyup', function(){
        regexReplace($(this), /[^a-zA-Z0-9]/g );
    });
    $("#denom2").on('change keyup', function(){
        regexReplace($(this), /[^a-zA-Z0-9 ]/g );
    });

    $("#inventario").keyup(function(){
        $(this).val( $(this).val().toUpperCase() );
    });
    $("#tag").keyup(function(){
        $(this).val( $(this).val().toUpperCase() );
    });
    $("#ceco").keyup(function(){
        $(this).val( $(this).val().toUpperCase() );
    });
    $("#placavehiculo").keyup(function(){
        $(this).val( $(this).val().toUpperCase() );
    });
    $('[name="id_numero_activo_destino"]').on('change keyup', function(){
        regexReplace($(this), /[^0-9]/g );
    });
});