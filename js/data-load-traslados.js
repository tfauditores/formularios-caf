(function($){
    /* success */
    function print_success(index, excelRow, positions){
        var appendBool = false;

        $.each($('.success-multiple tbody tr'), function(i,e){
            if(
                $(e).find('td:eq('+positions.activo_origen+')').text() == excelRow[positions.activo_origen] &&
                $(e).find('td:eq('+positions.sn_origen+')').text() == excelRow[positions.sn_origen] &&
                $(e).find('td:eq('+positions.activo_destino+')').text() == excelRow[positions.activo_destino] &&
                $(e).find('td:eq('+positions.sn_destino+')').text() == excelRow[positions.sn_destino]

            ){
                appendBool = true;
            }
        });

        if(appendBool)
            return false;
        
        var successCount = $('.nav-tabs li[data-type="success"] i').text();
        $('.nav-tabs li[data-type="success"] i').text(Number(successCount) + 1);

        $('.success-multiple tbody').append(
            $('<tr class="animated fadeIn">')
                .append( $('<td>',{
                    'html': index
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.activo_origen]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.sn_origen]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.denominacion_origen]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.noInventario_origen]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.fechaCapitalizacion_origen]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.valorAdquisicion_origen]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.activo_destino]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.sn_destino]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.denominacion_destino]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.noInventario_destino]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.fechaCapitalizacion_destino]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.valorTraslado]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.motivoTraslado]
                }) )
                .append( $('<td>',{
                    'html': excelRow[positions.observaciones]
                }) )
        );
    }
    /* error */
    function print_error(index,err){
        var errorCount = $('.nav-tabs li[data-type="error"] i').text();
        $('.nav-tabs li[data-type="error"] i').text(Number(errorCount) + 1);

        var html = '<div class="item" data-index="'+index+'"><h6 class="line">Fila '+index+'<span><i class="ion ion-bug"></i>'+(err.length > 1 ? err.length+' errores': err.length+' error')+'</span></h6><ul>';
        for( i = 0; i < err.length; i++ ){
            html += '<li>'+err[i]+'</li>';
        }
        html += '</ul></div>';
        $('.error-multiple').append(html);
    }
    /* validate */
    function validate(excelRow,rowLength,line){

        // console.log(excelRow);
        var positions = {
            activo_origen: 1,
            sn_origen: 2,
            denominacion_origen: 3,
            noInventario_origen: 4,
            fechaCapitalizacion_origen: 5,
            valorAdquisicion_origen: 6,
            activo_destino: 7,
            sn_destino: 8,
            denominacion_destino: 9,
            noInventario_destino: 10,
            fechaCapitalizacion_destino: 11,
            valorTraslado: 12,
            motivoTraslado: 13,
            observaciones: 14
        }

        var request = {
            activo_origen: excelRow[positions.activo_origen],
            sn_origen: excelRow[positions.sn_origen],
            activo_destino: excelRow[positions.activo_destino],
            sn_destino: excelRow[positions.sn_destino]
        }

        var url = 'include/cargaMasivaTraslados.php';
        $.getJSON(url, request, function(query){
            // console.log(query);
            var query_origen = query[0];
            var query_destino = query[1];
            var error_array = [];

            if(query.result != undefined){
                switch(query.result){
                    case 'invalid active':
                        error_array.push('El número de activo y subnúmero no existe en el auxiliar');
                    break;
                    case 'invalid format':
                        error_array.push('El formato de la fila es invalido');
                    break;
                    default:
                        error_array.push('unknow error, please contact with the administrator');
                }
            }
            else{

                if(String(excelRow[positions.noInventario_origen]).trim() != String(query_origen.inventario).trim())
                    error_array.push('El número de inventario del activo origen no coincide con el auxiliar');

                if(String(excelRow[positions.valorAdquisicion_origen]).trim() != String(query_origen.valor_adquisicion).trim())
                    error_array.push('El valor de adquisición del activo origen no coincide con el auxiliar');

                if(String(excelRow[positions.fechaCapitalizacion_origen]).trim() != String(query_origen.fecha_capitalizacion).trim())
                    error_array.push('La fecha de capitalización del activo origen no coincide con el auxiliar');

                if(String(excelRow[positions.noInventario_destino]).trim() != String(query_destino.inventario).trim())
                    error_array.push('El número de inventario del activo destino no coincide con el auxiliar');

                if(excelRow[positions.valorTraslado] > excelRow[positions.valorAdquisicion_origen])
                    error_array.push('El valor del traslado supera al valor de adquisición del activo origen');
                
                if(String(excelRow[positions.fechaCapitalizacion_destino]).trim() != String(query_destino.fecha_capitalizacion).trim())
                    error_array.push('La fecha de capitalización del activo destino no coincide con el auxiliar');

            }

            if(error_array.length){
                print_error(line, error_array);
            }
            else{
                print_success(line, excelRow , positions);
            }

            if(line == rowLength){
                $('#wait').fadeOut(400);
            }
        });
    }
    $(document).on('ready',function(){
        // excel file
            $('.upload-control').change(function(e){
                if(!$(this)[0].files.length){
                    return false;
                }

                var files = $(this)[0].files;
                var formData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    formData.append('excel', files[i]);
                }

                $('#wait').fadeIn(400);
                $('.multiple-view').fadeIn(300);
                $('.nav-tabs li[data-type="error"] i').text('0');
                $('.nav-tabs li[data-type="success"] i').text('0');
                $('.success-multiple tbody').html('');
                $('.error-multiple').html('');
                $('.single-view').hide();
                
                var url = 'include/cargaMasivaTraslados.php';
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        
                        var minRow = 7;
                        var maxRow = data.length - 2;

                        $.each(data,function(i,e){
                            if(i == 2 && String(e[0][3].trim()) != 'GFI-F-103'){
                                $('#wait').fadeOut(400);
                                print_error(1, ['formato inválido, utilice el formato GFI-F-103']);
                                return false;
                            }

                            if(i >= minRow && i < maxRow){
                                validate(e[0] , maxRow, i+1);
                            }
                        });
                    }
                });
            
            });
        //tabs
            $('.nav-tabs').on('click', 'li', function(e){
                e.preventDefault();
                if($(this).is('.active')){
                    return false;
                }
                var focus = $(this).parents('.nav-tabs').attr('data-tab');
                var ind = $(this).index();
                $(this).addClass('active');
                $(this).siblings('li').removeClass('active');

                $('.tabs[data-tab="'+focus+'"]').find('.tab').removeClass('active');
                $('.tabs[data-tab="'+focus+'"]').find('.tab:eq('+ind+')').stop().fadeIn(300,function(){
                    $(this).addClass('active').removeAttr('style');
                });
            });
        //error acordion
            $('.error-multiple').on('click','.line',function(){
                $(this).toggleClass('active');
                $(this).next('ul').stop().slideToggle(400);
            });
        // add to table
            $('.tab .btn-add').on('click', function(e){
                e.preventDefault();
                console.log('a');
            })
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);