(function($){
    var fileLocation = '';
    // get catalogo
        var catalogoJSON = [];
        $.getJSON("include/buscarCatalogo.php", function(data){
            catalogoJSON = data;
        });
    // get clases asociadas
        var clases = 0;
        $.getJSON('include/buscarClases.php', { 'type': 'json' }, function(data) {
            clases = data;
        });
    // get fabricantes
        var fabricantesJSON = [];
        $.getJSON("include/buscarFabricantes.php", function(data){
            fabricantesJSON = data;
        });
    // get cecos
        var cecosJSON = [];
        $.getJSON("include/buscarCecos.php", function(data){
            cecosJSON = data;
        });
    // get campos
        var camposJSON = [];
        $.getJSON("include/buscarCampos.php", function(data){
            camposJSON = data;
        });
    // get unidades asociadas
        var unidadesAsociadas = 0;
        $.getJSON('include/buscarUnidades.php', {'operacion': 'sap','type':'json'}, function(data) {
            unidadesAsociadas = data;
        });
    // get activo sunumero
        function getActivo(element, sn){
            var sn = sn || 0;
            var url = 'include/buscarPorActivo.php';
            var query = {
                get_option: element,
                subnum: sn
            }
            return $.post( url, query, function(data){
                return data;
            });
        }
    // get inventario
        function getInventario(element){
            var url = 'include/buscarPorInventario.php';
            var query = {
                get_option: element
            }
            return $.getJSON(url, query, function(data){
                return data;
                // console.log(data.count);
                // if(Number(data.count) > 0){
                //     return true;
                // }
                // else{
                //     return false;
                // }
            })
        }
    // get salario minimo
        var salary = 0;
        $.getJSON('include/buscarSalario.php', {'ano': new Date().getFullYear() }, function(data) {
            if(data.length){
                salary = Number(data[0].salario)/2;
            }
        });
    /* success */
        function print_success(index, excelRow, positions){
            var continueBool = false;
            var row = 0;
            var itemsPositions = [
                'operacion',
                'creacion',
                'activo',
                'subnumero',
                'clase',
                'denominacion',
                'denominacion2',
                'inventario',
                'fabricante',
                'modelo',
                'capacidad',
                'unidadMedida',
                'serie',
                'tag',
                'regCustodio',
                'custodio',
                'codMunicipio',
                'municipio',
                'codUbicacion',
                'ubicacion',
                'ceco',
                'vicepresidencia',
                'gerencia',
                'vidaUtilTotal',
                'costo',
                'codCampoPlanta',
                'campoPlanta',
                'matriculaVehiculo',
                'modeloVehiculo',
                'activoOrigen',
                'snOrigen',
                'fechaCapOrigen',
                'valorTraslado',
                'motivoTraslado',
                'observaciones'
            ];

            $.each($('.table-data').find('tbody tr'), function(i,e){
                if( 
                    String(excelRow[positions.inventario]).toUpperCase() == $(e).find('td:eq("9")').text() &&
                    String(excelRow[positions.activoOrigen]).toUpperCase() == $(e).find('td:eq("31")').text() &&
                    String(excelRow[positions.snOrigen]).toUpperCase() == $(e).find('td:eq("32")').text() 
                ){
                    continueBool = true;
                    row = i;
                    return false;
                }
            });

            if(continueBool){
                $.each( $('.table-data').find('tbody tr:eq('+row+') td'), function(i,e){
                    if( i > 1){
                        if( $(e).text() != excelRow[positions[itemsPositions[i-2]]] ){
                            $(e).text( String(excelRow[positions[itemsPositions[i-2]]]).toUpperCase() );
                        }
                    }
                });
            }
            else{
                var successCount = $('.table-data tbody tr').length;
                $('.nav-tabs li[data-type="success"] i').text(Number(successCount) + 1);

                var itemsAppend = '';
        
                for(var i = 0; i < (itemsPositions.length+2); i++){
                    itemsAppend += '<td>';
                    if(i == 0){
                        itemsAppend += '<span class="ion ion-trash-b remove-row"></span>';
                    }
                    else if(i == 1){
                        itemsAppend += $('.table-data tbody tr').length+1;
                    }
                    else{
                        itemsAppend += String(excelRow[positions[itemsPositions[i-2]]]).toUpperCase();
                    }
                    itemsAppend += '</td>';
                }
        
                $('.inline-table').find('.table tbody').prepend( $('<tr class="animated fadeIn">').append( itemsAppend ));
            }

            if(!$('.inline-table').is(':visible') && $('.nav-tabs li[data-type="success"]').is('.active')){
                $('.inline-table').fadeIn(400);
            }
        }
    /* error */
        function print_error(index,err){
            var errorCount = $('.nav-tabs li[data-type="error"] i').text();
            $('.nav-tabs li[data-type="error"] i').text(Number(errorCount) + 1);

            var html = '<div class="item" data-index="'+index+'"><h6 class="line">Fila '+index+'<span><i class="ion ion-bug"></i>'+(err.length > 1 ? err.length+' errores': err.length+' error')+'</span></h6><ul>';
            for( i = 0; i < err.length; i++ ){
                // console.log(err[i]);

                var options = '';

                if(err[i][0].match(/%%(.*)%%;/g)){
                    options = err[i][0].match(/%%(.*)%%;/g)[0];
                    options =  options.match(/[^%%|^%%;]*/g).filter(function(n){ return n != "" }) ;
                }

                var column = '';
                for( j = 1; j < err[i].length; j++ ){
                    column += err[i][j]+(j == err[i].length - 1 ? '': ';' );
                }

                if(err.length == 1){
                    html += '<li data-column="'+column+'">'+err[0][0].replace(/%%(.*)%%;/g, '')+'</li>';
                }
                else{
                    html += '<li data-column="'+column+'">'+err[i][0].replace(/%%(.*)%%;/g, '')+'</li>';
                }

                if(options != '' && options.length){
                    html += '<ol>';
                    for(var j = 0; j < options.length; j++){
                        html += '<li>'+options[j]+'</li>';
                    }
                    html += '</ol>';
                }
            }
            html += '</ul></div>';
            $('.error-multiple').append(html);
        }
    /* validate */
        function validate(excelRow, rowLength, line){
            console.time('query of masive');
            for(var i=0; i<excelRow.length; i++){
                if(excelRow[i] == null){
                    excelRow[i] = '';
                }
            }
            // console.log(excelRow);

            var positions = {
                operacion: 1,
                creacion: 2,
                activo: 3,
                subnumero: 4,
                clase: 5,
                denominacion: 6,
                denominacion2: 7,
                serie: 8,
                inventario: 9,
                capacidad: 10,
                unidadMedida: 11,
                tag: 12,
                ceco: 13,  
nombreCeco: 14,
                vicepresidencia: 15,
                gerencia: 16,
                regCustodio: 17,
                custodio: 18,
                codUbicacion: 19,
                ubicacion: 20,
                codCampoPlanta: 21,
                campoPlanta: 22,
                codMunicipio: 23,
                municipio: 24,
                fabricante: 25,
                modelo: 26,
                vidaUtilTotal: 27,
                costo: 28,
                matriculaVehiculo: 29,
                modeloVehiculo: 30,
                activoOrigen: 31,
                snOrigen: 32,
                fechaCapOrigen: 33,
                valorTraslado: 34,
                motivoTraslado: 35,
                observaciones: 36
            }

            var error_array = [];

            if(excelRow[positions.creacion] == "" || excelRow[positions.creacion] == null){
                $('#wait').fadeOut(400);
                return false;
            }

            function printErrorSuccess(){
                testIfExist();

                if(error_array.length){
                    print_error(line, error_array);
                }
                else{
                    print_success(line, excelRow , positions);
                }
                
                if(line >= rowLength){
                    console.timeEnd('query of masive');
                    $('#wait').fadeOut(400);
                }

            }

            function testIfExist(){
                var inventario = excelRow[positions.inventario];
                var activoOrigen = excelRow[positions.activoOrigen];
                var snOrigen = excelRow[positions.snOrigen];

                var inventarioExist = false;

                if(inventario != "" && activoOrigen == "" && snOrigen == ""){
                    $.each($('.table-data tbody tr'), function(i,el){
                        if($(el).find('td:eq('+positionValidation.inventario+')').text().trim() == inventario){
                            inventarioExist = true;
                            return false;
                        }
                    })
                }

                if(inventarioExist){
                    error_array.push(['Ya se asigno un activo para este número de inventario', positions.inventario]); 
                }
            }
            // posiciones de la tabla globales
                var positionValidation = {
                    inventario:     9,
                    activoOrigen:   31,
                    snOrigen:       32,
                    costo:          26,
                    valorTraslado:  34
                }
            // verificar denominación 2
                var regex = /^[a-zA-Z0-9_ ]+$/g;
                if(!regex.test(excelRow[positions.denominacion2])){
                    error_array.push(['La denominación complementaria debe tener caracteres alfa numéricos', positions.denominacion2]); 
                }
            // verificar activo, sn de traslado, fecha de capitalización de traslado
                function trasladoAjax(data){
                    if(excelRow[positions.activoOrigen] != '' || data == 'translate'){
                    
                        var url = 'include/consultasTraslados.php';
                        var query = {
                            'id_numero_activo': excelRow[positions.activoOrigen],
                            'id_subnumero_activo': excelRow[positions.snOrigen]
                        }
                        var translateValue = Number(String(excelRow[positions.valorTraslado]).replace(/[$\.]/g, "").replace(/\,/g,'.'));
                        var costValue = Number(String(excelRow[positions.costo]).replace(/[$\.]/g, "").replace(/\,/g,'.'));
                        var inventario = String(excelRow[positions.inventario]).trim();

                        return $.getJSON(url, query, function(data){
                            if( excelRow[positions.activoOrigen] == "" && excelRow[positions.snOrigen] == ""){
                                error_array.push(['Asigne valores de traslado para este activo', positions.activoOrigen, positions.snOrigen, positions.fechaCapOrigen, positions.valorTraslado, positions.motivoTraslado]);
                            }
                            else if(data.result){
                                error_array.push(['El activo y subnúmero del traslado no existe', positions.activoOrigen, positions.snOrigen]);
                            }
                            else{
                                // console.log(data);
                                $.each(data, function(i,e) {
                                    if(
                                        e.numero_activo == excelRow[positions.activoOrigen] &&
                                        e.sn == excelRow[positions.snOrigen]
                                    ){
                                        // verificar fecha de capitalización
                                            var dateCap = String(excelRow[positions.fechaCapOrigen]).replace(/[\.]/g, '/');
                                            if(e.fecha_capitalizacion != dateCap){
                                                error_array.push(['La fecha de capitalización debería ser: '+ e.fecha_capitalizacion, positions.fechaCapOrigen]);
                                            }
                                        // verificar valor de traslado
                                            if(translateValue > Number(e.valor_adquisicion)){
                                                error_array.push(['El valor máximo de traslado debería ser: '+ e.valor_adquisicion, positions.valorTraslado]);
                                            }
                                        // verificar motivo de traslado

                                            var regexArray = [
                                                'INVENTARIO',
                                                'DESAGREGACIÓN ASOCIADAS',
                                                'CORRECCIÓN DE CAPITALIZACIÓN'
                                            ]
                                            var regexBool = false;
                    
                                            for(var i = 0; i<regexArray.length; i++){
                                                var regex = new RegExp('^\\b'+regexArray[i]+'\\b' , 'g');
                                                var value = String(excelRow[positions.motivoTraslado]).trim();
                    
                                                regex.test( value ) ? regexBool = true: '';
                                            }
                    
                                            var options = '';
                                            for(var i = 0; i<regexArray.length; i++){
                                                options += '%%'+regexArray[i]+'%%;';
                                            }
                    
                                            !regexBool ? error_array.push(['El motivo del traslado debe coincidir con alguna de las siguientes opciones: '+options, positions.motivoTraslado]) : '';

                                        // verifica la suma de los traslados a un respectivo código de inventario
                                            var translatesActives = {};
                                            var elementExist = false;

                                            $.each($('.table-data tbody tr'), function(i,el){
                                                if(
                                                    e.numero_activo == $(el).find('td:eq('+positionValidation.activoOrigen+')').text().trim() &&
                                                    e.sn == $(el).find('td:eq('+positionValidation.snOrigen+')').text().trim()
                                                )
                                                {
                                                    elementExist = true;
                                                    return false;
                                                }
                                                var translateValue = Number($(el).find('td:eq('+positionValidation.valorTraslado+')').text().replace(/[$\.]/g,''));
                                                if( translateValue > 0){
                                                    var inventario = $(el).find('td:eq('+positionValidation.inventario+')').text().trim();
                                                    var costo = $(el).find('td:eq('+positionValidation.costo+')').text().replace(/[$\.]/g,'');
                                                    var traslado = $(el).find('td:eq('+positionValidation.valorTraslado+')').text().replace(/[$\.]/g,'');

                                                    var json = {
                                                        costo: Number(costo),
                                                        valorTraslado: Number(traslado)
                                                    }

                                                    translatesActives[inventario] == undefined ? translatesActives[inventario] = [] : '';
                                                    translatesActives[inventario].push(json);
                                                }
                                            });

                                            if(!elementExist){
                                                var countTranslate = translateValue;
    
                                                $.each(translatesActives[inventario], function(i,e){
                                                    countTranslate += e.valorTraslado;
                                                });
    
                                                var cost = costValue;
                                                if(countTranslate > cost){
                                                    error_array.push(["La suma de los traslados no debe superar el valor del costo ("+cost+") del nuevo inventario ("+inventario+").", positions.valorTraslado]);
                                                    return false;
                                                }
                                            }



                                    }
                                })


                            }
                        });
                    }
                    else{
                        return false;
                    }
                }
            // verificar fabricante
                if( excelRow[positions.fabricante].trim() == "" ){
                    error_array.push(['El campo fabricante debe estar diligenciado con una marca o con NO REGISTRA', positions.fabricante]); 
                }
            // verificar modelo
                var regex = /^[a-zA-Z0-9ñÑ +-_]*$/g;
                var model = excelRow[positions.modelo].trim();
                if( model == "" && !regex.test(model)){
                    error_array.push(['El campo modelo debe estar diligenciado con algun dato alfanumerico o con NO REGISTRA', positions.modelo]); 
                }
                else if( model[0] == "+" || model[0] == "-"){
                    error_array.push(['El campo modelo debe empezar con _ para que no quede como fórmula', positions.modelo]); 
                }
            // verificar serie
                var regex = /^[a-zA-Z0-9ñÑ +-_]*$/g;
                var serie = excelRow[positions.serie].trim();
                if( serie == "" && !regex.test(serie)){
                    error_array.push(['El campo serie debe estar diligenciado con algun dato alfanumerico o con NO REGISTRA', positions.serie]); 
                }
                else if( serie[0] == "+" || serie[0] == "-"){
                    error_array.push(['El campo serie debe empezar con _ para que no quede como fórmula', positions.serie]); 
                }
            // verificar costo del activo
                var costo = Number(String(excelRow[positions.costo]).trim().replace(/[$\.]/g, ""));
                var regex = /^[0-9]/g;
                if(!regex.test(costo) || costo <= salary){
                    error_array.push(['Verifique el costo del activo, debe ser un valor numérico mayor a '+salary, positions.costo]);
                }
            // verificar capacidad
                var regex = /^[0-9]+(\.[0-9]{0,3})?$/g;
                var capacity = Number(String(excelRow[positions.capacidad]).trim().replace(',','.'));
                if( !regex.test(capacity) ){
                    error_array.push(['El campo de capacidad debe ser numérico y tener como máximo 3 decimales', positions.capacidad]); 
                }
            // verificar cod campo planta y nombre
                var ceco = String(excelRow[positions.ceco]).trim();
                var regex = /^PR/gi;
                var campo = String(excelRow[positions.codCampoPlanta]).trim();
                var confirmCampo = false;

                if(regex.test(ceco) && campo == ""){
                    error_array.push(['El código de campo planta no debe estar vacio para un CeCo de producción, verifique el código', positions.codCampoPlanta, positions.campoPlanta]);
                }
                if(campo != ""){
                    $.each(camposJSON, function(i,e){
                        if(e.codigo == campo){
                            excelRow[positions.campoPlanta] != e.nombre ? excelRow[positions.campoPlanta] = e.nombre : '';
                            confirmCampo = true;
                            return false;
                        }
                    });
                    if(!confirmCampo){
                        error_array.push(['El código de campo planta no existen, verifique el código', positions.codCampoPlanta]);
                    }
                }
            // verificar matricula de vehiculo y año
                if(String(excelRow[positions.matriculaVehiculo]).trim() != ""){
                    var matricula = String(excelRow[positions.matriculaVehiculo]).trim();
                    // console.log(matricula);
                    var regex = /^[a-zA-Z0-9-]*$/g;
                    if(matricula.length > 7 || !regex.test(matricula)){
                        error_array.push(['La matricula del vehículo debe ser alfanumérico y contener máximo 7 caracteres', positions.matriculaVehiculo]);
                    }
                    var modelo = Number(excelRow[positions.modeloVehiculo]);
                    if( modelo < 1980 || modelo > (new Date().getFullYear() + 1) ){
                        error_array.push(['El modelo del vehículo debe estar entre 1980 y '+ (new Date().getFullYear() + 1), positions.modeloVehiculo]);
                    }
                }
            // verificar vicepresidencia y gerencia
                function jerarquizacionAjax(){
                    return $.ajax({
                        url: 'include/buscarPorCeco.php',
                        type: 'GET',
                        data: { 
                            get_option: excelRow[positions.ceco]
                        },
                        dataType: 'JSON',
                        async: true,
                        success: function(data){
                            if(data.result != undefined){
                                error_array.push(['El CeCo no existe dentro de jerarquización por favor verifique el código', positions.ceco]);
                            }
                            else{
                                if(excelRow[positions.vicepresidencia] != data.vicepresidencia){
                                    excelRow[positions.vicepresidencia] = data.vicepresidencia;
                                    // error_array.push(['La vicepresidencia debería ser: '+ data.vicepresidencia, positions.vicepresidencia]);
                                }
                                if(excelRow[positions.gerencia] != data.gerencia){
                                    excelRow[positions.gerencia] = data.gerencia;
                                    // error_array.push(['El gerencia debería ser: '+ data.gerencia, positions.gerencia]);
                                }
                            }
                            
                            // console.log(line,excelRow);
                        }
                    });
                } 
            // verificar codigo de municipio y ubicacion geografica
                function ubicacionAjax(){
                    var codMunicipio = excelRow[positions.codMunicipio];
                    var codUbicacion = excelRow[positions.codUbicacion];
                    var url = "include/cargaMasivaCreaciones.php";
                    var query = {
                        type: 'ubicacion',
                        municipio: codMunicipio,
                        ubicacion: codUbicacion
                    }
                    return $.ajax({
                        url: url,
                        type: 'GET',
                        data: query,
                        dataType: 'JSON',
                        async: true,
                        success: function(data){
                            if(data.result != undefined){
                                error_array.push(['El código de ubicación geográfica y municipio no son congruentes, verifique los códigos', positions.codMunicipio, positions.codUbicacion]);
                            }
                            else{
                                excelRow[positions.municipio] != data[0].municipio ? excelRow[positions.municipio] = data[0].municipio : '';
                                excelRow[positions.ubicacion] != data[0].ubicacion ? excelRow[positions.ubicacion] = data[0].ubicacion : '';
                            }
                        }
                    });
                }
            
            switch(excelRow[positions.creacion].toLowerCase().trim()){
                case 'principal':
                    // verificar # de inventario
                        var classCode = Number(excelRow[positions.clase]);
                        // para 11001, 11002, 33006 - terrenos debe tener al final _
                        if( classCode == 11001 || classCode == 11002 || classCode == 33006 )
                        {
                            var regex = /^[a-zA-Z0-9\/_]*$/;
                            var inventario = String(excelRow[positions.inventario]).trim();
                            if( 
                                !regex.test(inventario) && 
                                inventario.length > 25 
                            ){
                                error_array.push(['Verifique el número de matrícula inmobiliaria, debe finalizar con _', positions.inventario]);
                            }
                        }
                        // para clase 13001 recibe n letras y números sin espacio 
                        else if( classCode == 13001 )
                        {
                            var regex = /^[a-zA-Z0-9_]*$/;
                            var inventario = String(excelRow[positions.inventario]).trim();
                            if( 
                                !regex.test(inventario) && 
                                inventario.length > 25 
                            ){
                                error_array.push(['Verifique el número de inventario solo recibe caracteres alfa-númericos y _', positions.inventario]);
                            }
                        }
                        // 33001 - 33005 n longitud de digitos numericos con _ al final
                        else if( classCode == 33001 || classCode == 33002 || classCode == 33003 || classCode == 33004 || classCode == 33005 )
                        {
                            var regex = /^[a-zA-Z0-9 _]*$/;
                            var inventario = String(excelRow[positions.inventario]).trim();
                            if( 
                                inventario.toLowerCase() == ""
                            ){
                                error_array.push(['El inventario debe contener algún dato o NO APLICA en su defecto', positions.inventario]);
                            }
                        }
                    // verificar tag
                        var regex = /^[a-zA-Z0-9ñÑ +-_]*$/g;
                        var tag = excelRow[positions.tag].trim();
                        if( tag == "" && !regex.test(tag)){
                            error_array.push(['El campo tag debe estar diligenciado con algun dato alfanumerico o con NO REGISTRA', positions.tag]); 
                        }
                        else if( tag[0] == "+" || tag[0] == "-"){
                            error_array.push(['El campo tag debe empezar con _ para que no quede como fórmula', positions.tag]); 
                        }
                    // verificar registro de custodio y nombre
                    // verificar codigo ceco
                        var confirmCeco = false;
                        $.each(cecosJSON, function(i,e){
                            if(e.ceco == excelRow[positions.ceco]){
                                confirmCeco = true;
                                return false;
                            }
                        })
                        if(!confirmCeco){
                            error_array.push(['El código de CeCo no existen, verifique el código', positions.ceco]);
                        }
                    
                    // verificar vida útil total
                        var vidaUtil = Number(String(excelRow[positions.vidaUtilTotal]).trim());
                        var regex = /^[0-9]{1,3}/g
                        if(!regex.test(vidaUtil)){
                            error_array.push(['La vida útil total no debe ser mayor a 999', positions.vidaUtilTotal]);
                        }
                    if(excelRow[positions.operacion].toLowerCase().trim() == 'directa'){
                        // verificar código de clase, denominación y unidad de medida
                            var verifyCatalogo = false;
                            $.each(catalogoJSON, function(i,e){
                                if( 
                                    e.clase == excelRow[positions.clase] && 
                                    e.unidad == excelRow[positions.unidadMedida]
                                )
                                {
                                    var breakCatalogoWord = String(e.label).toLowerCase().split(' ');
                                    var breakExcelWord = String(excelRow[positions.denominacion]).toLowerCase().split(' ');
                                    if(breakCatalogoWord[0] == breakExcelWord[0]){
                                        verifyCatalogo = true;
                                    }
                                }
                            });
                            if(!verifyCatalogo){
                                error_array.push(['Verificar coungrencia de clase, denominación y unidad de medida en los datos del catálogo ', positions.clase, positions.denominacion, positions.unidadMedida]);    
                            }
                        
                    }
                    else if(excelRow[positions.operacion].toLowerCase().trim() == 'asociada'){
                        // verificar denominación
                            var regex = /^[a-zA-Z0-9_ ]*$/g;
                            if(!regex.test(excelRow[positions.denominacion])){
                                error_array.push(['Solo permite valores alfa-númericos y _', positions.denominacion]); 
                            }
                        // verificar clases asociadas
                            var clase = String(excelRow[positions.clase]).trim();
                            var confirmClase = false;
                            $.each(clases,function(i,e){
                                if(e.clase == clase){
                                    confirmClase = true;
                                    return false;
                                }
                            });
                            !confirmClase ? error_array.push(['verifique el número de clase permitido dentro de catálogo', positions.clase]): '';
                        // verificar unidades asociadas
                            var unidad = excelRow[positions.unidadMedida];
                            var confirmUnidad = false;
                            $.each(unidadesAsociadas,function(i,e){
                                if(e.unidad == unidad){
                                    confirmUnidad = true;
                                    return false;
                                }
                            });
                            !confirmUnidad ? error_array.push(['verifique la unidad de medida permitida para asociadas', positions.unidadMedida]): '';
                    }
                    else{
                        error_array.push(['Debe especificar si la operación es DIRECTA o ASOCIADA', positions.operacion]);
                    }

                    $.when( getInventario(excelRow[positions.inventario]), ubicacionAjax(), jerarquizacionAjax(), trasladoAjax(),  ).done(function(data) {
                        var data = data[0];
                        if( data.count > 0){
                            error_array.push(['El código de inventario ya se encuentra asignado a otro activo', positions.inventario])
                        }
                        printErrorSuccess();
                    });
                break;
                case 'subnumero':
                case 'subnúmero':
                    // verificar denominación
                        var regexArray = [
                            'MAYOR VALOR SUBREPARTO',
                            'MAYOR VALOR COSTOS FINANCIEROS',
                            'MAYOR VALOR COSTO DE ADQUISICION',
                            'MAYOR VALOR SERVICIO DE PERFORACION',
                            'MAYOR VALOR SERVICIO DE WORKOVER',
                            'MAYOR VALOR OBRAS CIVÍLES',
                            'ADICION \\+ ACCESORIOS',
                            'ADICION \\+ CAMPO',
                            'ADICION \\+ GESTION CONTRATO',
                            'ADICION \\+ GESTORIA \\+ TIPO DE GESTORIA (TECNICA, INTERVENTORIA, ETC)',
                            'ADICION \\+ AMPLIACION',
                            'ADICION \\+ EJECUCION CONTRATO',
                            'ADICION \\+ MATERIALES',
                            'ADICION \\+ LINEA DE FLUJO (EL VALOR DE MAS SE DEBE SUMAR A LA CAPACIDAD DEL ACTIVO PRINCIPAL)',
                            'AUMENTO DE LA VIDA ÚTIL',
                            'AMPLIACIÓN DE LA PRODUCTIVIDAD',
                            'REDUCCIÓN SIGNIFICATICA DE COSTOS DE OPERACIÓN',
                            'REEMPLAZO DE PARTE O COMPONENTE'
                        ]
                        var regexBool = false;

                        for(var i = 0; i<regexArray.length; i++){
                            var regex = new RegExp('^\\b'+regexArray[i]+'\\b' , 'g');
                            var value = String(excelRow[positions.denominacion]).trim();

                            regex.test( value ) ? regexBool = true: '';
                        }

                        var options = '';
                        for(var i = 0; i<regexArray.length; i++){
                            options += '%%'+String(regexArray[i]).replace(/[\\]/g,'')+'%%;';
                        }

                        !regexBool ? error_array.push(['La denominación debe coincidir con alguna de las siguientes opciones: '+options, positions.denominacion]) : '';
                    
                    if(excelRow[positions.activo] != "" && excelRow[positions.activo] != null){
                        $.when( getActivo(excelRow[positions.activo]), trasladoAjax(), jerarquizacionAjax() ).done(function(data) {
                            data = data[0];
    
                            if(data.result != undefined){
                                error_array.push(['El activo no existe en el auxiliar', positions.activo]);
                            }
                            else{
                                // verificar operacion
                                    var operacion = String(excelRow[positions.operacion]).toLowerCase().trim();
                                    if(String(data.tipoOperacion).toLowerCase() != operacion){
                                        error_array.push(['El tipo de operación debe ser: '+String(data.tipoOperacion).toUpperCase(), positions.operacion]);
                                    }
                                // verificar código de clase
                                    var codClase = String(excelRow[positions.clase]).toLowerCase().trim();
                                    if(String(data.clase).toLowerCase() != codClase){
                                        error_array.push(['El código de clase debe ser: '+String(data.clase).toUpperCase(), positions.clase]);
                                    }
                                // verificar número de inventario
                                    var inventario = String(excelRow[positions.inventario]).toLowerCase().trim();
                                    if(String(data.inventario).toLowerCase() != inventario){
                                        error_array.push(['El número de inventario debe ser: '+String(data.inventario).toUpperCase(), positions.inventario]);
                                    }
                                // verificar vida útil total
                                    // console.log(data);
                                    if( Number(data.vidaUtilRemanente) <= 0){
                                        error_array.push(['La vida útil remanente del activo principal es 0 por favor actualizar la vida útil antes de hacer creaciones para este activo', positions.vidaUtilTotal]);
                                    }
                                    else{
                                        var vidaUtil = Number(excelRow[positions.vidaUtilTotal]);
                                        if( vidaUtil > Number(data.vidaUtilRemanente) ){
                                            error_array.push(['La vida util total en meses debe ser menor o igual a: '+String(data.vidaUtilRemanente).toUpperCase(), positions.vidaUtilTotal]);
                                        }
                                        if( vidaUtil < 1){
                                            error_array.push(['La vida util total en meses debe ser mayor a 1 mes', positions.vidaUtilTotal]);
                                        }
                                    }
                                // verificar unidad de medida
                                    var unidadMedida = String(excelRow[positions.unidadMedida]).toUpperCase().trim();
                                    if(unidadMedida != ""){
                                        if( unidadMedida != "UN" && unidadMedida != String(data.unidadMedida).toUpperCase() ){
                                            var u = data.unidadMedida != "" ? 'ó '+data.unidadMedida : '';
                                            error_array.push(['La unidad de medida debe ser UN '+ u, positions.unidadMedida]);    
                                        }
                                    }
                                    else{
                                        error_array.push(['La unidad de medida debe contener algun dato', positions.unidadMedida]);
                                    }
                                // verificar tag
                                    var tag = String(excelRow[positions.tag]).toLowerCase().trim();
                                    if(String(data.tag).toLowerCase() != tag){
                                        error_array.push(['El tag debe ser: '+String(data.tag).toUpperCase(), positions.tag]);
                                    }
                                // verificar ubicación
                                    var codMunicipio = String(excelRow[positions.codMunicipio]).toUpperCase().trim();
                                    var codUbicacion = String(excelRow[positions.codUbicacion]).toUpperCase().trim();
                                    var nombreMunicipio = String(excelRow[positions.municipio]).toUpperCase().trim();
                                    var nombreUbicacion = String(excelRow[positions.ubicacion]).toUpperCase().trim();
                                    if(data.municipio != codMunicipio){
                                        error_array.push(['El código del municipio debería ser: '+ data.municipio, positions.codMunicipio]);
                                    }
                                    if(data.ubicacionGeografica != codUbicacion){
                                        error_array.push(['El código de ubicación geográfica debería ser: '+ data.ubicacionGeografica, positions.codUbicacion]);
                                    }
                                    if( data.nombreMunicipio != nombreMunicipio ){
                                        excelRow[positions.municipio] = data.nombreMunicipio;
                                    }
                                    if( data.nombreUbicacionGeografica != nombreUbicacion ){
                                        excelRow[positions.ubicacion] = data.nombreUbicacionGeografica;
                                    }
                            }
    
                            printErrorSuccess();
                        });
                    }
                    else{
                        // verificar los campos del subnúmero si se trata de un traslado
                        $.when( getInventario(excelRow[positions.inventario]), ubicacionAjax(), trasladoAjax('translate'), jerarquizacionAjax() ).done(function(data) {
                            
                            var data = data[0];
                            if( data.count > 0){
                                error_array.push(['El código de inventario ya se encuentra asignado a otro activo', positions.inventario])
                            }
                            // verificar operacion
                                if(
                                    excelRow[positions.operacion].toLowerCase().trim() != 'directa' &&
                                    excelRow[positions.operacion].toLowerCase().trim() != 'asociada'
                                ){
                                    error_array.push(['Debe especificar si la operación es DIRECTA o ASOCIADA', positions.operacion]);
                                }
                            // verificar código de clase
                                var clase = String(excelRow[positions.clase]).trim();
                                var confirmClase = false;
                                $.each(clases,function(i,e){
                                    if(e.clase == clase){
                                        confirmClase = true;
                                        return false;
                                    }
                                });
                                !confirmClase ? error_array.push(['verifique el número de clase permitido dentro de catálogo', positions.clase]): '';
                            // verificar vida útil total
                                var vidaUtil = Number(String(excelRow[positions.vidaUtilTotal]).trim());
                                var regex = /^[0-9]{1,3}/g
                                if(!regex.test(vidaUtil)){
                                    error_array.push(['La vida útil total no debe ser mayor a 999', positions.vidaUtilTotal]);
                                }
                            // verificar unidad de medida
                                if( excelRow[positions.operacion].toLowerCase().trim() != 'directa' ){
                                    var verifyCatalogo = false;
                                    $.each(catalogoJSON, function(i,e){
                                        if( 
                                            e.clase == excelRow[positions.clase] && 
                                            e.unidad == excelRow[positions.unidadMedida]
                                        )
                                        {
                                            verifyCatalogo = true;
                                        }
                                    });
                                    if(!verifyCatalogo){
                                        error_array.push(['Verificar coungrencia de clase y unidad de medida en los datos del catálogo ', positions.clase, positions.unidadMedida]);    
                                    }
                                }
                                if( excelRow[positions.operacion].toLowerCase().trim() != 'asociada' ){
                                    var unidad = excelRow[positions.unidadMedida];
                                    var confirmUnidad = false;
                                    $.each(unidadesAsociadas,function(i,e){
                                        if(e.unidad == unidad){
                                            confirmUnidad = true;
                                            return false;
                                        }
                                    });
                                    !confirmUnidad ? error_array.push(['verifique la unidad de medida permitida para asociadas', positions.unidadMedida]): '';
                                }
                                
                            // verificar tag
                                var regex = /^[a-zA-Z0-9ñÑ +-_]*$/g;
                                var tag = excelRow[positions.tag].trim();
                                if( tag == "" && !regex.test(tag)){
                                    error_array.push(['El campo tag debe estar diligenciado con algun dato alfanumerico o con NO REGISTRA', positions.tag]); 
                                }
                                else if( tag[0] == "+" || tag[0] == "-"){
                                    error_array.push(['El campo tag debe empezar con _ para que no quede como fórmula', positions.tag]); 
                                }


                            printErrorSuccess();
                        });
                    }
                break;
                default:
                    error_array.push(['Debe especificar si la creación es PRINCIPAL o SUBNÚMERO', positions.creacion]);
                    printErrorSuccess();
                break;
            }
        }
    /* add to table */
        function add2Table(element){
            var variables = {
                activo: $(element).find('td:eq(1)').text(),
                denominacion: $(element).find('td:eq(2)').text(),
                inventario: $(element).find('td:eq(3)').text(),
                serie: $(element).find('td:eq(4)').text(),
                clase: $(element).find('td:eq(5)').text(),
                cod_municipio: $(element).find('td:eq(6)').text(),
                municipio: $(element).find('td:eq(7)').text(),
                cod_ubicacion_geografica: $(element).find('td:eq(8)').text(),
                ubicacion_geografica: $(element).find('td:eq(9)').text(),
                ceco: $(element).find('td:eq(10)').text(),
                cod_campo_planta: $(element).find('td:eq(11)').text(),
                campo_planta: $(element).find('td:eq(12)').text(),
                vida_util_total: $(element).find('td:eq(13)').text(),
                vicepresidencia: $(element).find('td:eq(14)').text(),
                gerencia: $(element).find('td:eq(15)').text(),
                observaciones: $(element).find('td:eq(16)').text()
            }
            $('table.table-data tbody').each(function(i,e){
                $(e).append(
                    $('<tr>')
                    .append('<td><span class="ion ion-trash-b remove-row"></span></td')
                    .append( $('<td>').text( variables.activo ) )
                    .append( $('<td>').text( variables.denominacion ) )
                    .append( $('<td>').text( variables.inventario ) )
                    .append( $('<td>').text( variables.serie ) )
                    .append( $('<td>').text( variables.clase ) )
                    .append( $('<td>').text( '' ) )
                    .append( $('<td>').text( variables.cod_municipio ) )
                    .append( $('<td>').text( variables.cod_ubicacion_geografica ) )
                    .append( $('<td>').text( variables.vicepresidencia ) )
                    .append( $('<td>').text( variables.gerencia ) )
                    .append( $('<td>').text( variables.ceco ) )
                    .append( $('<td>').text( variables.cod_campo_planta ) )
                    .append( $('<td>').text( variables.vida_util_total ) )
                    .append( $('<td>').text( variables.observaciones ) )
                )
            });
        }
    $(document).on('ready',function(){
        // nav view selector
            $('.nav-view .btn').on('click',function(e){
                $(this).siblings('.btn').removeClass('active');
                $(this).addClass('active');

                if($(this).is('.btn-single')){
                    $('.multiple-view').stop().hide();
                    $('.single-view').stop().fadeIn(400);
                }
                if($('.inline-table tbody tr').length && $('.nav-tabs li[data-type="success"]').is('.active'))
                    $('.inline-table').stop().fadeIn(400);
            });
        // excel file
            $('.upload-control').change(function(e){
                if(!$(this)[0].files.length){
                    return false;
                }

                if($('.nav-tabs li[data-type="error"]').is('.active')){
                    $('.inline-table,.show-list').hide();
                }

                var files = $(this)[0].files;
                var formData = new FormData();

                for (var i = 0; i < files.length; i++) {
                    formData.append('excel', files[i]);
                }

                $('#wait').fadeIn(400);
                $('.multiple-view').fadeIn(300);
                $('.nav-tabs li[data-type="error"] i').text('0');
                $('.nav-tabs li[data-type="success"] i').text('0');
                $('.success-multiple tbody').html('');
                $('.error-multiple').html('');
                $('.single-view').hide();
                
                var url = 'include/cargaMasivaCreaciones.php';
                $.ajax({
                    url: url,
                    type: 'POST',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        // console.log(data);
                        // return false;
                        fileLocation = data[data.length - 1];
                        data.splice(data.length - 1, 1);

                        var minRow = 7;
                        var maxRow = data.length - 4;

                        $.each(data,function(i,e){
                            if(i == 3 && String(e[0][3]).trim() != 'GFI-F-049'){
                                $('#wait').fadeOut(400);
                                print_error(1, ['formato inválido, utilice el formato GFI-F-049']);
                                return false;
                            }

                            if(i >= minRow && i < maxRow){
                                // console.log(e[0]);
                                // $('#wait').fadeOut(400);
                                validate(e[0] , maxRow, i+1);
                            }
                        });

                        $('.upload-control').val('');
                    }
                });
            
            });
        // tabs
            $('.nav-tabs').on('click', 'li', function(e){
                e.preventDefault();
                if($(this).is('.active')){
                    return false;
                }
                var focus = $(this).parents('.nav-tabs').attr('data-tab');
                var ind = $(this).index();
                $(this).addClass('active');
                $(this).siblings('li').removeClass('active');

                $('.tabs[data-tab="'+focus+'"]').find('.tab').removeClass('active');
                $('.tabs[data-tab="'+focus+'"]').find('.tab:eq('+ind+')').stop().fadeIn(300,function(){
                    $(this).addClass('active').removeAttr('style');
                });

                // console.log($(this).attr('data-type'));
                if($(this).attr('data-type') == 'error'){
                    $('.inline-table,.show-list').stop().hide();
                }
                else{
                    $('.inline-table,.show-list').stop().fadeIn(300);
                }
            });
        // error acordion
            $('.error-multiple').on('click','.line',function(){
                $(this).toggleClass('active');
                $(this).next('ul').stop().slideToggle(400);
            });
        // download errors
            $('.btn-error').on('click',function(event){
                event.preventDefault();
                if($('.error-multiple .item').length){
                    $('#wait').fadeIn(400);
                    var errJSON = {};
                    $('.error-multiple .item').each(function(i,e){
                        var row = $(e).data('index');
                        errJSON[row] = [];
                        $(e).find('> ul > li').each(function(j,c){
                            var regex = /[;]/g;
                            var columnData = regex.test($(c).data('column')) ? String($(c).data('column')).split(';') : $(c).data('column');
                            var errorText = $(c).text();
                            if($(c).next('ol').length){
                                for(var i = 0; i < $(c).next('ol').find('li').length; i++){
                                    errorText += '\n'+$(c).next('ol').find('li:eq('+i+')').text();
                                }
                            }
                            if(columnData.length){
                                for(var i=0; i<columnData.length; i++){
                                    var obj = {
                                        column: Number(columnData[i]),
                                        error: errorText
                                    }
                                    errJSON[row].push(obj);
                                }
                            }
                            else{
                                var obj = {
                                    column: columnData,
                                    error: errorText
                                }
                                errJSON[row].push(obj);
                            }
                        });
                    });
                    
                    // console.log(errJSON);
                    
                    var url = 'include/downloadErrors.php';
                    var postQuery = {
                        path: fileLocation,
                        errors: errJSON,
                        format: 'creaciones'
                    }
                    // console.log(fileLocation, 'file');

                    $.post(url, postQuery, function(data){
                        // console.log(data);
                        // $('#wait').fadeOut(400);
                        // return false;
                        var fileName = 'ERROR-GFI-F-049-FORMATO CREACIÓN DE DATOS MAESTROS DE ACTIVOS FIJOS.'+data.ext;
                        var $a = $("<a>");
                        $a.attr("href","include/"+data.file);
                        $("body").append($a);
                        $a[0].setAttribute("download",fileName);
                        $a[0].click();
                        $a.remove();
                        $('#wait').fadeOut(400);
                    });
                    // $.each(errJSON, function(i,e){
                    //     console.log(i);
                    //     console.log(e);
                    // })
                    // console.log(fileLocation);
                }
            });
        // add to table
            // $('.tab .btn-add').on('click', function(e){
            //     e.preventDefault();
            //     $('.success-multiple tbody tr').each(function(i,e){
            //         add2Table(e);
            //     });
            // });
    });

    $(window).on('load',function(){
        // console.log('load');
    });

    $(window).resize(function(){
        // console.log('resize');
    });
})(jQuery);