$(function(){
    /* Si la tabla no tiene filas, la oculta */
    var rowCount = $("#tableBox > tbody > tr").length;
    if(rowCount === 0){
        $("#tableBox").hide();
    }
    
    /* Listeners sobre selectores y botones */
        /* Al dar clic en el botón agregar, muestra la tabla y agrega una fila con los datos del formulario */
        $( "#btn_agregar" ).click(function() {
            if($("#grupo").val() != "Seleccione Grupo de Activos..." ){
                $("#tableBox").show();
                agregarActivo();
            }
        });
        $( "#grupo" ).change(function() {
            buscarClase( $("#grupo").val() );
        });
        $( "#clase" ).change(function() {
            buscarDiversidad( $( this ).val() );
        });
        $( "#generarExcel" ).click(function() {
            var myJson = crearJson();
            console.log(myJson);
            $.ajax({
                type: 'POST',
                url: 'include/generarXls.php',
                data: { get_option: myJson },
                success: function(response){
                    console.log("Respuesta: \n" + response );
                    var $a = $("<a>");
                    $a.attr("href","include/xls/solicitud-"+response);
                    $("body").append($a);
                    $a.attr("download","solicitud.xls");
                    $a[0].click();
                    $a.remove();
                }
            });
        });
    
    /*  Muestra div #wait con imagen de precarga al iniciar el método ajax de jquery 
        Al completar el método ajax de jquery, esconde el div #wait */
    $(document)
        .ajaxStart(function(){
            $("#wait").css("display", "block");
        })
        .ajaxComplete(function(){
            $("#wait").css("display", "none");
        });
});

/* AGREGARACTIVO */ 
//Se toman valores de los campos de entrada y se agregan a la tabla html #tableBox
function agregarActivo(){
    $("#tableBox").find('tbody')
	.prepend($('<tr>')
		.append( $('<td>').text( $("#grupo").val() ) )
		.append( $('<td>').text( $("#clase").val() ) )
		.append( $('<td>').text( $("#diversidad").val() ) )
	);
}
/* BUSCARCLASE */ 
function buscarClase(val){
    $.ajax({
        type: 'post',
        url: 'include/buscarPorGrupo.php',
        data: {
            get_option:val
        },
        success: function (response) {
            document.getElementById("clase").innerHTML="<option>Seleccione la Clase del Activo...</option>"+response; 
            document.getElementById("diversidad").innerHTML="<option>Seleccione la Diversidad del Activo...</option>";
        }
    });
}
/* BUSCARDIVERSIDAD */ 
function buscarDiversidad(val){
    $.ajax({
        type: 'post',
        url: 'include/buscarPorClase.php',
        data: {
            get_option:val
        },
        success: function (response) {
            document.getElementById("diversidad").innerHTML="<option>Seleccione la Diversidad del Activo...</option>"+response; 
        }
    });
}
/* CREARJSON */ 
function crearJson(){
    var $table = $("#tableBox"),
    rows = [],
    header = [];
    //Encontramos los nombre de columna
    $table.find("thead th").each(function () {
        header.push($(this).html());
    });
    //Recorremos las filas
    $table.find("tbody tr").each(function () {
        var row = {};
        $(this).find("td").each(function (i) {
            var key     = header[i],
                value   = $(this).html();
        
            row[key] = value;
        });
        rows.push(row);
    });
    var myJsonString = JSON.stringify(rows);
    return myJsonString;
}