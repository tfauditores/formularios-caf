<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    /** Include Conection file */
    require_once 'include/conexion.php';
?>
<!DOCTYPE HTML>
<html lang="es_CO">
	<head>
		<title>datosTecnicos</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
        <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-darkness.css">
        <link rel="stylesheet" type="text/css" href="css/altas.css">
        <link rel="stylesheet" type="text/css" href="css/datosTecnicos.css">
        <link rel="stylesheet" type="text/css" href="css/masive.css">
        <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />

        <script src="vendor/jquery/jquery-1.12.4.js"></script>
        <script src="vendor/jqueryui/jquery-ui.js"></script>
        <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
        <script src="js/lib/jquery.priceformat.min.js"></script>
        <script src="js/lib/jquery.validate.min.js"></script>
        <script src="js/functions-datosTecnicos.js"></script>
        <script src="js/data-load-datostecnicos.js"></script>
        <script>
            $( function() {
                $( "#capitalizacionNew" ).datepicker({
                    changeMonth: true,
                    changeYear: true,
                    numberOfMonths: 1,
                    yearRange: "c-67:c+10",
                    minDate: new Date(1950, 1, 1),
                    maxDate: "+1m +1w",
                    nextText: "Siguiente",
                    prevText: "Anterior",
                    showMonthAfterYear: true
                });
                $( "#capitalizacionNew" ).datepicker( "option", "dateFormat", "yy-mm-dd" );    
            });
        </script>

	</head>
	<body>
        <div id="container">
            <div class="row"  style="display:none;">
                <div class="col-md-6">
                    <img src="img/logoForm.jpg" alt="Ecopetrol">
                </div>
                <div class="col-md-6">
                    <div class="row row-header">GERENCIA DE SERVICIOS COMPARTIDOS</div>
                    <div class="row row-header">DEPARTAMENTO DE SERVICIOS DE INFRAESTRUCTURA Y TIERRAS</div>
                    <div class="row row-header">SOLICITUD DE SERVICIO DE GESTIÓN INMOBILIARIA</div>
                </div>
            </div>

            <form id="formatoDatosTecnicos" name="formatoDatosTecnicoss" method="get" action="">
                <div class="row text-left nav-view">
                    <label class="btn btn-styled btn-single fix-margin active">
                        Actualización sencilla
                    </label>
                    <label class="btn btn-styled btn-multiple fix-margin">
                        Actualizaciones multiples
                        <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xls">
                    </label>
                </div>
                <div class="single-view">
                    <p class="red">* Ingrese número de activo o número de inventario para hacer la consulta</p>
                    <div class="form-group row box" id="gcmBox">
                        <div class="form-section col-sm-2">
                            
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="activo">Número de activo: </label>
                            <input class="form-control" type="text" value="" id="activo" name="activo" autofocus  >
                        </div>
                        <div class="form-section sn col-sm-2">
                            <label for="subn" class="">Subnúmero: </label>
                            <input type="number" id="subn" name="subn" class="form-control valid" placeholder="SUBNÚMERO" value="0" min="0" aria-invalid="false" readonly>
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="placa" class="">NÚMERO DE INVENTARIO (PLACA): </label>
                            <input class="form-control" type="text" value="" id="placa" name="placa"  >
                        </div>
                        <div class="form-section col-sm-2 operacionBox">
                            <label for="operacion" class="">Operación: </label>
                            <input class="form-control" type="text" value="" id="operacion" name="operacion" readonly >
                        </div>
                        
                    </div>  

                    <div class="row show-characteristics">
                        <span>Ver características <i class="ion ion-eye"></i></span>
                    </div>

                    <div class="slide-info row">
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Departamento:</label>
                                <span data-name="departamento"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Municipio:</label>
                                <span data-name="municipio"></span>
                            </div>
                        </div>  

                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Ubicación geográfica:</label>
                                <span data-name="ubicacion_geografica"></span>
                            </div>
                        </div>                    
                
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Área:</label>
                                <span data-name="area"></span>
                            </div>
                        </div>               
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>CECO:</label>
                                <span data-name="ceco"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Clave amortización:</label>
                                <span data-name="clave_amortizacion"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Campo planta:</label>
                                <span data-name="campo_planta"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Custodio:</label>
                                <span data-name="custodio"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Clase:</label>
                                <span data-name="clase"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Nombre clase:</label>
                                <span data-name="nombre_clase"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Vicepresidencia:</label>
                                <span data-name="vicepresidencia"></span>
                            </div>
                        </div>                    
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Gerencia:</label>
                                <span data-name="gerencia"></span>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4">
                            <div class="form-section">
                                <label>Tipo de operación:</label>
                                <span data-name="tipo_operacion"></span>
                            </div>
                        </div>   
                    </div>

                    <div class="modificaciones row">
                        <p style="text-align: left;color: red;margin-left: 10px;">* Edite únicamente los campos que requiere actualizar. </p>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section" style="width:100%;">
                                    <label>DENOMINACIÓN:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="denominacion" name="denominacion" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6 denominacionNew">
                                            <!-- <input class="form-control" type="text" value="" id="denominacionNew" name="denominacionNew"  > -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DENOMINACIÓN 2:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="denominacion2" name="denominacion2" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="denominacion2New" name="denominacion2New" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>No. Inventario:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="inventario" name="inventario" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="inventarioNew" name="inventarioNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>MARCA DEL FABRICANTE:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="fabricante" name="fabricante" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <select class="form-control" name="fabricanteNew" id="fabricanteNew">
                                                <option value="">Seleccione fabricante...</option>
                                                <option value="NO REGISTRA">- NO REGISTRA -</option>
                                                <?php
                                                    $select=mysqli_query($con, "SELECT DISTINCT fabricante FROM fabricantes ORDER BY fabricante ASC");
                                                    while($row=mysqli_fetch_array($select))
                                                    {
                                                        $dpto = utf8_encode($row['fabricante']);
                                                        echo "<option value=\"".$dpto."\">".$dpto."</option>";
                                                    }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>MODELO:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="modelo" name="modelo" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="modeloNew" name="modeloNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>SERIE:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="serie" name="serie" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="serieNew" name="serieNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>TAG:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="tag" name="tag" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="tagNew" name="tagNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CAPACIDAD:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="capacidad" name="capacidad" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="capacidadNew" name="capacidadNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>UNIDAD DE MEDIDA:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="unidad" name="unidad" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <!-- <input class="form-control" type="text" value="" id="unidadNew" name="unidadNew"  > -->
                                            <select class="form-control" name="unidadNew" id="unidadNew">
                                                <option value="">Seleccione Unidad de medida...</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>FECHA INICIAL VIDA ÚTIL REMANENTE:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="fecha_VURM" name="fecha_VURM" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" class="form-control datepicker" id="fecha_VURM_New" name="fecha_VURM_New" data-date-format="mm/dd/yyyy">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>Vida útil remanente (meses):</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="vurm" name="vurm" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="vurmNew" name="vurmNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>FECHA CAPITALIZACION:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="capitalizacion" name="capitalizacion" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <div class="input-group date" data-provide="datepicker">
                                                <input type="text" class="form-control user-input datepicker" id="capitalizacionNew" name="capitalizacionNew" data-date-format="mm/dd/yyyy">
                                                <div class="input-group-addon">
                                                    <span class="glyphicon glyphicon-th"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 matriculaVehiculo">
                                <div class="form-section">
                                    <label>MATRICULA VEHICULO:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-6">
                                            <input class="form-control" type="text" value="" id="matriculaV" name="matriculaV" readonly  >
                                        </div>
                                        <div class="form-section col-sm-6">
                                            <input class="form-control user-input" type="text" value="" id="matriculaVNew" name="matriculaVNew"  >
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                            <!-- <div class="col-lg-6 col-md-6">
                                <div class="form-section">
                                    <label>Vida útil:</label><br>
                                    <div class="row">
                                        <div class="form-section col-sm-4">
                                            <input class="form-control" type="text" value="" id="vuAnos" name="vuAnos" readonly  >
                                            <input class="form-control" type="text" value="" id="vuPeriodos" name="vuPeriodos" readonly  >
                                        </div>
                                        <div class="form-section col-sm-4">
                                            <input class="form-control user-input col-sm-3" type="text" id="vuANew" name="vuANew"  >
                                            <input class="form-control user-input col-sm-3" type="text" id="vuMNew" name="vuMNew"  >
                                        </div>
                                        <div class="form-section col-sm-4">
                                            <span>VU(meses)</span>
                                            <input class="form-control vumcalc" type="text" name="vumNew" id="vumNew" readonly  >
                                        </div>
                                    </div>
                                </div>
                            </div> -->
                        </div>

                        <div class="form-group row box" id="justificacion">
                            <div class="form-section col-sm-12">
                                <label for="justificacionBox" class="">Observaciones: </label>
                                <textarea class="form-control"  rows="3" cols="70" id="justificacionBox" name="justificacionBox"></textarea>  
                            </div>
                        </div>
                    </div>

                    

                    <!--<a href="javascript:void(0);" class="slider-arrow show">&laquo;</a>-->

                    <div class="form-group row box" id="buttonBox">
                        <div class="col-center col-sm-12"> 
                            <button type="button" id="agregar" class="btn btn-primary button1">Agregar a la solicitud</button>
                            <button type="button" id="reset" class="btn btn-info" style="display:none;">Reiniciar</button>
                            <button type="button" id="generarExcel" class="btn btn-secondary button2">Finalizar solicitud</button>
                        </div>
                    </div>
                </div>
                <div class="multiple-view">
                    <div class="status-block">
                        <nav class="nav-tabs" data-tab="multiple">
                            <ul>
                                <li class="active" data-type="success">Existosos <i>0</i></li>
                                <li data-type="error">Errores <i>0</i></li>
                            </ul>
                        </nav>
                        <div class="tabs" data-tab="multiple">
                            <div class="tab active">
                                <div class="cover-table">
                                    <table class="success-multiple">
                                        <thead>
                                            <tr>
                                                <th>Fila</th>
                                                <th>Código activo fijo</th>
                                                <th>Denominación</th>
                                                <th>No. Inventario</th>
                                                <th>Serie</th>
                                                <th>Cod. Clase</th>
                                                <th>Nuevo Cod. Municipio</th>
                                                <th>Nuevo Municipio</th>
                                                <th>Nuevo Cod. Ubicación Geográfica</th>
                                                <th>Nueva Ubicación Geográfica</th>
                                                <th>Nueva Cod. CeCo</th>
                                                <th>Nueva Cod. Campo Planta</th>
                                                <th>Nueva Campo Planta</th>
                                                <th>Vida Útil Total (en meses)</th>
                                                <th>Nueva Vicepresidencia</th>
                                                <th>Nueva Gerencia</th>
                                                <th>Observaciones</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                    <!-- <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button> -->
                                    <button type="button" id="generarExcel2" class="btn btn-secondary button2">Finalizar solicitud</button>
                                </div>
                            </div>
                            <div class="tab">
                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                </div>
                                <div class="error-multiple">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="table1" >
            <table class="table table-striped table-responsive table-bajas table-data">
                <thead>
                    <tr>
                        <th>ACCIONES</th>
                        <th>ACTIVO</th>
                        <th>SUBNÚMERO</th>
                        <th>DENOMINACION</th>
                        <th>DENOMINACION2</th>
                        <th>INVENTARIO</th>
                        <th>FABRICANTE</th>
                        <th>MODELO</th>
                        <th>SERIE</th>
                        <th>TAG</th>
                        <th>CAPACIDAD</th>
                        <th>UNIDAD</th>
                        <!-- <th>VUM</th> -->
                        <th>VURM</th>
                        <!-- <th>CAPITALIZACION</th>
                        <th>MATRICULAVEHICULO</th> -->
                        <th>OBSERVACIONES</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
        
        <div class="col-lg-12 show-list">
            <label data-modal="datosTecnicos">
                Ver listado de Solicitudes 
                <i class="ion ion-android-list" data-modal="datosTecnicos">
                    <sup>0</sup>
                </i>
            </label>
        </div>

        <!--html modalboz subnumbers-->
        <div class="modalbox" data-modal="subnumbers">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="tableBox" class="modal" >
            <div>
                <span class="close">&times;</span>
                <table class="table table-striped table-responsive table-bajas table-data">
                    <thead>
                        <tr>
                        <th>ACCIONES</th>
                        <th>ACTIVO</th>
                        <th>SUBNÚMERO</th>
                        <th>DENOMINACION</th>
                        <th>DENOMINACION2</th>
                        <th>INVENTARIO</th>
                        <th>FABRICANTE</th>
                        <th>MODELO</th>
                        <th>SERIE</th>
                        <th>TAG</th>
                        <th>CAPACIDAD</th>
                        <th>UNIDAD</th>
                        <!-- <th>VUM</th> -->
                        <th>VURM</th>
                        <!-- <th>CAPITALIZACION</th>
                        <th>MATRICULAVEHICULO</th> -->
                        <th>OBSERVACIONES</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>

        <div id="wait">
            <div>
                <img src='img/demo_wait.gif' width="64" height="64" />
                <br>Buscando...
            </div>
        </div>

        <div id="dialog-confirm" title="Se borrarán los campos">
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
                Al cambiar de tipo de activo se borrarán los campos digitados. Desea confirmar?
            </p>
        </div>

	</body>
</html>