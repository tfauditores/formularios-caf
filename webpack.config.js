var path = require('path');
var webpack = require('webpack');
// var ExtractTextPlugin = require('extract-text-webpack-plugin');
// var CopyWebpackPlugin = require('copy-webpack-plugin');
// var OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

var config = {
    entry: {
        'datos-maestros': './assets/js/datos-maestros.js',
        'bajas': './assets/js/bajas.js'
    },
    output: {
        path: path.join(__dirname, "js"),
		filename: "[name].js"
    }, 
    devtool: process.env.NODE_ENV === 'production ' ? false : 'source-map',
    module: {
        loaders: [
            {
                test: [/\.js$/,/\.es6$/,/\.jsx?$/],
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    query: {
                        presets: ['env'],
                        plugins: ['transform-es2015-modules-commonjs','transform-regenerator', 'transform-runtime']
                    }
                }
            },
            {
				test: /\.css$/,
				loader: 'css-loader'
			},
            {
				test: /\.json$/,
				loader: 'json-loader',
			}
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: {
              warnings: false,
            },
            output: {
                comments: false
            },
            mangle: {
              except: ['$','validate'],
            },
            sourceMap: true
        })
    ]
}



module.exports = config;