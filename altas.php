<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    /** Include Conection file */
    require_once 'include/conexion.php';
?>
<!DOCTYPE HTML>
<html lang="es_CO">
	<head>
		<title>Altas</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
        <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-darkness.css">
        <link rel="stylesheet" type="text/css" href="css/altas.css">
        <link rel="stylesheet" type="text/css" href="css/masive.css">
        <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	</head>
        <div id="container">
            <form id="formatoAltas" name="formatoAltas" method="get" action="">
                <div class="row text-left nav-view">
                    <label class="btn btn-styled btn-single fix-margin active">
                        creación sencilla
                    </label>
                    <label class="btn btn-styled btn-multiple fix-margin">
                        creaciones multiples
                        <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xls">
                    </label>
                </div>
                <div class="single-view">
                    <div class="form-group row box" id="tipoActivoBox">
                        <label class="col-sm-2 col-form-label">Tipo de Activo: </label>
                        <div class="col-md-3">
                            <label class="label-selector">
                                <input class="tipoActivo" name="tipoActivo" type="radio" value="principal" data-toggle="tooltip" data-placement="top" title="Agregar activo principal" checked />
                                <i></i>
                                <span>Activo Principal</span>
                            </label>
                        </div>
                        <div class="col-md-3">
                            <label class="label-selector">
                                <input class="tipoActivo" name="tipoActivo" type="radio" value="subnumero" data-toggle="tooltip" data-placement="top" title="Para agregar un subnúmero debe conocer el número del activo principal" />
                                <i></i>
                                <span>Subnúmero</span>
                            </label>
                        </div>
                    </div>

                    <div class="form-group row box" id="numActivoBox">
                        <div class="form-section col-sm-12">
                            <label for="numActivo">#Activo: </label>
                            <input type="number" min="0" id="numActivo" name="numActivo" class="form-control" placeholder="Escriba el número de activo principal... " data-toggle="tooltip" data-placement="top" title="Para subnúmeros debe especificar el # de activo fijo según registra en el Auxiliar de Activos Fijos." >
                            <p class="alert alert-danger"></p>
                            <input class="form-control"  type="hidden" name="activoOK" id="activoOK" value="">
                        </div>
                    </div> 

                    <div class="form-group row box" id="denomSearchBox">
                        <div class="form-section col-sm-12">
                            <label for="denomSearch">Buscar por denominación:</label>
                            <input class="form-control" type="text" autofocus id="denomSearch" name="denomSearch" placeholder="Escriba para buscar denominación..." data-toggle="tooltip" data-placement="top" title="Escriba la denominación del activo fijo de acuerdo al catálogo para comenzar la búsqueda">
                        </div>
                    </div>

                    <div class="form-group row box" id="denomBox">
                        <div class="form-section col-sm-6" id="d1">
                            <label for="denom">Denominación:</label>
                            <input class="form-control" type="text" autofocus id="denom" name="denom" readonly >
                        </div>

                        <div >
                        </div>

                        <div class="form-section col-sm-6" id="d2">
                            <label for="subdenom">Denominación:</label>
                            <select class="form-control" name="subdenom" id="subdenom">
                                <OPTION VALUE="">Seleccione...</OPTION>
                                <OPTION VALUE="MAYOR VALOR SUBREPARTO">MAYOR VALOR SUBREPARTO</OPTION>
                                <OPTION VALUE="MAYOR VALOR COSTOS FINANCIEROS">MAYOR VALOR COSTOS FINANCIEROS</OPTION>
                                <OPTION VALUE="MAYOR VALOR COSTO DE ADQUISICION">MAYOR VALOR COSTO DE ADQUISICION</OPTION>
                                <OPTION VALUE="MAYOR VALOR SERVICIO DE PERFORACION">MAYOR VALOR SERVICIO DE PERFORACION</OPTION>
                                <OPTION VALUE="MAYOR VALOR SERVICIO DE WORKOVER">MAYOR VALOR SERVICIO DE WORKOVER</OPTION>
                                <OPTION VALUE="MAYOR VALOR OBRAS CIVILES">MAYOR VALOR OBRAS CIVÍLES</OPTION>
                                <OPTION VALUE="ADICION + ACCESORIOS">ADICION + ACCESORIOS</OPTION>
                                <OPTION VALUE="ADICION + CAMPO">ADICION + CAMPO</OPTION>
                                <OPTION VALUE="ADICION + GESTION CONTRATO">ADICION + GESTION CONTRATO</OPTION>
                                <OPTION VALUE="ADICION + GESTORIA + TIPO DE GESTORIA (TECNICA, INTERVENTORIA, ETC)">ADICION + GESTORIA + TIPO DE GESTORIA (TECNICA, INTERVENTORIA, ETC)</OPTION>
                                <OPTION VALUE="ADICION + AMPLIACION">ADICION + AMPLIACION</OPTION>
                                <OPTION VALUE="ADICION + EJECUCION CONTRATO">ADICION + EJECUCION CONTRATO</OPTION>
                                <OPTION VALUE="ADICION + MATERIALES">ADICION + MATERIALES</OPTION>
                                <OPTION VALUE="ADICION + LINEA DE FLUJO">ADICION + LINEA DE FLUJO (EL VALOR DE MAS SE DEBE SUMAR A LA CAPACIDAD DEL ACTIVO PRINCIPAL)</OPTION>

                                <OPTION VALUE="AUMENTO DE VIDA UTIL">AUMENTO DE LA VIDA ÚTIL</OPTION>
                                <OPTION VALUE="AMPLIACION PRODUCTIVIDAD">AMPLIACIÓN DE LA PRODUCTIVIDAD</OPTION>
                                <OPTION VALUE="REDUCCION COSTOS DE OPERACION">REDUCCIÓN SIGNIFICATICA DE COSTOS DE OPERACIÓN</OPTION>
                                <OPTION VALUE="REEMPLAZO DE PARTE O COMPONENTE">REEMPLAZO DE PARTE O COMPONENTE</OPTION>
                            </select>
                        </div>
                        <div class="form-section col-sm-6" id="d3">
                            <label for="denom2">Denominación 2: </label>
                            <textarea class="form-control"  rows="1" cols="50" id="denom2" name="denom2" 
                                    placeholder="Describa la denominación 2 si aplica..." 
                                    data-toggle="tooltip" data-placement="bottom" 
                                    title="Puede quedar vacio, excepto para las clases 11*, 15002 a 15015, 16*, 17008 a 17010, 18*, 26002, 33*, para las cuales debe estar acorde con lo especificado en el Catalogo de activos fijos vigente GFI-T-004" required></textarea>  
                        </div>
                    </div>

                    <div class="form-group row box" id="gcmBox">
                        <div class="form-section col-sm-4">
                            <label for="grupo">Grupo: </label>
                            <input class="form-control" type="text" value="" id="grupo" name="grupo" readonly >
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="clase" class="">Clase: </label>
                            <input class="form-control" type="text" value="" id="clase" name="clase" readonly >
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="medida" class="">Unidad: </label>
                            <input class="form-control" type="text" value="" id="medida" name="medida" readonly >
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="capacidad" class="">Capacidad: </label>
                            <input  class="form-control" type="number" id="capacidad" name="capacidad"
                                    data-toggle="tooltip" data-placement="top" title="Este campo admite valores numéricos. Para decimales usar (,)">
                        </div>
                        <div class="form-section col-sm-2"> 
                            <label for="vida" class="">VUT sugerida: </label>
                            <input class="form-control" type="number" value="" min="0" step="1" class=" " id="vida" name="vida" />
                        </div>
                    </div>   
                    
                    <div class="form-group row box" id="inmuebles">
                        <div class="form-section col-sm-3">
                            <label for="sig">Código SIG: </label>
                            <input class="form-control" type="text" value="" id="sig" name="sig" >
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="cedula" class="">Cédula Catastral: </label>
                            <input class="form-control" type="text" value="" id="cedula" name="cedula" >
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="matricula" class="">Matrícula Inmobiliaria: </label>
                            <input class="form-control" type="text" value="" id="matricula" name="matricula" >
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="escritura" class="">Número de Escritura: </label>
                            <input  class="form-control" type="text" id="escritura" name="escritura">
                        </div>
                    </div> 
                    <div class="form-group row box" id="vehiculos">
                        <div class="form-section col-sm-6">
                            <label for="placaVehiculo">Placa del vehículo: </label>
                            <input class="form-control" type="text" value="" id="placaVehiculo" name="placaVehiculo" >
                        </div>
                        <div class="form-section col-sm-6">
                            <label for="modeloVehiculo" class="">Año modelo del vehículo: </label>
                            <input  class="form-control" type="text" value="" id="modeloVehiculo" name="modeloVehiculo" 
                                    data-toggle="tooltip" data-placement="top" title="Digite el año o modelo del vehículo">
                        </div>
                    </div> 

                    <div class="form-group row box" id="data3Box">
                        <div class="form-section col-sm-4">
                            <label for="fabricante">Fabricante: </label>
                            <select class="form-control" name="fabricante" id="fabricante">
                                <option value="">Seleccione fabricante...</option>
                                <option value="NO REGISTRA">- NO REGISTRA -</option>
                                <?php
                                    $select=mysqli_query($con, "SELECT DISTINCT fabricante FROM fabricantes ORDER BY fabricante ASC");
                                    while($row=mysqli_fetch_array($select))
                                    {
                                        $dpto = utf8_encode($row['fabricante']);
                                        echo "<option value=\"".$dpto."\">".$dpto."</option>";
                                    }
                                ?>
                            </select>
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="modelo">Modelo: </label>
                            <input  class="form-control" type="text" value="" id="modelo" name="modelo"
                                    data-toggle="tooltip" data-placement="top" title="Para terrenos indicar la notaría. Deje vacío si NO REGISTRA">
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="serie">Serie: </label>
                            <input  class="form-control" type="text" value="" id="serie" name="serie"
                                    data-toggle="tooltip" data-placement="top" title="Deje vacío si NO REGISTRA">
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="tag">Tag: </label>
                            <input  class="form-control" type="text"  id="tag" name="tag"
                                    data-toggle="tooltip" data-placement="top" title="Deje vacío si NO REGISTRA">
                        </div>
                        <div class="form-section col-sm-2">
                            <label for="costo">Costo (COP): </label>
                            <input class="form-control" type="text" value="" class="" id="costo" name="costo" />
                        </div>
                    </div>

                    <div class="form-group row box" id="ubiBox">
                        <div class="form-section col-sm-8">
                            <div class="row">
                                <div class="col-sm-4"> 
                                    <label for="departamento">Departamento: </label>
                                    <select class="form-control" name="departamento" id="departamento">
                                        <option value="">Seleccione Departamento</option>
                                        <?php
                                            $select=mysqli_query($con, "SELECT DISTINCT departamento FROM ubicaciones");
                                            while($row=mysqli_fetch_array($select))
                                            {
                                                $dpto = $row['departamento'];
                                                echo "<option value=\"".$dpto."\">".$dpto."</option>";
                                            }
                                        ?>
                                    </select>
                                </div>
                                <div class=" col-sm-4"> 
                                    <label for="departamento">Municipio: </label>
                                    <select class="form-control" name="municipio" id="municipio">
                                        <option value="">Seleccione Municipio</option>
                                    </select>
                                </div>
                                <div class=" col-sm-4"> 
                                    <label for="departamento">Ubicación geográfica: </label>
                                    <select class="form-control" name="ubicacion" id="ubicacion">
                                        <option value="">Seleccione Ubicación</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-sectoin col-sm-2">
                            <label for="ceco">CeCo: </label>
                            <input class="form-control" type="text" value="" id="ceco" name="ceco" />
                        </div>
                        <div class="form-sectoin col-sm-2">
                            <label for="inventario">No. Inventario: </label>
                            <input class="form-control" type="text" value="" id="inventario" name="inventario" />
                        </div>
                    </div>

                    <div class="form-group row box" id="ceco-info">
                        <div class="form-section col-sm-3">
                            <label for="descripcionCeco">Descripción CeCo </label>
                            <input readonly class="form-control" type="text" value="" id="descripcionCeco" name="descripcionCeco" >
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="area" class="">Area: </label>
                            <input readonly class="form-control" type="text" value="" id="area" name="area">
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="gerencia">Gerencia </label>
                            <input readonly class="form-control" type="text" value="" id="gerencia" name="gerencia" >
                        </div>
                        <div class="form-section col-sm-3">
                            <label for="vicepresidencia" class="">Vicepresidencia: </label>
                            <input readonly class="form-control" type="text" value="" id="vicepresidencia" name="vicepresidencia">
                        </div>
                    </div> 

                    <div class="form-group row box" id="vpr">
                        <div class="form-section col-sm-6">
                            <label for="nombreCampo">Nombre del campo: </label>
                            <input class="form-control" type="text" value="" id="nombreCampo" name="nombreCampo" >
                        </div>
                        <div class="form-section col-sm-6">
                            <label for="tipoPozo" class="">Tipo de Pozo: </label>
                            <input class="form-control" type="text" value="" id="tipoPozo" name="tipoPozo">
                        </div>
                    </div> 
                    
                    <div class="form-group row box" id="esTrasladoBox">
                        <div class="form-section col-sm-6">
                                <label class="label-selector">
                                    <input  type="checkbox"
                                            value="esTraslado"
                                            class="esTraslado"
                                            name="esTraslado"
                                            id="esTraslado"
                                            data-toggle="tooltip"
                                            data-placement="top"
                                            title="Esta creación es para hacer un traslado de valores"  />
                                    <i></i>
                                    <span>Esta creación es para solicitar un traslado de valores</span>
                                </label>
                            </div>
                    </div>


                    <div class="form-group row box" id="TrasladoBox">
                        <div class="form-section col-sm-4">
                            <label>NÚMERO DE ACTIVO ORIGEN:</label>
                            <input type="text" name="id_numero_activo_destino" class="form-control">
                        </div>
                        <div class="form-section col-sm-2 sn">
                            <label>Subnúmero:</label>
                            <input type="text" name="id_subnumero_activo_destino" class="form-control" placeholder="SUBNÚMERO" value="0" min="0" readonly>
                        </div>
                        <div class="form-section col-sm-3">
                            <label>MOTIVO DEL TRASLADO:</label>
                            <select name="motivo_traslado" class="form-control">
                                <option value="">SELECCIONE...</option>
                                <option value="INVENTARIO">INVENTARIO</option>
                                <option value="DESAGREGACIÓN ASOCIADAS">DESAGREGACIÓN ASOCIADAS</option>
                                <option value="CORRECCIÓN DE CAPITALIZACIÓN">CORRECCIÓN DE CAPITALIZACIÓN</option>
                            </select>
                        </div>
                        <div class="form-section col-sm-3">
                            <label>VALOR DE TRASLADO:</label>
                            <input type="text" name="valor_traslado" class="form-control">
                        </div>
                    </div>

                    <!--<a href="javascript:void(0);" class="slider-arrow show">&laquo;</a>-->

                    <div class="form-group row box" id="buttonBox">
                        <div class="col-center col-sm-12"> 
                            <button type="button" id="agregar" class="btn btn-primary button1">Agregar a la solicitud</button>
                            <button type="button" id="reset" class="btn btn-info hidden">Reiniciar</button>
                            <button type="button" id="generarExcel" class="btn btn-secondary button2">Finalizar solicitud</button>
                        </div>
                    </div>
                </div>
                <div class="multiple-view">
                    <div class="status-block">
                        <nav class="nav-tabs" data-tab="multiple">
                            <ul>
                                <li class="active" data-type="success">Existosos <i>0</i></li>
                                <li data-type="error">Errores <i>0</i></li>
                            </ul>
                        </nav>
                        <div class="tabs" data-tab="multiple">
                            <div class="tab active">
                                <div class="cover-table">
                                    <table class="success-multiple">
                                        <thead>
                                            <tr>
                                                <th>Fila</th>
                                                <th>Código activo fijo</th>
                                                <th>Denominación</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                    <!-- <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button> -->
                                    <button type="button" class="btn btn-secondary button2 btn-end">Finalizar solicitud</button>
                                </div>
                            </div>
                            <div class="tab">
                                <div class="text-center">
                                    <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                </div>
                                <div class="error-multiple">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div id="table1" >
            <table class="table table-striped table-responsive table-bajas">
                <thead>
                    <tr>
                        <th>ACCIONES</th>
                        <th>GRUPO</th>
                        <th>CODIGO</th>
                        <th>DENOMINACION</th>
                        <th>DENOMINACION2</th>
                        <th>SERIE</th>
                        <th>NUM_INVENTARIO</th>
                        <th>CAPACIDAD</th>
                        <th>UNIDAD_MEDIDA</th>
                        <th>TAG</th>
                        <th>CECO</th>
                        <th>COD_UBICACION</th>
                        <th>MUNICIPIO</th>
                        <th>COD_MUNICIPIO</th>
                        <th>FABRICANTE</th>
                        <th>MODELO</th>
                        <th>VU_TECNICA</th>
                        <th>COSTO</th>
                        <th>NOMBRE_CAMPO</th>
                        <th>TIPO_POZO</th>
                        <th>COD_SIG</th>
                        <th>CEDULA_CATASTRAL</th>
                        <th>MATRICULA_INM</th>
                        <th>ESCRITURA</th>
                        <th>PLACA_VEHICULO</th>
                        <th>MODELO_VEHICULO</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        </div>
        
        <div class="col-lg-12 show-list">
            <label data-modal="altas">
                Ver listado de Altas 
                <i class="ion ion-android-list" data-modal="bajas">
                    <sup>0</sup>
                </i>
            </label>
        </div>

        <!--html modalbox subnumbers-->
        <div class="modalbox" data-modal="subnumbers">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table"></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="tableBox" class="modal" >
            <div>
                <span class="close">&times;</span>
                <table class="table table-striped table-responsive table-bajas">
                    <thead>
                        <tr>
                            <th>ACCIONES</th>
                            <th>GRUPO</th>
                            <th>CODIGO</th>
                            <th>DENOMINACION</th>
                            <th>DENOMINACION2</th>
                            <th>SERIE</th>
                            <th>NUM_INVENTARIO</th>
                            <th>CAPACIDAD</th>
                            <th>UNIDAD_MEDIDA</th>
                            <th>TAG</th>
                            <th>CECO</th>
                            <th>COD_UBICACION</th>
                            <th>MUNICIPIO</th>
                            <th>COD_MUNICIPIO</th>
                            <th>FABRICANTE</th>
                            <th>MODELO</th>
                            <th>VU_TECNICA</th>
                            <th>COSTO</th>
                            <th>NOMBRE_CAMPO</th>
                            <th>TIPO_POZO</th>
                            <th>COD_SIG</th>
                            <th>CEDULA_CATASTRAL</th>
                            <th>MATRICULA_INM</th>
                            <th>ESCRITURA</th>
                            <th>PLACA_VEHICULO</th>
                            <th>MODELO_VEHICULO</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    </tbody>
                </table>
            </div>
        </div>

        <div id="wait">
            <div>
                <img src='img/demo_wait.gif' width="64" height="64" />
                <br>Buscando...
            </div>
        </div>

        <div id="dialog-confirm" title="Se borrarán los campos">
            <p>
                <span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>
                Al cambiar de tipo de activo se borrarán los campos digitados. Desea confirmar?
            </p>
        </div>

        <script src="vendor/jquery/jquery-1.12.4.js"></script>
        <script src="vendor/jqueryui/jquery-ui.js"></script>
        <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
        <script src="js/lib/jquery.priceformat.min.js"></script>
        <script src="js/lib/jquery.validate.min.js"></script>
        <script src="js/functions-altas.js"></script>
        <script src="js/data-load-altas.js"></script>
	</body>
</html>