<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    // session_start();
    // var_dump(session_id());
    // echo $_GET['asd'];
?>
<!DOCTYPE html>
<html lang="es_CO">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Altas auxiliar</title>
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
    <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-darkness.css">
    <link rel="stylesheet" href="css/altas.css">
    <link rel="stylesheet" href="css/generic.css">
    <link rel="stylesheet" type="text/css" href="css/masive.css">
    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <main class="main-content">
        <section class="module">
            <div class="container big-container">
                <form action="" method="post" class="withdrawal-form altas" id="formatoAltas">
                    <div class="row text-left nav-view">
                        <label class="btn btn-styled btn-single fix-margin active">
                            Creación sencilla
                        </label>
                        <label class="btn btn-styled2 btn-multiple fix-margin">
                            Creaciones múltiples
                            <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xlzs">
                        </label>
                        <a href="formats/GFI-F-049-formato-creacion-datos-maestros.xlsx" download>
                            <!-- <label class="btn btn-styled btn-format fix-margin">
                                Formato <i class="icon ion-document-text"></i>
                            </label> -->
                            <div class="btn-excel">
                                <span>FORMATO F-049</span>
                                <img src="img/excel.png" alt=""/>
                            </div>
                        </a>
                    </div>
                    <div class="single-view">
                        <div class="row fields-group">
                            <div class="col-lg-12 mb20">
                                <label class="label-selector">
                                    OPERACIÓN:
                                </label>
                                <select name="tipo_operacion" class="form-control">
                                    <option value="directa">DIRECTA</option>
                                    <option value="directa">ASOCIADA OPERADOR</option>
                                    <option value="asociada">ASOCIADA NO OPERADOR</option>
                                    <option value="asociada">EXTENSIÓN DE COMERCIALIDAD</option>
                                </select>
                                <label class="label-selector">
                                    CREACIÓN:
                                </label>
                                <select name="tipo_creacion" class="form-control">
                                    <option value="principal">PRINCIPAL</option>
                                    <option value="subnumero">SUBNÚMERO</option>
                                </select>
                            </div>
                        </div>
                        <div class="row row-creacion">
                            <div class="col-md-6 active">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>BUSCAR POR DENOMINACIÓN:</label>
                                        <input type="text" name="busqueda_denominacion" class="form-control autocomplete" placeholder="Escriba para buscar denominación...">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-section clearfix ">
                                    <div class="block">
                                        <label>NÚMERO DE ACTIVO:</label>
                                        <input type="text" name="id_numero_activo" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-section clearfix">
                                    <div class="block block-complement">
                                        <label>DENOMINACIÓN:</label>
                                        <input type="text" name="denominacion" class="form-control" placeholder="Denominación..." readonly>
                                        <input type="text" name="denominacion_complementaria" class="form-control complement" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>DENOMINACIÓN 2:</label>
                                        <input type="text" name="denominacion2" class="form-control" placeholder="Denominación complementaria" autocomplete="foo"> 
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>COD. CLASE:</label>
                                        <input type="text" name="clase" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>No INVENTARIO:</label>
                                        <input type="text" name="inventario" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>FABRICANTE:</label>
                                        <input type="text" name="fabricante" class="form-control autocomplete">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>MODELO:</label>
                                        <input type="text" name="modelo" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>VIDA ÚTIL TOTAL (EN MESES):</label>
                                        <input type="text" name="vida_util" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>UNIDAD DE MEDIDA:</label>
                                        <input type="text" name="unidad_medida" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>CAPACIDAD:</label>
                                        <input type="number" name="capacidad" class="form-control" data-toggle="tooltip" data-placement="top" title="Este campo admite valores numéricos. Para decimales usar (,)">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>SERIE:</label>
                                        <input type="text" name="serie" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>TAG:</label>
                                        <input type="text" name="tag" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>COSTO DEL ACTIVO:</label>
                                        <input type="text" name="costo" class="form-control format">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>DEPARTAMENTO:</label>
                                        <select name="departamento" class="form-control" autocomplete='foo'>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>MUNICIPIO:</label>
                                        <select name="municipio" class="form-control" autocomplete='foo'>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>UBICACIÓN GEOGRÁFICA:</label>
                                        <select name="ubicacion_geografica" class="form-control" autocomplete='foo'>
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 hide">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>UNIDAD FUNCIONAL:</label>
                                        <input type="text" name="unidad_funcional" class="form-control" >
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pl00">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-section clearfix">
                                        <div class="block">
                                            <label>CECO:</label>
                                            <input type="text" name="ceco" class="form-control autocomplete">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-section clearfix">
                                        <div class="block">
                                            <label>CAMPO PLANTA:</label>
                                            <input type="text" name="campo_planta" class="form-control autocomplete">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 pr00">
                            <div class="row row-vehiculos">
                                <div class="col-md-6">
                                    <div class="form-section clearfix">
                                        <div class="block">
                                            <label>MATRÍCULA DEL VEHÍCULO:</label>
                                            <input type="text" name="matricula" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-section clearfix">
                                        <div class="block">
                                            <label>AÑO MODELO DEL VEHÍCULO:</label>
                                            <input type="text" name="anio" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-section">
                                    <label>OBSERVACIONES:</label>
                                    <textarea name="observaciones" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-section">
                                    <label class="label-selector" data-toggle="tooltip" data-placement="top" title="" data-original-title="Esta creación es para hacer un traslado de valores">
                                        <input type="checkbox" name="traslado">
                                        <i></i>
                                        <span>Solicitar traslado</span>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row row-traslados">
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>ACTIVO ORIGEN:</label>
                                        <input type="text" name="activo_traslado" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>SN ORIGEN:</label>
                                        <select name="sn_traslado" class="form-control">
                                            <option value="">Seleccione...</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>FECHA CAPITALIZACIÓN ORIGEN:</label>
                                        <input type="text" name="fecha_traslado" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>VALOR A TRASLADAR:</label>
                                        <input type="text" name="valor_traslado" class="form-control format">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-section clearfix">
                                    <div class="block">
                                        <label>MOTIVO DE TRASLADO:</label>
                                        <select name="motivo_traslado" class="form-control">
                                            <option value="">SELECCIONE...</option>
                                            <option value="INVENTARIO">INVENTARIO</option>
                                            <option value="DESAGREGACIÓN ASOCIADAS">DESAGREGACIÓN ASOCIADAS</option>
                                            <option value="CORRECCIÓN DE CAPITALIZACIÓN">CORRECCIÓN DE CAPITALIZACIÓN</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 text-center">
                                <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button>
                                <button class="btn btn-styled2 btn-end" type="button">FINALIZAR SOLICITUD</button>
                            </div>
                        </div>
                    </div>
                    <div class="multiple-view">
                        <div class="status-block">
                            <nav class="nav-tabs" data-tab="multiple">
                                <ul>
                                    <li class="active" data-type="success">Activos agregados <i>0</i></li>
                                    <li data-type="error">Activos con errores <i>0</i></li>
                                </ul>
                            </nav>
                            <div class="tabs" data-tab="multiple">
                                <div class="tab active">
                                    <div class="cover-table">
                                        <table class="success-multiple">
                                            <thead>
                                                <tr>
                                                    <th>Fila</th>
                                                    <th>Código activo fijo</th>
                                                    <th>Denominación</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="text-center">
                                        <!-- <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button> -->
                                        <button type="button" class="btn btn-secondary button2 btn-end">Finalizar solicitud</button>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                    </div>
                                    <div class="error-multiple">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- <div class="ctrls-table">
                    <span class="ion ion-android-apps" data-toggle="tooltip" title="Ver otras características"></span>
                </div> -->
                <div class="inline-table">
                    <!-- <div class="ctrls-table">
                        <span class="ion ion-android-apps" data-toggle="tooltip" title="Ver otras características"></span>
                    </div> -->
                    <div class="cover-table">
                        <table class="table table-striped table-responsive table-data">
                            <thead class="thead-default">
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>Tipo de Operación</th>
                                    <th>Tipo de Creación</th>
                                    <th># de Activo</th>
                                    <th>SN</th>
                                    <th>Cod. Clase</th>
                                    <th>Denominación</th>
                                    <th>Denominación Complementaria</th>
                                    <th>Inventario</th>
                                    <th>Fabricante</th>
                                    <th>Modelo</th>
                                    <th>Capacidad</th>
                                    <th>Unidad de Medida</th>
                                    <th>Serie</th>
                                    <th>Tag</th>
                                    <th>Registro Custodio</th>
                                    <th>Custodio</th>
                                    <th>Cod. Municipio</th>
                                    <th>Municipio</th>
                                    <th>Cod. Ubicación Geográfica</th>
                                    <th>Ubicación Geográfica</th>
                                    <th>Cod. CeCo</th>
                                    <th>Vicepresidencia</th>
                                    <th>Gerencia</th>
                                    <th>Vida Útil Total</th>
                                    <th>Costo</th>
                                    <th>Cod. Campo Planta</th>
                                    <th>Campo Planta</th>
                                    <th>Matrícula del Vehículo</th>
                                    <th>Año Modelo del Vehículo</th>
                                    <th>Activo origen</th>
                                    <th>SN origen</th>
                                    <th>Fecha Capitalización Origen</th>
                                    <th>Valor del Traslado</th>
                                    <th>Motivo del Traslado</th>
                                    <th>Observaciones</th>
                                    <!-- <th class="always-hide-column">Denominación 2</th>
                                    <th class="hide-column">Observaciones</th> -->
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <div class="modalbox" data-modal="subnumbers">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="main-footer">
        <div class="col-lg-12 supportedBrowsers">Aplicación probada y soportada en los navegadores:</div>
        <div class="col-lg-12 browserIcons">
            <span title="Descargar Chrome" class="browser">
                <a href="https://www.google.com/chrome/" target="_blank">CHROME</a>
            </span>
            <span title="Descargar Firefox" class="browser firefox">
                <a href="https://www.mozilla.org/es-ES/firefox/" target="_blank">FIREFOX</a>
            </span>
            <span title="Abrir en Edge" class="browser edge">
                <a href="microsoft-edge:https://ecopetrol.sharepoint.com/sites/CAF/SitePages/solicitud-de-altas.aspx"> EDGE</a>
            </span>
            <span title="Descargar Opera" class="browser opera">
                <a href="https://www.opera.com/es-419/download" target="_blank">OPERA</a>
            </span>
        </div>
    </footer>
    <div class="thankyou-message">
        <div class="table">
            <div class="table-cell">
                <h2 class="title">GRACIAS POR REALIZAR LA SOLICITUD, <br>PRONTO ATENDEREMOS SU REQUERIMIENTO</h2>
            </div>
        </div>
    </div>
    <div id="wait">
        <div>
            <img src='img/demo_wait.gif' width="64" height="64" />
            <br>Cargando...
        </div>
    </div>
    <script src="vendor/jquery/jquery-1.12.4.js"></script>
    <script src="vendor/jqueryui/jquery-ui.js"></script>
    <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
    <script src="js/lib/jquery.priceformat.min.js"></script>
    <script src="js/lib/jquery.validate.min.js"></script>
    <script src="js/functions-altas2.js"></script>
    <script src="js/data-load-altas.js"></script>
</body>
</html>