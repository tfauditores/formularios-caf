require('es6-promise').polyfill()
import queries from '../modules/queries'
import draw from '../modules/draw'
import autocomplete from '../modules/autocomplete'
import storage from '../modules/storage'
import staticOptions from '../modules/staticOptions'
import {dataValidations, dataRules} from '../modules/dataValidations'
import '../modules/common'

/** get catalogo */
    {
        var catalogo = null
        if(storage.getCookie() == false || storage.getLocal('catalogo') == null){
            let params = [
                'descripcion_af as label',
                'diversidad as grupo',
                'clase',
                'unidad',
                'vu_sugerida as vida_sugerida',
                'observaciones'
            ]
            queries
                .getCatalogo(params.toString())
                .then(data => {
                    catalogo = data
                    storage.setLocal('catalogo', catalogo)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            catalogo = storage.getLocal('catalogo')
        }
    }
/** get unidades */
    {
        var unidadesAsociadas = []
        var unidadesDirectas = []
        if(storage.getCookie() == false || storage.getLocal('unidadesAsociadas') == null || storage.getLocal('unidadesDirectas') == null){
            queries
                .getUnidades()
                .then(data => {
                    for(let i of data){
                        if(i.tipo == 'sap'){
                            unidadesAsociadas.push(i)
                        }
                        else if(i.tipo == 'catalogo'){
                            unidadesDirectas.push(i)
                        }
                    }
                    storage.setLocal('unidadesAsociadas', unidadesAsociadas)
                    storage.setLocal('unidadesDirectas', unidadesDirectas)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            unidadesAsociadas = storage.getLocal('unidadesAsociadas')
            unidadesDirectas = storage.getLocal('unidadesDirectas')
        }
    }
/** get fabricantes */
    {
        var fabricantes = null
        if(storage.getCookie() == false || storage.getLocal('fabricantes') == null){
            let params = [
                'fabricante'
            ]
            queries
                .getFabricantes(params.toString())
                .then(data => {
                    fabricantes = data
                    storage.setLocal('fabricantes', fabricantes)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            fabricantes = storage.getLocal('fabricantes')
        }
    }
/** get ubicaciones */
    {
        var ubicaciones = null
        if(storage.getCookie() == false || storage.getLocal('ubicaciones') == null){
            let params = [
                'departamento',
                'codMun as codigoMunicipio',
                'municipio',
                'codUbi as codigoUbicacion',
                'ubicacion'
            ]
            queries
                .getUbicaciones(params.toString())
                .then(data => {
                    ubicaciones = data
                    storage.setLocal('ubicaciones', ubicaciones)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            ubicaciones = storage.getLocal('ubicaciones')
        }
    }
/** get cecos */
    {
        var cecos = null
        if(storage.getCookie() == false || storage.getLocal('cecos') == null){
            let params = [
                'ceco',
                'nombreCeco',
                'siglaVicepresidencia',
                'siglaGerencia',
                'campoPlanta',
                'nombreCampoPlanta'
            ]
            queries
                .getCecos(params.toString())
                .then(data => {
                    cecos = data
                    storage.setLocal('cecos', cecos)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            cecos = storage.getLocal('cecos')
        }
    }
/** get campo planta */
    {
        var campoPlanta = null
        if(storage.getCookie() == false || storage.getLocal('campoPlanta') == null){
            let params = [
                'codigo',
                'nombre'
            ]
            queries
                .getCampoPlanta(params.toString())
                .then(data => {
                    campoPlanta = data
                    storage.setLocal('campoPlanta', campoPlanta)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            campoPlanta = storage.getLocal('campoPlanta')
        }
    }
/** get custodios */
    {
        var custodios = null
        if(storage.getCookie() == false || storage.getLocal('custodios') == null){
            let params = [
                'id',
                'numeroPersonal',
                'registro',
                'correo',
                'primerNombre',
                'segundoNombre',
                'primerApellido',
                'segundoApellido'
            ]
            queries
                .getCustodios(params.toString())
                .then(data => {
                    custodios = data
                    storage.setLocal('custodios', custodios)
                })
                .catch(err => console.log(err.responseText))
        }
        else{
            custodios = storage.getLocal('custodios')
        }
    }
/** set cookie queries */
    if(!storage.getCookie()){
        storage.setCookie()
    }
/** cambio de campos según el número de activo o si es un subnúmero */
    const changeView = {
        primal(data){
            $('.double-item').removeClass('changed')
            /* cambio de ubicación */
                draw.locationFields(data, ubicaciones)
                draw.tiposDeOperacion(data,staticOptions.tiposDeOperacion)
            console.log("--",data.tipoOperacion)
            switch(data.tipoOperacion){
                case 'DIRECTA':
                case 'ASOCIADA OPERADA':
                    /* cambio de denominación */
                    {
                        changeView.denominacion('catalogo', data)
                    }
                    /* cambio de unidades */
                    {
                        changeView.unidades()
                    }
                break;
                case 'ASOCIADAS':
                    /* cambio de denominación */
                    {
                        changeView.denominacion()
                    }
                    /* cambio de unidades */
                    {
                        changeView.unidades('sap')
                    }
                break;
            }
            // console.log('principal')
            // console.log(data)
        },
        subnumber(data){
            $('.double-item').removeClass('changed')
            /* cambio de ubicación */
                draw.locationFields(data, ubicaciones)
            switch(data.tipoOperacion){
                case 'DIRECTA':
                case 'ASOCIADA OPERADA':
                    /* cambio de denominación */
                    {
                        changeView.denominacion('subnumeros')
                    }
                    /* cambio de unidades */
                    {
                        changeView.unidades()
                    }
                break;
                case 'ASOCIADAS':
                    /* cambio de denominación */
                    {
                        changeView.denominacion()
                    }
                    /* cambio de unidades */
                    {
                        changeView.unidades('sap')
                    }
                break;
            }
            // console.log('subnúmero')
            // console.log(data)
        },
        denominacion(status, data){
            switch(status){
                case 'catalogo':
                    {
                        let options = `<option value="">SELECCIONE...</option>`
                        for(let i of catalogo){
                            if(i.clase == data.clase){
                                options += `<option value="${i.label}" data-unidad="${i.unidad}">${i.label}</option>`
                            }
                        }
                        let html = `<select name="denominacion" class="form-control" autocomplete="foo">${options}</select>`
                        $('.form-control[name="denominacion"]').replaceWith(html)
                    }
                break;
                case 'subnumeros':
                    {
                        let subnumberDenominacion = staticOptions.subnumberDenominacion
                        let options = `
                                <option value="">SELECCIONE...</option>
                                ${
                                    subnumberDenominacion.map(
                                        (val, key) => `<option value="${val}">${val}</option>`
                                    )
                                }
                            `
                        
                        let html = `<select name="denominacion" class="form-control" autocomplete="foo">${options}</select>`
                        $('.form-control[name="denominacion"]').replaceWith(html)
                    }
                break;
                default:
                    {
                        let html = `<input type="text" name="denominacion" class="form-control">`
                        $('.form-control[name="denominacion"]').replaceWith(html)
                    }
                break;
            }
        },
        unidades(status){
            let focus = $('.form-control[name="unidadMedida"]').parents('.double-item')
            $(focus).removeClass('changed')
            $(focus).find('> span i').remove()
            switch(status){
                case 'sap':
                    {
                        let options = `<option value="">SELECCIONE...</option>`
                        for(let i of unidadesAsociadas){
                            options += `<option value="${i.unidad}">${i.nombre} (${i.unidad})</option>`
                        }
                        let html = `<select name="unidadMedida" class="form-control" autocomplete="foo">${options}</select>`
                        $('.form-control[name="unidadMedida"]').replaceWith(html)
                    }
                break;
                default:
                    {
                        let html = `<input type="text" name="unidadMedida" class="form-control" disabled>`
                        $('.form-control[name="unidadMedida"]').replaceWith(html)
                    }
                break;
            }
        }
    }

// consultar por número de activo o número de inventario
    var queryData = null

    $('.form-control[name="activo"], .form-control[name="inventario"]').change( function(e) {

        $('#wait').fadeIn(400)
        const params = [
            'activo',
            'sn',
            'noInventario',
            'clase',
            'denominacion',
            'denominacion2',
            'capacidad',
            'unidadMedida',
            'tag',
            'serie',
            'fabricante',
            'modelo',
            'departamento',
            'municipio',
            'nombreMunicipio',
            'ubicacionGeografica',
            'nombreUbicacionGeografica',
            'ceco',
            'campoPlanta',
            'nombreCampoPlanta',
            'vuRemanenteAr01',
            'custodio',
            'siglaVicepresidencia',
            'vicepresidencia',
            'siglaGerencia',
            'gerencia',
            'tipoOperacion',
            'fechaCapitalizacion',
            'matriculaVehiculo',
            'claveAr01',
        ]

        let number = 0,
            type = null,
            sn = $('.form-control[name="subnumero"]').val().trim()

        if($(this).is('[name="activo"]')){
            number = $('.form-control[name="activo"]').val().trim()
            type = 'activo'
        }
        if($(this).is('[name="inventario"]')){
            number = $('.form-control[name="inventario"]').val().trim()
            type = 'noInventario'
        }

        queries
            .getActivo( number, sn, true, params.toString(), type )
            .then(data => {
                $('#wait').fadeOut(400)

                for(let d of data){
                    for(let c of custodios){
                        if(d.custodio == c.numeroPersonal){
                            d['nombreCustodio'] = c.primerNombre+" "+c.segundoNombre+" "+c.primerApellido+" "+c.segundoApellido
                            break
                        }
                    }
                }
                const isRepeat = dataValidations.repetitiveAssets(data)
                if(isRepeat.count > 1)
                {
                    draw.whichRepeatedAsset(isRepeat)
                }
                else
                {
                    queryData = data
    
                    draw.clearFields()
                    draw.datosMaestros(data, sn)
                    let primalData = draw.getSnData(data,sn)
                    
                    sn == 0 ? changeView.primal(primalData) : changeView.subnumber(primalData)
                }
            })
            .catch(err => {
                $('#wait').fadeOut(400)
                draw.modalmessage({message: 'Ingresar un activo valido'})
                draw.clearFields()
                
                console.log(err.responseText)
            })
        
        
    })

    $(document).on('change', '.form-control[name="subnumero"]', function(e) {

        let sn = $('.form-control[name="subnumero"]').val().trim()
        if(queryData != null){
            
            $('.new-data .form-control').val('')
            draw.clearFields()
            draw.datosMaestros(queryData, sn)
            /** s */
            let primalData = draw.getSnData(queryData,sn)
            sn == 0 ? changeView.primal(primalData) : changeView.subnumber(primalData)
        }


    })

    // Seleccionar activo/inventario repetido
    $(document).on('click','.choose-asset tbody tr', function(e){
        let data = {
            activo: $(this).attr('data-activo'),
            inventario: $(this).attr('data-inventario'),
            original: JSON.parse(decodeURIComponent(atob($(this).attr('data-orginal')))),
        }
        const result = draw.selectionFromRepeatedAsset(data)

        $('.choose-asset').remove()

        queryData = result
    
        draw.clearFields()
        draw.datosMaestros(result, 0)
        let primalData = draw.getSnData(result,0)
        
        changeView.primal(primalData)
    })
// REGLA 37: cambio de unidad de medida por denominación para principales y subnúmeros en operación directa
    
    $(document).on('change', '.form-control[name="denominacion"]', function(e){
        if($(this).is('select')){
            let val = $(this).val()
            if(val == 'ADICION + ACCESORIOS' || val == 'REEMPLAZO DE PARTE O COMPONENTE'){
                changeView.unidades('sap')
            }
            else{
                changeView.unidades()
            }

            if($(this).find('option:selected').data('unidad') != undefined){
                let focus = $('.form-control[name="unidadMedida"]')
                let unity = $(this).find('option:selected').data('unidad')
                $(focus).val(unity).change()
                $(focus).parent().addClass('edit changed')
            }
        }
    })

// autocompletar campos

    $(document).on('keyup', '.autocomplete', function(e){
        switch(e.keyCode){
            case 38:
            case 40:
            case 13:
            break;
            default:
                let value = $(this).val()
                let name = $(this).attr('name')
                switch(name){
                    case 'fabricante':
                        {
                            let json = fabricantes,
                                searchedFields = ['fabricante'],
                                getFields = ['fabricante']
        
                            autocomplete.draw(value, json, searchedFields, getFields, this)
                        }
                    break;
                    case 'ceco':
                        {
                            let json = cecos,
                            searchedFields = ['ceco'],
                            getFields = ['ceco','nombreCeco','siglaVicepresidencia','siglaGerencia', 'campoPlanta','nombreCampoPlanta']
                            // console.log("PASO1 - keyup")
                            autocomplete.draw(value, json, searchedFields, getFields, this)
                        }
                    break;
                    case 'campoPlanta':
                        {
                            let json = campoPlanta,
                            searchedFields = ['nombre'],
                            getFields = ['codigo','nombre']

                            autocomplete.draw(value, json, searchedFields, getFields, this)
                        }
                    break;
                }
            break;
        }
    })

// cambio de ubicaciones de departamento municipio y ubicación geográfica

    $(document).on('change','.form-control[name="departamento"]', function(){
        let data = {
            departamentoAux: String($('[data-name="departamento"]').html()).replace(/(?=<i>).*<\/i>/g, '').trim(),
            departamento: $(this).val()
        }
        draw.municipioSelector(data, ubicaciones)
    })

    $(document).on('change','.form-control[name="municipio"]', function(){
        let data = {
            departamentoAux: String($('[data-name="departamento"]').html()).replace(/(?=<i>).*<\/i>/g, '').trim(),
            departamento: $('[name="departamento"]').val(),
            municipioAux: String($('[data-name="municipio"]').html()).replace(/(?=<i>).*<\/i>/g, '').trim(),
            municipio: $(this).val()
        }
        draw.ubicacionSelector(data, ubicaciones)
    })

// validar campos
    $('#formatoDatosMaestros').validate(dataValidations.datosMaestros())
// Calc new VUT (vida útil técnica) from change of VUR (vida útil remanente) /
    var calcVUT = () => {
        
        let capDate = $('[data-name="fecha_capitalizacion"]').text().split('/')
        capDate = {
            "dia":capDate[2],
            "mes":capDate[1],
            "anho":capDate[0]
        }
        let vurDate = $('[name="fechaVUR"]').val().split('/')
        vurDate = {
            "dia":vurDate[2],
            "mes":vurDate[1],
            "anho":vurDate[0]
        }
        let vurValue = $('[name="vuRemanente"]').val(),
            newAge = ( ( Number(vurDate.anho) - Number(capDate.anho) ) *12 ) + ( Number(vurDate.mes) - Number(capDate.mes) ),
            newVUT = newAge + Number(vurValue)

        return `<div class="vut_value"><span>${newVUT} meses</span></div>`

    }
    $('.form-control[name="vuRemanente"],.form-control[name="fechaVUR"]').on('change', function(e){
        if($(this).attr("name") == "vuRemanente") {
            $('.form-control[name="fechaVUR"]').parents('.double-item').addClass('required')
            $('.form-control[name="fechaVUR"]').focus()
        }
        $('.informative-vut').hide()

        if($('[name="vuRemanente"]').parents('.double-item').is('.changed')){

            let html = calcVUT()

            $('.informative-vut').find('.vut_value').remove()
            $('.informative-vut').find('.form-section').append(html)
            $('.informative-vut').show()
        }
    })