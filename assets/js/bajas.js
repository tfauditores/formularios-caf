require('es6-promise').polyfill();
import queries from '../modules/queries'
import draw from '../modules/draw'
import storage from '../modules/storage'
import staticOptions from '../modules/staticOptions'
import {dataValidations, dataRules} from '../modules/dataValidations'
import '../modules/common'

// get custodios
    var custodios = null
    if(storage.getCookie() == false || storage.getLocal('custodios') == null){
        let params = [
            'id',
            'numeroPersonal',
            'registro',
            'correo',
            'primerNombre',
            'segundoNombre',
            'primerApellido',
            'segundoApellido'
        ]
        queries
            .getCustodios(params.toString())
            .then(data => {
                custodios = data
                storage.setLocal('custodios', custodios)
            })
            .catch(err => console.log(err.responseText))
    }
    else{
        custodios = storage.getLocal('custodios')
    }
// get SMM
    if(storage.getCookie() == false || storage.getLocal('SMM') == null){
        queries
            .getSMM()
            .then(data => {
                storage.setLocal('SMM', data)
            })
            .catch(err => console.log(err.responseText))
    }

// calcular sigma del valor contable neto
    const calcSigmaNet = (arr) => {
        const len = arr.length
        let sigma = 0
        for(let i of arr){
            sigma += Number(i.valorContableNetoAr01)
        }
        return sigma
    }
// calcular sigma del valor de adquisicion
    const calcSigmaAdq = (arr) => {
        const len = arr.length
        let sigma = 0
        for(let i of arr){
            sigma += Number(i.valorAdquisicionAr01)
        }
        return sigma
    }
// selección de baja total y baja parcial
    $('.label-selector').on('click', function(e) {
        
        e.preventDefault()

        $('.label-selector').find('[name="tipo_baja"]').removeAttr('checked')

        let $focus = $(this).find('[name="tipo_baja"]')
        let type = $focus.val()

        $focus[0].checked = true

        switch(type) {
            case 'baja_parcial':
                $('.sigma').stop().hide()
                $('.withdrawal-partial').stop().fadeIn(300)
                $('.show-subnumber').removeClass('hide-subnumber')
            break;
            default:
                $('.withdrawal-partial').hide()
                $('.sigma').stop().fadeIn(300)
                $('.show-subnumber').addClass('hide-subnumber')
            break;
        }
        $('.form-control[name="activo"], .form-control[name="inventario"]').change();

    })
// consultar por número de activo o número de inventario
    var queryData = null

    $('.form-control[name="activo"], .form-control[name="inventario"]').change( function(e) {

        $('#wait').fadeIn(400)

        const params = [
            'activo',
            'sn',
            'noInventario',
            'departamento',
            'nombreMunicipio',
            'nombreUbicacionGeografica',
            'nombreCeco',
            'ceco',
            'clase',
            'area',
            'siglaVicepresidencia',
            'siglaGerencia',
            'denominacion',
            'denominacion2',
            'fabricante',
            'modelo',
            'serie',
            'tag',
            'ubicacionGeografica',
            'municipio',
            'fechaCapitalizacion2',
            'fechaCapitalizacion',
            'valorContableNetoAr01',
            'valorAdquisicionAr01',
            'depreciacionAcumuladaAr01',
            'tipoOperacion',
            'custodio'
        ]

        let number = 0,
            type = null,
            sn = $('.form-control[name="subnumero"]').val().trim()

        if($(this).is('[name="activo"]')){
            number = $('.form-control[name="activo"]').val().trim()
            type = 'activo'
        }
        if($(this).is('[name="inventario"]')){
            number = $('.form-control[name="inventario"]').val().trim()
            type = 'noInventario'
        }

        queries
            .getActivo( number, sn, true, params.toString(), type )
            .then(data => {
                $('#wait').fadeOut(400)
                // isRepeat{count, originalData[], repeatElements[]}
                // Buscar si hay activos repetidos, ya sea por #Activo o #Inventario
                const isRepeat = dataValidations.repetitiveAssets(data)
                
                // A los datos originales de cada activo, se le agregan los sigmas y el nombre de custodio
                for(let d of data){
                    var sigma     = isRepeat.count>1 ? d['valorContableNetoAr01'] : calcSigmaNet(data)
                    var sigmaAdq  = isRepeat.count>1 ? d['valorAdquisicionAr01'] : calcSigmaAdq(data)
                    d['sigma'] = sigma
                    d['sigmaAdq'] = sigmaAdq
                    for(let c of custodios){
                        if(d.custodio == c.numeroPersonal){
                            d['nombreCustodio'] = c.primerNombre+" "+c.segundoNombre+" "+c.primerApellido+" "+c.segundoApellido
                            break
                        }
                    }
                }
                // Si hay activos repetidos muestra la tabla de selección
                if(isRepeat.count > 1)
                {
                    draw.whichRepeatedAsset(isRepeat)
                }
                else
                {
                    queryData = data
                   
                    draw.clearFields()
                    draw.bajas(data, sn)

                    $('.show-characteristics').fadeIn()
                }
           

            })
            .catch(err => {
                $('#wait').fadeOut(400)
                draw.modalmessage({message: 'Ingresar un activo valido'})
                draw.clearFields()
                
                console.log(err.responseText)
            })
        
        
    })

    $(document).on('change', '.form-control[name="subnumero"]', function(e) {

        let sn = $('.form-control[name="subnumero"]').val().trim()
        if(queryData != null){
            
            $('.new-data .form-control').val('')
            draw.clearFields()
            draw.bajas(queryData, sn)

        }

    })
    // Seleccionar activo/inventario repetido
    $(document).on('click','.choose-asset tbody tr', function(e){

        let data = {
            activo: $(this).attr('data-activo'),
            inventario: $(this).attr('data-inventario'),
            original: JSON.parse(decodeURIComponent(atob($(this).attr('data-orginal')))),
        }
        const result = draw.selectionFromRepeatedAsset(data)

        $('.choose-asset').remove()

        queryData = result
        draw.clearFields()
        draw.bajas(result, 0)

        $('.show-characteristics').fadeIn()
    })
// limitar le valor de baja parcial cuando cambia
    $('[name="number_baja_parcial"]').on('keyup', function(e){
        let value = Number($(this).unmask())
        let limit = Number($(this).attr('data-limit'))

        if(/\%/g.test($(this).val()))
        {
            value = Math.round(Number($(this).val().replace(/[\.\,\$\%]/g,'')) * limit / 100)
        }
        
        value > limit ? $(this).val(limit) : $(this).val(value)

        $(this).priceFormat({
            prefix:"$ ",
            centsSeparator:",",
            thousandsSeparator:".",
            clearOnEmpty:!0,
            centsLimit: 0,
            allowNegative: false
        })
        
        draw.caclValorNetoMinusBajaParcial($('[name="subnumero"]').val())
    })
// cargar disposición final cuando se modifica motivo de retiro
    $('[name="motivo_retiro"]').change(function(e){
        let retirement = $(this).val(),
            html = `<option value="">SELECCIONE...</option>`
        for(let i of staticOptions.motivoRetiro_disposicionFinal)
        {
            if(i.name == retirement)
            {
                for(let df of i.options)
                {
                    html += `<option value="${df}">${df}</option>`
                }
            }
        }
        $('[name="disposicion_final"]').html(html)
    })
// validar campos
    $('#formatoBajas').validate(dataValidations.bajas())
// Cambio de opciones de motivo de retiro
    {
        let html = '<option value="">SELECCIONE...</option>'
        for(let i of staticOptions.motivoRetiro_disposicionFinal)
        {
            html += `<option value="${i.name}">${i.name}</option>`
        }
        $('[name="motivo_retiro"]').html(html)
    }
