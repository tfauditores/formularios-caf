const storage = {
    setLocal(key, value){
        // localStorage.setItem(key, btoa(JSON.stringify(value)) )
        localStorage.setItem(key, JSON.stringify(value) )
    },
    getLocal(key){
        // return JSON.parse( atob(localStorage.getItem(key)) )
        return JSON.parse( localStorage.getItem(key) )
    },
    setCookie(cname = 'queries', cvalue = '0.1', exdays = 7){
        let d = new Date()
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000) )
        let expires = "expires="+d.toUTCString()
        document.cookie = `${cname}=${cvalue};${expires};path=/`
    },
    getCookie(cname = 'queries'){
        let name = `${cname}=`,
            ca = document.cookie.split(';')
        for(let i of ca){
            let c = i
            while(c.charAt(0) == ' '){
                c = c.substring(1)
            }
            if(c.indexOf(name) == 0){
                return c.substring(name.length, c.length)
            }
        }
        return false
    }
}

export default storage