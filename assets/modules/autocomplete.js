const autocomplete = {
    getData(value, json, searchedFields, getFields){
        let regExp = new RegExp('^'+value, 'i')
        let arr = new Array()

        for(let row of json){
            for(let field of searchedFields){
                if( regExp.test(row[field]) ){
                    let tempJSON = {}
                    for(let gf of getFields){
                        tempJSON[gf] = row[gf]
                    }
                    arr.push(tempJSON)
                }
            }
        }
        return arr
    },
    draw(value, json, searchedFields, getFields, element) {
        if(value.trim().length > 0){
            let data = this.getData(value, json, searchedFields, getFields),
                options = '',
                focus = $(element).attr('name')
            
            if(focus == 'fabricante'){
                for(let i of data){
                    options += `<div class="selector">${String(i.fabricante).toUpperCase()}</div>`
                }
            }
            if(focus == 'ceco'){
                for(let i of data){
                    //console.log(i)
                    options += `<div class="selector" data-ceco="${String(i.ceco).toUpperCase()}" data-campo="${String(i.campoPlanta).toUpperCase()}" data-nombre-campo="${String(i.nombreCampoPlanta).toUpperCase()}" >
                        ${String(i.nombreCeco).toUpperCase()} - ${String(i.ceco).toUpperCase()}
                        ${i.siglaVicepresidencia != "" ? " - " +String(i.siglaVicepresidencia).toUpperCase() : ""}
                        ${i.siglaGerencia != "" ? " - " +String(i.siglaGerencia).toUpperCase() : ""}
                    </div>`
                }
            }
            if(focus == 'campoPlanta'){
                for(let i of data){
                    options += `<div class="selector" data-code="${String(i.codigo).toUpperCase()}" data-name="${String(i.nombre).toUpperCase()}">${String(i.nombre).toUpperCase()} - ${String(i.codigo).toUpperCase()}</div>`
                }
            }
    
            $(element).next('.autocomplete-wrapper').off().remove()
            let html = `<div class="autocomplete-wrapper">
                <div class="autocomplete-box">
                    ${options}
                </div>
            </div>`
            $(element).after(html)
            // console.log("PASO 2 - draw.autocomplete")
        }
    }
}
export default autocomplete
