jQuery.validator.addMethod( 
    "regex", 
    function(value, element, regexp) {
        let re = new RegExp(regexp);
        return this.optional(element) || re.test(value);
    },
    "El campo contiene caracteres no permitidos"
)
jQuery.validator.addMethod(
    'vidaUtil',
    function(value, element, params){
        let type = Number($('[name="subnumero"]').val()) > 0 ? 'subnumero' : 'principal'
        let val = Number(value);
        switch(type){
            case 'principal':
                //if(val < 12){
                if(val < 0){
                    return false;
                }
            break;
            case 'subnumero':
                //if(val < 1){
                if(val < 0){
                    return false;
                }
            break;
        }
        return true;
    }, 
    function(params){
        let type = Number($('[name="subnumero"]').val()) > 0 ? 'subnumero' : 'principal'
        switch(type){
            case 'principal':
                return 'La vida útil debe ser mayor a 1 mes ';
            break;
            case 'subnumero':
                return 'La vida útil debe ser mayor a 1 mes ';
            break;
        }
    }
)
jQuery.validator.addMethod(
    'inventory',
    function(value, element){
        var validate = Number($(element).attr('data-exist'));

        if( validate == 0 || isNaN(validate) ){
            return true;
        }
        else{
            return false;
        }
    },
    'El número de inventario ya existe'
)
jQuery.validator.addMethod(
    'seleccion',
    function(value, element, params){
        if(value == "" || value == "SELECCIONE..."){
            return false
        }
        return true
    }, 
    'Seleccione alguna opción'
)
const dataRules = {
    rules: {
        activo: {
            required: true
        },
        denominacion: {
            required: true,
            maxlength: 50
        },
        denominacion_complementaria: {
            required: true,
            regex:  "^[a-zA-Z0-9À-ÿ\#\-\_\. ]*$",
        },
        denominacion2: {
            minlength: 5,
            maxlength: 50,
            regex:  "^[a-zA-Z0-9À-ÿ \+\-\_]*$",
            required: true
        },
        clase: {
            required: true,
            clase: true
        },
        inventario: {
            required: true,
            maxlength: 7,
            regex: "^[a-zA-Z0-9]*$",
            inventory: 1
        },
        matricula_inmobiliaria:{
            required: true,
            maxlength: 25,
            regex: "^[a-zA-Z0-9]*$",
            inventory: 1
        },
        fabricante: {
            maxlength: 30,
            required: true,
            regex: "^[a-zA-Z0-9À-ÿ \+\-\_]*$"
        },
        modelo: {
            maxlength:25,
            regex: "^[a-zA-Z0-9À-ÿ \+\-\_]*$"
        },
        serie: {
            maxlength: 18,
            regex: "^[a-zA-Z0-9À-ÿ \+\-\_]*$"
        },
        tag: {
            maxlength: 15,
            regex: "^[a-zA-Z0-9À-ÿ \+\-\_]*$"
        },
        vida_util: {
            required: true,
            maxlength: 3,
            regex: "[^0-9]*$",
            number: true,
            vidaUtil: true
        },
        unidad_medida: {
            required: true
        },
        capacidad: {
            required: true,
            maxlength: 10,
            regex: "^[0-9]{1,4}([\.][0-9]{0,3})?$"
        },
        departamento: {
            required:true
        },
        municipio: {
            required:true
        },
        ubicacion_geografica: {
            required:true
        },
        unidad_funcional: {
            required:true
        },
        costo: {
            required:true
        },
        ceco:{
            required: true,
            minlength: 6,
            maxlength: 6,
            regex: "^[A-Za-z]{2,3}[0-9]{3,4}$"
        },
        campo_planta: {
            regex: "^[a-zA-Z0-9À-ÿ \(\)-\._]*$"
        },
        activo_traslado: {
            required: true
        },
        sn_traslado: {
            required: true
        },
        fecha_traslado:{
            required: true
        },
        valor_traslado:{
            required: true
        },
        motivo_traslado:{
            required: true
        },
        matricula:{
            required: true,
            maxlength: 15,
            regex: "^[a-zA-Z0-9]*$"
        },
        anio:{
            required: true,
            number: true,
            maxlength: 4,
            minlength: 4,
            max: (new Date()).getFullYear() + 1,
            min: 1980
        },
        motivo_retiro: {
            required: true,
            seleccion: true
        },
        disposicion_final: {
            required: true,
            seleccion: true
        },
        baja_parcial: {
            required: true,
        },
        observaciones:{
            maxlength: 300,
        },
        matriculaVehiculo:{
            required:true,
            minlength: 4,
            maxlength: 11,
            regex: "^[a-zA-Z0-9 -]*$"
        }
    },
    messages: {
        activo: {
            required: "El número debe conicidir con un número de activo"
        },
        denominacion: {
            required: "Este campo es requerido",
            maxlength: "Excede el límite de caracteres según Diccionario"
        },
        denominacion_complementaria: {
            required: "Este campo es requerido",
        },
        denominacion2: {
            minlength: "La denominación 2 es muy corta",
            maxlength: "Excede el límite de caracteres según Diccionario",
            required: "Este campo es requerido",
        },
        clase: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        inventario: {
            maxlength: "Excede el límite de caracteres en número de inventario",
            required: "Este campo es requerido",
        },
        matricula_inmobiliaria:{
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        fabricante: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
            regex: "El campo contiene caracteres no permitidos"
        },
        modelo: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        serie: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        tag: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        vida_util: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        unidad_medida: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        capacidad: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
            regex: "Hay caracteres no permitidos o más de 3 cifras decimales"
        },
        departamento: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        municipio: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        ubicacion_geografica:{
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        unidad_funcional: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        costo:{
            maxlength: "Excede el límite de caracteres",
            min: "El valor no debe ser menor a ",
            required: "Este campo es requerido",
        },
        ceco:{
            required: "Este campo es requerido",
            minlength: "La longitud del CECO debe ser de 6 caracteres",
            maxlength: "Excede el límite de caracteres",
            regex: "El código no cumple con la estructura de un CECO"
        },
        campo_planta:{
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        activo_traslado: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        sn_traslado: {
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        fecha_traslado:{
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        valor_traslado:{
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        motivo_traslado:{
            maxlength: "Excede el límite de caracteres",
            required: "Este campo es requerido",
        },
        matricula: {
            required: "Este campo es requerido para vehículos",
            maxlength: "Máximo 15 caracteres Alfanuméricos",
            regex: "El campo contiene caracteres no permitidos, se admiten números y letras",
        },
        anio: {
            required: "Este campo es requerido para vehículos",
            number: "Este campo solamente acepta números",
            maxlength: "Máximo 4 caracteres de tipo numérico",
            minlength: "Mínimo 4 caracteres de tipo numérico",
            max: "El modelo no puede ser superior a "+ (new Date().getFullYear() + 1),
            min: "El modelo no puede ser menor a 1980",
        },
        motivo_retiro: {
            required: "Se debe seleccionar alguna opción"
        },
        disposicion_final: {
            required: "Se debe seleccionar alguna opción"
        },
        baja_parcial: {
            required: "Ingresar un valor de baja"
        },
        observaciones:{
            maxlength: "Supera el límite de caracteres permitidos",
        },
        matriculaVehiculo:{
            required:"Ingresa el valor para la nueva matrícula",
            minlength:"La longitud del nuevo valor es muy corta",
            maxlength: "El valor supera el límite de caracteres establecido",
            regex: "El campo contiene caracteres no permitidos"
        }
    },
    rule(focus){
        return this.rules[focus]
    },
    message(focus){
        return this.messages[focus]
    }
}
const dataValidations = {
    bajas(){
        let shapeObject = {
            debug: true,
            rules: {
                activo: dataRules.rule('activo'),
                motivo_retiro: dataRules.rule('motivo_retiro'),
                disposicion_final: dataRules.rule('disposicion_final'),
                number_baja_parcial: dataRules.rule('baja_parcial'),
            },
            messages: {
                activo: dataRules.message('activo'),
                motivo_retiro: dataRules.message('motivo_retiro'),
                disposicion_final: dataRules.message('disposicion_final'),
                number_baja_parcial: dataRules.message('baja_parcial'),
            }
        }
        return shapeObject
    },
    datosMaestros(){
        let shapeObject = {
            debug: true,
            rules: {
                denominacion: dataRules.rule('denominacion'),
                denominacion2: dataRules.rule('denominacion2'),
                capacidad: dataRules.rule('capacidad'),
                unidadMedida: dataRules.rule('unidad_medida'),
                noInventario: dataRules.rule('inventario'),
                tag: dataRules.rule('tag'),
                serie: dataRules.rule('serie'),
                fabricante: dataRules.rule('fabricante'),
                modelo: dataRules.rule('modelo'),
                departamento: dataRules.rule('departamento'),
                municipio: dataRules.rule('municipio'),
                ubicacionGeografica: dataRules.rule('ubicacion_geografica'),
                ceco: dataRules.rule('ceco'),
                campoPlanta: dataRules.rule('campo_planta'),
                vuRemanente: dataRules.rule('vida_util'),
                fechaVUR: dataRules.rule('fecha_traslado'),
                observaciones: dataRules.rule('observaciones'),
                matriculaVehiculo: dataRules.rule('matriculaVehiculo'),
            },
            messages: {
                denominacion: dataRules.message('denominacion'),
                denominacion2: dataRules.message('denominacion2'),
                capacidad: dataRules.message('capacidad'),
                unidadMedida: dataRules.message('unidad_medida'),
                noInventario: dataRules.message('inventario'),
                tag: dataRules.message('tag'),
                serie: dataRules.message('serie'),
                fabricante: dataRules.message('fabricante'),
                modelo: dataRules.message('modelo'),
                departamento: dataRules.message('departamento'),
                municipio: dataRules.message('municipio'),
                ubicacionGeografica: dataRules.message('ubicacion_geografica'),
                ceco: dataRules.message('ceco'),
                campoPlanta: dataRules.message('campo_planta'),
                vuRemanente: dataRules.message('vida_util'),
                fechaVUR: dataRules.message('fecha_traslado'),
                observaciones: dataRules.message('observaciones'),
                matriculaVehiculo: dataRules.message('matriculaVehiculo'),
            }
        }
        return shapeObject
    },
    altasTralados(){
    },
    otherValidations(element){
        let field = element.attr('name')
        switch(field){
            case 'ceco': 
                {
                    document.querySelector('[name="vuRemanente"]').parentNode.classList.remove('required','edit')
                    document.querySelector('[name="campoPlanta"]').parentNode.classList.remove('required','edit')

                    document.querySelector('[name="vuRemanente"]').parentNode.parentNode.classList.remove('hide')
                    document.querySelector('[name="fechaVUR"]').parentNode.parentNode.classList.remove('hide')

                    $('[name="campoPlanta"]').rules('remove','required')
                    let oldCeco = element[0].parentNode.querySelector('[data-name="ceco"]').getAttribute('data-ceco')
                    let newCeco = element[0].value
                    let RegExp = /^PR/g
                // REGLA 43
                // REGLA 44
                // Para cambio de CeCo Se evalúa el cambio entre CeCos de Producción(PR) a otros
                    // Si el Ceco anterior es PR
                        if(RegExp.test(oldCeco) == true){
                            RegExp.lastIndex = 0
                            // si el cambio es de PR a otro
                            if( RegExp.test(newCeco) == false){
                                document.querySelector('[name="vuRemanente"]').parentNode.classList.add('required','edit')
                            }
                        }
                    // Si el Ceco anterior es distinto a PR
                        else{
                            RegExp.lastIndex = 0
                            // si el CeCo nuevo es PR
                            if( RegExp.test(newCeco) == true ){
                                // Despliega la edición de campo planta y lo hace requerido
                                document.querySelector('[name="campoPlanta"]').parentNode.classList.add('required','edit')
                                $('[name="campoPlanta"]').rules('add',{
                                    required: true
                                })
                                // Oculta las casillas de Vida Util ya que la vida útil será por UoP
                                document.querySelector('[name="vuRemanente"]').parentNode.parentNode.classList.add('hide')
                                document.querySelector('[name="fechaVUR"]').parentNode.parentNode.classList.add('hide')
                            }
                        }
                    // Sugerencia del campoPlanta según CECO
                    // REGLA 65
                    {
                        let newCampo = document.querySelector('[name="ceco"]').getAttribute('data-campo'),
                            newNombreCampo = document.querySelector('[name="ceco"]').getAttribute('data-nombre-campo'),
                            msg = "Sugerencia de Campo Planta"
                        if(newCampo!=null && newNombreCampo!=null){
                            let html = 
                                `<div class="campoSuggest"><div class="table"><div class="table-cell">
                                <div class="content">
                                    <h2 class="title">${msg}</h2>
                                    <h4 class="">${newNombreCampo}</h4>
                        
                                    <button type="button" class="btn btn-styled" data-campo="${newCampo}" data-nombre-campo="${newNombreCampo}" >Aceptar sugerencia</button>
                                    <button type="button" class="btn btn-styled2" >No usar</button>
                        
                                </div>
                                </div></div></div>`
                            $('.campoSuggest').remove()
                            $('body').append(html)
                        }

                        /*
                        let newCampo = document.querySelector('[name="ceco"]').getAttribute('data-campo')
                        let newNombreCampo = document.querySelector('[name="ceco"]').getAttribute('data-nombre-campo')
                        let nodeLength = document.querySelector('[data-name="campoPlanta"]').childNodes.length
                        if(nodeLength>1){
                            let i = document.querySelector('[data-name="campoPlanta"]').childNodes[1]
                            document.querySelector('[data-name="campoPlanta"]').removeChild(i)
                        }
                        let oldCampo = document.querySelector('[data-name="campoPlanta"]').textContent.trim()

                        let html = oldCampo + ' <i>'+newNombreCampo+'<i>'
                        document.querySelector('[data-name="campoPlanta"]').innerHTML= html
                        document.querySelector('[data-name="campoPlanta"] i').setAttribute('data-codigo-campo', newCampo)
                        document.querySelector('[name="campoPlanta"').parentNode.classList.add('changed')
                        */
                    }
                    //
                }
            break;
            case 'departamento':
                {
                    document.querySelector('[name="municipio"]').parentNode.classList.remove('required')
                    document.querySelector('[name="ubicacionGeografica"]').parentNode.classList.remove('required')

                    let oldDepartamento = element.parentNode.querySelector('[data-name="departamento"]').getAttribute('data-departamento'),
                        newDepartamento = element.value

                    if(oldDepartamento.trim().toLowerCase() != newDepartamento.trim().toLowerCase()){
                        document.querySelector('[name="municipio"]').parentNode.classList.add('required')
                        document.querySelector('[name="ubicacionGeografica"]').parentNode.classList.add('required')
                    }
                }
            break;
            case 'municipio':
                {
                    document.querySelector('[name="ubicacionGeografica"]').parentNode.classList.remove('required')
                    
                    let oldMunicipio = element.parentNode.querySelector('[data-name="municipio"]').getAttribute('data-municipio'),
                        newMunicipio = element.value
                        
                    if(oldMunicipio.trim().toLowerCase() != newMunicipio.trim().toLowerCase()){
                        document.querySelector('[name="ubicacionGeografica"]').parentNode.classList.add('required')
                    }
                }
            break;
            case 'vuRemanente':
                {
                    document.querySelector('[name="fechaVUR"]').parentNode.classList.remove('required')
                    
                    let oldVUR = element.parentNode.querySelector('[data-name="vuRemanente"]').getAttribute('data-vur'),
                        newVUR = element.value
                        
                    if(oldVUR.trim().toLowerCase() != newVUR.trim().toLowerCase()){
                        document.querySelector('[name="fechaVUR"]').parentNode.classList.add('required')
                    }
                }
            break;
            case 'fechaVUR':
                {
                    document.querySelector('[name="vuRemanente"]').parentNode.classList.remove('required')
                    
                    if( !document.querySelector('[name="vuRemanente"]').parentNode.classList.contains('edit') ){
                        document.querySelector('[name="vuRemanente"]').parentNode.classList.add('required')
                    }
                }
            break;
        }
    },
    repetitiveAssets(data){
        // Crea nuevo array con los elementos que cumplen la condición 
        const arrData = data.filter(obj => obj.sn == 0)
        // Devuelte un array {cantidad de items con sn=0 , array con los elementos filtrados , array con los datos originales}
        return {count: arrData.length, repeatElements: arrData, originalData: data}
    }
}

export {dataValidations, dataRules }