import {dataRules} from '../modules/dataValidations'
import queries from './queries'
import staticOptions from './staticOptions'
import storage from '../modules/storage'

const draw = {
    getSnData(data, sn){
        let primalData = data[0]
        let count = 0
        for(let i of data){
            if(i.sn == sn){
                primalData = data[count]
            }
            count++
        }

        return primalData
    },
    datosMaestros(data, sn) {
        this.clearFields('fields')
        /** Por defecto en data viene la lista de principal y subnúmeros, entonces se lee solamente lo que indique SN */
        let primalData = draw.getSnData(data,sn)
        /** Si consulta un subnúmero antes del principal deja por defecto el límite de vidautil el mismo que ya tiene*/
        if (!$('[name="vuRemanente"]').is('data-vidautil')){
            $('[name="vuRemanente"]').attr('data-vidautil', primalData.vuRemanenteAr01) 
        }
        /** Si el SN=0 vuelve a cargar la VUR en el atributo 'data-vidautil' del input VUR */
        if(sn == 0){
            $('[name="vuRemanente"]').removeAttr('data-vidautil')
            $('[name="vuRemanente"]').attr('data-vidautil', primalData.vuRemanenteAr01)
        }
        /** Verifica si la clase del activo hace parte de la lista de clases de INMUEBLES para hacer cambio de labels*/
        this.inmueblesNames(primalData.clase)
        /** Verifica si para la clase del activo aplica la edición de SERIE para mostrar input de edición de SERIE*/
        this.showSerialInput(primalData.clase)
        /** Verifica si la clase del activo hace parte de la lista de clases de VEHICULOS para mostrar input de edición MATRICULA*/
        this.isVehiculo(primalData.clase)
        /** Verifica si es subnúmero o principal para no habilitar algunos input */
        this.isSubnumero(sn)
        /** Verifica la clave de Depreciación. Si no es ZVUR, no solicita cambio de VU */
        this.isUOP(primalData.claveAr01)

        /** Muestra las opciones detalladas del formulario */
        $('.show-characteristics').fadeIn(400)
        $('.new-data').fadeIn(400)
        /** Se muestra información extra del activo (no modificable) */
        $('[data-name="operacion"]').text( String(primalData.tipoOperacion).toUpperCase() )
        $('[data-name="clase"]').text( String(primalData.clase).toUpperCase() )
        $('[data-name="custodio"]').text( String(primalData.nombreCustodio).toUpperCase() )
        $('[data-name="vicepresidencia"]').text( String(primalData.vicepresidencia).toUpperCase() ).attr('data-code', primalData.siglaVicepresidencia)
        $('[data-name="gerencia"]').text( String(primalData.gerencia).toUpperCase() ).attr('data-code', primalData.siglaGerencia)
        $('[data-name="fecha_capitalizacion"]').text( String(primalData.fechaCapitalizacion).toUpperCase().replace(/[-]/g,'/') )
        /** Se muestra información "editable" del activo */
        $('[data-name="denominacion"]').text( String(primalData.denominacion).toUpperCase() ).attr('data-denominacion', primalData.denominacion)
        $('[data-name="denominacion2"]').text( String(primalData.denominacion2).toUpperCase() )
        $('[data-name="capacidad"]').text( String(primalData.capacidad).toUpperCase() )
        $('[data-name="unidadMedida"]').text( String(primalData.unidadMedida).toUpperCase() )
        $('[data-name="noInventario"]').text( String(primalData.noInventario).toUpperCase() )
        $('[data-name="tag"]').text( String(primalData.tag).toUpperCase() ).attr('data-tag', primalData.tag )
        $('[data-name="serie"]').text( String(primalData.serie).toUpperCase() ).attr('data-serie', primalData.serie)
        $('[data-name="fabricante"]').text( String(primalData.fabricante).toUpperCase() )
        $('[data-name="modelo"]').text( String(primalData.modelo).toUpperCase() )
        $('[data-name="departamento"]').text( String(primalData.departamento).toUpperCase() ).attr('data-departamento', primalData.departamento)
        $('[data-name="municipio"]').text( String(primalData.nombreMunicipio).toUpperCase() ).attr('data-municipio', primalData.nombreMunicipio)
        $('[data-name="ubicacionGeografica"]').text( String(primalData.nombreUbicacionGeografica).toUpperCase() )
        $('[data-name="ceco"]').text( String(primalData.ceco).toUpperCase() ).attr('data-ceco', primalData.ceco)
        $('[data-name="campoPlanta"]').text( String(primalData.nombreCampoPlanta).toUpperCase() )
        $('[data-name="vuRemanente"]').text( String(primalData.vuRemanenteAr01).toUpperCase() ).attr('data-vur', primalData.vuRemanenteAr01)
        $('[data-name="tipoOperacion"]').text( String(primalData.tipoOperacion).toUpperCase() ).attr('data-tipoOperacion', primalData.tipoOperacion)
        $('[data-name="matriculaVehiculo"]').text( String(primalData.matriculaVehiculo).toUpperCase() ).attr('data-matriculaVehiculo', primalData.matriculaVehiculo)

        /** Si data contiene más de un subnúmero reconstruye el selector de subnúmeros */
        if(data.length > 1){
            let subnumbers = `<select name="subnumero" class="form-control">`
            for(let i of data){
                subnumbers += `<option value="${i.sn}">${i.sn}</option>`
            }
            subnumbers += `</select>`
            $('.form-control[name="subnumero"]').replaceWith(subnumbers)
        }
        /** Si data contiene SOLAMENTE un subnúmero (el principal) reconstruye el selector de subnúmeros */
        else{
            let subnumbers = `<input type="number" name="subnumero" class="form-control" placeholder="SUBNÚMERO" value="0" min="0">`
            $('.form-control[name="subnumero"]').replaceWith(subnumbers)
        }
        $('.form-control[name="activo"]').val(primalData.activo)
        $('.form-control[name="subnumero"]').val(primalData.sn)
        $('.form-control[name="inventario"]').val(primalData.noInventario)
    },
    bajas(data, sn) {

        this.clearFields('fields')

        let primalData = draw.getSnData(data,sn)
        //console.log(primalData)
        $('[data-name="departamento"]').text( String(primalData.departamento).toUpperCase() )
        $('[data-name="municipio"]').text( String(primalData.nombreMunicipio).toUpperCase() )
        $('[data-name="ubicacion_geografica"]').text( String(primalData.nombreUbicacionGeografica).toUpperCase() )
        $('[data-name="nombre_ceco"]').text( String(primalData.nombreCeco).toUpperCase() )
        $('[data-name="numero_ceco"]').text( String(primalData.ceco).toUpperCase() )
        $('[data-name="area"]').text( String(primalData.area).toUpperCase() )
        $('[data-name="vicepresidencia"]').text( String(primalData.siglaVicepresidencia).toUpperCase() )
        $('[data-name="gerencia"]').text( String(primalData.siglaGerencia).toUpperCase() )
        $('[data-name="denominacion"]').text( String(primalData.denominacion).toUpperCase() )
        $('[data-name="denominacion_2"]').text( String(primalData.denominacion2).toUpperCase() )
        $('[data-name="marca_fabricante"]').text( String(primalData.fabricante).toUpperCase() )
        $('[data-name="modelo"]').text( String(primalData.modelo).toUpperCase() )
        $('[data-name="serie"]').text( String(primalData.serie).toUpperCase() )
        $('[data-name="tag"]').text( String(primalData.tag).toUpperCase() )
        $('[data-name="custodio"]')
            .text( String(primalData.nombreCustodio).toUpperCase() )
            .attr('data-custodio', String(primalData.custodio).toUpperCase())
        $('[data-name="cod_municipio"]').text( String(primalData.municipio).toUpperCase() )
        $('[data-name="cod_ubicacion_geografica"]').text(String(primalData.ubicacionGeografica).toUpperCase());
        $('[data-name="fecha_capitalizacion"]').text( (primalData.fechaCapitalizacion).replace(/-/g,'/').replace(' 0:00:00','') )
        $('[data-name="tipo_operacion"]').text( String(primalData.tipoOperacion).toUpperCase() )
        //asignación de valores en los inputs dependiendo si es baja parcial o baja total
        //si se trata de baja total, se da de baja el valor neto
        //si se trata de paja parcial se tiene en cuenta el valor de acdquisicion
        let adq = $('[name="tipo_baja"]:checked').val() == 'baja_total' ? primalData.sigmaAdq : primalData.valorAdquisicionAr01
        $('[data-name="valor_adquisicion"]')
            .text(adq)
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            })
        let net = $('[name="tipo_baja"]:checked').val() == 'baja_total' ? primalData.sigma : primalData.valorContableNetoAr01
        $('[data-name="valor_contable_neto"]')
            .text(net)
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            })

        $('[name="valor_adquisicion"], [name="number_baja_total"], [name="number_baja_parcial"]')
            .val( Number(adq) )
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            })
        $('[name="number_adq_total"]')
            .val( Number(primalData.sigmaAdq) )
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            })

        this.caclValorNetoMinusBajaParcial(primalData.sn)

        if(data.length > 1){
            let subnumbers = `<select name="subnumero" class="form-control">`
            for(let i of data){
                subnumbers += `<option value="${i.sn}">${i.sn}</option>`
            }
            subnumbers += `</select>`
            $('.form-control[name="subnumero"]').replaceWith(subnumbers)
        }
        else{
            let subnumbers = `<input type="number" name="subnumero" class="form-control" placeholder="SUBNÚMERO" value="0" min="0">`
            $('.form-control[name="subnumero"]').replaceWith(subnumbers)
        }

        $('.sigma .value')
            .text(primalData.sigma)
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            })
        primalData.sigma > Number(primalData.valorContableNetoAr01) ? $('.sigma label span').text('(0+SN)') : $('.sigma label span').text('PRINCIPAL')

        $('.form-control[name="activo"]').val(primalData.activo)
        $('.form-control[name="subnumero"]').val(primalData.sn)
        $('.form-control[name="inventario"]').val(primalData.noInventario)
    },
    inmueblesNames(clase){

        let clasesInmuebles = staticOptions.clasesInmuebles
        /**
         * REGLA 38
         * REGLA 40 (regla de validación)
         *  Si la clase del activo corresponde a inmueble, cambiar:
         *  "noINVENTARIO" por "MATRICULA INMOBILIARIA"
         *  "TAG" por "CODIGO SIG"
         *  "FABRICANTE" por "NO. ESCRITURA"
         *  "MODELO" por "NOTARIA DE REGISTRO"
         */
        if(clasesInmuebles.indexOf(clase) != -1){
            // "noINVENTARIO" por "MATRICULA INMOBILIARIA"
            // Cambiar el texto del label y las reglas de validación del input
            {
                let focus = $('.form-control[name="noInventario"]'),
                    {required, maxlength, regex, inventory} = dataRules.rule('matricula_inmobiliaria')
                focus.parents('.form-section').find('label').text('No. MATRÍCULA INMOBILIARIA:')
                focus.rules('remove')
                focus.rules('add', {
                        required, 
                        maxlength, 
                        regex, 
                        inventory
                    })
            }
            // "TAG" por "CODIGO SIG"
            // Se conserva la misma regla
            $('.form-control[name="tag"]').parents('.form-section').find('label').text('CÓDIGO SIG:')
            
            // "FABRICANTE" por "NO. ESCRITURA"
            // Se conserva la misma regla
            // Se remueve la opción de autocompletado de fabricante
            $('.form-control[name="fabricante"]').removeClass('autocomplete').parents('.form-section').find('label').text('No. ESCRITURA:')

            // "MODELO" por "NOTARIA DE REGISTRO"
            // Se conserva la misma regla
            $('.form-control[name="modelo"]').parents('.form-section').find('label').text('NOTARÍA DE REGISTRO:')
        }
        /**
         *  Si la clase del activo _NO_ corresponde a inmueble, cambiar:
         *  "MATRICULA INMOBILIARIA" por "noINVENTARIO"
         *  "CODIGO SIG" por "TAG"
         *  "NO. ESCRITURA" por "FABRICANTE"
         *  "NOTARIA DE REGISTRO" por "MODELO"
         */
        else{
            // Matrícula inmobiliaria
            {
                let focus = $('.form-control[name="noInventario"]'),
                    {required, maxlength, regex, inventory} = dataRules.rule('inventario')
                focus.parents('.form-section').find('label').text('NO INVENTARIO:')
                focus.rules('remove')
                focus.rules('add', {
                        required, 
                        maxlength, 
                        regex, 
                        inventory
                    })
            }
            // "CODIGO SIG" por "TAG"
            $('.form-control[name="tag"]').parents('.form-section').find('label').text('TAG:')
            // "NO. ESCRITURA" por "FABRICANTE"
            // Se habilita la opción de autocompletado de fabricante
            $('.form-control[name="fabricante"]').addClass('autocomplete').parents('.form-section').find('label').text('FABRICANTE:')
            // "NOTARIA DE REGISTRO" por "MODELO"
            $('.form-control[name="modelo"]').parents('.form-section').find('label').text('MODELO:')
        }
    },
    showSerialInput(clase){
        // REGLA 64
        let clasesNoSerie = staticOptions.clasesNoSerie
        if(clasesNoSerie.indexOf( clase.substring(0,2) ) != -1){
            $('[data-name="serie"]').parents('.double-item').removeClass('changed required edit')
            $('[data-name="serie"]').parents('.form-section').hide()
        }
        else{
            $('[data-name="serie"]').parents('.form-section').show()
        }
    },
    isVehiculo(clase){
        // REGLA 77
        let clasesVehiculos = staticOptions.clasesVehiculos
        if(clasesVehiculos.indexOf(clase) != -1){
            $('[data-name="matriculaVehiculo"]').parents('.form-section').show()
        }
        else{
            $('[data-name="matriculaVehiculo"]').parents('.double-item').removeClass('changed required edit')
            $('[data-name="matriculaVehiculo"]').parents('.form-section').hide()
        }
    },
    isSubnumero(sn){
        // Si el activo es un subnúmero no permite la edición de los siguientes campos
        if( Number(sn) > 0 ){
            $('[data-name="noInventario"]').parents('.double-item').addClass('blocked')
            $('[data-name="tipoOperacion"]').parents('.double-item').addClass('blocked')
            $('[data-name="ceco"]').parents('.double-item').addClass('blocked')
            $('[data-name="campoPlanta"]').parents('.double-item').addClass('blocked')
            $('[data-name="departamento"]').parents('.double-item').addClass('blocked')
            $('[data-name="municipio"]').parents('.double-item').addClass('blocked')
            $('[data-name="ubicacionGeografica"]').parents('.double-item').addClass('blocked')
            $('[data-name="matriculaVehiculo"]').parents('.double-item').addClass('blocked')
        }     
    },
    isUOP(claveAr01){
        console.log("CLAVE DEPRECIACION: "+ claveAr01)
        if(claveAr01 != "ZVUR"){
            $('[data-name="vuRemanente"]').parents('.form-section').hide()
            $('[data-name="fechaVUR"]').parents('.form-section').hide()
            $('.vut_value').parents('.form-section').hide()
        }
        else{
            $('[data-name="vuRemanente"]').parents('.form-section').show()
            $('[data-name="fechaVUR"]').parents('.form-section').show()
            $('.vut_value').parents('.form-section').show()
        }
    },
    tiposDeOperacion(data, tiposDeOperacion){
        let tipoOperacion = data.tipoOperacion
        let optionsOPeracion = `
            ${
                tiposDeOperacion.map(
                    (val, key) => `<option value="${val}">${val}</option>`
                )
            }
        `
        let html = `
            <select name="tipoOperacion" class="form-control" autocomplete="foo">
                <option value="">SELECCIONE...</option>
                ${optionsOPeracion}
            </select>
        `

        $('.form-control[name="tipoOperacion"]').replaceWith(html)
    },
    locationFields(data, ubicaciones){
        // REGLA 45
        let location = {
            departamento: data.departamento,
            codMunicipio: data.municipio,
            municipio: data.nombreMunicipio,
            codUbicacionGeografica: data.ubicacionGeografica,
            ubicacionGeografica: data.nombreUbicacionGeografica,
        }
        let departamento = null,
            municipio = null,
            ubicacionGeografica = null,
            optionsDepartamento = new String(),
            optionsMunicipio = new String(),
            optionsUbicacion = new String()

        // REGLA 50
        for(let u of ubicaciones){
            optionsDepartamento += `<option value="${u.departamento}">${u.departamento}</option>`

            if(u.departamento == location.departamento){
                for(let m of u.municipios){
                    optionsMunicipio += `<option value="${m.municipio}" data-code="${m.codigoMunicipio}">${m.municipio}</option>`

                    if(m.codigoMunicipio == location.codMunicipio){
                        for(let l of m.ubicaciones){
                            optionsUbicacion += `<option value="${l.ubicacion}" data-code="${l.codigoUbicacion}">${l.ubicacion}</option>`
                        }
                    }
                }
                
            }

        }
        departamento = `<select name="departamento" class="form-control" autocomplete="foo">
            <option value="">SELECCIONE...</option>
            ${optionsDepartamento}
        </select>`

        municipio = `<select name="municipio" class="form-control" autocomplete="foo">
            <option value="">SELECCIONE...</option>
            ${optionsMunicipio}
        </select>`

        ubicacionGeografica = `<select name="ubicacionGeografica" class="form-control" autocomplete="foo">
            <option value="">SELECCIONE...</option>
            ${optionsUbicacion}
        </select>`


        $('.form-control[name="departamento"]').replaceWith(departamento)
        $('.form-control[name="municipio"]').replaceWith(municipio)
        $('.form-control[name="ubicacionGeografica"]').replaceWith(ubicacionGeografica)
    },
    municipioSelector(data, ubicaciones){
        //REGLA 45
        let municipio = null,
            ubicacionGeografica = null,
            optionsMunicipio = new String()

        for(let i of ubicaciones){
            if(i.departamento == data.departamento){
                for(let m of i.municipios){
                    optionsMunicipio += `<option value="${m.municipio}" data-code="${m.codigoMunicipio}">${m.municipio}</option>`
                }
            }
        }

        municipio = `<select name="municipio" class="form-control "autocomplete="foo">
            <option value="">SELECCIONE...</option>
            ${optionsMunicipio}
        </select>`
        //Reset de Ubicación geográfica hasta que se selecciones municipio.
        ubicacionGeografica = `<select name="ubicacionGeografica" class="form-control" autocomplete="foo">
            <option value="">SELECCIONE...</option>
        </select>`

        $('[name="municipio"]').parents('.double-item').removeClass('changed')
        $('[data-name="municipio"]').find('i').remove()

        $('[name="ubicacionGeografica"]').parents('.double-item').removeClass('changed')
        $('[data-name="ubicacionGeografica"]').find('i').remove()

        $('.form-control[name="municipio"]').replaceWith(municipio)
        $('[name="municipio"]').parents('.double-item').addClass('required')

        $('.form-control[name="ubicacionGeografica"]').replaceWith(ubicacionGeografica)
        $('[name="ubicacionGeografica"]').parents('.double-item').addClass('required')
    },
    ubicacionSelector(data, ubicaciones){
        //REGLA 45
        // console.log(data)
        let departamento = data.departamento == "" ? data.departamentoAux : data.departamento,
            ubicacionGeografica = null,
            optionsUbicacion = new String()

        for(let i of ubicaciones){
            if(i.departamento == departamento){
                for(let m of i.municipios){
                    if(m.municipio == data.municipio){
                        for(let u of m.ubicaciones){
                            optionsUbicacion += `<option value="${u.ubicacion}" data-code="${u.codigoUbicacion}">${u.ubicacion}</option>`
                        }
                    }
                }
            }
        }

        ubicacionGeografica = `<select name="ubicacionGeografica" class="form-control" autocomplete="foo">
            <option value="">SELECCIONE...</option>
            ${optionsUbicacion}
        </select>`

        $('[name="ubicacionGeografica"]').parents('.double-item').removeClass('changed')
        $('[data-name="ubicacionGeografica"]').find('i').remove()

        $('.form-control[name="ubicacionGeografica"]').replaceWith(ubicacionGeografica)
        $('[name="ubicacionGeografica"]').parents('.double-item').addClass('required')

    },
    modalmessage(json){

        $('.modalMessage').remove();
        clearTimeout(timeMessage);

        var html = `<div class="modalMessage ${json.type}"><h3>${json.message}</h3><span class="ion ion-android-cancel close-message"></span></div>`;
        $('body').append(html);

        var timeMessage = setTimeout(function(){
            $('.modalMessage').fadeOut(400, function(){
                $(this).remove();
            })
        }, 30000);

    },
    clearFields(type = null){
        if(type != 'fields'){
            for(let f of $('[data-name]')){
                $(f).text('')
            }
        }
        
        $('.double-item').removeClass('changed required edit blocked')
        $('.form-control').val('')
        $('.form-control[name="activo"]').val('')
        $('.form-control[name="subnumero"]').val(0)
        $('.form-control[name="inventario"]').val('')

        if($('[name="campoPlanta"]').length){
            $('[name="campoPlanta"]').removeAttr('data-code')
        }
        if($('.informative-vut').length){
            $('.informative-vut').hide()
        }

        $('.sigma span').text("0")
        
    },
    clearTable(){
        $('.table-data tbody tr').remove()
        $('.inline-table').hide()
    },
    validateEspecialFields(value){
        let RegExp = /^[\+\*\(\)\"\'\.\;\,\-\/\\]/g
        if(RegExp.test(value))
            return `_${value}`
        return value
    },
    existInTable(form, object){
        for(let i of $('.table-data tbody tr')){
            if(form == 'formatoDatosMaestros'){
                if(
                    $(i).find('td:eq(2)').text() == object.activo &&
                    $(i).find('td:eq(3)').text() == object.subnumero &&
                    $(i).find('td:eq(5)').text() == object.inventario 
                ){
                    return [$(i).index(), true]
                    break
                }
            }
            if(form == 'formatoBajas'){
                if(
                    $(i).find('td:eq(3)').text() == object.activo &&
                    $(i).find('td:eq(4)').text() == object.subnumero &&
                    $(i).find('td:eq(6)').text() == object.inventario 
                ){
                    return [$(i).index(), true]
                    break
                }
            }
        }
        return [null, false]
    },
    replaceIntoTable(index, row){
        for(let i of $('.table-data tbody tr:eq('+index+') td')){
            // se omiten las columnas de la caneca y el contador de fila
            if( $(i).index() > 1 ){
                // Si el valor en la tabla difiere del dato enviado en "row" lo reemplaza
                if($(i).text() != row[$(i).index()]){
                    $(i).text( row[$(i).index()] )
                }
            }
        }
    },
    addToTable(form, object){
        let row = null,
            exist = this.existInTable(form, object)
        /**
         * Se lee object de acuerdo al formulario de entrada
         */
        switch(form){
            case 'formatoDatosMaestros':
                {   
                    // console.log(object)
                    let rowCount = $('.table-data tbody tr').length + 1
                    row = [
                        '<span class="ion ion-trash-b remove-row"></span>',
                        rowCount,
                        object.activo,
                        object.subnumero,
                        object.denominacion,
                        object.inventario,
                        object.serie,
                        object.tag,
                        object.codigo_clase,
                        object.nueva_denominacion,
                        this.validateEspecialFields(object.nueva_denominacion_complementaria),
                        this.validateEspecialFields(object.nueva_serie),
                        object.nuevo_inventario,
                        object.nueva_capacidad,
                        object.nueva_unidad_medida,
                        this.validateEspecialFields(object.nuevo_tag),
                        object.nueva_matricula,
                        object.nuevo_fabricante,
                        this.validateEspecialFields(object.nuevo_modelo),
                        object.nuevo_codigo_ceco,
                        object.nueva_vicepresidencia != null ? object.nueva_vicepresidencia : '',
                        object.nueva_gerencia != null ? object.nueva_gerencia : '' ,
                        object.nuevo_registroCustodio,
                        object.nuevo_codigo_ubicacion_geografica != undefined ? object.nuevo_codigo_ubicacion_geografica : '',
                        object.nueva_ubicacion_geografica,
                        object.nuevo_codigo_campo_planta,
                        object.nuevo_campo_planta,
                        object.tipo_operacion,
                        object.nuevo_codigo_municipio != undefined ? object.nuevo_codigo_municipio : '',
                        object.nuevo_municipio,
                        object.ceco,
                        object.vicepresidencia,
                        object.gerencia,
                        object.fecha_inicial_VUR,
                        object.nueva_vida_util_remanente,
                        object.observaciones
                    ]
                }
            break;
            case 'formatoBajas':
                {
                    // console.log(object)
                    let rowCount = $('.table-data tbody tr').length + 1
                    row = [
                        '<span class="ion ion-trash-b remove-row"></span>',
                        rowCount,
                        object.tipo_baja,
                        object.activo,
                        object.subnumero,
                        object.denominacion,
                        object.inventario,
                        object.serie,
                        object.tag,
                        object.ceco,
                        object.siglaVicepresidencia,
                        object.siglaGerencia,
                        object.valor_baja,
                        object.motivo_retiro,
                        object.disposicion_final,
                        object.observaciones,
                    ]
                }
            break;
        }
        /** 
         * si encuentra repetido el activo lo reemplaza en la tabla
         *  con los datos nuevos, si no, agrega la nueva fila. 
         */
        if(exist[1]){
            let repeatedIndex = exist[0]
            this.replaceIntoTable(repeatedIndex, row)
        }
        else{
            // se estructura HTML para insertar en la tabla
            let fields = ``
            for(let i of row){
                fields += `<td>${i}</td>`
            }
            let html = `<tr class="animated fadeIn">${fields}</tr>`
            $('.inline-table').fadeIn(300)
 
            $('.table-data tbody').append(html)
        }
    },
    checkData(form){
        switch(form){
            case 'formatoDatosMaestros':
                {
                    let object = {
                        activo: $('[name="activo"]').val(),
                        subnumero: $('[name="subnumero"]').val(),
                        inventario: $('[name="inventario"]').val(),
                        denominacion: $('[data-name="denominacion"]').attr('data-denominacion'),
                        serie: $('[data-name="serie"]').attr('data-serie'),
                        tag: $('[data-name="tag"]').attr('data-tag'),
                        codigo_clase: $('[data-name="clase"]').text().trim(),
                        nueva_denominacion: $('[name="denominacion"]').val(),
                        nueva_denominacion_complementaria: $('[name="denominacion2"]').val(),
                        nueva_capacidad: $('[name="capacidad"]').val(),
                        nueva_unidad_medida: $('[name="unidadMedida"]').val(),
                        nuevo_inventario: $('[name="noInventario"]').val(),
                        nuevo_inventario: $('[name="noInventario"]').val(),
                        nuevo_tag: $('[name="tag"]').val(),
                        nueva_serie: $('[name="serie"]').val(),
                        nuevo_fabricante: $('[name="fabricante"]').val(),
                        nuevo_modelo: $('[name="modelo"]').val(),
                        nuevo_codigo_municipio: $('[name="municipio"]').find('option:selected').attr('data-code'),
                        nuevo_municipio: $('[name="municipio"]').val(),
                        nuevo_codigo_ubicacion_geografica: $('[name="ubicacionGeografica"]').find('option:selected').attr('data-code'),
                        nueva_ubicacion_geografica: $('[name="ubicacionGeografica"]').val(),
                        nuevo_codigo_ceco: $('[name="ceco"]').val(),
                        nueva_vicepresidencia: null,
                        nueva_gerencia: null,
                        nuevo_codigo_campo_planta: $('[name="campoPlanta"]').attr('data-code') != undefined ? $('[name="campoPlanta"]').attr('data-code') : '',
                        nuevo_campo_planta: $('[data-name="campoPlanta"] i').text(),
                        ceco: $('[data-name="ceco"]').attr('data-ceco'),
                        vicepresidencia: $('[data-name="vicepresidencia"]').attr('data-code'),
                        gerencia: $('[data-name="gerencia"]').attr('data-code'),
                        fecha_inicial_VUR: $('[name="fechaVUR"]').val(),
                        nueva_vida_util_remanente: $('[name="vuRemanente"]').val(),
                        observaciones: $('[name="observaciones"]').val(),
                        tipo_operacion: $('[name="tipoOperacion"]').val(),
                        nueva_matricula: $('[name="matriculaVehiculo"]').val() ,
                        nuevo_registroCustodio: ''
                    }

                    let params = [
                        'ceco',
                        'vicepresidencia',
                        'gerencia'
                    ]
                    queries
                        .getJerarquizacion(params.toString(), $('[name="ceco"]').val())
                        .then(data => {
                            if(data.error == undefined){
                                object.nueva_vicepresidencia = data[0].vicepresidencia
                                object.nueva_gerencia = data[0].gerencia
    
                                this.addToTable(form, object)
                            }
                            else{
                                this.addToTable(form, object)
                            }
                        })
                        .catch(err => console.log(err.responseText))
    
                }
            break;
            case 'formatoBajas':
                {
                    let object = {
                        tipo_baja: $('[name="tipo_baja"]:checked').val() == 'baja_total' ? 'BAJA TOTAL': 'BAJA PARCIAL',
                        activo: $('[name="activo"]').val(),
                        subnumero: $('[name="subnumero"]').val(),
                        inventario: $('[name="inventario"]').val(),
                        denominacion: $('[data-name="denominacion"]').text(),
                        serie: $('[data-name="serie"]').text(),
                        tag: $('[data-name="tag"]').text(),
                        ceco: $('[data-name="numero_ceco"]').text(),
                        siglaVicepresidencia: $('[data-name="vicepresidencia"]').text(),
                        siglaGerencia: $('[data-name="gerencia"]').text(),
                        valor_baja: $('[name="tipo_baja"]:checked').val() == 'baja_total' ? $('[name="number_baja_total"]').val(): $('[name="number_baja_parcial"]').val(),
                        motivo_retiro: $('[name="motivo_retiro"]').val(),
                        disposicion_final: $('[name="disposicion_final"]').val(),
                        observaciones: $('[name="observaciones"]').val(),
                    }
                    //console.log(object.valor_baja)
                    this.addToTable(form, object)
                }
            break;
        }
    },
    async createJsonFromTable(){
        let form = $('form').attr('id')

        switch(form){
            case 'formatoDatosMaestros':
                {
                    let arrayJson = []
                    for(let i of $('.table-data tbody tr')){
                        // console.log(i)
                        let tempJson = {
                            activo:                                 $(i).find('td:eq(2)').text(),
                            subnumero:                              $(i).find('td:eq(3)').text(),
                            denominacion:                           $(i).find('td:eq(4)').text(),
                            inventario:                             $(i).find('td:eq(5)').text(),
                            serie:                                  $(i).find('td:eq(6)').text(),
                            tag:                                    $(i).find('td:eq(7)').text(),
                            codigo_clase:                           $(i).find('td:eq(8)').text(),
                            nueva_denominacion:                     $(i).find('td:eq(9)').text(),
                            nueva_denominacion_complementaria:      $(i).find('td:eq(10)').text(),
                            nueva_serie:                            $(i).find('td:eq(11)').text(),
                            nuevo_inventario:                       $(i).find('td:eq(12)').text(),
                            nueva_capacidad:                        $(i).find('td:eq(13)').text(),
                            nueva_unidad_medida:                    $(i).find('td:eq(14)').text(),
                            nuevo_tag:                              $(i).find('td:eq(15)').text(),
                            nueva_matricula:                        $(i).find('td:eq(16)').text(),
                            nuevo_fabricante:                       $(i).find('td:eq(17)').text(),
                            nuevo_modelo:                           $(i).find('td:eq(18)').text(),
                            nuevo_codigo_ceco:                      $(i).find('td:eq(19)').text(),
                            nueva_vicepresidencia:                  $(i).find('td:eq(20)').text(),
                            nueva_gerencia:                         $(i).find('td:eq(21)').text(),
                            nuevo_registro_custodio:                $(i).find('td:eq(22)').text(),
                            nuevo_codigo_ubicacion_geografica:      $(i).find('td:eq(23)').text(),
                            nueva_ubicacion_geografica:             $(i).find('td:eq(24)').text(),
                            nuevo_codigo_campo_planta:              $(i).find('td:eq(25)').text(),
                            nuevo_campo_planta:                     $(i).find('td:eq(26)').text(),
                            nuevo_tipo_operacion:                   $(i).find('td:eq(27)').text(),
                            nuevo_codigo_municipio:                 $(i).find('td:eq(28)').text(),
                            nuevo_municipio:                        $(i).find('td:eq(29)').text(),
                            ceco:                                   $(i).find('td:eq(30)').text(),
                            vicepresidencia:                        $(i).find('td:eq(31)').text(),
                            gerencia:                               $(i).find('td:eq(32)').text(),
                            fecha_inicial_VUR:                      $(i).find('td:eq(33)').text(),
                            nueva_vida_util_remanente:              $(i).find('td:eq(34)').text(),
                            observaciones:                          $(i).find('td:eq(35)').text(),
                        }
                        arrayJson.push(tempJson);
                    }

                    return arrayJson
                }
            break;
            case 'formatoBajas':
                {
                    //console.log("formato bajas")
                    let params = [
                        'activo',
                        'sn',
                        'noInventario',
                        'denominacion',
                        'serie',
                        'ceco',
                        'valorAdquisicionAr01',
                    ],
                    arrayJson = [],
                    queriesArray = []

                    for(let i of $('.table-data tbody tr').get().reverse()){
                        // console.log(i)
                        let tempJson = {
                            tipo_baja:              $(i).find('td:eq(2)').text(),
                            activo:                 $(i).find('td:eq(3)').text(),
                            subnumero:              $(i).find('td:eq(4)').text(),
                            denominacion:             $(i).find('td:eq(5)').text(),
                            inventario:           $(i).find('td:eq(6)').text(),
                            serie:                  $(i).find('td:eq(7)').text(),
                            tag:                  $(i).find('td:eq(8)').text(),
                            ceco:                   $(i).find('td:eq(9)').text(),
                            sigla_vicepresidencia:  $(i).find('td:eq(10)').text(),
                            sigla_gerencia:         $(i).find('td:eq(11)').text(),
                            valor_baja:             $(i).find('td:eq(12)').text(),
                            motivo_retiro:          $(i).find('td:eq(13)').text(),
                            disposicion_final:      $(i).find('td:eq(14)').text(),
                            observaciones:          $(i).find('td:eq(15)').text()
                        }
                        //console.log("TempJson: "+JSON.stringify(tempJson))
                        if(tempJson.tipo_baja == 'BAJA TOTAL')
                        {
                            queriesArray[queriesArray.length] = queries
                                .getActivo( tempJson.activo, tempJson.subnumero, true, params.toString() )
                                .then(data => {
                                    //arrayJson.push(tempJson)

                                    if(data.length >= 1)
                                    {
                                        //data = data.slice(1,data.length)
                                        for(let child of data){
                                            let tempChildJson = {
                                                tipo_baja:          tempJson.tipo_baja,
                                                activo:             child.activo,
                                                subnumero:          child.sn,
                                                inventario:         child.noInventario,
                                                denominacion:       child.denominacion,
                                                serie:              child.serie,
                                                tag:                tempJson.tag,
                                                ceco:               child.ceco,
                                                sigla_vicepresidencia:  tempJson.sigla_vicepresidencia,
                                                sigla_gerencia:         tempJson.sigla_gerencia,
                                                valor_baja:         child.valorAdquisicionAr01,
                                                motivo_retiro:      tempJson.motivo_retiro,
                                                disposicion_final:  tempJson.disposicion_final,
                                                observaciones:      tempJson.observaciones
                                            }
                                            //console.log("tempChildJson: "+JSON.stringify(tempChildJson))
                                            arrayJson.push(tempChildJson)
                                        }
                                    }

                                })
                                .catch(err => {
                                    console.error(err.responseText)
                                })
                        }
                        else
                        {
                            arrayJson.push(tempJson)
                        }
                    }
                    //console.log(arrayJson)
                    return await Promise.all(queriesArray)
                        .then(values => { 
                            return arrayJson
                        })
                }
            break;
            default:
                return false
            break;
        }
    },
    caclValorNetoMinusBajaParcial(sn){
        let totalValue = Number( $('[name="number_baja_total"]').unmask() )
        let partialValue = Number( $('[name="number_baja_parcial"]').unmask() )
        let adquisitionValue = Number( $('[name="number_adq_total"]').unmask() )

        if(Number(sn) == 0){
            let availableValue = totalValue - (Number(storage.getLocal('SMM').salario)/2)
            partialValue = partialValue > Math.round( availableValue ) ?  Math.round( availableValue ) : partialValue
        }
        else{
            partialValue = partialValue > totalValue ? totalValue : partialValue
        }

        //partialValue = partialValue > totalValue ? totalValue : partialValue

        $('[name="number_baja_parcial"]').val(partialValue).attr('data-limit', totalValue)
        $('.withdrawal-result').text( totalValue - partialValue )

        $('.withdrawal-result,[name="number_baja_parcial"]')
            .priceFormat({
                prefix:"$ ",
                centsSeparator:",",
                thousandsSeparator:".",
                clearOnEmpty:!0,
                centsLimit: 0,
                allowNegative: false
            })
    },
    whichRepeatedAsset(data){
        var count = {},
            msg = null,
            elements = new String()
        // check if asset or inventory is repeated
            for(let e of data.repeatElements){
                for(let k in e){
                    if(count.hasOwnProperty(k))
                    {
                        let exist = false
                        for(let v of count[k]){
                            if(v.value == e[k])
                            {
                                v.sigma = v.sigma+1
                                exist = true
                                break
                            }
                        }

                        !exist && 
                        count[k].push({value: e[k], sigma: 1})
                    }
                    else
                    {
                        count[k] = [{value: e[k], sigma: 1}]
                    }
                }
            }

            if(count.activo.length == 1)
            {
                msg = staticOptions.selectRepeatAsset
            }
            else if(count.noInventario.length == 1)
            {
                msg = staticOptions.selectRepeatInventory
            }

        // build repeated elements selector. Los datos originales los codifica en base 64, pero primero codifica/decodifica el string para evitar errores con caracteres latinos 
            for(let e of data.repeatElements){
                elements += `<tr 
                    data-activo="${e.activo}"
                    data-inventario="${e.noInventario}"
                    data-orginal="${btoa(encodeURIComponent( JSON.stringify(data.originalData) ))}">
                    <td>${e.activo}</td>
                    <td>${e.noInventario}</td>
                    <td>${e.denominacion}</td>
                    <td>${e.tipoOperacion}</td>
                </tr>`
            }

        const html = 
        `<div class="choose-asset"><div class="table"><div class="table-cell">
        <div class="content">
            <h2 class="title">${msg}</h2>
            <table>
                <thead>
                    <tr>
                        <th># Activo</th>
                        <th># Inventario</th>
                        <th>Denominación</th>
                        <th>Tipo operación</th>
                    </tr>
                </thead>
                <tbody>
                    ${elements}
                </tbody>
            </table>
        </div>
        </div></div></div>`

        $('body').append(html)
    },
    selectionFromRepeatedAsset(data){
        const result = data.original.filter(obj => obj.activo == data.activo && obj.noInventario == data.inventario )
        return result
    },
}

export default draw
