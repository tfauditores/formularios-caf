const staticOptions = {
    subnumberDenominacion: [
        'MAYOR VALOR SUBREPARTO',
        'MAYOR VALOR COSTOS FINANCIEROS',
        'MAYOR VALOR COSTO DE ADQUISICION',
        'MAYOR VALOR SERVICIO DE PERFORACION',
        'MAYOR VALOR SERVICIO DE WORKOVER',
        'MAYOR VALOR OBRAS CIVÍLES',
        'ADICION + ACCESORIOS',
        'ADICION + CAMPO',
        'ADICION + GESTION CONTRATO',
        'ADICION + GESTORIA + TIPO DE GESTORIA (TECNICA, INTERVENTORIA, ETC)',
        'ADICION + AMPLIACION',
        'ADICION + EJECUCION CONTRATO',
        'ADICION + MATERIALES',
        'ADICION + LINEA DE FLUJO (EL VALOR DE MAS SE DEBE SUMAR A LA CAPACIDAD DEL ACTIVO PRINCIPAL)',
        'AUMENTO DE LA VIDA ÚTIL',
        'AMPLIACIÓN DE LA PRODUCTIVIDAD',
        'REDUCCIÓN SIGNIFICATICA DE COSTOS DE OPERACIÓN',
        'REEMPLAZO DE PARTE O COMPONENTE'
    ],
    clasesInmuebles: [
        '33006', 
        '11001', 
        '11002', 
        '28001', 
        '28002', 
        '15001', 
        '15002', 
        '15003', 
        '15004', 
        '15005', 
        '15009', 
        '15015'
    ],
    clasesVehiculos: [
        '23001',
        '23002',
        '23003',
        '23004'
    ],
    clasesNoSerie: [
        '11', 
        '15', 
        '16', 
        '26', 
        '28'
    ],
    tiposDeOperacion: [
        'DIRECTA',
        'ASOCIADA OPERADOR',
        'ASOCIADA NO OPERADOR',
        'EXTENSIÓN DE COMERCIALIDAD'
    ],
    motivoRetiro_disposicionFinal: [
        {
            name: 'DACIÓN EN PAGO',
            options: [
                'ENTREGA DEL ACTIVO',
            ]
        },
        {
            name: 'DEMOLICIÓN DE CONSTRUCCIONES O DESMANTELAMIENTO DE FACILIDADES',
            options: [
                'ENTREGA DEL ACTIVO',
                'VENTA COMO CHATARRA',
                'RESIDUO NO APROVECHABLE',
            ]
        },
        {
            name: 'DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO',
            options: [
                'VENTA COMO CHATARRA',
                'RESIDUO NO APROVECHABLE',
            ]
        },
        {
            name: 'ESCISIÓN',
            options: [
                'ENTREGA DEL ACTIVO',
            ]
        },
        {
            name: 'NO SE REQUIERE PARA LA OPERACIÓN',
            options: [
                'VENTA COMO ACTIVO FUNCIONAL',
                'CESIÓN SIN COSTO',
                'ABANDONO TÉCNICO',
                'VENTA COMO CHATARRA',
            ]
        },
        {
            name: 'OBSOLESCENCIA O DESUSO',
            options: [
                'VENTA COMO ACTIVO FUNCIONAL',
                'VENTA COMO CHATARRA',
                'RESIDUO NO APROVECHABLE', 
                'NO APLICA. FINALIZACIÓN USO ACTIVO INTANGIBLE'               
            ]
        },
        {
            name: 'PÉRDIDA O HURTO',
            options: [
                'NO APLICA',
            ]
        },
        {
            name: 'PERMUTA O CAMBIO',
            options: [
                'ENTREGA DEL ACTIVO',
            ]
        },
        {
            name: 'VENTA POR DIRECTRIZ DEL NEGOCIO',
            options: [
                'ENTREGA DEL ACTIVO',
            ]
        },
        {
            name: 'SANEAMIENTO CONTABLE',
            options: [
                'NO APLICA',
            ]
        }
    ],
    selectRepeatInventory : "Existe varios activos con el mismo número de inventario, seleccione un activo",
    selectRepeatAsset : "Existe varios activos con el mismo número de activo, seleccione un activo",
}

export default staticOptions