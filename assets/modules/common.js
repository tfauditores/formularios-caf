import queries from './queries'
import {dataValidations} from './dataValidations'
import draw from './draw'
import multiples from './multiples'

/* autocomplete events */
    // asignar valor al campo según sea la busqueda
    var setValue = (element, selector, change=true) =>{
        let field = $(element).attr('name')

        switch(field){
            case 'fabricante':
                {
                    let value = $(selector).text()
                    $(element).val(value).change()
                }
            break;
            case 'ceco':
                {
                    let value = $(selector).attr('data-ceco')
                    let campo = $(selector).attr('data-campo')
                    let nombreCampo = $(selector).attr('data-nombre-campo')
                    $(element).attr('data-campo',campo)
                    $(element).attr('data-nombre-campo',nombreCampo)
                    if (change) $(element).val(value).change()
                    else $(element).val(value)
                }
            break;
            case 'campoPlanta':
                {
                    let value = $(selector).attr('data-name')
                    let code = $(selector).attr('data-code')
                    $(element).val(value).attr('data-code', code).change()
                }
            break;
            default:
                {
                    let value = $(selector).text()
                    $(element).val(value).change()
                }
            break;
        }
    }
    var cambiaCeco = () =>{
        // Existe CeCo?
        let element = $('[name="ceco"]')
        let val = $('[name="ceco"]').val()
        queries
            .getCecoByName('*', 'getCecoByName', val)
            .then(data=>{
                console.log("PASO 5 - cambia ceco - validations")
                if(data.error==undefined){
                    dataValidations.otherValidations(element)
                }
                else{
                console.log("PASO 5 - Ceco no existe")
                    draw.modalmessage({message: 'El CeCo indicado no existe', type: 'warning'})
                    $(element).removeAttr('data-campo').removeAttr('data-nombre-campo')
                }
            })
            .catch(err => console.log(err.responseText))
    }
    // scroll function
    var scrollAutocomplete = () => {
        let wrapperTop = $('.autocomplete-wrapper').offset().top
        let selectorTop = $('.selector.hover').offset().top
        let scroll = $('.autocomplete-box').scrollTop()
        $('.autocomplete-box').scrollTop( selectorTop-wrapperTop+scroll )
    }
    // cerrar autocomplete con un click afuera
    $(document).on('click', function(e){
        if($(e.target).closest('.autocomplete-wrapper').length == 0){
            $('.autocomplete-wrapper').off().remove()
            //cambiaCeco()
            console.log("PASO 4B - Click por fuera")
        }
    })
    // selecciona el dato con click
    $(document).on('click', '.autocomplete-wrapper .selector', function(e){
        console.log("PASO 4 - selector click")
        let focus = $(this).parents('.autocomplete-wrapper')
        setValue($(focus).siblings('.form-control.autocomplete'), $(this))
        // si es "ceco" sugiere el campo con la funcion cambiaCeco
        focus.siblings('span').attr('data-name') == "ceco" ? cambiaCeco() : ''

        $(focus).off().remove()
    })
    // keys 
    $(document).on('keyup', '.autocomplete', function(e){
        let element = $(this)
        
        switch(e.keyCode){
            case 38:
                //up
                {
                    let $focus = $(element).siblings('.autocomplete-wrapper').find('.selector.hover')
                    if($focus.length){
                        if($focus.prev().length){
                            let val = $focus.prev().text()
                            $focus.prev().addClass('hover')
                            $focus.removeClass('hover')
                            setValue(element, $focus.prev(), false)
                        }
                    }
                    else{
                        $(element).siblings('.autocomplete-wrapper').find('.selector:last-child').addClass('hover')
                        let $focus = $(element).siblings('.autocomplete-wrapper').find('.selector.hover')
                        setValue(element, $focus, false)
                    }
                    scrollAutocomplete()
                }
            break;
            case 40:
                //down
                {
                    let $focus = $(element).siblings('.autocomplete-wrapper').find('.selector.hover')
                    if($($focus).length){
                        if($focus.next().length){
                            let val = $focus.next().text()
                            $focus.next().addClass('hover')
                            $focus.removeClass('hover')
                            setValue(element, $focus.next(), false)
                        }
                    }
                    else{
                        $(element).siblings('.autocomplete-wrapper').find('.selector:first-child').addClass('hover')
                        let $focus = $(element).siblings('.autocomplete-wrapper').find('.selector.hover')
                        setValue(element, $focus, false)
                    }
                    scrollAutocomplete()
                }
            break;
            case 13:
                //enter
                {
                    let $focus = $(element).siblings('.autocomplete-wrapper').find('.selector.hover')
                    if($focus.length){
                        console.log("PASO 4C: Enter")
                        setValue(element, $focus, false)
                        $(element).siblings('.autocomplete-wrapper').off().remove()
                        // si es "ceco" sugiere el campo con la funcion cambiaCeco
                        element.attr('name') == "ceco" ? cambiaCeco() : ''
                    }
                    else{ 
                        console.log("PASO 4C: Enter sin resultados")
                        $('[name="ceco"]').removeAttr('data-campo').removeAttr('data-nombre-campo')
                        // si es "ceco" sugiere el campo con la funcion cambiaCeco
                        element.attr('name') == "ceco" ? cambiaCeco() : ''
                    }
                }
            break;
        }
    })
    $(document).on('keydown', '.autocomplete', function(evt){
        let element = $(this)
        let e = event || evt;
        let charCode = e.which || e.keyCode;
        // TAB
        if(charCode == 9){
            let $focus = $(element).siblings('.autocomplete-wrapper').find('.selector.hover')
            if($focus.length){
                console.log("PASO 4C: TAB")
                setValue(element, $focus, false)
                $(element).siblings('.autocomplete-wrapper').off().remove()
                // si es "ceco" sugiere el campo con la funcion cambiaCeco
                element.attr('name') == "ceco" ? cambiaCeco() : ''
            }
            else{
                console.log("PASO 4C: TAB sin resultados")
                $('[name="ceco"]').removeAttr('data-campo').removeAttr('data-nombre-campo')
                // si es "ceco" sugiere el campo con la funcion cambiaCeco
                element.attr('name') == "ceco" ? cambiaCeco() : ''
            }
        }
    })

/* show characteristics */
    $('.show-characteristics span').on('click', function() {
        var html = $(this).html()
        if($(this).find('i').is('.ion-eye')){
            $(this).html( html.replace('Ver', 'Ocultar') )
        }
        if($(this).find('i').is('.ion-eye-disabled')){
            $(this).html( html.replace('Ocultar', 'Ver') )
        }
        $(this).find('.ion').toggleClass('ion-eye ion-eye-disabled')
        $('.slide-info').stop().slideToggle(400)
    })

/* close modalmessage*/
    $(document).on('click', '.modalMessage',function(e){
        $(this).fadeOut(400, function(){
            $(this).remove()
        })
    })

/* tabs view*/
    $('.ctrl-tabs .tabs').on('click', function(e){
      e.preventDefault()
      $(this).addClass('active')
      $(this).siblings('.tabs').removeClass('active')
      let ind = $(this).index()
      $('.block-tabs .tabs').removeClass('active').removeAttr('style')
      $('.block-tabs .tabs:eq('+ind+')').fadeIn(400, function() {
        $(this).addClass('active')
      })
    })

/* tooltip */
    $('[data-toggle="tooltip"]').tooltip({
        trigger: "hover"
    })

/* uppercase fields*/
    $(document).on('change','.form-control', function(e){
        $(this).val(String($(this).val()).toUpperCase())
    })
/** edit field */
    $(document).on('change', '.double-item .form-control', function(event) {
        event.preventDefault()
        //console.log("PASO 3 - JS detecta un cambios")
        // $(this).parent().removeClass('edit')
        $(this).prev().find('i').remove()
        let val = $(this).val()
        let old = $(this).prev().text().trim()
        if(val != old && val != ""){
            $(this).parent().addClass('changed')
            $(this).prev().html( old + ` <i>${val}</i>`)
        }
        else{
            $(this).parent().removeClass('changed')
        }
    })
/** Aceptar sugerencia Ceco -> Campo */
    $(document).on('click', '.campoSuggest .btn-styled', function(e){
        let campo = $(this).siblings('h4').text(),
            codCampo = $(this).attr('data-campo')
        console.log( campo )

        $('[data-name="campoPlanta"]').find('i').remove()
        let old = $('[data-name="campoPlanta"]').text().trim()
        $('[data-name="campoPlanta"]').parent().addClass('changed')
        $('[data-name="campoPlanta"]').html(old +` <i>${campo}</i>`)
        
        $('[name="campoPlanta"]').removeAttr('disabled')
        $('[name="campoPlanta"]').text(campo).attr('data-code',codCampo)

        $('.campoSuggest').remove()
    })
/** Cancelar sugerencia Ceco -> Campo */
    $(document).on('click', '.campoSuggest .btn-styled2', function(e){
        console.log( "CANCELADO" )
        $('[name="campoPlanta"]').removeAttr('disabled')
        $('[name="campoPlanta"]').parent().addClass('edit').addClass('required')
        $('.campoSuggest').remove()
    })
/** */
    $('.double-item .ion').on('click', function(e){
        // $(this).toggleClass('ion-android-create ion-checkmark')
        if(!$(this).parent().is('.required')){

            if($(this).parent().is('.edit')){
                $(this).parent().removeClass('edit')
            }
            else{
                $(this).parent().addClass('edit')
            }

        }

    })
/** Add data to table*/
    $('.btn-add').on('click', function(e){
        e.preventDefault()
        // Identifica el formulario por ID, aplica para bajas, DT y altas.
            // let focusform = "formatoBajas"
            // let validator = $('#formatoBajas').validate()
            // validator.form('#formatoBajas')
        let focusForm = $('form').attr('id')
        let validator = $('#'+focusForm).validate();
        validator.form('#'+focusForm);
        // Si no hay errores procede
        if(validator.numberOfInvalids() == 0){

            if(focusForm == 'formatoDatosMaestros'){
                // Si no hay cambios, muestra mensaje en ventana modal
                if($('.double-item.changed').length == 0){
                    draw.modalmessage({message: 'Por favor modifique algún tipo de información en el formulario', type: 'warning'})
                    return false
                }
                // Si hay campos en edición vacios, muestra mensaja en ventana modal
                for(let i of $('.double-item.edit')){
                    if($(i).hasClass('changed') == false && $(i).find('.form-control').val() == ""){
                        draw.modalmessage({message: 'Por favor complete todos los campos que quiere editar', type: 'warning'})
                        return false
                    }
                }

                if ($('.edit [name="vuRemanente"]').length > 0){
                    let value = $('[name="vuRemanente"]').val()
                    let activo = $('[name="activo"]').val(),
                        sn = $('[name="subnumero"]').val()
                    if(Number(sn) == 0){
                        for(let i of $('.table-data tbody tr')){
                            // Busca en la tabla VUR de subnúmeros y la actualiza si es mayor a VALUE
                            if( $(i).find('td:eq(2)').text() == activo && $(i).find('td:eq(3)').text() != "0" ){
                                if( Number($(i).find('td:eq(34)').text()) > Number(value) ){
                                    $(i).find('td:eq(34)').text(value)
                                }
                            }
                        }
                    }
                }

            }
            // draw.checkData("formatoBajas")
            // draw.checkData("formatoDatosMaestros")
            draw.checkData(focusForm)
        }
    })
/** datepicker */
    $( '.datepicker' ).datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        dateFormat: 'yy/mm/dd',
        closeText: 'Cerrar',
        changeMonth: true,
        changeYear: true,
        prevText: '< Ant',
        nextText: 'Sig >',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        yearRange: "-100:+0"
    })
/** VUR datosmaestros */
    if($('[name="fechaVUR"]').length){
        let d = new Date()
        /** 
         * min Date = primer día del mes anterior 
         * Si el mes actual es Enero, el mes mínimo es enero
         *
         * max Date = último día del mes siguiente
         * Si el mes actual es (Nov, Dic) el mes máximo es Diciembre
         */
        $('[name="fechaVUR"]').datepicker('option', {
            'minDate': new Date(d.getFullYear(),d.getMonth() - 1 < 0 ? 0: d.getMonth() - 1, 1),
            'maxDate': new Date(d.getFullYear(),d.getMonth() + 2 > 11 ? 12: d.getMonth() + 2, 0)
        })
    }
/* auto change values in some fields*/
    // vida útil remanente
        $(document).on('keyup','.form-control[name="vuRemanente"]', function(e){
            // Valores numéricos mayores a 0
            $(this).val($(this).val().replace(/[^0-9]+/g,''))
            let value = Number($(this).val())
            let activo = $('[name="activo"]').val(),
                sn = $('[name="subnumero"]').val(),
                max = 999
            if(Number(sn) > 0){
                // SOLAMENTE EN FORMULARIO DATOS MAESTROS
                if($('form').attr('id') == 'formatoDatosMaestros'){
                    // Si la vida útil remanente del principal ya se modificó en la tabla de solicitudes, la captura
                    for(let i of $('.table-data tbody tr')){
                        if( $(i).find('td:eq(2)').text() == activo && $(i).find('td:eq(3)').text() == "0" ){
                            max = $(i).find('td:eq(34)').text()
                        }
                    }
                    // Compara la máxima VUR entre la original del Activo Principal y la Solicitada previamente en la tabla 
                    let maxParent = $(this).attr('data-vidautil') != undefined ? $(this).attr('data-vidautil') : 0
                    max = max>Number(maxParent) ? max : maxParent
                }
                // SOLAMENTE EN FORMULARIO BAJAS
                if($('form').attr('id') == 'formatoBajas'){
                    max = $(this).attr('data-vidautil') != undefined ? $(this).attr('data-vidautil') : 999
                }
            }
            if(value > max){
                $(this).val(max)
            }
        })
    // capacidad
        $(document).on('keyup','.form-control[name="capacidad"]', function(e){
            e.preventDefault()
            // Si escriben caracteres diferentes a números, coma(,) o punto(.) los quita de la cadena
            // Si escriben coma(,) la reemplaza por punto(.)
            $(this).val($(this).val().replace(/[^0-9,.]+/g,''))
            $(this).val($(this).val().replace(/[,]+/g,'.'))

            switch(e.keyCode){
                case 110:
                case 188:
                case 190:
                    {   
                        let largo = $(this).val().length
                        let punto = $(this).val().indexOf(".")
                    // Si ya hay otro punto, borra el último digitado
                        if(punto!=largo-1){
                            $(this).val( $(this).val().substring(0, largo-1) )
                        }
                    // Si el usuario escribe "," o "." en la posición inicial, agrega un 0
                        let x = largo == 1 ? '0.' : $(this).val()
                        $(this).val(x)
                    }
                break;
            }

        })

/* verify if inventory number exist in auxiliar */
    $(document).on('change', '.form-control[name="noInventario"]', function(){
        let element = $(this)

        $(element).removeAttr('data-exist')
        let val = $(element).val()

        queries
            .getExistingInvetory(val)
            .then(data => {
                if(data.error == undefined){
                    $(element).attr('data-exist', data.length)
                    
                    if($(element).parents('.double-item').length){
                        $(element).parents('.double-item').removeClass('changed')
                        $('[data-name="noInventario"]').find('i').remove()
                    }
                    
                }
                $(element).valid()
            })
            .catch(err => console.log(err.responseText))
    })
/* remove row from data table */
    $(document).on('click', '.remove-row', function(){
        $(this).parents('tr').remove()

        $('.table-data tbody tr').length < 1 ? $('.inline-table').fadeOut(300) : ''

        let count = 1
        for(let i of $('.table-data tbody tr')){
            $(i).find('td:eq(1)').text(count)
            count++
        }
    })
/* download file - create excel */
    $('.btn-end').on('click', function(e){
        e.preventDefault()
        if($('.table-data tbody tr').length){

            $('#wait').fadeIn(400)

            draw.createJsonFromTable()
                .then(json => {
                    queries
                        .postCreateExcel(json)
                        .then( excelString => {
                            var excel = JSON.parse(excelString);
        
                            var $link = $("<a>");
                            $link.attr("href","include/"+excel.route);
                            $("body").append($link);
                            $link[0].setAttribute("download",excel.name);
                            $link[0].click();
                            $link.remove();
                            $('#wait').fadeOut(400);
                            $('.thankyou-message').fadeIn(400);
                            draw.clearFields();
                            draw.clearTable();
                            setTimeout(function(){
                                $('.inline-table').hide()
                                $('.multiple-view').hide()
                                $('.single-view').show()
        
                                $('.thankyou-message').fadeOut(400);
                            }, 5000);
                        })
                        .catch(err => {
                            console.log(err.responseText)
                            $('#wait').fadeOut(400)
                        })

                })
                .catch(err => {
                    console.log(err)
                    $('#wait').fadeOut(400)
                })

        }
    })
/* upload excel of multiple validations */
    var fileLocation = null

    $('.upload-control').change(function(e){
        console.log("----------- se está cargando un archivo -----------")
        if(!$(this)[0].files.length){
            return false;
        }

        if($('.nav-tabs li[data-type="error"]').is('.active')){
            $('.inline-table,.show-list').hide();
        }

        var files = $(this)[0].files;
        var formData = new FormData();

        for (var i = 0; i < files.length; i++) {
            formData.append('excel', files[i]);
        }
        
        $('#wait').fadeIn(400);
        $('.multiple-view').fadeIn(300);
        $('.nav-tabs li[data-type="error"] i').text('0');
        $('.nav-tabs li[data-type="success"] i').text('0');
        $('.success-multiple tbody').html('');
        $('.error-multiple').html('');
        $('.single-view').hide();

        queries
            .postLoadExcelMultiple(formData)
            .then(data => {
                fileLocation = data[data.length - 1]
                //console.log("fileLocation")
                //console.log(fileLocation)
                data.splice(data.length - 1, 1)
                //console.log("data")
                //console.log(data)
                const form = $('form').attr('id')
                let minRow, 
                    maxRow = data.length - 5,
                    format = null
                if(form == 'formatoDatosMaestros'){
                    minRow = 7
                    format = 'GFI-F-104'
                }
                if(form == 'formatoBajas'){
                    minRow = 6
                    format = 'GFI-F-055'
                }

                let counter = 0
                
                let rows = maxRow-minRow;
                console.log('%cSe detectaron '+rows+' filas... Analizando datos...','background:#458e92; color:#fff; padding:15px 50px;')
                for(let i of data){
                    i = i[0]
                    if( counter == 3 && String(i[3]).trim() != format){
                        $('#wait').fadeOut(400)
                        multiples.printError(1, [`formato inválido, utilice el formato ${format}`])
                        return false
                    }
                    if(counter >= minRow && counter < maxRow){
                       // console.log(i, maxRow, counter+1)
                       // console.log('%c '+counter,'background:#000;color:yellow;')
                        multiples.validate(i , maxRow, counter+1)
                    }
                    counter++
                }

                $('.upload-control').val('')

                // $('#wait').fadeOut(400)
            })
            .catch(err => {
                console.log(err)
                $('#wait').fadeOut(400)
            })
            
    });
/* nav view selector */
    $('.nav-view .btn').on('click',function(e){
        $(this).siblings('.btn').removeClass('active')
        $(this).addClass('active')

        if($(this).is('.btn-single')){
            $('.multiple-view').stop().hide()
            $('.single-view').stop().fadeIn(400)
        }
        if($('.inline-table tbody tr').length && $('.nav-tabs li[data-type="success"]').is('.active'))
            $('.inline-table').stop().fadeIn(400)
    })
/* tabs multiples */
    $('.nav-tabs').on('click', 'li', function(e){
        e.preventDefault()
        if($(this).is('.active')){
            return false
        }
        var focus = $(this).parents('.nav-tabs').attr('data-tab')
        var ind = $(this).index()
        $(this).addClass('active')
        $(this).siblings('li').removeClass('active')

        $('.tabs[data-tab="'+focus+'"]').find('.tab').removeClass('active')
        $('.tabs[data-tab="'+focus+'"]').find('.tab:eq('+ind+')').stop().fadeIn(300,function(){
            $(this).addClass('active').removeAttr('style')
        })

        if($(this).attr('data-type') == 'error'){
            $('.inline-table,.show-list').stop().hide()
        }
        else{
            $('.inline-table,.show-list').stop().fadeIn(300)
        }
    })
/* error acordion */
    $('.error-multiple').on('click', '.line', function(){
        $(this).toggleClass('active')
        $(this).next('ul').stop().slideToggle(400)
    })
/* download errors */
    $('.btn-error').on('click',function(event){
        event.preventDefault()
        if($('.error-multiple .item').length){
            $('#wait').fadeIn(400)
            var errJSON = {}

            $('.error-multiple .item').each(function(i,e){
                var row = $(e).data('index');
                errJSON[row] = [];
                $(e).find('> ul > li').each(function(j,c){
                    var regex = /[;]/g;
                    var columnData = regex.test($(c).data('column')) ? String($(c).data('column')).split(';') : $(c).data('column');
                    var errorText = $(c).text();
                    if($(c).next('ol').length){
                        for(var i = 0; i < $(c).next('ol').find('li').length; i++){
                            errorText += '\n'+$(c).next('ol').find('li:eq('+i+')').text();
                        }
                    }
                    if(columnData.length){
                        for(var i=0; i<columnData.length; i++){
                            var obj = {
                                column: Number(columnData[i]),
                                error: errorText
                            }
                            errJSON[row].push(obj);
                        }
                    }
                    else{
                        var obj = {
                            column: columnData,
                            error: errorText
                        }
                        errJSON[row].push(obj);
                    }
                });
            });
            
            let form = $('form').attr('id'),
                file = null,
                format = null
            switch(form){
                case 'formatoDatosMaestros':
                    file = 'ERRORES-GFI-F-104-FORMATO DE ACTUALIZACION DE DATOS MAESTROS DE ACTIVOS FIJOS.'
                    format = 'datosMaestros'
                break;
                case 'formatoBajas':
                    file = 'ERRORES-GFI-F-104-FORMATO DE AUTORIZACION PARA RETIRO DE ACTIVOS FIJOS DE LA OPERACION.'
                    format = 'bajas'
                break;
            }
            
            var url = 'include/downloadErrors.php';
            var postQuery = {
                path: fileLocation,
                errors: errJSON,
                format: format
            }

            $.post(url, postQuery, function(data){
                
                var fileName = file+data.ext;
                var $a = $("<a>");
                $a.attr("href","include/"+data.file);
                $("body").append($a);
                $a[0].setAttribute("download",fileName);
                $a[0].click();
                $a.remove();
                $('#wait').fadeOut(400);
            });
        }
    });