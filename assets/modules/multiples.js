import queries from './queries'
import draw from './draw'
import {dataRules} from './dataValidations'
import storage from '../modules/storage'
import staticOptions from '../modules/staticOptions'

const multiples = {
    setOptionsError(arr){
        let options = ''
        for(let i = 0; i < arr.length; i++){
            options += '%%'+arr[i]+'%%;';
        }
        return options
    },
    getPositionsTable(){
        let position = new Array()
        let format = $('form').attr('id')

        if(format == 'formatoDatosMaestros'){
            position = [
                'activo',
                'subnumero',
                'denominacion',
                'inventario',
                'serie',
                'tag',
                'codClase',
                'nuevaDenominacion',
                'nuevaDenominacion2',
                'nuevaSerie',
                'nuevoInventario',
                'nuevaCapacidad',
                'nuevaUnidadMedidad',
                'nuevoTag',
                'nuevaMatricula',
                'nuevoFabricante',
                'nuevoModelo',
                'nuevoCodCeco',
                'nuevaVicepresidencia',
                'nuevaGerencia',
                'nuevoRegistroCustodio',
                'nuevoCodUbicacion',
                'nuevaUbicacion',
                'nuevoCodCampoPlanta',
                'nuevoCampoPlanta',
                'nuevoTipoOperacion',
                'nuevoCodMunicipio',
                'nuevoMunicipio',
                'ceco',
                'vicepresidencia',
                'gerencia',
                'fechaVUR',
                'nuevaVUR',
                'observaciones'
                
            ]
        }
        if(format == 'formatoBajas'){
            position = [
                'tipoBaja',
                'activo',
                'subnumero',
                'denominacion',
                'inventario',
                'serie',
                'tag',
                'ceco',
                'vicepresidencia',
                'gerencia',
                'valorBaja',
                'motivoRetiro',
                'disposicionFinal',
                'observaciones'
            ]
        }

        return position
    },
    getPositionsExcel(){
        
        let position = new Object()
        let format = $('form').attr('id')

        if(format == 'formatoDatosMaestros'){
            position = {
                activo : 1,
                subnumero : 2,
                denominacion : 3,
                inventario : 4,
                serie : 5,
                tag: 6,
                codClase : 7,
                nuevaDenominacion : 8,
                nuevaDenominacion2 : 9,
                nuevaSerie : 10,
                nuevoInventario : 11,
                nuevaCapacidad : 12,
                nuevaUnidadMedidad : 13,
                nuevoTag : 14,
                nuevaMatricula: 15,
                nuevoFabricante : 16,
                nuevoModelo : 17,
                nuevoCodCeco : 18,
                nuevaVicepresidencia : 19,
                nuevaGerencia : 20,
                nuevoRegistroCustodio: 21,
                nuevoCodUbicacion : 22,
                nuevaUbicacion : 23,
                nuevoCodCampoPlanta : 24,
                nuevoCampoPlanta : 25,
                nuevoTipoOperacion: 26,
                nuevoCodMunicipio : 27,
                nuevoMunicipio : 28,
                ceco : 29,
                vicepresidencia : 30,
                gerencia : 31,
                fechaVUR : 32,
                nuevaVUR : 33,
                observaciones : 34,
            }
        }
        if(format == 'formatoBajas'){
            position = {
                tipoBaja: 1,
                activo : 2,
                subnumero : 3,
                denominacion : 4,
                inventario : 5,
                serie : 6,
                tag : 7,
                ceco : 8,
                vicepresidencia : 9,
                gerencia : 10,
                valorBaja : 11,
                motivoRetiro : 12,
                disposicionFinal : 13,
                observaciones : 14,
            }
        }

        return position

    },
    destructureError(err){
        let options = '',
            errControl = err,
            columns = ''
        
        if(typeof(err) == 'object'){
            errControl = err[0]
            err.shift()

            for(let i of err){
                columns += `${i};`
            }

            columns = columns.slice(0, -1)
        }

        if(errControl.match(/%%(.*)%%;/g)){
            options = errControl.match(/%%(.*)%%;/g)[0];
            options =  options.match(/[^%%|^%%;]*/g).filter((n) => {return n != ""} )
        }

        errControl = errControl.replace(/%%(.*)%%;/g, '')
        
        return {errControl, columns, options}
    },
    printErrorSuccess(err, line, excelRow, positions){
        if(err.length){
            this.printError(line, err)
        }
        else{
            this.printSuccess(line, excelRow , positions)
        }
    },
    printSuccess(index, excelRow, positions){
        let existRow = false,
            rowIndex = 0,
            tablePositions = this.getPositionsTable(),
            positionsASI = new Array(),
            form = $('form').attr('id')

        if(form == 'formatoDatosMaestros')
        {
            // posiciones de (activo, sn, inventario) en la tabla
            positionsASI = [2,3,5]
        }
        if(form == 'formatoBajas')
        {
            // posiciones de (activo, sn, inventario) en la tabla
            positionsASI = [3,4,6]
        }
      
        for(let tr of $('.table-data tbody tr')){
            if(
                String(excelRow[positions.activo]).toUpperCase() == $(tr).find('td:eq("'+positionsASI[0]+'")').text().toUpperCase() &&
                String(excelRow[positions.subnumero]).toUpperCase() == $(tr).find('td:eq("'+positionsASI[1]+'")').text().toUpperCase() &&
                String(excelRow[positions.inventario]).toUpperCase() == $(tr).find('td:eq("'+positionsASI[2]+'")').text().toUpperCase()
            ){
                existRow = true
                // console.log("YA EXISTE")
                rowIndex = $(tr).index()
                break
            }
        }

        let successCount = $('.table-data tbody tr').length
        $('.nav-tabs li[data-type="success"] i').text(Number(successCount))

        if(existRow){
            let count = 0
            for(let td of $('.table-data tbody tr:eq('+rowIndex+') td')){
                if(count > 1){
                    if($(td).text() != excelRow[positions[tablePositions[count - 2]]]){
                        $(td).text( String(excelRow[positions[tablePositions[count - 2]]]).toUpperCase() )
                    }
                }
                count++
            }
        }
        else{

            let successCount = $('.table-data tbody tr').length
            $('.nav-tabs li[data-type="success"] i').text(Number(successCount) + 1)

            let itemsAppend = ''
    
            for(let i = 0; i < ( tablePositions.length + 2 ); i++){
                itemsAppend += '<td>'
                if(i == 0){
                    itemsAppend += '<span class="ion ion-trash-b remove-row"></span>';
                }
                else if(i == 1){
                    itemsAppend += $('.table-data tbody tr').length+1
                }
                else{
                    itemsAppend += String(excelRow[positions[tablePositions[i-2]]]).toUpperCase()
                }
                itemsAppend += '</td>'
            }
    
            $('.inline-table').find('.table tbody').prepend( $('<tr class="animated fadeIn">').append( itemsAppend ))

        }

        if(!$('.inline-table').is(':visible') && $('.nav-tabs li[data-type="success"]').is('.active')){
            $('.inline-table').fadeIn(400)
        }

    },
    printError(ind, err){
        
        let errorCount = $('.nav-tabs li[data-type="error"] i').text()
        $('.nav-tabs li[data-type="error"] i').text(Number(errorCount) + 1)
        
        let html = `<div class="item" data-index="${ind}">
            <h6 class="line">
                Fila ${ind} <span><i class="ion ion-bug"></i>
                    ${err.length > 1 ? `${err.length} errores`: `${err.length} error`}
                </span>
            </h6><ul>`
        
        for(let i of err){
            let errorData = this.destructureError(i)
            
            html += `<li data-column="${errorData.columns}">${errorData.errControl}</li>`

            if(errorData.options.length){
                html += `<ol>`
                for(let o of errorData.options){
                    html += `<li>${o}</li>`
                }
                html += `</ol>`
            }

        }
        
        html += '</ul></div>';
        
        $('.error-multiple').append(html);

    },
    validate(excelRow, rowLength, line){
        const form = $('form').attr('id')
        var error_array = []
        const positions = this.getPositionsExcel()
        this.setOptionsError = this.setOptionsError.bind(this)
        // if cell data is empty, set empty string
            for(var i = 0; i < excelRow.length; i++){
                if(excelRow[i] == null){
                    excelRow[i] = ''
                }
                else{
                    excelRow[i] == String(excelRow[i]).trim()
                }
            }
        // check if it is data here
            if( excelRow[positions.activo] == "" && excelRow[positions.inventario] == "" && excelRow[positions.denominacion] == ""){
                $('#wait').fadeOut(400)
                return false
            }
        if(form === 'formatoDatosMaestros')
        {
            var dataActive = null,
                existInventory = null,
                cecoCongruence = null
            {
                let params = [
                    'activo',
                    'sn',
                    'noInventario',
                    'clase',
                    'denominacion',
                    'serie',
                    'ceco',
                    'siglaVicepresidencia',
                    'siglaGerencia',
                    'tipoOperacion',
                    'municipio',
                    'ubicacionGeografica',
                    'vuRemanenteAr01 as vur',
                    'claveAr01'
                ],
                number = excelRow[positions.activo] != "" ? excelRow[positions.activo] : excelRow[positions.inventario],
                sn = excelRow[positions.subnumero],
                type = excelRow[positions.activo] != "" ? 'activo' : 'noInventario'
                
                // Valida Nuevo CECO, vicepresidencia y gerencia
                if(excelRow[positions.nuevoCodCeco] != ""){
                    //Si es un subnúmero muestra el error
                    if( Number(excelRow[positions.subnumero])>0 ){
                        error_array.push([`No puede editar el CECO para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                    }
                    else{
                        if(excelRow[positions.nuevaVicepresidencia] == "")
                            error_array.push([`Si solicita cambio de CeCo, debe especificar la sigla de la NUEVA VICEPRESIDENCIA`, positions.nuevaVicepresidencia])
                        if(excelRow[positions.nuevaGerencia] == "")
                            error_array.push([`Si solicita cambio de CeCo, debe especificar la sigla de la NUEVA GERENCIA`, positions.nuevaGerencia])
    
                        let regex = new RegExp(dataRules.rules.ceco.regex, 'g')
                        if(!regex.test(excelRow[positions.nuevoCodCeco]))
                            error_array.push([`el nuevo CECO contiene caracteres no permitidos o no cumple con la estructura de un CECO`, positions.nuevoCodCeco])
    
                        let paramsCeco = [
                            'ceco',
                            'siglaVicepresidencia',
                            'siglaGerencia',
                            'campoPlanta',
                            'nombreCampoPlanta'
                        ]
                        cecoCongruence = queries
                            .getJerarquizacion(paramsCeco.toString(), excelRow[positions.nuevoCodCeco])
                            .then(data => {
                                // Existe el Ceco ?
                                if(data.error == undefined){
                                    excelRow[positions.nuevaVicepresidencia] = data[0].siglaVicepresidencia
                                    excelRow[positions.nuevaGerencia] = data[0].siglaGerencia
                                }
                                else{
                                    error_array.push([`El nuevo COODIGO DE CECO no se encontró en la tabla de Jerarquización`, positions.nuevoCodCeco])
                                }
                                // CeCo y Campo Planta son coherentes ?
                                if( excelRow[positions.nuevoCodCampoPlanta] == data[0].campoPlanta ){
                                    // Campo planta - código campo planta son coherentes ?
                                    if( excelRow[positions.nuevoCampoPlanta] != data[0].nombreCampoPlanta ){
                                        error_array.push([`El código de CAMPO PLANTA no corresponde con el nombre del CAMPO PLANTA`, positions.nuevoCodCampoPlanta])
                                    }
                                }
                                else{
                                    error_array.push([`El nuevo CAMPO PLANTA está vacío o no corresponde con el nuevo Ceco`, positions.nuevoCodCeco])
                                }
                            })
                            .catch(err => console.log("getJerarquizacion"+err.responseText))
                    }
                }

                // otras validaciones
                dataActive = queries
                .getActivo( number, sn, true, params.toString(), type )
                .then(data => {
                    let spaces = "               "
                    console.groupCollapsed('%c FILA: '+line+spaces+'Tipo: '+type+'%c  Número: '+number+'%c  SN: '+sn+spaces, "background:#000;color:#bd5;", "background:#000;color:#bd5;", "background:#000;color:#bd5;text-align:center;")
                    let parentData = draw.getSnData(data,0)
                    let primalData = draw.getSnData(data,sn)
                    /* get catalogo */
                        let catalogo = storage.getLocal('catalogo')
                    /* get unidades */
                        let unidadesAsociadas = storage.getLocal('unidadesAsociadas')
                        let unidadesDirectas = storage.getLocal('unidadesDirectas')
                    /* get ubicaciones */
                        let ubicaciones = storage.getLocal('ubicaciones')
                    /* get campoPlanta */
                        let campoPlanta = storage.getLocal('campoPlanta')
                    /* get Custodios */
                        let custodios = storage.getLocal('custodios')
                // Inicio de las validaciones
                    // Validaciones de identificacion 
                        {
                            console.log("- Verificando datos de identificación del activo -")
                            if(excelRow[positions.activo] != primalData.activo){
                                error_array.push([`Según Auxiliar, el número de activo debería ser ${primalData.activo}`, positions.activo, positions.sn])
                            }
                            if(excelRow[positions.inventario] != primalData.noInventario){
                                error_array.push([`Según Auxiliar, el número de inventario debería ser ${primalData.noInventario}`, positions.inventario])
                            }
                            if(excelRow[positions.denominacion] != primalData.denominacion){
                                error_array.push([`Según Auxiliar, la denominación debería ser ${primalData.denominacion}`, positions.denominacion])
                            }
                            if(excelRow[positions.codClase] != primalData.clase){
                                error_array.push([`Según Auxiliar, el código de Clase debería ser ${primalData.clase}`, positions.codClase])
                            }
                            if(excelRow[positions.serie] != primalData.serie){
                                error_array.push([`Según Auxiliar, la serie debería ser ${primalData.serie}`, positions.serie])
                            }
                        }
                    // nueva denominacion
                        if(excelRow[positions.nuevaDenominacion] != ""){
                            console.groupCollapsed("%cNueva denominación",'color:brown;')
                            if(String(primalData.tipoOperacion).toUpperCase() == 'ASOCIADAS'){
                                console.log("--- Operación asociada --")
                                let regex = new RegExp(dataRules.rules.denominacion_complementaria.regex, 'g')
                                
                                if(!regex.test(excelRow[positions.nuevaDenominacion]))
                                error_array.push([`La NUEVA DENOMINACIÓN contiene caracteres no permitidos`, positions.nuevaDenominacion])
                            }
                            var operacionDirecta = ["DIRECTA", "ASOCIADA OPERADA"];
                            if( operacionDirecta.indexOf( String(primalData.tipoOperacion).toUpperCase() ) >= 0 ){
                                console.log("--- Operación directa --")
                                if( parseInt(excelRow[positions.subnumero]) == 0){
                                    console.log("---- Activo principal ----")
                                    let denominacion = []
                                    for(let i of catalogo){
                                        if(i.clase == primalData.clase){
                                            denominacion.push(i.label)
                                        }
                                    }
                                    console.log("Denominación: "+denominacion)
                                    let isEqual = false
                                    for(let i of denominacion){
                                        if(i == excelRow[positions.nuevaDenominacion]){
                                            isEqual = true
                                            break
                                        }
                                    }
                                    
                                    if(!isEqual){
                                        let optionsError = this.setOptionsError(denominacion)
                                        error_array.push([`no se reconoce la NUEVA DENOMINACIÓN, verifique las denominaciones válidas según la clase ${excelRow[positions.codClase]} en el <b>Catálogo</b>`, positions.nuevaDenominacion])
                                    }
                                }
                                else{
                                    console.log("---- Subnúmero ----")
                                    let isEqual = false
                                    for(let i of staticOptions.subnumberDenominacion){
                                        if(i == excelRow[positions.nuevaDenominacion]){
                                            isEqual = true
                                            break
                                        }
                                    }
                                    if(!isEqual){
                                        let optionsError = this.setOptionsError(staticOptions.subnumberDenominacion)
                                        error_array.push([`La nueva denominación debe ser alguna de las siguientes: ${optionsError}`, positions.nuevaDenominacion])
                                    }
                                }
                            }
                            let maxLenght = dataRules.rules.denominacion2.maxlength
                            if( String(excelRow[positions.nuevaDenominacion]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`La nueva DENOMINACIÓN excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevaDenominacion])
                            }
                            console.groupEnd()
                        }
                    // nueva denominacion complementaria
                        if(excelRow[positions.nuevaDenominacion2] != ""){
                            console.groupCollapsed("%cNueva denominación complementaria", 'color:brown;')
                            let regex = new RegExp(dataRules.rules.denominacion_complementaria.regex, 'g')
                            if(!regex.test(excelRow[positions.nuevaDenominacion2]))
                                error_array.push([`La nueva denominación complementaria contiene caracteres no permitidos`, positions.nuevaDenominacion2])
                            // Validación de longitud
                            let maxLenght = dataRules.rules.denominacion2.maxlength
                            if( String(excelRow[positions.nuevaDenominacion2]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`La nueva DENOMINACIÓN COMPLEMENTARIA excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevaDenominacion2])
                            }
                            console.groupEnd()
                        }
                    // nueva serie
                        if(excelRow[positions.nuevaSerie] != ""){
                            console.groupCollapsed("%cNueva Serie", 'color:brown;')
                            let regex = new RegExp(dataRules.rules.serie.regex, 'g')
                            if(!regex.test(excelRow[positions.nuevaSerie]))
                                error_array.push([`La nueva serie contiene caracteres no permitidos`, positions.nuevaSerie])
                            // Validación de longitud
                            let maxLenght = dataRules.rules.serie.maxlength
                            if( String(excelRow[positions.nuevaSerie]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`La nueva SERIE excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevaDenominacion2])
                            }
                            console.groupEnd()
                        }
                    // nuevo número de inventario
                        if(excelRow[positions.nuevoInventario] != ""){
                            console.groupCollapsed("%cNuevo número de inventario",'color:brown;')
                            //Si es un subnúmero muestra el error
                            if( Number(excelRow[positions.subnumero])>0 ){
                                error_array.push([`No puede editar el NUMERO DE INVENTARIO para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                            }
                            else{
                                // Verificar si ya existe el número de inventario
                                existInventory = queries
                                .getExistingInvetory(excelRow[positions.nuevoInventario])
                                .then(data => {
                                    if(data.error != undefined)
                                        error_array.push([`El nuevo número de inventario ya existe, por favor diligencie otro número`, positions.nuevoInventario])
                                })
                                .catch(err => console.log("getExistingInvetory"+err.responseText))
                                // ¿ Es un inmueble ?
                                let isBuilding = false
                                for(let i of staticOptions.clasesInmuebles){
                                if(primalData.clase == i){
                                    isBuilding = true
                                    break
                                }
                                }
                                // Si la clase del activo se encuentra en el listado de clases de inmuebles
                                if(isBuilding){
                                console.log("--- Es un inmueble ---")
                                let regex = new RegExp(dataRules.rules.matricula_inmobiliaria.regex, 'g'),
                                    maxChar = dataRules.rules.matricula_inmobiliaria.maxlength

                                if(!regex.test(excelRow[positions.nuevoInventario]))
                                    error_array.push([`El nuevo NÚMERO DE INVENTARIO contiene caracteres no permitidos`, positions.nuevoInventario])
                                if( String(excelRow[positions.nuevoInventario]).length > maxChar )
                                    error_array.push([`El nuevo NÚMERO DE INVENTARIO debe tener máximo ${maxChar} caracteres`, positions.nuevoInventario])
                                }
                                // Si el activo no es un inmueble
                                else{
                                console.log("--- No es un inmueble ---")
                                let regex = new RegExp(dataRules.rules.inventario.regex, 'g'),
                                    maxChar = dataRules.rules.inventario.maxlength

                                if(!regex.test(excelRow[positions.nuevoInventario]))
                                    error_array.push([`El nuevo NÚMERO DE INVENTARIO contiene caracteres no permitidos`, positions.nuevoInventario])
                                if( String(excelRow[positions.nuevoInventario]).length > maxChar )
                                    error_array.push([`El nuevo NÚMERO DE INVENTARIO debe tener máximo ${maxChar} caracteres`, positions.nuevoInventario])
                                }
                            }
                            console.groupEnd()
                        }                        
                    // nueva capacidad
                        if(excelRow[positions.nuevaCapacidad] != ""){
                            console.groupCollapsed("%cNueva capacidad",'color:brown;')
                            let regex = new RegExp(dataRules.rules.capacidad.regex, 'g')
                            if(!regex.test( String(excelRow[positions.nuevaCapacidad]) )){
                                error_array.push([`La nueva capacidad debe ser numérica. Puede contener 4 dígitos y hasta 3 decimales `, positions.nuevaCapacidad])
                            }
                            console.groupEnd()
                        }
                    // nueva unidad de medida
                        if(excelRow[positions.nuevaUnidadMedidad] != ""){
                            console.groupCollapsed("%cNueva unidad de medida-",'color:brown;')
                            var operacionDirecta = ["DIRECTA", "ASOCIADA OPERADA"]
                            if( operacionDirecta.indexOf( String(primalData.tipoOperacion).toUpperCase() ) >=0 ){
                                if( parseInt(excelRow[positions.subnumero]) == 0 ){
                                    console.log("--- Principal ---")
                                    if(excelRow[positions.nuevaDenominacion] == ""){
                                        error_array.push([`La nueva UNIDAD DE MEDIDA debe ser acorde a la denominación en el catálogo, por favor ingrese una nueva denominación`, positions.nuevaDenominacion])
                                    }
                                    else{
                                        let object = []
                                        for(let i of catalogo){
                                            if(i.clase == primalData.clase){
                                                object.push({
                                                    denominacion: i.label,
                                                    unidad: i.unidad
                                                })
                                            }
                                        }
                                        for(let o of object){
                                            if(excelRow[positions.nuevaDenominacion] == o.denominacion){
                                                if(excelRow[positions.nuevaUnidadMedidad] != o.unidad){
                                                    error_array.push([`La nueva UNIDAD DE MEDIDA debería ser: ${o.unidad}`, positions.nuevaUnidadMedidad])
                                                }
                                            }
                                        }
                                    }
                                }
                                else{
                                    console.log("--- Subnúmero ---")
                                    // Para subnúmeros la unidad de medida debe ser UN excepto para Accesorios o Parte 
                                    if(excelRow[positions.nuevaDenominacion] == 'ADICION + ACCESORIOS' || excelRow[positions.nuevaDenominacion] == 'REEMPLAZO DE PARTE O COMPONENTE'){
                                        let existUnit = false
                                        for(let i of unidadesAsociadas){
                                            if(excelRow[positions.nuevaUnidadMedidad] == i.unidad){
                                                existUnit = true
                                                break  
                                            }
                                        }
                                        if(!existUnit){
                                            error_array.push([`La nueva UNIDAD DE MEDIDA no se encuentra habilitada para ingresar a SAP, por favor verifique la nueva unidad de medida`, positions.nuevaUnidadMedidad])
                                        }
                                    }
                                    else if(excelRow[positions.nuevaUnidadMedidad] != 'UN'){
                                        error_array.push([`La nueva UNIDAD DE MEDIDA debería ser: UN`, positions.nuevaUnidadMedidad])
                                    }
                                }
                            }
                            console.groupEnd()
                        }
                    // nuevo tag
                        if(excelRow[positions.nuevoTag] != ""){
                            console.groupCollapsed("%cNuevo TAG",'color:brown;')
                            let regex = new RegExp(dataRules.rules.tag.regex, 'g')
                            if(!regex.test(excelRow[positions.nuevoTag]))
                                error_array.push([`El nuevo TAG contiene caracteres no permitidos`, positions.nuevoTag])
                        // Validación de longitud
                            let maxLenght = dataRules.rules.tag.maxlength
                            if( String(excelRow[positions.nuevoTag]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`El nuevo TAG excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevoTag])
                            }
                            console.groupEnd()
                        }
                    // Nueva matrícula de vehículo
                        if(excelRow[positions.nuevaMatricula] != ""){
                            console.groupCollapsed('%cNueva Matrícula de vehículo','color:brown;')
                            
                            let isVehiculo = false
                            for(let veh of staticOptions.clasesVehiculos){
                                if( veh == String(primalData.clase).toUpperCase().trim() )
                                {
                                    isVehiculo = true
                                }
                            }

                            if(isVehiculo){
                                //Si es un subnúmero muestra el error
                                if( Number(excelRow[positions.subnumero])>0 ){
                                    error_array.push([`No puede editar la MATRÍCULA VEHÍCULO para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                                }
                                else{
                                    let regex = new RegExp(dataRules.rules.matriculaVehiculo.regex, 'g')
                                    if(!regex.test(excelRow[positions.nuevaMatricula]))
                                        error_array.push([`La nueva MATRICULA DE VEHICULO contiene caracteres no permitidos`, positions.nuevaMatricula])
                                // Validación de longitud
                                    let maxLenght = dataRules.rules.matriculaVehiculo.maxlength
                                    if( String(excelRow[positions.nuevaMatricula]).length > maxLenght ){
                                        console.log("--- Excede longitud ---")
                                        error_array.push([`La nueva MATRICULA DE VEHICULO excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevaMatricula])
                                    }
                                }
                            }
                            else{
                                error_array.push([`El activo indicado no es un Vehículo, no aplica "Matrícula de Vehículo"`,positions.nuevaMatricula])
                            }
                            
                            console.groupEnd()
                        }
                    // nuevo fabricante
                        if(excelRow[positions.nuevoFabricante] != ""){
                            console.groupCollapsed("%cNuevo FABRICANTE",'color:brown;')
                            let regex = new RegExp(dataRules.rules.fabricante.regex, 'g')
                            
                            if(!regex.test(excelRow[positions.nuevoFabricante]))
                                error_array.push([`El nuevo FABRICANTE contiene caracteres no permitidos`, positions.nuevoFabricante])
                        // Validación de longitud
                            let maxLenght = dataRules.rules.fabricante.maxlength
                            if( String(excelRow[positions.nuevoFabricante]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`El nuevo FABRICANTE excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevoTag])
                            }
                            console.groupEnd()
                        }
                    // nuevo modelo
                        if(excelRow[positions.nuevoModelo] != ""){
                            console.groupCollapsed("%cNuevo MODELO",'color:brown;')
                            let regex = new RegExp(dataRules.rules.modelo.regex, 'g')
                            
                            if(!regex.test(excelRow[positions.nuevoModelo]))
                                error_array.push([`El nuevo MODELO contiene caracteres no permitidos`, positions.nuevoModelo])
                        // Validación de longitud
                            let maxLenght = dataRules.rules.modelo.maxlength
                            if( String(excelRow[positions.nuevoModelo]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`El nuevo MODELO excede el límite de caracteres permitidos(${maxLenght})`,positions.nuevoTag])
                            }
                            console.groupEnd()
                        }
                    // nuevo custodio
                        if(excelRow[positions.nuevoRegistroCustodio] != ""){
                            console.groupCollapsed('%cNuevo REGISTRO CUSTODIO','color:brown;')
                            let existcustodio = false
                            
                            for(let cust of custodios){
                                if( cust.registro == String(excelRow[positions.nuevoRegistroCustodio]).toUpperCase().trim())
                                {
                                    existcustodio = true
                                    break
                                }
                            }

                            if(!existcustodio){
                                error_array.push([`No se identificó el nuevo REGISTRO DEL CUSTODIO ingresado`,positions.nuevoRegistroCustodio])
                            }
                            console.groupEnd()
                        }
                    // nuevo código ubicación geográfica
                        if(excelRow[positions.nuevaUbicacion] != ""){
                            if(excelRow[positions.nuevoCodUbicacion] == ""){
                                error_array.push(['Para cambiar la nueva UBICACIÓN GEOGRÁFICA debe ingresar el CÓDIGO de la ubicación',positions.nuevaUbicacion])
                            }
                        }
                        if(excelRow[positions.nuevoCodUbicacion] != ""){
                            console.groupCollapsed("%cNuevo CÓDIGO UBICACIÓN GEOGRÁFICA",'color:brown;')
                            //Si es un subnúmero muestra el error
                            if( Number(excelRow[positions.subnumero])>0 ){
                                error_array.push([`No puede editar el CÓDIGO UBICACIÓN GEOGRÁFICA para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                            }
                            else{
                                let municipioObjetivo = excelRow[positions.nuevoCodMunicipio] == "" ? primalData.municipio : excelRow[positions.nuevoCodMunicipio]

                                let existLocation = false
                                let location = {}
                                for(let d of ubicaciones){
                                    for(let m of d.municipios){
                                        if(m.codigoMunicipio == municipioObjetivo )
                                        {
                                            location = m
                                            for(let ug of m.ubicaciones){
                                                if(ug.codigoUbicacion == excelRow[positions.nuevoCodUbicacion]){
                                                    existLocation = true
                                                    location = ug
                                                    break
                                                }
                                            }
                                        }
                                    }
                                }
                                // si se encuentra el código, asigna el nombre
                                if(existLocation){
                                    excelRow[positions.nuevaUbicacion] = location.ubicacion
                                    excelRow[positions.nuevoCodUbicacion] = location.codigoUbicacion
                                }
                                else{
                                    error_array.push([`El nuevo código de ubicación geográfica debe ser acorde a las ubicaciones del Municipio ${location.municipio}`, positions.nuevoCodUbicacion])
                                }
                            }
                            console.groupEnd()
                        }
                    // nuevo tipo de operación
                        if(excelRow[positions.nuevoTipoOperacion] != ""){
                            console.groupCollapsed('%cNuevo TIPO DE OPERACIÓN','color:brown;')
                            //Si es un subnúmero muestra el error
                            if( Number(excelRow[positions.subnumero])>0 ){
                                error_array.push([`No puede editar el TIPO DE OPERACIÓN para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                            }
                            else{
                                let existTipoOperacion = false,
                                    optionsTipoOperacion = []
    
                                for(let op of staticOptions.tiposDeOperacion){
                                    optionsTipoOperacion.push(op)
                                    if( op == String(excelRow[positions.nuevoTipoOperacion]).toUpperCase().trim())
                                    {
                                        existTipoOperacion = true
                                    }
                                }
                                if(!existTipoOperacion){
                                    let optionsError = this.setOptionsError(optionsTipoOperacion);
                                    error_array.push([`El nuevo TIPO DE OPERACIÓN no está definido, debería ser uno de los siguientes ${optionsError}`,positions.nuevoTipoOperacion])
                                }
                            }
                            console.groupEnd()
                        }
                    // nuevo código municipio
                        if(excelRow[positions.nuevoMunicipio] != ""){
                            if(excelRow[positions.nuevoCodMunicipio] == ""){
                                error_array.push(['Para cambiar el MUNICIPIO debe ingresar el CÓDIGO del municipio',positions.nuevoCodMunicipio])
                            }
                        }
                        if(excelRow[positions.nuevoCodMunicipio] != ""){
                            console.groupCollapsed("%cNuevo CÓDIGO DE MUNICIPIO",'color:brown;')
                            //Si es un subnúmero muestra el error
                            if( Number(excelRow[positions.subnumero])>0 ){
                                error_array.push([`No puede editar el CÓDIGO DE MUNICIPIO para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                            }
                            else{
                                let existMunicipio = false
                                let location = {}
                                for(let d of ubicaciones){
                                    for(let m of d.municipios){
                                        if(m.codigoMunicipio == excelRow[positions.nuevoCodMunicipio])
                                        {
                                            existMunicipio = true
                                            location = m
                                            break
                                        }
                                    }
                                }
                                if(existMunicipio){
                                    excelRow[positions.nuevoMunicipio] = location.municipio
    
                                    // validar código de ubicación geográfica
                                    if(excelRow[positions.nuevoCodUbicacion] != ""){
                                        let existUbicacion = false
                                        for(let ug of location.ubicaciones){
                                            if(ug.codigoUbicacion == excelRow[positions.nuevoCodUbicacion]){
                                                existUbicacion = true
                                                location = ug
                                                break
                                            }
                                        }
    
                                        if(existUbicacion){
                                            excelRow[positions.nuevaUbicacion] = location.ubicacion
                                        }
                                    }
                                    else{
                                        error_array.push([`Es necesario asignar un nuevo CÓDIGO DE UBICACIÓN GEOGRÁFICA acorde al municipio, por favor verifique el código`, positions.nuevoCodMunicipio]) 
                                    }
                                }
                                else{
                                    error_array.push([`El nuevo CÓDIGO DE MUNICIPIO no está registrado en el auxiliar, por favor verifique el código`, positions.nuevoCodMunicipio])
                                }
                            }
                            console.groupEnd()
                        }
                    // nuevo código campo planta
                        if(excelRow[positions.nuevoCampoPlanta] != ""){
                            if(excelRow[positions.nuevoCodCampoPlanta] == ""){
                                error_array.push(['Para cambiar el CAMPO PLANTA debe ingresar el CÓDIGO del campo planta',positions.nuevoCodCampoPlanta])
                            }
                        }
                        if(excelRow[positions.nuevoCodCampoPlanta] != ""){
                            console.groupCollapsed("%cNuevo CÓDIGO DE CAMPO PLANTA",'color:brown;')
                            //Si es un subnúmero muestra el error
                            if( Number(excelRow[positions.subnumero])>0 ){
                                error_array.push([`No puede editar el CAMPO PLANTA para subnúmeros, edite el activo principal`,positions.nuevoInventario])
                            }
                            else{
                                let existCampo = false,
                                    campo = {}
                                for(let i of campoPlanta){
                                    if(excelRow[positions.nuevoCodCampoPlanta] == i.codigo){
                                        existCampo = true
                                        campo = i
                                        break
                                    }
                                }
                                if(existCampo){
                                    excelRow[positions.nuevoCampoPlanta] = campo.nombre
                                }
                                else{
                                    error_array.push([`No se encontró el nuevo CÓDIGO DE CAMPO PLANTA, por favor verifique el código`, positions.nuevoCodCampoPlanta])
                                }
                                // Congruencia con Ceco ya se revisa en verificación de Nuevo CeCo
                            }
                            console.groupEnd()
                        }
                    // nueva VUR
                        if(excelRow[positions.nuevaVUR] != ""){
                            console.groupCollapsed("%cNueva VUR (vida útil remanente)",'color:brown;')
                            if(primalData.claveAr01 != "ZVUR"){
                                error_array.push(['La VIDA ÚTIL REMANENTE no aplica para activos que no deprecian en línea recta', positions.nuevaVUR]) 
                            }
                            else{
                                let validateVUR = true
                                if( isNaN(excelRow[positions.nuevaVUR]) ){
                                    error_array.push(['La nueva VIDA ÚTIL REMANENTE debe ser numérico y estar entre 0 y 999', positions.nuevaVUR])
                                    validateVUR = false
                                }
                                if(excelRow[positions.nuevaVUR] == primalData.vur){
                                    error_array.push(['La nueva VIDA ÚTIL REMANENTE no debe ser igual a la que está registrada en SAP', positions.nuevaVUR])
                                    validateVUR = false
                                }
                                if(Number(excelRow[positions.nuevaVUR]) < 0 || Number(excelRow[positions.nuevaVUR]) > 999){
                                    error_array.push(['La nueva VIDA ÚTIL REMANENTE debe estar entre 0 y 999', positions.nuevaVUR])
                                    validateVUR = false
                                }
                                if(validateVUR){
                                    if(excelRow[positions.fechaVUR] != ""){
                                        let d = new Date(),
                                            firstDate = new Date(d.getFullYear(), d.getMonth()-1, 1),
                                            lastDate = new Date(d.getFullYear(), d.getMonth()+2, 0),
                                            readDate = String(excelRow[positions.fechaVUR]).split(/[\,\.\/]/g)
                                        readDate = new Date( Number(readDate[2]), Number(readDate[1])-1, Number(readDate[0])+1 )
                                            //Si la fecha es una fecha
                                        if( !(readDate instanceof Date && !isNaN(readDate)) ){
                                            console.log("ERROR EN LA FECHA DE ENTRADA")
                                            error_array.push(['Verifique el formato de la FECHA INICIAL PARA VUR', positions.fechaVUR])
                                        }
                                        if(readDate.getTime()<firstDate.getTime()) {
                                            console.log("Fecha menor")
                                            error_array.push([`La FECHA INICIAL PARA VUR es menor al rango (${firstDate.getDate()}/${firstDate.getMonth() + 1}/${firstDate.getFullYear()} - ${lastDate.getDate()}/${lastDate.getMonth() + 1}/${lastDate.getFullYear()})`, positions.fechaVUR])
                                        }
                                        if(readDate.getTime()>lastDate.getTime()) {
                                            console.log("Fecha mayor")
                                            error_array.push([`La FECHA INICIAL PARA VUR es mayor al rango (${firstDate.getDate()}/${firstDate.getMonth() + 1}/${firstDate.getFullYear()} - ${lastDate.getDate()}/${lastDate.getMonth() + 1}/${lastDate.getFullYear()})`, positions.fechaVUR])
                                        }
                                        // Por regla de Erika, debe diligenciar las columa CECO, vicepresidencia y Gerencia                                    
                                        if(excelRow[positions.ceco] != ""){
                                            if(String(excelRow[positions.ceco]).toUpperCase() != primalData.ceco){
                                                error_array.push([`El ceco indicado no corresponde al activo. Debería ser: ${primalData.ceco}`, positions.ceco])
                                            }
                                            if( String(excelRow[positions.vicepresidencia]).toUpperCase() != primalData.siglaVicepresidencia ){
                                                error_array.push([`La Vicepresidencia indicada no corresponde con el CECO, verifique la tabla e Jerarquización, debería ser: ${primalData.siglaVicepresidencia}`, positions.vicepresidencia])
                                            }
                                            if( String(excelRow[positions.gerencia]).toUpperCase() != primalData.siglaGerencia ){
                                                error_array.push([`La Gerencia indicada no corresponde con el CECO, verifique la tabla e Jerarquización, debería ser: ${primalData.siglaGerencia}`, positions.vicepresidencia])
                                            }
                                            // Con estas líneas omite la Vicepresidenciay Gerencia que escriba el usuario en excel y las lee del auxiliar
                                            // excelRow[positions.vicepresidencia] = primalData.siglaVicepresidencia
                                            // excelRow[positions.gerencia] = primalData.siglaGerencia
                                        }
                                    }
                                    else{
                                        error_array.push(['Para cambiar la VUR, debe ingresar una fecha válida', positions.fechaVUR])
                                    }
                                }
                            }
                            console.groupEnd()
                        }
                    // Observaciones
                        if(excelRow[positions.observaciones] != ""){
                            console.groupCollapsed("%cOBSERVACIONES",'color:brown;')
                        // Validación de longitud
                            let maxLenght = dataRules.rules.observaciones.maxlength
                            if( String(excelRow[positions.observaciones]).length > maxLenght ){
                                console.log("--- Excede longitud ---")
                                error_array.push([`La OBSERVACION excede el límite de caracteres permitidos(${maxLenght})`,positions.observaciones])
                            }
                            console.groupEnd()
                        }
                // Fin de las validaciones
                    if(error_array.length>0){
                        console.groupCollapsed(`%c No se puede agregar el activo ${primalData.activo} `,'color:white;background:red;')
                        for(let error of error_array){
                            console.log('%c * '+error[0],'color:dodgerblue;')
                        }
                        console.groupEnd()
                    }
                    else
                        console.log(`%c El activo ${primalData.activo} fue agregado a la solicitud `,'color:white;background:green;')
                    //console.log('%c---------------------------------------------------------','background:#000;color:#bd5;')
                    console.groupEnd()
                })
                .catch(err => {
                    error_array.push(['El activo, inventario o subnúmero no existe', positions.activo, positions.sn, positions.inventario])
                    console.log("getActivo"+err.responseText)
                })
                    
            }
            Promise.all([dataActive, existInventory, cecoCongruence]).then(val => {
                this.printErrorSuccess(error_array, line, excelRow, positions)
            })
        }

        if(form === 'formatoBajas')
        {
            let params = [
                'activo',
                'sn',
                'noInventario as inventario',
                'clase',
                'denominacion',
                'serie',
                'ceco',
                'siglaVicepresidencia',
                'siglaGerencia',
                'valorAdquisicionAr01 as valorAdquisicion',
                'valorContableNetoAr01 as valorContableNeto'
            ],
            number = excelRow[positions.activo] != "" ? excelRow[positions.activo] : excelRow[positions.inventario],
            sn = excelRow[positions.subnumero],
            type = excelRow[positions.activo] != "" ? 'activo' : 'noInventario'

            dataActive = queries
                .getActivo( number, sn, false, params.toString(), type )
                .then(data => {
                    data = data[0]
                    // Verificar #activo
                        excelRow[positions.activo] != data.activo &&
                        error_array.push([`El número de activo debería ser ${data.activo}`, positions.activo])
                    // Verificar #inventario
                        excelRow[positions.inventario] != data.inventario &&
                        error_array.push([`El número de inventario debería ser ${data.inventario}`, positions.inventario])
                    // Verificar tipo de baja
                        {
                            let optionsError = this.setOptionsError(['BAJA TOTAL','BAJA PARCIAL']);
                            
                            (String(excelRow[positions.tipoBaja]).toUpperCase() != 'BAJA TOTAL' && 
                             String(excelRow[positions.tipoBaja]).toUpperCase() != 'BAJA PARCIAL') &&
                            error_array.push([`El tipo de baja debería ser uno de los siguientes: ${optionsError}`, positions.tipoBaja])
                        }
                    // Verificar denominación
                        excelRow[positions.denominacion] != data.denominacion &&
                        error_array.push([`La denominacion debería ser ${data.denominacion}`, positions.denominacion])
                    // Verificar serie
                        excelRow[positions.serie] != data.serie &&
                        error_array.push([`La serie debería ser ${data.serie}`, positions.serie])
                    // Verificar centro de costos
                        excelRow[positions.ceco] != data.ceco &&
                        error_array.push([`El centro de costos debería ser ${data.ceco}`, positions.ceco])
                    // Verificar Vicepresidencia
                        excelRow[positions.vicepresidencia] == "" &&
                        error_array.push([`Es necesario diligenciar la vicepresidencia.`, positions.vicepresidencia])

                        excelRow[positions.vicepresidencia] != data.siglaVicepresidencia &&
                        error_array.push([`La Vicepresidencia debería ser ${data.siglaVicepresidencia}`, positions.vicepresidencia])
                    // Verificar Gerencia
                        excelRow[positions.gerencia] == "" &&
                        error_array.push([`Es necesario diligenciar la gerencia.`, positions.gerencia])

                        excelRow[positions.gerencia] != data.siglaGerencia &&
                        error_array.push([`La Gerencia debería ser ${data.siglaGerencia}`, positions.gerencia])

                    // Valor de baja
                        switch(String(excelRow[positions.tipoBaja]).toUpperCase()){
                            case 'BAJA TOTAL':
                                // Si se trata de un subnúmero, genera error
                                if(Number(excelRow[positions.subnumero]) > 0)
                                {
                                    error_array.push(['El subnúmero indicado para BAJA TOTAL no es válido. Revise si desea hacer una BAJA TOTAL del activo principal o una BAJA PARCIAL de un subnúmero.', positions.subnumero])
                                    // error_array.push(['La baja del 100% del valor de un subnúmero NO se considera baja TOTAL. Para una baja total de un subnúmero se debe hacer la baja total del activo principal o cambiar el tipo de baja a PARCIAL y asignarle el máximo valor permitido', positions.tipoBaja, positions.valorBaja])
                                }
                                // Su se trata de un activo principal, lo importa con valor de baja 0 ya que es total.
                                else
                                {
                                    excelRow[positions.valorBaja] = 0
                                   // Number(String(excelRow[positions.valorBaja]).replace(/[$\.\,]/g,'')) != data.valorAdquisicion &&
                                   // error_array.push([`El valor de baja debe ser igual a ${data.valorAdquisicion}`, positions.valorBaja])
                                }
                            break;
                            case 'BAJA PARCIAL':
                                if(Number(excelRow[positions.subnumero]) >= 0)
                                {
                                    // El valor de baja solicitado no puede ser mayor al valor de adquisición
                                    Number(String(excelRow[positions.valorBaja]).replace(/[$\.\,]/g,'')) > data.valorAdquisicion &&
                                    error_array.push(['El valor de la baja no debe superar el valor de adquisicion del activo', positions.valorBaja])
                                }
                                else if(Number(excelRow[positions.subnumero]) == 0)
                                {
                                   // Number(String(excelRow[positions.valorBaja]).replace(/[$\.\,]/g,'')) > Math.round( Number(data.valorContableNeto - (Number(storage.getLocal('SMM').salario)/2) ) ) &&
                                   // error_array.push([`En el valor de la baja se debe dejar al menos un valor simbólico de la mitad del salario mínimo del año actual que equivale a: ${Number(storage.getLocal('SMM').salario)/2}`, positions.valorBaja])
                                }
                                if( Number(String(excelRow[positions.valorBaja]).replace(/[$\.\,]/g,'')) == 0 ){
                                    // No permitir bajas parciales con valor cero o nulo
                                    error_array.push(['El valor de la BAJA PARCIAL, no puede ser cero ni estar vacío', positions.valorBaja])
                                }
                            break;
                        }
                    // Motivo de retiro y disposición final
                        {
                            excelRow[positions.motivoRetiro] == "" &&
                            error_array.push([`Es necesario diligenciar el motivo de retiro.`, positions.motivoRetiro])
                            
                            excelRow[positions.disposicionFinal] == "" &&
                            error_array.push([`Es necesario diligenciar la disposición final.`, positions.disposicionFinal])

                            let existMotivoRetiro = false
                            let existDisposicionFinal = false
                            let disposicionFinalMatch = []
                            
                            for(let i of staticOptions.motivoRetiro_disposicionFinal){
                                if(String(i.name) == String(excelRow[positions.motivoRetiro]).toUpperCase().trim())
                                {
                                    existMotivoRetiro = true
                                    disposicionFinalMatch = i.options
                                    break
                                }
                            }

                            if(existMotivoRetiro)
                            {
                                for(let i of disposicionFinalMatch){
                                    if( i == String(excelRow[positions.disposicionFinal]).toUpperCase().trim()){
                                        existDisposicionFinal = true
                                        break
                                    }
                                }

                                !existDisposicionFinal &&
                                error_array.push([`La disposición final debe ser igual a alguna de las siguientes opciones: ${this.setOptionsError(disposicionFinalMatch)}`, positions.disposicionFinal])
                            }
                            else
                            {
                                let optionsMotivoRetiro = []

                                for(let i of staticOptions.motivoRetiro_disposicionFinal){
                                    optionsMotivoRetiro.push(i.name)
                                }

                                let optionsError = this.setOptionsError(optionsMotivoRetiro);

                                error_array.push([`El motivo de retiro debe ser igual a alguna de las siguientes opciones: ${optionsError}`, positions.motivoRetiro])
                                
                                error_array.push([`La disposición final está sujeta a la selección del motivo de retiro, por favor ingrese un valor congruente para los dos`, positions.disposicionFinal])
                            }
                        }
                })
                .catch(err => {
                    error_array.push(['El activo, inventario o subnúmero no existe', positions.activo, positions.sn, positions.inventario])
                    console.log("getActivo2"+err)
                })
            
            Promise.all([dataActive]).then(val => {
                this.printErrorSuccess(error_array, line, excelRow, positions)
            })
        }
        // check if is the last row
            if(line >= rowLength){
                // console.timeEnd('query of masive bajas')
                $('#wait').fadeOut(400)
            }
        
    }
}

export default multiples