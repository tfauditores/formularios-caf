const url = 'include/api.php'

const queries = {
    async getActivo(
        number = 0, 
        sn = 0, 
        list = true, 
        params = '*', 
        type = 'activo', 
        method = 'getAsset'
    ){
    let response = await $.get( 
        url, 
        {number, sn, list, params, type, method}, 
        (data) => {
            return data
        })
        return response
    },
    async getExistingInvetory(number = 0,method = 'getExistingInvetory'){
        let response = await $.get( 
            url, 
            {number, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getCatalogo(params = '*', method = 'getCatalogo'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getUnidades(params = '*', method = 'getUnidades'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getFabricantes(params = '*', method = 'getFabricantes'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getUbicaciones(params = '*', method = 'getUbicaciones'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getCecos(params = '*', method = 'getCecos'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getCecoByName(params = '*', method = 'getCecoByName', name=''){
        let response = await $.get(
            url, 
            {params, method, name}, 
            (data) => {
                return data
            })
        return response
    },
    async getCampoPlanta(params = '*', method = 'getCampoPlanta'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getJerarquizacion(params = '*', value = 0, method = 'getJerarquizacion'){
        let response = await $.get(
            url, 
            {params, value, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getCustodios(params = '*', method = 'getCustodios'){
        let response = await $.get(
            url, 
            {params, method}, 
            (data) => {
                return data
            })
        return response
    },
    async getSMM(method = 'SMM'){
        let response = await $.get(
            url, 
            {method}, 
            (data) => {
                return data
            })
        return response[0]
    },
    async postCreateExcel(values){
        const form = $('form').attr('id')
        let urlExcel = null
        if(form == 'formatoDatosMaestros'){
            urlExcel = 'include/generarXlsDatosMaestros.php'
        }
        if(form == 'formatoBajas'){
            urlExcel = 'include/generarXlsBajas.php'
        }

        let response = await $.post(
            urlExcel,
            { jsonExcel: JSON.stringify(values) },
            (data) => {
                return data
            })
        return response
    },
    async postLoadExcelMultiple(values){
        const form = $('form').attr('id')
        let urlExcel = null
        if(form == 'formatoDatosMaestros'){
            urlExcel = 'include/cargaMasivaDatosMaestros.php'
        }
        if(form == 'formatoBajas'){
            urlExcel = 'include/cargaMasivaBajas.php'
        }

        let response = await $.ajax({
            url: urlExcel,
            type: 'POST',
            data: values,
            cache: false,
            contentType: false,
            processData: false,
            success: (data) => {
                return data
            }
        })

        return response
    }
}

export default queries