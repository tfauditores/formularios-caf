<?php

if ("GET" == $_SERVER["REQUEST_METHOD"]) {
	if (isset($_SERVER["HTTP_ORIGIN"])) {
		$http_origin = $_SERVER['HTTP_ORIGIN'];
		$allowed_domains = array(
			'https://ecopetrol.sharepoint.com'
		);
		if (in_array($http_origin, $allowed_domains))
		{  
			header("Access-Control-Allow-Origin: $http_origin");
		}
	}
}

?>