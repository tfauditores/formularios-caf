<?php
    /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('America/Bogota');
    /** Include PHPExcel */
        require_once 'Classes/PHPExcel.php';
    /** Crea un nuevo objeto PHPExcel */
        $objPHPExcel = new PHPExcel();
    /** Establecer propiedades del documento a generar*/
        $objPHPExcel->getProperties()
                    ->setCompany("ECOPETROL")
                    ->setCreator("ECOPETROL - CAF")
                    ->setLastModifiedBy("ECOPETROL - CAF")
                    ->setTitle("ACTUALIZACIÓN DE DATOS MAESTROS")
                    ->setSubject("ACTUALIZACIÓN DE DATOS MAESTROS")
                    ->setDescription("FORMATO DE ACTUALIZACIÓN DE DATOS MAESTROS")
                    ->setKeywords("GSC, CAF, DATOS MAESTROS, GFI-F-104")
                    ->setCategory("SERVICIOS");
    /** Formatos de celdas */
        $properties = array(
            'fontVerdana' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontVerdanaBold' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontCalibri' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontCalibriBold' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontSize11' => array(
                'font' => array(
                    'size' => 11
                )
            ),
            'fontSize12' => array(
                'font' => array(
                    'size' => 12
                )
            ),
            'fontSize14' => array(
                'font' => array(
                    'size' => 14
                )
            ),
            'fontColorWhite' => array(
                'font' => array(
                    'color' => array('rgb' => 'ffffff')
                )
            ),
            'fontColorRed' => array(
                'font' => array(
                    'color' => array('rgb' => 'ff0000')
                )
            ),
            'fontColorGreen' => array(
                'font' => array(
                    'color' => array('rgb' => '004237')
                )
            ),
            'greenLink' => array(
                'font' => array(
                    'color' => array('rgb' => '008800'),
                    'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'eeffee')
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'fondoVerde' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C9D200')
                )
            ),
            'fondoVerdeOscuro' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '004238')
                )
            ),
            'fondoGris' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E6E6E6')
                )
            ),
            'fondoGrisMedio' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c3c4c6')
                )
            ),
            'fondoGrisClaro' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'EEECE1')
                )
            ),
            'alignRight' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignLeft' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenter' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterHorizontal' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterVertical' => array(
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'cellBorderThin' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellBorderMedium' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderDouble' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderThin' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderMedium' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinTop' => array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderColorWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellBorderColorGreen' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '9bbb59')
                    )
                )
            ),
            'cellBorderColorGray' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'bfbfbf')
                    )
                )
            ),
            'cellBorderColorBlack' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
    /** inicializar contador */
        $i = 0;
    /** Print Header */
        function print_headerFormato($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->mergeCells('B2:C4'); // logo
                $objPHPExcel->getSheet($sheet)->mergeCells('D2:L2'); // fila 1
                $objPHPExcel->getSheet($sheet)->mergeCells('M2:AI2'); // fila 1
                $objPHPExcel->getSheet($sheet)->mergeCells('D3:L3'); // fila 2
                $objPHPExcel->getSheet($sheet)->mergeCells('M3:AI3'); // fila 2
                $objPHPExcel->getSheet($sheet)->mergeCells('E4:J4'); // fila 3
                $objPHPExcel->getSheet($sheet)->mergeCells('K4:L4'); // fila 3
                $objPHPExcel->getSheet($sheet)->mergeCells('M4:AI4'); // fila 3
                $objPHPExcel->getSheet($sheet)->mergeCells('B5:C5'); // fila 4
                $objPHPExcel->getSheet($sheet)->mergeCells('E5:G5'); // fila 4
                $objPHPExcel->getSheet($sheet)->mergeCells('H5:L5'); // fila 4
                $objPHPExcel->getSheet($sheet)->mergeCells('M5:AI5'); // fila 4
                $objPHPExcel->getSheet($sheet)->mergeCells('B6:H6'); // fila 5
                $objPHPExcel->getSheet($sheet)->mergeCells('I6:R6'); // fila 5
                $objPHPExcel->getSheet($sheet)->mergeCells('S6:AC6'); // fila 5
                $objPHPExcel->getSheet($sheet)->mergeCells('AD6:AH6'); // fila 5
            /** Asignación de formatos a celdas y rengos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B2:AI6")
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B2:AI6")
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['fontVerdanaBold']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B5")
                    ->applyFromArray($properties['greenLink']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("E5")
                    ->applyFromArray($properties['alignLeft']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B6:AI6")
                    ->applyFromArray($properties['fontColorGreen'])
                    ->applyFromArray($properties['fondoGris']);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimensionByColumn('A')->setWidth('1');
            /** Dimensiones de filas*/
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')->setRowHeight(5);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')->setRowHeight(16);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')->setRowHeight(30);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('4')->setRowHeight(16);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('5')->setRowHeight(16);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('D2', "FORMATO DE ACTUALIZACIÓN DE DATOS MAESTROS DE ACTIVOS FIJOS")
                    ->setCellValue('D3', "GESTIÓN FINANCIERA\n GSC - DEPARTAMENTO DE INFRAESTRUCTURA Y TIERRAS - GRUPO CONTROL ACTIVOS FIJOS")
                    ->setCellValue('D4', "GFI-F-104")
                    ->setCellValue('E4', "Elaborado: 2018/06/30")
                    ->setCellValue('K4', "Versión: 1")
                    ->setCellValue('B5','Ver tablas de insumos')
                    ->setCellValue('D5', "Formato generado el:");
                $url = 'https://ecopetrol.sharepoint.com/sites/CAF/Documetos%20Auxiliar/Forms/AllItems.aspx?RootFolder=%2Fsites%2FCAF%2FDocumetos%20Auxiliar%2FTablas%20de%20par%C3%A1metros%20para%20solicitudes&FolderCTID=0x0120006997594AF377B149B84014536F20EA2E&View=%7BB7BD0F33-0633-431E-ADC3-070004CF6A8C%7D' ;
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setUrl($url);
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setTooltip('Ver en SharePoint');
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
            
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(100);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(300);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(45);
                $objDrawing->setOffsetY(5);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
        function print_headerAyuda($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->mergeCells('B2:B4'); // logo
                $objPHPExcel->getSheet($sheet)->mergeCells('C2:E2'); // fila 1
                $objPHPExcel->getSheet($sheet)->mergeCells('C3:E3'); // fila 2
                $objPHPExcel->getSheet($sheet)->mergeCells('B6:E6'); // fila 5 *
                $objPHPExcel->getSheet($sheet)->mergeCells('B7:E7'); // fila 6 **
                /** Asignación de formatos a celdas y rangos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['fontVerdana'])
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['cellBorderColorWhite']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B2:E5")
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['fontVerdanaBold'])
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B5")
                    ->applyFromArray($properties['greenLink']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("D5")
                    ->applyFromArray($properties['alignLeft']);    
                /** Dimensiones de filas y columnas*/
                $objPHPExcel->getSheet($sheet)->getRowDimension('1')->setRowHeight(5);
                $objPHPExcel->getSheet($sheet)->getRowDimension('2')->setRowHeight(15);
                $objPHPExcel->getSheet($sheet)->getRowDimension('3')->setRowHeight(30);
                $objPHPExcel->getSheet($sheet)->getRowDimension('4')->setRowHeight(15);
                $objPHPExcel->getSheet($sheet)->getRowDimension('5')->setRowHeight(15);
                $objPHPExcel->getSheet($sheet)->getRowDimension('6')->setRowHeight(15);
                $objPHPExcel->getSheet($sheet)->getRowDimension('7')->setRowHeight(15);
                
                $objPHPExcel->getSheet($sheet)->getColumnDimension('A')->setWidth(1);
                $objPHPExcel->getSheet($sheet)->getColumnDimension('B')->setWidth(40);
                $objPHPExcel->getSheet($sheet)->getColumnDimension('C')->setWidth(45);
                $objPHPExcel->getSheet($sheet)->getColumnDimension('D')->setWidth(120);
                $objPHPExcel->getSheet($sheet)->getColumnDimension('E')->setWidth(45);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('C2', "FORMATO DE ACTUALIZACIÓN DE DATOS MAESTROS DE ACTIVOS FIJOS")
                    ->setCellValue('C3', "GESTIÓN FINANCIERA\n GSC - DEPARTAMENTO DE INFRAESTRUCTURA Y TIERRAS - GRUPO CONTROL ACTIVOS FIJOS")
                    ->setCellValue('C4', "GFI-F-104")
                    ->setCellValue('D4', "Elaborado: 2018/06/30")
                    ->setCellValue('E4', "Versión: 1")
                    ->setCellValue('B5', "Ver tablas de insumos")
                    ->setCellValue('C5', "Formato generado el:");    
                $url = 'https://ecopetrol.sharepoint.com/sites/CAF/Documetos%20Auxiliar/Forms/AllItems.aspx?RootFolder=%2Fsites%2FCAF%2FDocumetos%20Auxiliar%2FTablas%20de%20par%C3%A1metros%20para%20solicitudes&FolderCTID=0x0120006997594AF377B149B84014536F20EA2E&View=%7BB7BD0F33-0633-431E-ADC3-070004CF6A8C%7D' ;
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setUrl($url);
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setTooltip('Ver en SharePoint');
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(100);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(300);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(15);
                $objDrawing->setOffsetY(5);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
    /** CREAR PAGINAS (sin datos) */
        while($i < 2){
            /** Crear pagina*/
                $objPHPExcel->createSheet($i);
            switch ($i){
                case 0:
                    /** ENCABEZADO PRINCIPAL */
                        print_headerFormato($objPHPExcel, $i, $properties);
                    /** ASIGNAR FORMATOS A CELDAS Y RANGOS */
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B7:AI7")
                            ->applyFromArray($properties['fontVerdanaBold'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B7:AI7")
                            ->applyFromArray($properties['cellBorderColorGreen']);    
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B7:H7")
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("I7:AC7")
                            ->applyFromArray($properties['fondoVerde'])
                            ->applyFromArray($properties['fontColorGreen']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("AD7:AF7")
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("AG7:AH7")
                            ->applyFromArray($properties['fondoVerde'])
                            ->applyFromArray($properties['fontColorGreen']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("AI7")
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite']);

                    /** AGREGAR ENCABEZADOS DE TABLA */
                        $datenow = date('d/m/Y');
                        $columnas = array(
                            "fecha"=>'E5',
                            "grupoIdentificacion"   =>'B6',
                            "grupoDatosTecnicos"    =>'I6',
                            "grupoTransferencias"   =>'S6',
                            "grupoVidasUtiles"      =>'AD6',
                            "grupoObservaciones"    =>'AI6',
                            "codigoActivo"          =>'B7',
                            "subnumero"             =>'C7',
                            "denominacion"          =>'D7',
                            "inventario"            =>'E7',
                            "serie"                 =>'F7',
                            "tag"                   =>'G7',
                            "codigoClase"           =>'H7',
                            "nuevaDenominacion"     =>'I7',
                            "nuevaDenominacion2"    =>'J7',
                            "nuevaSerie"            =>'K7',
                            "nuevoInventario"       =>'L7',
                            "nuevaCapacidad"        =>'M7',
                            "nuevaUnidad"           =>'N7',
                            "nuevoTag"              =>'O7',
                            "nuevaMatricula"        =>'P7',
                            "nuevoFabricante"       =>'Q7',
                            "nuevoModelo"           =>'R7',
                            "nuevoCeco"             =>'S7',
                            "nuevaVicepresidencia"  =>'T7',
                            "nuevaGerencia"         =>'U7',
                            "nuevoCustodio"         =>'V7',
                            "nuevoCodUbicacion"     =>'W7',
                            "nuevaUbicacion"        =>'X7',
                            "nuevoCodCampo"         =>'Y7',
                            "nuevoCampo"            =>'Z7',
                            "nuevoTipoOperacion"    =>'AA7',
                            "nuevoCodMunicipio"     =>'AB7',
                            "nuevoMunicipio"        =>'AC7',
                            "ceco"                  =>'AD7',
                            "vicepresidencia"       =>'AE7',
                            "gerencia"              =>'AF7',
                            "fechaInicialVUR"       =>'AG7',
                            "nuevaVUR"              =>'AH7',
                            "observaciones"         =>'AI7'
                        );
                        $objPHPExcel->setActiveSheetIndex($i)
                            ->setCellValue($columnas['fecha'], $datenow)
                            ->setCellValue($columnas['grupoIdentificacion'], "IDENTIFICACIÓN")
                            ->setCellValue($columnas['grupoDatosTecnicos'], "DATOS TÉCNICOS")
                            ->setCellValue($columnas['grupoTransferencias'], "TRANSFERENCIAS")
                            ->setCellValue($columnas['grupoVidasUtiles'], "ACTUALIZACIÓN VIDAS ÚTILES")
                            ->setCellValue($columnas['grupoObservaciones'], "OBSERVACIONES")
                            ->setCellValue($columnas['codigoActivo'], "CÓDIGO ACTIVO FIJO *")
                            ->setCellValue($columnas['subnumero'], "SUBNÚMERO *")
                            ->setCellValue($columnas['denominacion'], "DENOMINACIÓN ACTIVO FIJO *")
                            ->setCellValue($columnas['inventario'], "No. INVENTARIO *")
                            ->setCellValue($columnas['serie'], "SERIE *")
                            ->setCellValue($columnas['tag'], "TAG")
                            ->setCellValue($columnas['codigoClase'], "COD. CLASE *")
                            ->setCellValue($columnas['nuevaDenominacion'], "NUEVA DENOMINACIÓN ACTIVO FIJO")
                            ->setCellValue($columnas['nuevaDenominacion2'], "NUEVA DENOMINACIÓN COMPLEMENTARIA")
                            ->setCellValue($columnas['nuevaSerie'], "NUEVA SERIE")
                            ->setCellValue($columnas['nuevoInventario'], "NUEVO No. INVENTARIO")
                            ->setCellValue($columnas['nuevaCapacidad'], "NUEVA CAPACIDAD")
                            ->setCellValue($columnas['nuevaUnidad'], "NUEVA UNIDAD DE MEDIDA")
                            ->setCellValue($columnas['nuevoTag'], "NUEVO TAG")
                            ->setCellValue($columnas['nuevaMatricula'], "NUEVA MATRICULA")
                            ->setCellValue($columnas['nuevoFabricante'], "NUEVO FABRICANTE")
                            ->setCellValue($columnas['nuevoModelo'], "NUEVO MODELO")
                            ->setCellValue($columnas['nuevoCeco'], "NUEVO COD. CECO")
                            ->setCellValue($columnas['nuevaVicepresidencia'], "NUEVA VICEPRESIDENCIA")
                            ->setCellValue($columnas['nuevaGerencia'], "NUEVA GERENCIA")
                            ->setCellValue($columnas['nuevoCustodio'], "NUEVO REGISTRO CUSTODIO")
                            ->setCellValue($columnas['nuevoCodUbicacion'], "NUEVO CÓDIGO \nUBICACIÓN GEOGRÁFICA")
                            ->setCellValue($columnas['nuevaUbicacion'], "NOMBRE NUEVA \n UBICACIÓN GEOGRÁFICA")
                            ->setCellValue($columnas['nuevoCodCampo'], "NUEVO COD. CAMPO PLANTA")
                            ->setCellValue($columnas['nuevoCampo'], "NOMBRE NUEVO CAMPO PLANTA")
                            ->setCellValue($columnas['nuevoTipoOperacion'], "NUEVO TIPO DE OPERACIÓN")
                            ->setCellValue($columnas['nuevoCodMunicipio'], "NUEVO COD. MUNICIPIO")
                            ->setCellValue($columnas['nuevoMunicipio'], "NOMBRE NUEVO MUNICIPIO")
                            ->setCellValue($columnas['ceco'], "CECO **")
                            ->setCellValue($columnas['vicepresidencia'], "VICEPRESIDENCIA")
                            ->setCellValue($columnas['gerencia'], "GERENCIA")
                            ->setCellValue($columnas['fechaInicialVUR'], "FECHA INICIAL PARA VIDA ÚTIL REMANENTE **")
                            ->setCellValue($columnas['nuevaVUR'], "NUEVA VIDA ÚTIL REMANENTE (meses)")
                            ->setCellValue($columnas['observaciones'], "OBSERVACIONES");
                        
                    /** COMENTARIOS ENCABEZADOS */
                        /** Comentario para CODIGO ACTIVO FIJO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun("Diligenciar el ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun("código ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun("de identificación del activo según el auxiliar de activos fijos ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun("SAP");
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setWidth('220pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para SUBNUMERO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getText()->createTextRun("Diligenciar el dígito del ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getText()->createTextRun("subnúmero ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getText()->createTextRun("a actualizar información");
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para DENOMINACION */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun("Diligenciar ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun("denominación ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun("del activo según el auxiliar de activos fijos ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun("SAP");
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para INVENTARIO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('Diligenciar el');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('número de inventario ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setWidth('220pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para SERIE */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('serie ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para TAG */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('TAG ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getFillColor()->setRGB('EEEEEE');  
                        /** Comentario para COD CLASE */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->getText()->createTextRun('Diligenciar el código de la');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->getText()->createTextRun('CLASE ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoClase'])->getFillColor()->setRGB('EEEEEE'); 
                        /** Comentario para NUEVA DENOMINACION */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("- Aplica cuando ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("NO ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("implique cambio de clase \n- Diligenciar la ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("nueva denominación ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("del activo, según ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("Catálogo ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("si es de operacion ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getText()->createTextRun("directa ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA DENOMINACION COMPLEMENTARIA */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getText()->createTextRun("- Diligenciar la ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getText()->createTextRun("nueva ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getText()->createTextRun("descripción adicional del activo \n- Para ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getText()->createTextRun("terrenos o servidumbres ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getText()->createTextRun("se debe indicar la nueva ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getText()->createTextRun("cédula catastral ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaDenominacion2'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA SERIE */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->getText()->createTextRun('nueva serie ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->getText()->createTextRun('con la cual estará identificado el activo');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaSerie'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO NO. INVENTARIO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('nuevo número de inventario');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun("\n- Para ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('terrenos o servidumbres ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('se debe indicar el nuevo ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('FMI ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun("\n- Para el caso de activos ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('operados por terceros ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('se debe diligenciar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getText()->createTextRun('"NO REGISTRA" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->setWidth('300pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->setHeight('60pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoInventario'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA CAPACIDAD  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->getText()->createTextRun('nueva capacidad ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->getText()->createTextRun('del activo');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaCapacidad'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA UNIDAD DE MEDIDA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getText()->createTextRun('nueva unidad de medida ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getText()->createTextRun('del activo, según ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getText()->createTextRun('Catálogo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getText()->createTextRun('si es operación ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getText()->createTextRun('directa ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUnidad'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO TAG  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('nuevo TAG');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun("\n- Para ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('terrenos y servidumbres ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('indicar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('nuevo código SIG ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun("\n- Para el caso de activos ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('operados por terceros ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('se debe dilienciar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getText()->createTextRun('"NO REGISTRA" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTag'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA MATRICULA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->getText()->createTextRun('nueva matrícula del vehículo');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaMatricula'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO FABRICANTE  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('nuevo fabricante ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun("o marca \n- Para ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('terrenos o servidumbres ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('se debe indicar el nuevo número de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('Escritura Pública ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('y el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getText()->createTextRun('año ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoFabricante'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO MODELO  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('nuevo modelo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun("del activo fijo \n- Para ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('terrenos o servidumbres ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('se debe indicar el nuevo nombre de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('Notaría ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('más la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('ciudad ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getText()->createTextRun('de registro');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->setHeight('60pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoModelo'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO COD. CECO  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->getText()->createTextRun('nuevo centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->getText()->createTextRun("que será asignado al activo \n- Debe ser ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->getText()->createTextRun('congruente ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->getText()->createTextRun('con la ubicación geográfica, el campo planta y el municipio');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->setHeight('60pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCeco'])->getFillColor()->setRGB('EEEEEE');     

                        /** Comentario para NUEVA VICEPRESIDENCIA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('Se puede dejar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('vacía ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('o diligenciar la sigla de la  ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('Vicepresidencia ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('según el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getText()->createTextRun('del activo ');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVicepresidencia'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA GERENCIA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('Se puede dejar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('vacía ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('o diligenciar la sigla de la  ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('Gerencia ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('según el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getText()->createTextRun('del activo ');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaGerencia'])->getFillColor()->setRGB('EEEEEE');     


                        /** Comentario para NUEVO CUSTODIO  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->getText()->createTextRun('Diligenciar el código de');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->getText()->createTextRun('registro');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->getText()->createTextRun('del ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->getText()->createTextRun('nuevo custodio ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->getText()->createTextRun('del activo ');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCustodio'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO COD UBICACIÓN GEOGRÁFICA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->getText()->createTextRun('- Diligenciar el código de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->getText()->createTextRun('nueva ubicación geográfica ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->getText()->createTextRun("\n- Debe ser ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->getText()->createTextRun('congruente ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->getText()->createTextRun('con el CEO, el campo planta y el municipio ');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->setWidth('300pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodUbicacion'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO NOMBRE UBICACIÓN GEOGRÁFICA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getText()->createTextRun('nombre ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getText()->createTextRun('de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getText()->createTextRun('nueva ubicación geográfica ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getText()->createTextRun('según la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getText()->createTextRun('"Tabla_Ubicaciones_geográficas" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->setWidth('180pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaUbicacion'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO COD CAMPO PLANTA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->getText()->createTextRun('- Diligenciar el código del nuevo ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->getText()->createTextRun('campo planta ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->getText()->createTextRun("\n- Debe ser ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->getText()->createTextRun('congruente ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->getText()->createTextRun('con el CEO, el municipio y la ubicación geográfica');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodCampo'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO NOMBRE CAMPO PLANTA  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getText()->createTextRun('nombre ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getText()->createTextRun('del');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getText()->createTextRun('nuevo campo planta ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getText()->createTextRun('según la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getText()->createTextRun('"Tabla_Campo_Planta" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCampo'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO TIPO DE OPERACIÓN  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->getText()->createTextRun('Diligenciar según sea el caso: ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->getText()->createTextRun("\n- DIRECTA \n- ASOCIADA OPERADOR \n- ASOCIADA NO OPERADOR \n- EXTENSIÓN DE COMERCIALIDAD ");
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->setWidth('180pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->setHeight('75pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoTipoOperacion'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO COD MUNICIPIO  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun('- Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun('código ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun('del ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun('nuevo municipio ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun("\n- Debe ser ");
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun('congruente ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getText()->createTextRun('con el CECO, la ubicación geográfica y el campo planta');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->setWidth('240pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoCodMunicipio'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVO NOMBRE MUNICIPIO  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getText()->createTextRun('nombre ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getText()->createTextRun('del ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getText()->createTextRun('nuevo municipio ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getText()->createTextRun('según la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getText()->createTextRun('"Tabla_Municipios" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevoMunicipio'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para CECO _VU_  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('asignado al activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('SAP ');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setWidth('250pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setHeight('30pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para VICEPRESIDENCIA _VU_  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('Diligenciar la sigla de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('Vicepresidencia ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('según el  ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('del activo, de acuerdo con la ');                        
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('"Tabla_Jerarquizacion" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setWidth('240pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setHeight('30pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para GERENCIA _VU_  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('Diligenciar la sigla de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('Gerencia o Departamento ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('según el  ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('del activo, de acuerdo con la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('"Tabla_Jerarquizacion" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setHeight('60pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para FECHA INICIAL PARA VUR _VU_  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getText()->createTextRun('fecha ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getText()->createTextRun('desde la cual ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getText()->createTextRun('aplica el cambio ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getText()->createTextRun('de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getText()->createTextRun('vida útil remanente ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['fechaInicialVUR'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para NUEVA VUR (meses) _VU_  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->getText()->createTextRun('nueva vida útil remanente ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->getText()->createTextRun('= (Vida útil técnica - Edad del activo)');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['nuevaVUR'])->getFillColor()->setRGB('EEEEEE');     
                        /** Comentario para OBSERVACIONES  */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getText()->createTextRun('Diligenciar los ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getText()->createTextRun('comentarios relevantes o aclaraciones ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getText()->createTextRun('sobre los activos ');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getFillColor()->setRGB('EEEEEE');     

                    /** HABILITAR FILTROS PARA LAS COLUMNAS DE CONTENIDO */
                        $objPHPExcel->getSheet($i)->setAutoFilter('B7:AI7');
                    /** ANCHO DE LAS COLUMNAS */    
                        // Se desactiva el autosize pues no es preciso cuando el formato de la fuente ha cambiado
                            // foreach(range('B','AE') as $columnID) {
                            //     $objPHPExcel->getSheet($i)->getColumnDimension($columnID)
                            //         ->setAutoSize(true);
                            // };
                        // Tamaño de columnas manual
                            $objPHPExcel->getSheet($i)->getColumnDimension('A')->setWidth(1);
                            $objPHPExcel->getSheet($i)->getColumnDimension('B')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('C')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('D')->setWidth(24);
                            $objPHPExcel->getSheet($i)->getColumnDimension('E')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('F')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('G')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('H')->setWidth(16);

                            $objPHPExcel->getSheet($i)->getColumnDimension('I')->setWidth(30);
                            $objPHPExcel->getSheet($i)->getColumnDimension('J')->setWidth(30);
                            $objPHPExcel->getSheet($i)->getColumnDimension('K')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('L')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('M')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('N')->setWidth(19);
                            $objPHPExcel->getSheet($i)->getColumnDimension('O')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('P')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('Q')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('R')->setWidth(16);

                            $objPHPExcel->getSheet($i)->getColumnDimension('S')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('T')->setWidth(20);
                            $objPHPExcel->getSheet($i)->getColumnDimension('U')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('V')->setWidth(20);
                            $objPHPExcel->getSheet($i)->getColumnDimension('W')->setWidth(27);
                            $objPHPExcel->getSheet($i)->getColumnDimension('X')->setWidth(27);
                            $objPHPExcel->getSheet($i)->getColumnDimension('Y')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('Z')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AA')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AB')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AC')->setWidth(19);

                            $objPHPExcel->getSheet($i)->getColumnDimension('AD')->setWidth(14);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AE')->setWidth(25);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AF')->setWidth(16);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AG')->setWidth(28);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AH')->setWidth(28);
                            $objPHPExcel->getSheet($i)->getColumnDimension('AI')->setWidth(22);
                    
                break;
                case 1:
                    print_headerAyuda($objPHPExcel, $i, $properties);
                    /** Combinar celdas */
                        $objPHPExcel->getSheet($i)->mergeCells('B8:E8');
                        $objPHPExcel->getSheet($i)->mergeCells('B17:E17');
                        $objPHPExcel->getSheet($i)->mergeCells('B28:E28');
                        $objPHPExcel->getSheet($i)->mergeCells('B40:E40');
                    /** Asignación de formatos a celdas y rengos */
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B6:E46")
                            ->applyFromArray($properties['fontCalibri'])
                            ->applyFromArray($properties['fontSize11'])
                            ->getAlignment()
                            ->setWrapText(true);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoGrisClaro'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B9:E9")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B17")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoGris'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B28")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoGris'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B40")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoGris'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8:E46")
                            ->applyFromArray($properties['cellBorderThin'])
                            ->applyFromArray($properties['cellBorderColorBlack']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8:E46")
                            ->applyFromArray($properties['cellOutlineBorderMedium']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("E9:E46")
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B9:B46")
                            ->applyFromArray($properties['alignCenter']);    

                    /** Agregar valores de plantilla y encabezados de tabla */
                        function HelpTitles($text1,$text2,$order=2){
                            switch($order){
                                case 1:
                                    $objRichText = new PHPExcel_RichText();
                                    $run1 = $objRichText->createTextRun($text1);
                                    $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                    $run2 = $objRichText->createTextRun($text2);
                                    $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                break;
                                case 2:
                                default:
                                    $objRichText = new PHPExcel_RichText();
                                    $run1 = $objRichText->createTextRun($text1);
                                    $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                    $run2 = $objRichText->createTextRun($text2);
                                    $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                break;
                            }
                            return $objRichText;
                        }
                    /** FILAS PRELIMINARES Y ENCABEZADOS */
                        $datenow = date('d/m/Y');
                        $objPHPExcel->setActiveSheetIndex($i)
                            ->setCellValue('D5', $datenow)
                            ->setCellValue('B6', HelpTitles('* ','Campos Obligatorios',1) )    
                            ->setCellValue('B7', HelpTitles('** ','Obligatorio en caso de solicitar ajuste en la vida útil del activo',1) )
                            ->setCellValue('B8', "INFORMACIÓN DEL FORMATO")
                            ->setCellValue('B9', "CAMPO")
                            ->setCellValue('C9', "DESCRIPCIÓN")
                            ->setCellValue('D9', "DILIGENCIAMIENTO")
                            ->setCellValue('E9', "PARA OPERACION DIRECTA VER \n\"TABLA CATÁLOGO DE ACTIVOS \n FIJOS -GFI-T-004\" ")
                            ->setCellValue('B17', "DILIGENCIE LOS DATOS MAESTROS A MODIFICAR ")
                            ->setCellValue('B28', "DILIGENCIE EN CASO DE TRANSFERENCIA ")
                            ->setCellValue('B40', "DILIGENCIE EN CASO DE ACTUALIZACIÓN DE VIDA ÚTIL TÉCNICA ");

                    /** FILAS PRINCIPALES DE AYUDA */
                        /** FILA CODIGO ACTIVO FIJO */
                            /** DESCRIPCION */
                                $objRichText_codigo_desc = new PHPExcel_RichText();
                                $objRichText_codigo_desc->createTextRun("Código de identificación del activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** DILIGENCIAMIENTO */
                                $objRichText_codigo_dilig = new PHPExcel_RichText();
                                $objRichText_codigo_dilig->createTextRun("En la columna ");
                                $objRichText_codigo_dilig->createTextRun("\"CÓDIGO ACTIVO FIJO\" ")->getFont()->setItalic(true);
                                $objRichText_codigo_dilig->createTextRun("diligenciar el código de identificación del activo según el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B10', HelpTitles('CÓDIGO ACTIVO FIJO ','*'))
                                    ->setCellValue('C10', $objRichText_codigo_desc)
                                    ->setCellValue('D10', $objRichText_codigo_dilig);
                        /** FILA SUBNUMERO */
                            /** DESCRIPCION */
                                $objRichText_subn_desc = new PHPExcel_RichText();
                                $objRichText_subn_desc->createTextRun("Dígitos adicionales al código activo fijo de SAP");
                            /** DILIGENCIAMIENTO */
                                $objRichText_subn_dilig = new PHPExcel_RichText();
                                $objRichText_subn_dilig->createTextRun("En la columna ");
                                $objRichText_subn_dilig->createTextRun("\"SUBNÚMERO\" ")->getFont()->setItalic(true);
                                $objRichText_subn_dilig->createTextRun("diligenciar según sea el caso: \n    - El activo principal es identificado con el dígito ");
                                $objRichText_subn_dilig->createTextRun("0 ")->getFont()->setBold(true);
                                $objRichText_subn_dilig->createTextRun("\n    - Las capitalizaciones como mayor valor, adiciones o mejoras al activo principal son identificadas con un dígito igual o mayor a ");
                                $objRichText_subn_dilig->createTextRun("1 ")->getFont()->setBold(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B11', HelpTitles('SUBNÚMERO ','*'))
                                    ->setCellValue('C11', $objRichText_subn_desc)
                                    ->setCellValue('D11', $objRichText_subn_dilig);
                        /** FILA DENOMINACION ACTIVO FIJO */
                            /** DESCRIPCION */
                                $objRichText_denom_desc = new PHPExcel_RichText();
                                $objRichText_denom_desc->createTextRun("Nombre o denominación del activo fijo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_denom_dilig = new PHPExcel_RichText();
                                $objRichText_denom_dilig->createTextRun("En la columna ");
                                $objRichText_denom_dilig->createTextRun("\"DENOMINACIÓN ACTIVO FIJO\" ")->getFont()->setItalic(true);
                                $objRichText_denom_dilig->createTextRun("diligenciar nombre o denominación con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B12', HelpTitles('DENOMINACIÓN ACTIVO FIJO ','*'))
                                    ->setCellValue('C12', $objRichText_denom_desc)
                                    ->setCellValue('D12', $objRichText_denom_dilig);
                        /** FILA NO. INVENTARIO */
                            /** DESCRIPCION */
                                $objRichText_inv_desc = new PHPExcel_RichText();
                                $objRichText_inv_desc->createTextRun("Consecutivo placa de inventario que permite la identificación física del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_inv_dilig = new PHPExcel_RichText();
                                $objRichText_inv_dilig->createTextRun("En la columna ");
                                $objRichText_inv_dilig->createTextRun("\"No. INVENTARIO\" ")->getFont()->setItalic(true);
                                $objRichText_inv_dilig->createTextRun("diligenciar el número de inventario con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B13', HelpTitles('No. INVENTARIO ','*'))
                                    ->setCellValue('C13', $objRichText_inv_desc)
                                    ->setCellValue('D13', $objRichText_inv_dilig);
                        /** FILA SERIE */
                            /** DESCRIPCION */
                                $objRichText_serie_desc = new PHPExcel_RichText();
                                $objRichText_serie_desc->createTextRun("Consecutivo único e irrepetible del fabricante que permite la identificación del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_serie_dilig = new PHPExcel_RichText();
                                $objRichText_serie_dilig->createTextRun("En la columna ");
                                $objRichText_serie_dilig->createTextRun("\"SERIE\" ")->getFont()->setItalic(true);
                                $objRichText_serie_dilig->createTextRun("diligenciar el número serie con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B14', HelpTitles('SERIE ','*'))
                                    ->setCellValue('C14', $objRichText_serie_desc)
                                    ->setCellValue('D14', $objRichText_serie_dilig);                              
                        /** FILA TAG */
                            /** DESCRIPCION */
                                $objRichText_tag_desc = new PHPExcel_RichText();
                                $objRichText_tag_desc->createTextRun("Código de identificación física asignada por la operación para efectos de mantenimiento");
                            /** DILIGENCIAMIENTO */
                                $objRichText_tag_dilig = new PHPExcel_RichText();
                                $objRichText_tag_dilig->createTextRun("En la columna ");
                                $objRichText_tag_dilig->createTextRun("\"TAG\" ")->getFont()->setItalic(true);
                                $objRichText_tag_dilig->createTextRun("diligenciar el TAG con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B15', "TAG")
                                    ->setCellValue('C15', $objRichText_tag_desc)
                                    ->setCellValue('D15', $objRichText_tag_dilig);   
                        /** FILA COD CLASE */
                            /** DESCRIPCION */
                                $objRichText_codClase_desc = new PHPExcel_RichText();
                                $objRichText_codClase_desc->createTextRun("Determina la asignación de las cuentas de balance y el grupo al cual pertenece el activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_codClase_dilig = new PHPExcel_RichText();
                                $objRichText_codClase_dilig->createTextRun("En la columna ");
                                $objRichText_codClase_dilig->createTextRun("\"COD. CLASE\" ")->getFont()->setItalic(true);
                                $objRichText_codClase_dilig->createTextRun("diligenciar el código de la clase asignada al activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B16', HelpTitles('COD. CLASE', '*'))
                                    ->setCellValue('C16', $objRichText_codClase_desc)
                                    ->setCellValue('D16', $objRichText_codClase_dilig);   
                        /** FILA NUEVA DENOMINACIÓN */
                            /** DESCRIPCION */
                                $objRichText_newDenom_desc = new PHPExcel_RichText();
                                $objRichText_newDenom_desc->createTextRun("Nombre o denominación del activo fijo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newDenom_dilig = new PHPExcel_RichText();
                                $objRichText_newDenom_dilig->createTextRun("Actualización denominación únicamente cuando NO implique cambio de clase:")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newDenom_dilig->createTextRun("\n- En la columna ");
                                $objRichText_newDenom_dilig->createTextRun("\"NUEVA DENOMINACIÓN ACTIVO FIJO\" ")->getFont()->setItalic(true);
                                $objRichText_newDenom_dilig->createTextRun("diligenciar el ");
                                $objRichText_newDenom_dilig->createTextRun("nuevo ")->getFont()->setBold(true);
                                $objRichText_newDenom_dilig->createTextRun("nombre o denominación con el cual estará identificado el activo \n- Para el caso de ");
                                $objRichText_newDenom_dilig->createTextRun("subnúmeros, ")->getFont()->setItalic(true);
                                $objRichText_newDenom_dilig->createTextRun("se debe ingressar el ");
                                $objRichText_newDenom_dilig->createTextRun("nuevo ")->getFont()->setBold(true);
                                $objRichText_newDenom_dilig->createTextRun("concepto del bien (adición, mejoa, mayor valor) más la descripción del mismo");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B18', "NUEVA DENOMINACIÓN ACTIVO FIJO")
                                    ->setCellValue('C18', $objRichText_newDenom_desc)
                                    ->setCellValue('D18', $objRichText_newDenom_dilig)   
                                    ->setCellValue('E18', "APLICA");
                        /** FILA NUEVA DENOMINACIÓN COMPLEMENTARIA */
                            /** DESCRIPCION */
                                $objRichText_newDenom2_desc = new PHPExcel_RichText();
                                $objRichText_newDenom2_desc->createTextRun("Nombre o denominación complementaria del activo fijo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newDenom2_dilig = new PHPExcel_RichText();
                                $objRichText_newDenom2_dilig->createTextRun("- En la columna ");
                                $objRichText_newDenom2_dilig->createTextRun("\"NUEVA DENOMINACIÓN ACTIVO FIJO\" ")->getFont()->setItalic(true);
                                $objRichText_newDenom2_dilig->createTextRun("diligenciar la ");
                                $objRichText_newDenom2_dilig->createTextRun("nueva ")->getFont()->setBold(true)->setItalic(true);
                                $objRichText_newDenom2_dilig->createTextRun("descripción adicional que permite complementar la denominación del activo \n- Para el caso de ");
                                $objRichText_newDenom2_dilig->createTextRun("servidumbres ")->getFont()->setItalic(true);
                                $objRichText_newDenom2_dilig->createTextRun("se debe indicar la ");
                                $objRichText_newDenom2_dilig->createTextRun("nueva ")->getFont()->setBold(true)->setItalic(true);
                                $objRichText_newDenom2_dilig->createTextRun("cédula catastral ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B19', "NUEVA DENOMINACIÓN COMPLEMENTARIA")
                                    ->setCellValue('C19', $objRichText_newDenom2_desc)
                                    ->setCellValue('D19', $objRichText_newDenom2_dilig)   
                                    ->setCellValue('E19', "APLICA");
                        /** FILA NUEVA SERIE */
                            /** DESCRIPCION */
                                $objRichText_newSerie_desc = new PHPExcel_RichText();
                                $objRichText_newSerie_desc->createTextRun("Consecutivo único e irrepetible del fabricante que permite la identificación del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newSerie_dilig = new PHPExcel_RichText();
                                $objRichText_newSerie_dilig->createTextRun("- En la columna ");
                                $objRichText_newSerie_dilig->createTextRun("\"SERIE\" ")->getFont()->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("diligenciar la ");
                                $objRichText_newSerie_dilig->createTextRun("nueva ")->getFont()->setBold(true)->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("serie con la cual estará identificado el activo");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B20', "NUEVA SERIE")
                                    ->setCellValue('C20', $objRichText_newSerie_desc)
                                    ->setCellValue('D20', $objRichText_newSerie_dilig);
                        /** FILA NUEVO No. INVENTARIO */
                            /** DESCRIPCION */
                                $objRichText_newSerie_desc = new PHPExcel_RichText();
                                $objRichText_newSerie_desc->createTextRun("Consecutivo de placa de inventario que permite la identificación física del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newSerie_dilig = new PHPExcel_RichText();
                                $objRichText_newSerie_dilig->createTextRun("- En la columna ");
                                $objRichText_newSerie_dilig->createTextRun("\"No. Inventario\" ")->getFont()->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("diligenciar el ");
                                $objRichText_newSerie_dilig->createTextRun("nuevo ")->getFont()->setBold(true)->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("número de inventario con el cual estará idenfiticado el activo \n- Para el caso de ");
                                $objRichText_newSerie_dilig->createTextRun("terrenos o servidumbres ")->getFont()->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("se debe indicar el ");
                                $objRichText_newSerie_dilig->createTextRun("nuevo ")->getFont()->setBold(true)->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("Folio de Matrícula inmobiliaria \n- Para el caso de activos ");
                                $objRichText_newSerie_dilig->createTextRun("operados por terceros ")->getFont()->setItalic(true);
                                $objRichText_newSerie_dilig->createTextRun("se debe diligenciar \"NO REGISTRA\"");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B21', "NUEVO No. INVENTARIO")
                                    ->setCellValue('C21', $objRichText_newSerie_desc)
                                    ->setCellValue('D21', $objRichText_newSerie_dilig);
                        /** FILA NUEVA CAPACIDAD */
                            /** DESCRIPCION */
                                $objRichText_newCapacidad_desc = new PHPExcel_RichText();
                                $objRichText_newCapacidad_desc->createTextRun("Dimensión del activo del activo fijo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCapacidad_dilig = new PHPExcel_RichText();
                                $objRichText_newCapacidad_dilig->createTextRun("En la columna ");
                                $objRichText_newCapacidad_dilig->createTextRun("\"NUEVA CAPACIDAD\" ")->getFont()->setItalic(true);
                                $objRichText_newCapacidad_dilig->createTextRun("diligenciar la nueva dimensión o capacidad del activo");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B22', "NUEVA CAPACIDAD")
                                    ->setCellValue('C22', $objRichText_newCapacidad_desc)
                                    ->setCellValue('D22', $objRichText_newCapacidad_dilig)
                                    ->setCellValue('E22', "APLICA");
                        /** FILA NUEVA UNIDAD DE MEDIDA */
                            /** DESCRIPCION */
                                $objRichText_newUnidad_desc = new PHPExcel_RichText();
                                $objRichText_newUnidad_desc->createTextRun("Magnitud física, definida y adoptada por convención");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newUnidad_dilig = new PHPExcel_RichText();
                                $objRichText_newUnidad_dilig->createTextRun("- En la columna ");
                                $objRichText_newUnidad_dilig->createTextRun("\"NUEVA UNIDAD DE MEDIDA\" ")->getFont()->setItalic(true);
                                $objRichText_newUnidad_dilig->createTextRun("diligenciar la ");
                                $objRichText_newUnidad_dilig->createTextRun("nueva ")->getFont()->setBold(true);
                                $objRichText_newUnidad_dilig->createTextRun("magnitud física, definida y adoptada por convención \n- Para  ");
                                $objRichText_newUnidad_dilig->createTextRun("operación asociada ")->getFont()->setItalic(true);
                                $objRichText_newUnidad_dilig->createTextRun("se ");
                                $objRichText_newUnidad_dilig->createTextRun("sugiere ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newUnidad_dilig->createTextRun("tomar como ");
                                $objRichText_newUnidad_dilig->createTextRun("guía ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newUnidad_dilig->createTextRun("la ");
                                $objRichText_newUnidad_dilig->createTextRun("\"Tabla Catálogo de Activos Fijos-GFI-T-004\"")->getFont()->setItalic(true);
                                $objRichText_newUnidad_dilig->createTextRun(", en caso de ser un activo no catalogado se deben indicar las unidades de medida que acepta SAP de acuerdo con ");
                                $objRichText_newUnidad_dilig->createTextRun("\"Tabla_Unidades_Medida\" ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B23', "NUEVA UNIDAD DE MEDIDA")
                                    ->setCellValue('C23', $objRichText_newUnidad_desc)
                                    ->setCellValue('D23', $objRichText_newUnidad_dilig)
                                    ->setCellValue('E23', "APLICA");
                        /** FILA NUEVO TAG */
                            /** DESCRIPCION */
                                $objRichText_newTag_desc = new PHPExcel_RichText();
                                $objRichText_newTag_desc->createTextRun("Código de identificación física asignada por la operación para efectos de mantenimiento");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newTag_dilig = new PHPExcel_RichText();
                                $objRichText_newTag_dilig->createTextRun("- En la columna ");
                                $objRichText_newTag_dilig->createTextRun("\"NUEVO TAG\" ")->getFont()->setItalic(true);
                                $objRichText_newTag_dilig->createTextRun("diligenciar el ");
                                $objRichText_newTag_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newTag_dilig->createTextRun("código asignado por el área de mantenimiento \n- Para el caso de ");
                                $objRichText_newTag_dilig->createTextRun("terrenos y servidumbres ")->getFont()->setItalic(true);
                                $objRichText_newTag_dilig->createTextRun("indicar el ");
                                $objRichText_newTag_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newTag_dilig->createTextRun("codigo  ");
                                $objRichText_newTag_dilig->createTextRun("SIG ")->getFont()->setItalic(true);
                                $objRichText_newTag_dilig->createTextRun("\n- Para el caso de activos ");
                                $objRichText_newTag_dilig->createTextRun("operados por asociados ")->getFont()->setItalic(true);
                                $objRichText_newTag_dilig->createTextRun("se debe diligenciar \"NO REGISTRA \"");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B24', "NUEVO TAG")
                                    ->setCellValue('C24', $objRichText_newTag_desc)
                                    ->setCellValue('D24', $objRichText_newTag_dilig);
                        /** FILA NUEVA MATRICULA */
                            /** DESCRIPCION */
                                $objRichText_newMatricula_desc = new PHPExcel_RichText();
                                $objRichText_newMatricula_desc->createTextRun("Consecutivo que permite la identificación del vehículo ante las entidades de tránsito");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newMatricula_dilig = new PHPExcel_RichText();
                                $objRichText_newMatricula_dilig->createTextRun("En la columna ");
                                $objRichText_newMatricula_dilig->createTextRun("\"NUEVA MATRÍCULA\" ")->getFont()->setItalic(true);
                                $objRichText_newMatricula_dilig->createTextRun("diligenciar la ");
                                $objRichText_newMatricula_dilig->createTextRun("nueva ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newMatricula_dilig->createTextRun("matrícula del vehículo");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B25', "NUEVA MATRICULA")
                                    ->setCellValue('C25', $objRichText_newMatricula_desc)
                                    ->setCellValue('D25', $objRichText_newMatricula_dilig);
                        /** FILA NUEVO FABRICANTE */
                            /** DESCRIPCION */
                                $objRichText_newFabricante_desc = new PHPExcel_RichText();
                                $objRichText_newFabricante_desc->createTextRun("Persona jurídica que elabora o construye el activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newFabricante_dilig = new PHPExcel_RichText();
                                $objRichText_newFabricante_dilig->createTextRun("- En la columna ");
                                $objRichText_newFabricante_dilig->createTextRun("\"NUEVO FABRICANTE\" ")->getFont()->setItalic(true);
                                $objRichText_newFabricante_dilig->createTextRun("diligenciar el ");
                                $objRichText_newFabricante_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newFabricante_dilig->createTextRun("fabricante o marca del activo fijo \n- Para el caso de ");
                                $objRichText_newFabricante_dilig->createTextRun("terrenos o servidumbres ")->getFont()->setItalic(true);
                                $objRichText_newFabricante_dilig->createTextRun("se debe indicar el ");
                                $objRichText_newFabricante_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newFabricante_dilig->createTextRun("número de la ");
                                $objRichText_newFabricante_dilig->createTextRun("Escritura Pública ")->getFont()->setItalic(true);
                                $objRichText_newFabricante_dilig->createTextRun("y el ");
                                $objRichText_newFabricante_dilig->createTextRun("año, ")->getFont()->setItalic(true);
                                $objRichText_newFabricante_dilig->createTextRun("según la siguiente estructura:  ");
                                $objRichText_newFabricante_dilig->createTextRun("ESC_NÚMERO DE ESCRITURA/AÑO, ")->getFont()->setItalic(true);
                                $objRichText_newFabricante_dilig->createTextRun("por ejemplo: ESC_6/2009");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B26', "NUEVO FABRICANTE")
                                    ->setCellValue('C26', $objRichText_newFabricante_desc)
                                    ->setCellValue('D26', $objRichText_newFabricante_dilig);
                        /** FILA NUEVO MODELO */
                            /** DESCRIPCION */
                                $objRichText_newModelo_desc = new PHPExcel_RichText();
                                $objRichText_newModelo_desc->createTextRun("Referencia que caracteriza un activo con ciertas especificaciones técnicas");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newModelo_dilig = new PHPExcel_RichText();
                                $objRichText_newModelo_dilig->createTextRun("- En la columna ");
                                $objRichText_newModelo_dilig->createTextRun("\"NUEVO MODELO\" ")->getFont()->setItalic(true);
                                $objRichText_newModelo_dilig->createTextRun("diligenciar el ");
                                $objRichText_newModelo_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newModelo_dilig->createTextRun("modelo del activo fijo");
                                $objRichText_newModelo_dilig->createTextRun("\n- Para el caso de")->getFont()->setItalic(true);
                                $objRichText_newModelo_dilig->createTextRun("terrenos o servidumbres ")->getFont()->setItalic(true);
                                $objRichText_newModelo_dilig->createTextRun("se debe indicar el ");
                                $objRichText_newModelo_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newModelo_dilig->createTextRun("número de la ");
                                $objRichText_newModelo_dilig->createTextRun("Notaría ")->getFont()->setItalic(true);
                                $objRichText_newModelo_dilig->createTextRun("más la ");
                                $objRichText_newModelo_dilig->createTextRun("ciudad ")->getFont()->setItalic(true);
                                $objRichText_newModelo_dilig->createTextRun("del registro");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B27', "NUEVO MODELO")
                                    ->setCellValue('C27', $objRichText_newModelo_desc)
                                    ->setCellValue('D27', $objRichText_newModelo_dilig);
                        /** FILA NUEVO COD. CECO */
                            /** DESCRIPCION */
                                $objRichText_newCodCeco_desc = new PHPExcel_RichText();
                                $objRichText_newCodCeco_desc->createTextRun("Centro de costo responsable del activo, el cual, asume los costos o gastos por depreciación");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCodCeco_dilig = new PHPExcel_RichText();
                                $objRichText_newCodCeco_dilig->createTextRun("- En la columna ");
                                $objRichText_newCodCeco_dilig->createTextRun("\"NUEVO COD. CECO\" ")->getFont()->setItalic(true);
                                $objRichText_newCodCeco_dilig->createTextRun("diligenciar el ");
                                $objRichText_newCodCeco_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newCodCeco_dilig->createTextRun("centro de costo que será asignado al activo \n- Debe ser congruente con la ubicación geográfica, el campo planta y el municipio");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B29', "NUEVO COD. CECO")
                                    ->setCellValue('C29', $objRichText_newCodCeco_desc)
                                    ->setCellValue('D29', $objRichText_newCodCeco_dilig);
                        /** FILA NUEVA VICEPRESIDENCIA */
                            /** DESCRIPCION */
                                $objRichText_newVice_desc = new PHPExcel_RichText();
                                $objRichText_newVice_desc->createTextRun("Vicepresidencia dada por la asignación del centro de costo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newVice_dilig = new PHPExcel_RichText();
                                $objRichText_newVice_dilig->createTextRun("En la columna ");
                                $objRichText_newVice_dilig->createTextRun("\"NUEVA VICEPRESIDENCIA\" ")->getFont()->setItalic(true);
                                $objRichText_newVice_dilig->createTextRun("diligenciar la sigla de la ");
                                $objRichText_newVice_dilig->createTextRun("nueva ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newVice_dilig->createTextRun("Vicepresidencia según el nuevo centro de costo del activo, de acuerdo con la ");
                                $objRichText_newVice_dilig->createTextRun("\"Tabla_Jerarquizacion\" ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B30', "NUEVA VICEPRESIDENCIA")
                                    ->setCellValue('C30', $objRichText_newVice_desc)
                                    ->setCellValue('D30', $objRichText_newVice_dilig);
                        /** FILA NUEVA GERENCIA */
                            /** DESCRIPCION */
                                $objRichText_newGerencia_desc = new PHPExcel_RichText();
                                $objRichText_newGerencia_desc->createTextRun("Gerencia o Departamento dada por la asignación del centro de costo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newGerencia_dilig = new PHPExcel_RichText();
                                $objRichText_newGerencia_dilig->createTextRun("En la columna ");
                                $objRichText_newGerencia_dilig->createTextRun("\"NUEVA GERENCIA\" ")->getFont()->setItalic(true);
                                $objRichText_newGerencia_dilig->createTextRun("diligenciar la sigla de la ");
                                $objRichText_newGerencia_dilig->createTextRun("nueva ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newGerencia_dilig->createTextRun("Gerencia o Departamento según el nuevo centro de costo del activo, de acuerdo con la ");
                                $objRichText_newGerencia_dilig->createTextRun("\"Tabla_Jerarquizacion\" ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B31', "NUEVA GERENCIA")
                                    ->setCellValue('C31', $objRichText_newGerencia_desc)
                                    ->setCellValue('D31', $objRichText_newGerencia_dilig);
                        /** FILA NUEVO REGISTRO CUSTODIO */
                            /** DESCRIPCION */
                                $objRichText_newCustodio_desc = new PHPExcel_RichText();
                                $objRichText_newCustodio_desc->createTextRun("Funcionario de Ecopetrol que es el custodio y responsable del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCustodio_dilig = new PHPExcel_RichText();
                                $objRichText_newCustodio_dilig->createTextRun("En la columna ");
                                $objRichText_newCustodio_dilig->createTextRun("\"NUEVO REGISTRO CUSTODIO\" ")->getFont()->setItalic(true);
                                $objRichText_newCustodio_dilig->createTextRun("diligenciar el código de registro del ");
                                $objRichText_newCustodio_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newCustodio_dilig->createTextRun("custodio del activo");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B32', "NUEVO REGISTRO CUSTODIO")
                                    ->setCellValue('C32', $objRichText_newCustodio_desc)
                                    ->setCellValue('D32', $objRichText_newCustodio_dilig);
                        /** FILA NUEVO COD. UBICACIÓN GEOGRÁFICA */
                            /** DESCRIPCION */
                                $objRichText_newCodUbi_desc = new PHPExcel_RichText();
                                $objRichText_newCodUbi_desc->createTextRun("Código de localización o un punto físico donde se encuentra instalado el activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCodUbi_dilig = new PHPExcel_RichText();
                                $objRichText_newCodUbi_dilig->createTextRun("En la columna ");
                                $objRichText_newCodUbi_dilig->createTextRun("\"NUEVO COD. UBICACIÓN GEOGRÁFICA\" ")->getFont()->setItalic(true);
                                $objRichText_newCodUbi_dilig->createTextRun("diligenciar el código de la ");
                                $objRichText_newCodUbi_dilig->createTextRun("nueva ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newCodUbi_dilig->createTextRun("localización geográfica, de acuerdo con la");
                                $objRichText_newCodUbi_dilig->createTextRun("\"Tabla_Ubicaciones_greográficas\"")->getFont()->setItalic(true);
                                $objRichText_newCodUbi_dilig->createTextRun("\n- Debe ser congruente con el CECO, el campo planta y el municipio");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B33', "NUEVO COD. UBICACIÓN GEOGRÁFICA")
                                    ->setCellValue('C33', $objRichText_newCodUbi_desc)
                                    ->setCellValue('D33', $objRichText_newCodUbi_dilig);
                        /** FILA NOMBRE NUEVA UBICACIÓN GEOGRÁFICA */
                            /** DESCRIPCION */
                                $objRichText_newNombreUbi_desc = new PHPExcel_RichText();
                                $objRichText_newNombreUbi_desc->createTextRun("Nombre de localización o un punto físico donde se encuentra instalado el activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newNombreUbi_dilig = new PHPExcel_RichText();
                                $objRichText_newNombreUbi_dilig->createTextRun("En la columna ");
                                $objRichText_newNombreUbi_dilig->createTextRun("\"NOMBRE NUEVA UBICACIÓN GEOGRÁFICA\" ")->getFont()->setItalic(true);
                                $objRichText_newNombreUbi_dilig->createTextRun("diligenciar el nombre de la ");
                                $objRichText_newNombreUbi_dilig->createTextRun("nueva ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newNombreUbi_dilig->createTextRun("localización geográfica, de acuerdo con la");
                                $objRichText_newNombreUbi_dilig->createTextRun("\"Tabla_Ubicaciones_greográficas\"")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B34', "NOMBRE NUEVA UBICACIÓN GEOGRÁFICA")
                                    ->setCellValue('C34', $objRichText_newNombreUbi_desc)
                                    ->setCellValue('D34', $objRichText_newNombreUbi_dilig);
                        /** FILA NUEVO COD. CAMPO PLANTA */
                            /** DESCRIPCION */
                                $objRichText_newCodCampo_desc = new PHPExcel_RichText();
                                $objRichText_newCodCampo_desc->createTextRun("Código que identifica físicamente el activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCodCampo_dilig = new PHPExcel_RichText();
                                $objRichText_newCodCampo_dilig->createTextRun("- En la columna ");
                                $objRichText_newCodCampo_dilig->createTextRun("\"NUEVO COD. CAMPO PLANTA\" ")->getFont()->setItalic(true);
                                $objRichText_newCodCampo_dilig->createTextRun("diligenciar el código del ");
                                $objRichText_newCodCampo_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newCodCampo_dilig->createTextRun("campo planta, de acuerdo con la");
                                $objRichText_newCodCampo_dilig->createTextRun("\"Tabla_Campo Planta\"")->getFont()->setItalic(true);
                                $objRichText_newCodCampo_dilig->createTextRun("\n- Debe ser congruente con el CECO, la ubicación geográfica y el municipio");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B35', "NUEVO COD. CAMPO PLANTA")
                                    ->setCellValue('C35', $objRichText_newCodCampo_desc)
                                    ->setCellValue('D35', $objRichText_newCodCampo_dilig);
                        /** FILA NOMBRE NUEVO CAMPO PLANTA */
                            /** DESCRIPCION */
                                $objRichText_newNombreCampo_desc = new PHPExcel_RichText();
                                $objRichText_newNombreCampo_desc->createTextRun("Nombre de la identificación físicamente el activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newNombreCampo_dilig = new PHPExcel_RichText();
                                $objRichText_newNombreCampo_dilig->createTextRun("En la columna ");
                                $objRichText_newNombreCampo_dilig->createTextRun("\"NOMBRE NUEVO CAMPO PLANTA\" ")->getFont()->setItalic(true);
                                $objRichText_newNombreCampo_dilig->createTextRun("diligenciar el nombre del ");
                                $objRichText_newNombreCampo_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newNombreCampo_dilig->createTextRun("campo planta, de acuerdo con la");
                                $objRichText_newNombreCampo_dilig->createTextRun("\"Tabla_Campo Planta\"")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B36', "NOMBRE NUEVO CAMPO PLANTA")
                                    ->setCellValue('C36', $objRichText_newNombreCampo_desc)
                                    ->setCellValue('D36', $objRichText_newNombreCampo_dilig);
                        /** FILA NUEVO TIPO DE OPERACIÓN */
                            /** DESCRIPCION */
                                $objRichText_newTipoOperacion_desc = new PHPExcel_RichText();
                                $objRichText_newTipoOperacion_desc->createTextRun("Identificador de propiedad del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newTipoOperacion_dilig = new PHPExcel_RichText();
                                $objRichText_newTipoOperacion_dilig->createTextRun("En la columna ");
                                $objRichText_newTipoOperacion_dilig->createTextRun("\"NUEVO TIPO DE OPERACIÓN\" ")->getFont()->setItalic(true);
                                $objRichText_newTipoOperacion_dilig->createTextRun("diligenciar según sea el caso: ");
                                $objRichText_newTipoOperacion_dilig->createTextRun("\n- DIRECTA \n- ASOCIADA OPERADOR \n- ASOCIADA NO OPERADOR \n- EXTENSIÓN DE COMERCIALIDAD ")->getFont()->setBold(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B37', "NUEVO TIPO DE OPERACIÓN")
                                    ->setCellValue('C37', $objRichText_newTipoOperacion_desc)
                                    ->setCellValue('D37', $objRichText_newTipoOperacion_dilig);
                        /** FILA NUEVO COD.MUNICIPIO */
                            /** DESCRIPCION */
                                $objRichText_newCodMunicipio_desc = new PHPExcel_RichText();
                                $objRichText_newCodMunicipio_desc->createTextRun("Código de la entidad administrativa que puede agrupar una o varias localidades");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCodMunicipio_dilig = new PHPExcel_RichText();
                                $objRichText_newCodMunicipio_dilig->createTextRun("En la columna ");
                                $objRichText_newCodMunicipio_dilig->createTextRun("\"NUEVO COD.MUNICIPIO\" ")->getFont()->setItalic(true);
                                $objRichText_newCodMunicipio_dilig->createTextRun("diligenciar el código del ");
                                $objRichText_newCodMunicipio_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newCodMunicipio_dilig->createTextRun("municipio, de acuerdo con la ");
                                $objRichText_newCodMunicipio_dilig->createTextRun("\"Tabla_Municipios\" ")->getFont()->setItalic(true);
                                $objRichText_newCodMunicipio_dilig->createTextRun(" \n- Debe ser congruente con el CECO, la ubicación geográfica y el campo planta  ");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B38', "NUEVO COD.MUNICIPIO")
                                    ->setCellValue('C38', $objRichText_newCodMunicipio_desc)
                                    ->setCellValue('D38', $objRichText_newCodMunicipio_dilig);
                        /** FILA NOMBRE NUEVO MUNICIPIO */
                            /** DESCRIPCION */
                                $objRichText_newnombreMunicipio_desc = new PHPExcel_RichText();
                                $objRichText_newnombreMunicipio_desc->createTextRun("Nombre de la entidad administrativa que puede agrupar una o varias localidades");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newnombreMunicipio_dilig = new PHPExcel_RichText();
                                $objRichText_newnombreMunicipio_dilig->createTextRun("En la columna ");
                                $objRichText_newnombreMunicipio_dilig->createTextRun("\"NOMBRE NUEVO MUNICIPIO\" ")->getFont()->setItalic(true);
                                $objRichText_newnombreMunicipio_dilig->createTextRun("diligenciar el nombre del ");
                                $objRichText_newnombreMunicipio_dilig->createTextRun("nuevo ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newnombreMunicipio_dilig->createTextRun("municipio, de acuerdo con la ");
                                $objRichText_newnombreMunicipio_dilig->createTextRun("\"Tabla_Municipios\" ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B39', "NOMBRE NUEVO MUNICIPIO")
                                    ->setCellValue('C39', $objRichText_newnombreMunicipio_desc)
                                    ->setCellValue('D39', $objRichText_newnombreMunicipio_dilig);
                        /** FILA CECO ** */
                            /** DESCRIPCION */
                                $objRichText_newCecoVU_desc = new PHPExcel_RichText();
                                $objRichText_newCecoVU_desc->createTextRun("Centro de costo responsable del activo, el cual, asume los costos o gastos por depreciación");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newCecoVU_dilig = new PHPExcel_RichText();
                                $objRichText_newCecoVU_dilig->createTextRun("En la columna ");
                                $objRichText_newCecoVU_dilig->createTextRun("\"CECO \" ")->getFont()->setItalic(true);
                                $objRichText_newCecoVU_dilig->createTextRun("diligenciar el código del centro de costo asignado al activo de acuerdo con el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B41', HelpTitles("CECO","**"))
                                    ->setCellValue('C41', $objRichText_newCecoVU_desc)
                                    ->setCellValue('D41', $objRichText_newCecoVU_dilig);
                        /** FILA FECHA INICIAL PARA VIDA ÚTIL REMANENTE ** */
                            /** DESCRIPCION */
                                $objRichText_newFechaVUR_desc = new PHPExcel_RichText();
                                $objRichText_newFechaVUR_desc->createTextRun("Fecha desde la cual aplica el cambio de la vida útil remanente");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newFechaVUR_dilig = new PHPExcel_RichText();
                                $objRichText_newFechaVUR_dilig->createTextRun("En la columna ");
                                $objRichText_newFechaVUR_dilig->createTextRun("\"FECHA INICIAL PARA VIDA ÚTIL REMANENTE \" ")->getFont()->setItalic(true);
                                $objRichText_newFechaVUR_dilig->createTextRun("diligenciar la fecha desde la cual aplica el cambio de vida útil remanente, formato ");
                                $objRichText_newFechaVUR_dilig->createTextRun("DD/MM/AAAA ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B42', HelpTitles("FECHA INICIAL PARA VIDA ÚTIL REMANENTE","**"))
                                    ->setCellValue('C42', $objRichText_newFechaVUR_desc)
                                    ->setCellValue('D42', $objRichText_newFechaVUR_dilig);
                        /** FILA NUEVA VIDA ÚTIL REMANENTE (MESES) ** */
                            /** DESCRIPCION */
                                $objRichText_newVUR_desc = new PHPExcel_RichText();
                                $objRichText_newVUR_desc->createTextRun("Vida útil remanente del activo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_newVUR_dilig = new PHPExcel_RichText();
                                $objRichText_newVUR_dilig->createTextRun("En la columna ");
                                $objRichText_newVUR_dilig->createTextRun("\"NUEVA VIDA ÚTIL REMANENTE\" ")->getFont()->setItalic(true);
                                $objRichText_newVUR_dilig->createTextRun("diligenciar la ");
                                $objRichText_newVUR_dilig->createTextRun("nueva ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_newVUR_dilig->createTextRun("vida útil remanente, teniendo en cuenta que su cálculo corresponde a: ");
                                $objRichText_newVUR_dilig->createTextRun("Vida útil técnica - Edad del activo")->getFont()->setItalic(true)->setBold(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B43', HelpTitles("NUEVA VIDA ÚTIL REMANENTE (MESES)","**"))
                                    ->setCellValue('C43', $objRichText_newVUR_desc)
                                    ->setCellValue('D43', $objRichText_newVUR_dilig);
                        /** FILA VICEPRESIDENCIA */
                            /** DESCRIPCION */
                                $objRichText_vicepresidenciaVU_desc = new PHPExcel_RichText();
                                $objRichText_vicepresidenciaVU_desc->createTextRun("Vicepresidencia dada por la asignación del centro de costo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_vicepresidenciaVU_dilig = new PHPExcel_RichText();
                                $objRichText_vicepresidenciaVU_dilig->createTextRun("En la columna ");
                                $objRichText_vicepresidenciaVU_dilig->createTextRun("\"VICEPRESIDENCIA\" ")->getFont()->setItalic(true);
                                $objRichText_vicepresidenciaVU_dilig->createTextRun("se puede dejar ");
                                $objRichText_vicepresidenciaVU_dilig->createTextRun("vacía ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_vicepresidenciaVU_dilig->createTextRun("o diligenciar la sigla de la Vicepresidencia según el centro de costo del activo, de acuerdo con la ");
                                $objRichText_vicepresidenciaVU_dilig->createTextRun("\"Tabla_Jerarquizacion\" ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B44', "VICEPRESIDENCIA")
                                    ->setCellValue('C44', $objRichText_vicepresidenciaVU_desc)
                                    ->setCellValue('D44', $objRichText_vicepresidenciaVU_dilig);
                        /** FILA GERENCIA */
                            /** DESCRIPCION */
                                $objRichText_gerenciaVU_desc = new PHPExcel_RichText();
                                $objRichText_gerenciaVU_desc->createTextRun("Gerencia o Departamento dada por la asignación del centro de costo");
                            /** DILIGENCIAMIENTO */
                                $objRichText_gerenciaVU_dilig = new PHPExcel_RichText();
                                $objRichText_gerenciaVU_dilig->createTextRun("En la columna ");
                                $objRichText_gerenciaVU_dilig->createTextRun("\"GERENCIA\" ")->getFont()->setItalic(true);
                                $objRichText_gerenciaVU_dilig->createTextRun("se puede dejar ");
                                $objRichText_gerenciaVU_dilig->createTextRun("vacía ")->getFont()->setItalic(true)->setBold(true);
                                $objRichText_gerenciaVU_dilig->createTextRun("o diligenciar la sigla de la Gerencia o Departamento según el centro de costo del activo, de acuerdo con la ");
                                $objRichText_gerenciaVU_dilig->createTextRun("\"Tabla_Jerarquizacion\" ")->getFont()->setItalic(true);
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B45', "GERENCIA")
                                    ->setCellValue('C45', $objRichText_gerenciaVU_desc)
                                    ->setCellValue('D45', $objRichText_gerenciaVU_dilig);
                        /** FILA OBSERVACIONES */
                            /** DESCRIPCION */
                                $objRichText_observacioens_desc = new PHPExcel_RichText();
                                $objRichText_observacioens_desc->createTextRun("Comentarios relevantes o aclaraciones");
                            /** DILIGENCIAMIENTO */
                                $objRichText_observacioens_dilig = new PHPExcel_RichText();
                                $objRichText_observacioens_dilig->createTextRun("En la columna ");
                                $objRichText_observacioens_dilig->createTextRun("\"OBSERVACIONES\" ")->getFont()->setItalic(true);
                                $objRichText_observacioens_dilig->createTextRun("diligenciar los comentarios relevantes o aclaraciones  sobre los activos");
                            /** INSERTAR TEXTOS */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B46', "OBSERVACIONES")
                                    ->setCellValue('C46', $objRichText_observacioens_desc)
                                    ->setCellValue('D46', $objRichText_observacioens_dilig);


                                    
                break;
            }
            $i++;
        }
    /*remueve la ultima hoja creada por defecto*/
        $objPHPExcel->removeSheetByIndex(2);
    /*PARENT'S ROWS*/
        /** CAPTURAR INFORMACION VIA POST */
            $grupo = isset($_POST['jsonExcel']) ? $_POST['jsonExcel'] : null ;
            $obj = array();
            $vShift = 8;

            if($grupo){
                $obj = json_decode($grupo, true);
                // print_r($obj);
                /** Recorrer el objeto recibido para poblar el archivo excel */
                for($i=0; $i<sizeof($obj); $i++){
                    $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('B'.($i+$vShift), $obj[$i]['activo'])
                    ->setCellValue('C'.($i+$vShift), $obj[$i]['subnumero'])
                    ->setCellValue('D'.($i+$vShift), $obj[$i]['denominacion'])
                    ->setCellValue('E'.($i+$vShift), $obj[$i]['inventario'])
                    ->setCellValue('F'.($i+$vShift), $obj[$i]['serie'])
                    ->setCellValue('G'.($i+$vShift), $obj[$i]['tag'])
                    ->setCellValue('H'.($i+$vShift), $obj[$i]['codigo_clase'])
                    ->setCellValue('I'.($i+$vShift), $obj[$i]['nueva_denominacion'])
                    ->setCellValue('J'.($i+$vShift), $obj[$i]['nueva_denominacion_complementaria'])
                    ->setCellValue('K'.($i+$vShift), $obj[$i]['nueva_serie'])
                    ->setCellValue('L'.($i+$vShift), $obj[$i]['nuevo_inventario'])
                    ->setCellValue('M'.($i+$vShift), $obj[$i]['nueva_capacidad'])
                    ->setCellValue('N'.($i+$vShift), $obj[$i]['nueva_unidad_medida'])
                    ->setCellValue('O'.($i+$vShift), $obj[$i]['nuevo_tag'])
                    ->setCellValue('P'.($i+$vShift), $obj[$i]['nueva_matricula'])
                    ->setCellValue('Q'.($i+$vShift), $obj[$i]['nuevo_fabricante'])
                    ->setCellValue('R'.($i+$vShift), $obj[$i]['nuevo_modelo'])
                    ->setCellValue('S'.($i+$vShift), $obj[$i]['nuevo_codigo_ceco'])
                    ->setCellValue('T'.($i+$vShift), $obj[$i]['nueva_vicepresidencia'])
                    ->setCellValue('U'.($i+$vShift), $obj[$i]['nueva_gerencia'])
                    ->setCellValue('V'.($i+$vShift), $obj[$i]['nuevo_registro_custodio'])
                    ->setCellValue('W'.($i+$vShift), $obj[$i]['nuevo_codigo_ubicacion_geografica'])
                    ->setCellValue('X'.($i+$vShift), $obj[$i]['nueva_ubicacion_geografica'])
                    ->setCellValue('Y'.($i+$vShift), $obj[$i]['nuevo_codigo_campo_planta'])
                    ->setCellValue('Z'.($i+$vShift), $obj[$i]['nuevo_campo_planta'])
                    ->setCellValue('AA'.($i+$vShift), $obj[$i]['nuevo_tipo_operacion'])
                    ->setCellValue('AB'.($i+$vShift), $obj[$i]['nuevo_codigo_municipio'])
                    ->setCellValue('AC'.($i+$vShift), $obj[$i]['nuevo_municipio'])
                    ->setCellValue('AD'.($i+$vShift), $obj[$i]['ceco'])
                    ->setCellValue('AE'.($i+$vShift), $obj[$i]['vicepresidencia'])
                    ->setCellValue('AF'.($i+$vShift), $obj[$i]['gerencia'])
                    ->setCellValue('AG'.($i+$vShift), $obj[$i]['fecha_inicial_VUR'])
                    ->setCellValue('AH'.($i+$vShift), $obj[$i]['nueva_vida_util_remanente'])
                    ->setCellValue('AI'.($i+$vShift), $obj[$i]['observaciones']);
                }
            }

        /** Si no hay un objeto entonces le da formato a 5 filas hoja 0 */
            $objSize = sizeof($obj)>0 ? sizeof($obj) : 5;

            // Formato de celdas de datos hoja 0
            $vShiftTotal = "B".$vShift.":AI".($vShift + $objSize - 1) ;
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftTotal)
                ->applyFromArray($properties['cellBorderColorGray']);
            // Outline principal hoja 0
            $vShiftGlobal = "B1:AI".($vShift + $objSize - 1) ;
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftGlobal)
                ->applyFromArray($properties['cellOutlineBorderMedium']);
            // Outline datos técnicos hoja 0
            $vShiftPartial = "I6:R".($vShift + $objSize - 1);
            $objPHPExcel->getSheet(0)
                ->getStyle("$vShiftPartial")
                ->applyFromArray($properties['cellOutlineBorderMedium']);
            // Outline Transferencias hoja 0
            $vShiftPartial2 = "S6:AC".($vShift + $objSize - 1);
            $objPHPExcel->getSheet(0)
                ->getStyle("$vShiftPartial2")
                ->applyFromArray($properties['cellOutlineBorderMedium']);
            // Outline vidas útiles hoja 0
            $vShiftPartial3 = "AD6:AH".($vShift + $objSize - 1);
            $objPHPExcel->getSheet(0)
                ->getStyle("$vShiftPartial3")
                ->applyFromArray($properties['cellOutlineBorderMedium']);

    /** mensajes adicionales hoja 0 */
            // Mensajes * y **
                $vRow = $vShift + $objSize + 1;
                $objPHPExcel->getSheet(0)
                    ->getRowDimension($vRow)->setRowHeight(15);
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("B".($vShift + $objSize + 1), "* Campos Obligatorios")
                    ->setCellValue("B".($vShift + $objSize + 2), "** Aplica para actualización de vidas útiles");
            // Nota: Pie de página
                $vShiftMessage = "B".($vShift + $objSize + 4).":AI".($vShift + $objSize + 4) ;
        
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("B".($vShift + $objSize + 4), "Todos los derechos reservados para Ecopetrol S.A. Ninguna reproducción externa copia o transmisión digital de esta publicación puede ser hecha sin permiso escrito. Ningún párrafo de esta publicación puede ser reproducido, copiado o transmitido digitalmente sin un consentimiento escrito o de acuerdo con las leyes que regulan los derechos de autor y con base en la regulación vigente.");
                $objPHPExcel->getSheet(0)
                    ->mergeCells($vShiftMessage)
                    ->getStyle($vShiftMessage)
                    ->getAlignment()
                    ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
                    ->setWrapText(true);
                $objPHPExcel->getSheet(0)
                    ->getStyle($vShiftMessage)
                    ->applyFromArray($properties['cellBorderMedium']);

    /** Renombrar Hojas */
        // $objPHPExcel->getSheet(0)
        //            ->setTitle('Datos Maestros')
        //            ->freezePaneByColumnAndRow(0,8);
        $objPHPExcel->getSheet(0)
                    ->setTitle('Datos Maestros');
        $objPHPExcel->getSheet(1)
                    ->setTitle('Ayuda');
    /** Establecer la hoja activa, para que cuando se abra el documento se muestre primero. */
        $objPHPExcel->setActiveSheetIndex(0);
    
    /** AGREGAR SEGURIDAD A LA HOJA 0 CON CONTRASEÑA */
        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setPassword('password');

    /** Borrar todos los archivos excel previamente generados */    
        $files = glob('xls/datosmaestros/*'); // get all file names
        foreach($files as $file){ // iterate files
          if(is_file($file))
            unlink($file); // delete file
        }
    /** Nombrar y generar el archivo XLS */
        // header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $route = 'xls/datosmaestros/GFI-F-104-';
        $datenow = date('YmdHis');
        $extension = '.xlsx';
        $name = $route . $datenow . $extension;
        $objWriter->save($name);
        $objName = array(
            'route' => $name,
            'name' => 'GFI-F-104-actualizacion-datos-maestros-de-activos-fijos-'.$datenow.$extension
        );
        die( json_encode( $objName ) );
?>