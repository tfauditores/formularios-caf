<?php
    require_once('conexion.php');

    $query = "  SELECT  c.descripcion_af, c.diversidad, c.clase, c.unidad, c.vu_sugerida, c.observaciones
                FROM catalogo AS c
                WHERE 1
    ";
    $results=mysqli_query($con, $query);
    $numResults = mysqli_num_rows($results);
    if ($numResults > 0) {
        $data = array();
        while($row=mysqli_fetch_array($results  ))
        {
            $dataTemp = [   
                'label' => $row['descripcion_af'],
                'grupo' => $row['diversidad'],
                'clase' => $row['clase'],
                'unidad' => $row['unidad'],
                'vida_sugerida' => $row['vu_sugerida'],
                'observaciones' => $row['observaciones']
            ];
            array_push($data,$dataTemp);
        }
    }
    else{
        $data = [ 'result' => 'invalid query' ] ;
    }
    header('Content-type: application/json');
    http_response_code(200);
    echo json_encode( $data );
?>