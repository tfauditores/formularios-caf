<?php
    // Archivo de conexi��n a base de datos
    require_once('conexion.php');
    // Lectura de variables POST o GET
    $reference          = isset($_REQUEST['id_referencia']) ? $_REQUEST['id_referencia'] : '';
    $numberRequest      = isset($_REQUEST['id_numero_referencia']) ? $_REQUEST['id_numero_referencia'] : '';
    $subnumberRequest   = isset($_REQUEST['id_subnumero_referencia']) ? $_REQUEST['id_subnumero_referencia'] : 0;
    $subnFlag = false;
    $count = 0;

    /* si se aplica el # de activo se trae el subnumero para verificar si tiene activos hijos*/
    if( $subnumberRequest !='0' && $subnumberRequest !='' ){
        $reference = "_key";
        $numberRequest = $numberRequest.$subnumberRequest;
    }

    //si se trata de un principal cuenta el n��mero de hijos
    if($reference == "activo" && $subnumberRequest == 0){
        $query2 = "   SELECT COUNT(a.$reference) 
        FROM auxiliar AS a 
        WHERE a.$reference LIKE '$numberRequest' ";
        $results = mysqli_query($con, $query2);
        $count = mysqli_fetch_array($results)[0] - 1;
    
    }

    if( $reference == 'subnumeros'){
        $reference = 'activo';
        $subnFlag = true;
    }

    //consulta de tabla auxiliar
    $query = "  SELECT  a.activo, a.sn, a.matriculaVehiculo, a.clase, a.nombreClase,
                        a.denominacion, a.denominacion2, a.noInventario,
                        a.fabricante, a.modelo, a.serie, a.tipoOperacion,
                        a.tag, a.capacidad, a.unidadMedida,
                        a.vuAnosAr01, a.vuPeriodosAr01, a.vuRemanenteAr01, a.fechaCapitalizacion,
                        a.tipoOperacion, a.departamento, a.nombreMunicipio, a.nombreUbicacionGeografica,
                        a.area, a.vicepresidencia, a.siglaVicepresidencia, a.gerencia, a.siglaGerencia,
                        a.ceco, a.claveAr01, a.campoPlanta, a.custodio
                FROM auxiliar AS a
                WHERE a.$reference LIKE '$numberRequest'";
    
    //PARA NO INCLUIR EL NUMERO PRINCIPAL
    //$query .= $subnFlag ? " AND a.sn != '0'" : " AND a.sn = $subnumberRequest" ;

    $query .= $subnFlag ? " " : " AND a.sn = $subnumberRequest" ;

    $results = mysqli_query($con, $query);
    $numResults = mysqli_num_rows($results);

    if ($numResults > 0) {
        $data = array();
        while( $row = mysqli_fetch_array($results) ){
            $dataTemp = [  
                'activo' => $row['activo'],
                'sn' => $row['sn'],
                'matriculaVehiculo' => $row['matriculaVehiculo'],
                'clase' => $row['clase'],
                'nombre_clase' => $row['nombreClase'],
                'denominacion' => $row['denominacion'],
                'denominacion2' => $row['denominacion2'],
                'inventario' => $row['noInventario'],
                'fabricante' => $row['fabricante'],
                'modelo' => $row['modelo'],
                'serie' => $row['serie'],
                'tag' => $row['tag'],
                'capacidad' => $row['capacidad'],
                'unidad' => $row['unidadMedida'],
                'vuAnos' => $row['vuAnosAr01'],
                'vuPeriodos' => $row['vuPeriodosAr01'],
                'vurm' => $row['vuRemanenteAr01'],
                'capitalizacion' => $row['fechaCapitalizacion'],
                'matricula' => $row['matriculaVehiculo'],
                'operacion' => $row['tipoOperacion'],
                'departamento' => $row['departamento'],
                'municipio' => $row['nombreMunicipio'],
                'ubicacion_geografica' => $row['nombreUbicacionGeografica'],
                'area' => $row['area'],
                'vicepresidencia' => $row['vicepresidencia'],
                'siglaVicepresidencia' => $row['siglaVicepresidencia'],
                'gerencia' => $row['gerencia'],
                'siglaGerencia' => $row['siglaGerencia'],
                'ceco' => $row['ceco'],
                'clave_amortizacion' => $row['claveAr01'],
                'campo_planta' => $row['campoPlanta'],
                'custodio' => $row['custodio'],
                'tipo_operacion' => $row['tipoOperacion'],
                'subnumeros' => $count
            ];
            array_push($data, $dataTemp);
        }
        if(!$subnFlag){
            $data = $data[0];
        }
    }
    else{
        $data = [ 'result' => 'invalid active' ] ;
    }

    //Se envia la informaci��n en formato JSON
    header('Content-type: application/json');
    http_response_code(200);
    echo json_encode( $data );
    
?>