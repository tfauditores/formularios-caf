<?php
    /*Manejo de consulta en metodo POST*/
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        /** Error reporting */
            // error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            error_reporting(E_ERROR | E_PARSE);
            date_default_timezone_set('Europe/London');

            // var_dump($_FILES);
            $path = $_POST['path'];
            $errors = $_POST['errors'];
            $format = $_POST['format'];
            $ext = end((explode(".", $path)));

            switch($format){
                case 'bajas':
                    $rowHeadeNumber = 6;
                    $lastColumnFile = 'O';
                break;
                case 'datosMaestros':
                    $rowHeadeNumber = 7;
                    $lastColumnFile = 'AI';
                break;
                case 'creaciones':
                    $rowHeadeNumber = 7;
                    $lastColumnFile = 'AJ';
                break;
            }
        /** Include PHPExcel */
            require_once 'Classes/PHPExcel.php';
            
            switch($ext){
                case 'xls':
                case 'XLS':
                    $fileType = 'Excel5';
                break;
                case 'xlsx':
                case 'XLSX':
                    $fileType = 'Excel2007';
                break;
                default:
                    die;
            }
        // Read the file
            $objReader = PHPExcel_IOFactory::createReader($fileType);
            $objPHPExcel = $objReader->load( $path );

        // Change the file
            $sheet = $objPHPExcel->getSheet(0); 
            // $lastColumn = $sheet->getHighestColumn();
            // echo $lastColumn;
            $lastColumnIndex = PHPExcel_Cell::columnIndexFromString($lastColumnFile) - 1;
            $count = 1;

            for ($column = 'B'; $column != $lastColumnFile; $column++) {
                $cell = $sheet->getCellByColumnAndRow($count,$rowHeadeNumber)->getValue();
                
                $newColumn = PHPExcel_Cell::stringFromColumnIndex($count+$lastColumnIndex);

                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue("$newColumn"."$rowHeadeNumber", "ERROR $cell");
                
                $count++;
            }

            // error format
            $properties = array(
                'errorCell' => array(
                    'font' => array(
                        'bold' => true,
                        'size' => 9.5,
                        'name' => 'Verdana',
                        'color' => array('rgb' => 'ffffff')
                    ),
                    'alignment' => array(
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'wrap' => true
                    ),
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                            'color' => array('rgb' => '000000')
                        )
                    ),
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'b10707')
                    )
                )
            );
            $lastColumn = PHPExcel_Cell::stringFromColumnIndex($lastColumnIndex+1);
            $objPHPExcel->getSheet(0)
                    ->getStyle("$lastColumn"."$rowHeadeNumber:".$newColumn."$rowHeadeNumber")
                    ->applyFromArray($properties['errorCell']);
            // data error
            foreach($errors as $key=>$err){
                $row = $key;
                foreach($err as $e){
                    $column = PHPExcel_Cell::stringFromColumnIndex( intval($e['column'])+$lastColumnIndex );
                    $errorText = $e['error'];
                    
                    $objPHPExcel->setActiveSheetIndex(0)
                        ->setCellValue("$column"."$row", "\"".$e['error']."\"");
                }
            }
            // AutoSize
            /** AutoSize de las columnas de contenido */    
                $newLastColumn = $sheet->getHighestColumn();
                $newLastColumn++;
                for( $i = $lastColumn; $i != $newLastColumn; $i++) {
                    // echo $i."\n";
                    $objPHPExcel->getSheet(0)->getColumnDimension($i)
                        ->setWidth('30');
                };
                /** Habilitar los filtros para las columnnas de contenido */
                $newLastColumn = $sheet->getHighestColumn();
                $objPHPExcel->getSheet(0)->setAutoFilter("B"."$rowHeadeNumber".":".$newLastColumn."$rowHeadeNumber");

        // Write the file
            $path = explode( "/", $path );
            $dir = $path[0].'/'.$path[1];
            $path[2] = 'error_'.$path[2];
            $path = implode( "/", $path);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $fileType);
            $objWriter->save( $path );
            
            
        // remove old files

            $lastDate = new DateTime("sunday last week");

            if (is_dir($dir)){
                if ($dh = opendir($dir)){
                    while (($file = readdir($dh)) !== false){
                        if ($file == '.' || $file == '..') continue;
                        if(intval( filemtime($dir."/".$file) ) < intval( $lastDate -> getTimestamp () ) ){
                            unlink($dir."/".$file);
                        }
                    }
                    closedir($dh);
                }
            }

            $data = array(
                "file" => $path,
                "ext" => $ext
            );
            
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
            die;
               
    }
    /*Manejo de consulta en metodo GET*/
    else if($_SERVER['REQUEST_METHOD'] === 'GET'){
        echo 'hello get';
    }