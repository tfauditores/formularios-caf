<?php
    if(isset($_POST['get_option']))
    {
        require_once('conexion.php');
        $area = $_POST['get_option'];
        //$query = "SELECT DISTINCT vicepresidencia FROM jerarquizacion WHERE area='$area' ORDER BY vicepresidencia ASC";
        $query = "  SELECT DISTINCT j.vicepresidencia, v.nombre 
                    FROM jerarquizacion AS j 
                    LEFT JOIN vicepresidencias AS v ON j.vicepresidencia = v.sigla 
                    WHERE area='$area' ORDER BY j.vicepresidencia ASC";
        $find = mysqli_query($con, $query);
        while($row=mysqli_fetch_array($find))
        {
            echo "<option value=\"". $row['vicepresidencia'] ."\">".  $row['nombre']. " - (" . $row['vicepresidencia']. ")" ."</option>";
        }
        exit;
    }
?>