<?php
    /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('America/Bogota');
    /** Include PHPExcel */
        require_once 'Classes/PHPExcel.php';
    /** Crea una nueva instancia de objeto PHPExcel */
        $objPHPExcel = new PHPExcel();
    /** Propiedades del documento a generar*/
        $objPHPExcel->getProperties()
                    ->setCompany("ECOPETROL")
                    ->setCreator("ECOPETROL - CAF")
                    ->setLastModifiedBy("ECOPETROL - CAF")
                    ->setTitle("BAJAS DE ACTIVOS")
                    ->setSubject("BAJAS DE ACTIVOS")
                    ->setDescription("FORMATO SOLICITUD DE BAJAS DE ACTIVOS")
                    ->setKeywords("GSC, CAF, BAJAS, GFI-055")
                    ->setCategory("SERVICIOS");
    /** Formatos de celdas */
        $properties = array(
            'fontVerdana' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontVerdanaBold' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontCalibri' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontCalibriBold' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontSize11' => array(
                'font' => array(
                    'size' => 11
                )
            ),
            'fontSize12' => array(
                'font' => array(
                    'size' => 12
                )
            ),
            'fontSize14' => array(
                'font' => array(
                    'size' => 14
                )
            ),
            'fontColorWhite' => array(
                'font' => array(
                    'color' => array('rgb' => 'ffffff')
                )
            ),
            'fontColorRed' => array(
                'font' => array(
                    'color' => array('rgb' => 'ff0000')
                )
            ),
            'fontColorGreen' => array(
                'font' => array(
                    'color' => array('rgb' => '004237')
                )
            ),
            'greenLink' => array(
                'font' => array(
                    'color' => array('rgb' => '008800'),
                    'underline' => PHPExcel_Style_Font::UNDERLINE_SINGLE
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'eeffee')
                ),
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'fondoVerde' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C9D200')
                )
            ),
            'fondoVerdeOscuro' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '004238')
                )
            ),
            'fondoGris' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E6E6E6')
                )
            ),
            'fondoGrisMedio' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c3c4c6')
                )
            ),            
            'fondoGrisClaro' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'EEECE1')
                )
            ),
            'alignRight' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignLeft' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenter' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterHorizontal' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterVertical' => array(
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'cellBorderThin' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellBorderMedium' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderDouble' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderThin' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderMedium' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinTop' => array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderColorWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellBorderColorGreen' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '9bbb59')
                    )
                )
            ),
            'cellBorderColorGray' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'bfbfbf')
                    )
                )
            ),
            'cellBorderColorBlack' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
    /** inicializar contador */
        $i = 0;
    /** Print Header */
        function print_headerFormato($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->mergeCells('B2:C4');
                $objPHPExcel->getSheet($sheet)->mergeCells('D2:O2');
                $objPHPExcel->getSheet($sheet)->mergeCells('D3:O3');
                $objPHPExcel->getSheet($sheet)->mergeCells('E4:N4');
                $objPHPExcel->getSheet($sheet)->mergeCells('D5:E5');
                $objPHPExcel->getSheet($sheet)->mergeCells('F5:O5');
            /** Asignación de formatos a celdas y rangos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['cellBorderColorWhite'])
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B2:O4")
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['fontVerdanaBold']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B5:O5")
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['alignRight'])
                    ->applyFromArray($properties['fontVerdanaBold']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B5")
                    ->applyFromArray($properties['greenLink']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("D5:E5")
                    ->applyFromArray($properties['alignLeft']);    
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimensionByColumn('A')
                    ->setWidth('1');
            /** Dimensiones de filas */
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')->setRowHeight(5);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')->setRowHeight(16);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')->setRowHeight(30);
                    $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('4')->setRowHeight(16);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('5')->setRowHeight(16);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('D2', "FORMATO DE AUTORIZACIÓN PARA RETIRO DE ACTIVOS FIJOS DE LA OPERACIÓN")
                    ->setCellValue('D3', "GESTIÓN FINANCIERA \n GSC - DEPARTAMENTO DE TIERRAS Y CONTROL DE ACTIVOS FIJOS - GRUPO CONTROL DE ACTIVOS FIJOS")
                    ->setCellValue('D4', "GFI-F-055")
                    ->setCellValue('E4', "Elaborado: 2018/06/30")
                    ->setCellValue('O4', "Versión: 4")
                    ->setCellValue('B5','Ver tablas de insumos')
                    ->setCellValue('C5', "Formato generado el:");
                $url = 'https://ecopetrol.sharepoint.com/sites/CAF/Documetos%20Auxiliar/Forms/AllItems.aspx?RootFolder=%2Fsites%2FCAF%2FDocumetos%20Auxiliar%2FTablas%20de%20par%C3%A1metros%20para%20solicitudes&FolderCTID=0x0120006997594AF377B149B84014536F20EA2E&View=%7BB7BD0F33-0633-431E-ADC3-070004CF6A8C%7D' ;
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setUrl($url);
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setTooltip('Ver en SharePoint');
            /** Agregar Imagen logo*/
                $objDrawing = new PHPExcel_Worksheet_Drawing();
            
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(100);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(300);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(75);
                $objDrawing->setOffsetY(5);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
        function print_headerAyuda($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->mergeCells('B2:B4');
                $objPHPExcel->getSheet($sheet)->mergeCells('C2:E2');
                $objPHPExcel->getSheet($sheet)->mergeCells('C3:E3');
                $objPHPExcel->getSheet($sheet)->mergeCells('B5:B6');
                $objPHPExcel->getSheet($sheet)->mergeCells('C5:E6');
            /** Asignación de formatos a celdas y rengos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['cellBorderColorWhite'])
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B2:E4")
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("C2:E4")
                    ->applyFromArray($properties['fontVerdanaBold']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B5:B6")
                    ->applyFromArray($properties['greenLink'])
                    ->applyFromArray($properties['fontVerdanaBold'])
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B7:E7")
                    ->applyFromArray($properties['fontVerdanaBold']);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimensionByColumn('A')
                    ->setWidth('1');
            /** Dimensiones de columnas y filas*/
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')->setRowHeight(5);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')->setRowHeight(16);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')->setRowHeight(30);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('4')->setRowHeight(16);

                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('B')->setWidth(27);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('C')->setWidth(32);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('D')->setWidth(80);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('E')->setWidth(20);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('C2', "FORMATO DE AUTORIZACIÓN PARA RETIRO DE ACTIVOS FIJOS DE LA OPERACIÓN")
                    ->setCellValue('C3', "GESTIÓN FINANCIERA \n DEPARTAMENTO DE TIERRAS Y CONTROL DE ACTIVOS FIJOS")
                    ->setCellValue('C4', "GFI-F-055")
                    ->setCellValue('D4', "Elaborado: 2018/06/30")
                    ->setCellValue('E4', "Versión: 4")
                    ->setCellValue('B5','Ver tablas de insumos');
                $url = 'https://ecopetrol.sharepoint.com/sites/CAF/Documetos%20Auxiliar/Forms/AllItems.aspx?RootFolder=%2Fsites%2FCAF%2FDocumetos%20Auxiliar%2FTablas%20de%20par%C3%A1metros%20para%20solicitudes&FolderCTID=0x0120006997594AF377B149B84014536F20EA2E&View=%7BB7BD0F33-0633-431E-ADC3-070004CF6A8C%7D' ;
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setUrl($url);
                $objPHPExcel->setActiveSheetIndex($sheet)->getCell('B5')->getHyperlink()->setTooltip('Ver en SharePoint');
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
            
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(100);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(300);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(5);
                $objDrawing->setOffsetY(5);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
    /** CREAR PAGINAS*/
        while($i < 2){
                $objPHPExcel->createSheet($i);
            switch($i){
                case 0:
                    /** Encabezado Principal */
                        print_headerFormato($objPHPExcel, $i, $properties);
                    /** Asignación de formatos a celdas y rengos */
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B6:O6")
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontVerdanaBold'])
                            ->applyFromArray($properties['fontColorWhite'])
                            ->applyFromArray($properties['alignCenter']);
                    /** Agregar valores de plantilla y encabezados de tabla */
                        $datenow = date('d/m/Y');
                        $columnas = array(
                            "fecha"=>'D5',
                            "tipoBaja"=>'B6',
                            "codigoActivo"=>'C6',
                            "subnumero"=>'D6',
                            "denominacion"=>'E6',
                            "inventario"=>'F6',
                            "serie"=>'G6',
                            "tag"=>'H6',
                            "ceco"=>'I6',
                            "vicepresidencia"=>'J6',
                            "gerencia"=>'K6',
                            "valorBaja"=>'L6',
                            "motivoRetiro"=>'M6',
                            "disposicion"=>'N6',
                            "observaciones"=>'O6'
                        );
                        $objPHPExcel->setActiveSheetIndex($i)
                            ->setCellValue($columnas['fecha'], $datenow)
                            ->setCellValue($columnas['tipoBaja'], "TIPO DE\n BAJA *")
                            ->setCellValue($columnas['codigoActivo'], "CÓDIGO\n ACTIVO FIJO *")
                            ->setCellValue($columnas['subnumero'], "SUBNÚMERO *")
                            ->setCellValue($columnas['denominacion'], "DENOMINACIÓN\n ACTIVO FIJO *")
                            ->setCellValue($columnas['inventario'], "No.\n INVENTARIO *")
                            ->setCellValue($columnas['serie'], "SERIE *")
                            ->setCellValue($columnas['tag'], "TAG")
                            ->setCellValue($columnas['ceco'], "COD.\n CECO *")
                            ->setCellValue($columnas['vicepresidencia'], "SIGLA\n VICEPRESIDENCIA *")
                            ->setCellValue($columnas['gerencia'], "SIGLA GERENCIA.\n Y/O DPTO *")
                            ->setCellValue($columnas['valorBaja'], "VALOR DE\n BAJA *")
                            ->setCellValue($columnas['motivoRetiro'], "MOTIVO DE\n RETIRO *")
                            ->setCellValue($columnas['disposicion'], "DISPOSICIÓN FINAL\n PROPUESTA *")
                            ->setCellValue($columnas['observaciones'], "OBSERVACIONES");

                    /** COMENTARIOS ENCABEZADOS */
                        /** Comentario para TIPO DE BAJA */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->getText()->createTextRun('Diligenciar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->getText()->createTextRun('BAJA TOTAL ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->getText()->createTextRun('o ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->getText()->createTextRun('BAJA PARCIAL ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->getText()->createTextRun(', según hoja de "Ayuda"');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tipoBaja'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para CODIGO ACTIVO FIJO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun('código ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun('de identificación del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setWidth('220pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['codigoActivo'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para SUBNUMERO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getText()->createTextRun('Diligenciar el dígito del ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getText()->createTextRun('subnúmero ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getText()->createTextRun('a dar de baja, según hoja de "Ayuda"');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['subnumero'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para DENOMINACION */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun('Diligenciar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun('denominación ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['denominacion'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para INVENTARIO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('Diligenciar el');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('número de inventario ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setWidth('220pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['inventario'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para SERIE */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('Diligenciar la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('serie ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['serie'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para TAG */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('TAG ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('del activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['tag'])->getFillColor()->setRGB('EEEEEE');    
                        /** Comentario para CECO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('asignado al activo según el auxiliar de activos fijos ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getText()->createTextRun('SAP');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setWidth('220pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['ceco'])->getFillColor()->setRGB('EEEEEE');    
                        /** Comentario para VICEPRESIDENCIA */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('Diligenciar la sigla de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('Vicepresidencia ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('según el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('del activo, de acuerdo con la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getText()->createTextRun('"Tabla_Jerarquizacion" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setWidth('240pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['vicepresidencia'])->getFillColor()->setRGB('EEEEEE');    
                        /** Comentario para GERENCIA */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('Diligenciar la sigla de la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('Gerencia o Departamento ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('según el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('centro de costo ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('del activo, de acuerdo con la ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getText()->createTextRun('"Tabla_Jerarquizacion" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setWidth('240pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setHeight('45pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['gerencia'])->getFillColor()->setRGB('EEEEEE');    
                        /** Comentario para VALOR DE BAJA */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->getText()->createTextRun('Diligenciar el ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->getText()->createTextRun('valor de adquisición ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->getText()->createTextRun('en pesos correspondiente a la baja. Para baja total de un principal o subnúmero dejar ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->getText()->createTextRun('vacía ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->setWidth('230pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->setHeight('50pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['valorBaja'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para MOTIVO DE RETIRO */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->getText()->createTextRun('Diligenciar según la tabla ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->getText()->createTextRun('Anexo 1.1');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->getText()->createTextRun(', según la hoja de ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->getText()->createTextRun('"Ayuda" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['motivoRetiro'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para DISPOSICION */
                            $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->setAuthor('CAF');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->getText()->createTextRun('Diligenciar según la tabla ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->getText()->createTextRun('Anexo 1.1');
                            $objCommentRichText->getFont()->setBold(true);
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->getText()->createTextRun(', según la hoja de ');
                            $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->getText()->createTextRun('"Ayuda" ');
                            $objCommentRichText->getFont()->setBold(true);
                            $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->setWidth('200pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->setHeight('40pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->setMarginLeft('150pt');
                            $objPHPExcel->getActiveSheet()->getComment($columnas['disposicion'])->getFillColor()->setRGB('EEEEEE');
                        /** Comentario para OBSERVACIONES */
                                $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setAuthor('CAF');
                                $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getText()->createTextRun('Diligenciar los ');
                                $objCommentRichText = $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getText()->createTextRun('comentarios relevantes o aclaraciones');
                                $objCommentRichText->getFont()->setBold(true);
                                $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setWidth('150pt');
                                $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setHeight('40pt');
                                $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->setMarginLeft('150pt');
                                $objPHPExcel->getActiveSheet()->getComment($columnas['observaciones'])->getFillColor()->setRGB('EEEEEE');

                    /** Habilitar los filtros para las columnnas de contenido */
                        $objPHPExcel->getSheet($i)->setAutoFilter('B6:O6');
                    /** Ancho de las columnas */    
                        // Se desactiva el autosize pues no es preciso cuando el formato de la fuente ha cambiado
                            // foreach(range('B','O') as $columnID) {
                            //     $objPHPExcel->getSheet($i)->getColumnDimension($columnID)
                            //         ->setAutoSize(true);
                            // };
                        // Tamaño de columnas manual
                            $objPHPExcel->getSheet($i)->getColumnDimension('B')->setWidth(25);
                            $objPHPExcel->getSheet($i)->getColumnDimension('C')->setWidth(21);
                            $objPHPExcel->getSheet($i)->getColumnDimension('D')->setWidth(15);
                            $objPHPExcel->getSheet($i)->getColumnDimension('E')->setWidth(27);
                            $objPHPExcel->getSheet($i)->getColumnDimension('F')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('G')->setWidth(12);
                            $objPHPExcel->getSheet($i)->getColumnDimension('H')->setWidth(8);
                            $objPHPExcel->getSheet($i)->getColumnDimension('I')->setWidth(12);
                            $objPHPExcel->getSheet($i)->getColumnDimension('J')->setWidth(22);
                            $objPHPExcel->getSheet($i)->getColumnDimension('K')->setWidth(18);
                            $objPHPExcel->getSheet($i)->getColumnDimension('L')->setWidth(14);
                            $objPHPExcel->getSheet($i)->getColumnDimension('M')->setWidth(14);
                            $objPHPExcel->getSheet($i)->getColumnDimension('N')->setWidth(19);
                            $objPHPExcel->getSheet($i)->getColumnDimension('O')->setWidth(17);

                break;
                case 1:
                    /** Encabezado hoja de ayuda */            
                        print_headerAyuda($objPHPExcel, $i, $properties);
                    /** Combinar celdas */
                        $objPHPExcel->getSheet($i)->mergeCells('B7:E7');
                        $objPHPExcel->getSheet($i)->mergeCells('B8:E8');
                        $objPHPExcel->getSheet($i)->mergeCells('B28:C28');
                        $objPHPExcel->getSheet($i)->mergeCells('B31:B33');
                        $objPHPExcel->getSheet($i)->mergeCells('B34:B35');
                        $objPHPExcel->getSheet($i)->mergeCells('B37:B40');
                        $objPHPExcel->getSheet($i)->mergeCells('B41:B44');

                        for($j=9; $j<=25; $j++){
                            $cellPosition = 'D'.$j.':E'.$j;
                            $objPHPExcel->getSheet($i)->mergeCells($cellPosition);
                        };
                    /** Altura de fiilas */
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('9')->setRowHeight(30);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('10')->setRowHeight(150);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('11')->setRowHeight(45);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('12')->setRowHeight(45);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('13')->setRowHeight(30);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('16')->setRowHeight(45);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('17')->setRowHeight(45);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('22')->setRowHeight(105);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('23')->setRowHeight(60);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('25')->setRowHeight(30);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('47')->setRowHeight(30);
                    /** Asignación de formatos a celdas y rengos */
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B7:E48")
                            ->applyFromArray($properties['fontCalibri'])
                            ->applyFromArray($properties['fontSize11'])
                            ->getAlignment()
                            ->setWrapText(true);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoGrisClaro'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B9:E9")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B29:C29")
                            ->applyFromArray($properties['fontCalibriBold'])
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B10:D25")
                            ->applyFromArray($properties['alignCenterVertical']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B28")
                            ->applyFromArray($properties['fondoGris'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B30:C48")
                            ->applyFromArray($properties['alignCenterVertical']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8:E25")
                            ->applyFromArray($properties['cellBorderThin'])
                            ->applyFromArray($properties['cellBorderColorBlack'])
                            ->applyFromArray($properties['cellOutlineBorderMedium']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B28:C48")
                            ->applyFromArray($properties['cellBorderThin'])
                            ->applyFromArray($properties['cellBorderColorBlack'])
                            ->applyFromArray($properties['cellOutlineBorderMedium']);

                    /** Función para agregar formato de color al asterisco de obligatoriedad */
                        function HelpTitles($text1,$text2,$order=2){
                            switch($order){
                                case 1:
                                    $objRichText = new PHPExcel_RichText();
                                    $run1 = $objRichText->createTextRun($text1);
                                    $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                    $run2 = $objRichText->createTextRun($text2);
                                    $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                break;
                                case 2:
                                default:
                                    $objRichText = new PHPExcel_RichText();
                                    $run1 = $objRichText->createTextRun($text1);
                                    $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                    $run2 = $objRichText->createTextRun($text2);
                                    $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                break;
                            }
                            return $objRichText;
                        }

                    /** Filas preliminares y encabezados */
                        $objPHPExcel->setActiveSheetIndex($i)
                            ->setCellValue('B7', HelpTitles('* ','Campos Obligatorios',1) )
                            ->setCellValue('B8', "INFORMACIÓN DEL FORMATO")
                            ->setCellValue('B9', "CAMPO")
                            ->setCellValue('C9', "DESCRIPCIÓN")
                            ->setCellValue('D9', "DILIGENCIAMIENTO \n Aplica para operación directa y operación asociada");

                    /**
                     * FILAS PRINCIPALES DE AYUDA
                     */
                        /**
                         *  Fila TIPO DE BAJA
                         */
                            /** Texto Descripción */
                                $objRichText_tipo_desc = new PHPExcel_RichText();
                                $objRichText_tipo_desc->createTextRun("Tipo de baja contable que puede ser registrada en SAP: ");
                                $objRichText_tipo_desc->createTextRun("BAJA TOTAL o BAJA PARCIAL")->getFont()->setBold(true);
                            /** Texto Diligenciamiento */
                                $objRichText_tipo_dilig = new PHPExcel_RichText();
                                $objRichText_tipo_dilig->createTextRun("• Baja ");
                                $objRichText_tipo_dilig->createTextRun("total ")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("de un ");
                                $objRichText_tipo_dilig->createTextRun("activo ")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("(principal + subnúmeros)")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun(": \n    - En la columna ");
                                $objRichText_tipo_dilig->createTextRun("\"TIPO DE BAJA\" ")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun("diligenciar ");
                                $objRichText_tipo_dilig->createTextRun("BAJA TOTAL")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun(" \n    - En la columna ");
                                $objRichText_tipo_dilig->createTextRun("\"SUBNÚMERO\" ")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun("relacionar solamente el activo principal (subnúmero 0). De este modo, en el sistema SAP se generará la baja del activo principal y sus subnúmeros \n• Baja ");
                                $objRichText_tipo_dilig->createTextRun("total ")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("de un ");
                                $objRichText_tipo_dilig->createTextRun("subnúmero:")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("\n    - En la columna ");
                                $objRichText_tipo_dilig->createTextRun("\"TIPO DE BAJA\"")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun("diligenciar ");
                                $objRichText_tipo_dilig->createTextRun("BAJA PARCIAL ")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("\n    - En la columna ");
                                $objRichText_tipo_dilig->createTextRun("\"SUBNÚMERO\" ")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun("relacionar solamente el subnúmero \n• Baja ");
                                $objRichText_tipo_dilig->createTextRun("parcial ")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("de un ");
                                $objRichText_tipo_dilig->createTextRun("activo ")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("(principal + subnúmero)")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun("\n    - En la columna ");
                                $objRichText_tipo_dilig->createTextRun("\"TIPO DE BAJA\" ")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun("diligenciar ");
                                $objRichText_tipo_dilig->createTextRun("BAJA PARCIAL")->getFont()->setBold(true);
                                $objRichText_tipo_dilig->createTextRun("\n    - En la columna");
                                $objRichText_tipo_dilig->createTextRun("\"SUBNÚMERO\" ")->getFont()->setItalic(true);
                                $objRichText_tipo_dilig->createTextRun(" relacionar solamente el subnúmero");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i)    
                                    ->setCellValue('B10', HelpTitles('TIPO DE BAJA ','*'))
                                    ->setCellValue('C10', $objRichText_tipo_desc)
                                    ->setCellValue('D10', $objRichText_tipo_dilig);
                        
                        /** 
                         * Fila CODIGO ACTIVO FIJO
                         */
                            /** Texto Descripción */
                                $objRichText_codigo_desc = new PHPExcel_RichText();
                                $objRichText_codigo_desc->createTextRun("Código de identificación del activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Texto Diligenciamiento */
                                $objRichText_codigo_dilig = new PHPExcel_RichText();
                                $objRichText_codigo_dilig->createTextRun("En la columna ");
                                $objRichText_codigo_dilig->createTextRun("\"CÓDIGO ACTIVO FIJO\" ")->getFont()->setItalic(true);
                                $objRichText_codigo_dilig->createTextRun("diligenciar el código de identificación del activo según el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B11', HelpTitles('CÓDIGO ACTIVO FIJO ','*'))
                                    ->setCellValue('C11', $objRichText_codigo_desc)
                                    ->setCellValue('D11', $objRichText_codigo_dilig);
                        /** 
                         * Fila SUBNÚMERO
                         */
                            /** Texto Descripción */
                                $objRichText_subn_desc = new PHPExcel_RichText();
                                $objRichText_subn_desc->createTextRun("Dígitos adicionales al código activo fijo de SAP");
                            /** Texto Diligenciamiento */
                                $objRichText_subn_dilig = new PHPExcel_RichText();
                                $objRichText_subn_dilig->createTextRun("En la columna ");
                                $objRichText_subn_dilig->createTextRun("\"SUBNÚMERO\" ")->getFont()->setItalic(true);
                                $objRichText_subn_dilig->createTextRun("diligenciar según sea el caso: \n    - El activo principal es identificado con el dígito ");
                                $objRichText_subn_dilig->createTextRun("0 ")->getFont()->setBold(true);
                                $objRichText_subn_dilig->createTextRun("\n    - Las capitalizaciones como mayor valor, adiciones o mejoras al activo principal son identificadas con un dígito igual o mayor a ");
                                $objRichText_subn_dilig->createTextRun("1 ")->getFont()->setBold(true);
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B12', HelpTitles('SUBNÚMERO ','*'))
                                    ->setCellValue('C12', $objRichText_subn_desc)
                                    ->setCellValue('D12', $objRichText_subn_dilig);
                        /** 
                         * Fila DENOMINACIÓN
                         */
                            /** Texto Descripción */
                                $objRichText_denom_desc = new PHPExcel_RichText();
                                $objRichText_denom_desc->createTextRun("Nombre o denominación del activo fijo");
                            /** Texto Diligenciamiento */
                                $objRichText_denom_dilig = new PHPExcel_RichText();
                                $objRichText_denom_dilig->createTextRun("En la columna ");
                                $objRichText_denom_dilig->createTextRun("\"DENOMINACIÓN ACTIVO FIJO\" ")->getFont()->setItalic(true);
                                $objRichText_denom_dilig->createTextRun("diligenciar nombre o denominación con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B13', HelpTitles('DENOMINACIÓN ACTIVO FIJO ','*'))
                                    ->setCellValue('C13', $objRichText_denom_desc)
                                    ->setCellValue('D13', $objRichText_denom_dilig);
                        /** 
                         * Fila INVENTARIO
                         */
                            /** Texto Descripción */
                                $objRichText_inv_desc = new PHPExcel_RichText();
                                $objRichText_inv_desc->createTextRun("Consecutivo placa de inventario que permite la identificación física del activo");
                            /** Texto Diligenciamiento */
                                $objRichText_inv_dilig = new PHPExcel_RichText();
                                $objRichText_inv_dilig->createTextRun("En la columna ");
                                $objRichText_inv_dilig->createTextRun("\"No. INVENTARIO\" ")->getFont()->setItalic(true);
                                $objRichText_inv_dilig->createTextRun("diligenciar el número de inventario con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B14', HelpTitles('No. INVENTARIO ','*'))
                                    ->setCellValue('C14', $objRichText_inv_desc)
                                    ->setCellValue('D14', $objRichText_inv_dilig);                                
                        /** 
                         * Fila SERIE   
                         */
                            /** Texto Descripción */
                                $objRichText_serie_desc = new PHPExcel_RichText();
                                $objRichText_serie_desc->createTextRun("Consecutivo único e irrepetible del fabricante que permite la identificación del activo");
                            /** Texto Diligenciamiento */
                                $objRichText_serie_dilig = new PHPExcel_RichText();
                                $objRichText_serie_dilig->createTextRun("En la columna ");
                                $objRichText_serie_dilig->createTextRun("\"SERIE\" ")->getFont()->setItalic(true);
                                $objRichText_serie_dilig->createTextRun("diligenciar el número serie con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B15', HelpTitles('SERIE ','*'))
                                    ->setCellValue('C15', $objRichText_serie_desc)
                                    ->setCellValue('D15', $objRichText_serie_dilig);                                
                        /** 
                         * Fila TAG   
                         */
                            /** Texto Descripción */
                                $objRichText_tag_desc = new PHPExcel_RichText();
                                $objRichText_tag_desc->createTextRun("Código de identificación física asignada por la operación para efectos de mantenimiento");
                            /** Texto Diligenciamiento */
                                $objRichText_tag_dilig = new PHPExcel_RichText();
                                $objRichText_tag_dilig->createTextRun("En la columna ");
                                $objRichText_tag_dilig->createTextRun("\"TAG\" ")->getFont()->setItalic(true);
                                $objRichText_tag_dilig->createTextRun("diligenciar el TAG con el cual está identificado el activo en el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B16', "TAG")
                                    ->setCellValue('C16', $objRichText_tag_desc)
                                    ->setCellValue('D16', $objRichText_tag_dilig);   
                        /** 
                         * Fila CECO   
                         */
                            /** Texto Descripción */
                                $objRichText_ceco_desc = new PHPExcel_RichText();
                                $objRichText_ceco_desc->createTextRun("Centro de costo responsable del activo, el cual, asume los costos o gastos por depreciación");
                            /** Texto Diligenciamiento */
                                $objRichText_ceco_dilig = new PHPExcel_RichText();
                                $objRichText_ceco_dilig->createTextRun("En la columna ");
                                $objRichText_ceco_dilig->createTextRun("\"COD. CECO\" ")->getFont()->setItalic(true);
                                $objRichText_ceco_dilig->createTextRun("diligenciar el centro de costo asignado al activo de acuerdo con el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B17', HelpTitles('COD. CECO ','*'))
                                    ->setCellValue('C17', $objRichText_ceco_desc)
                                    ->setCellValue('D17', $objRichText_ceco_dilig);   
                        /** 
                         * Fila VICEPRESIDENCIA   
                         */
                            /** Texto Descripción */
                                $objRichText_vice_desc = new PHPExcel_RichText();
                                $objRichText_vice_desc->createTextRun("Vicepresidencia dada por la asignación del centro de costo");
                            /** Texto Diligenciamiento */
                                $objRichText_vice_dilig = new PHPExcel_RichText();
                                $objRichText_vice_dilig->createTextRun("En la columna ");
                                $objRichText_vice_dilig->createTextRun("\"SIGLA VICEPRESIDENCIA\" ")->getFont()->setItalic(true);
                                $objRichText_vice_dilig->createTextRun("diligenciar la sigla de la Vicepresidencia según el centro de costo del activo, de acuerdo con la ");
                                $objRichText_vice_dilig->createTextRun("\"Tabla_Jerarquizacion\" ")->getFont()->setItalic(true);
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B18', HelpTitles('SIGLA VICEPRESIDENCIA ','*'))
                                    ->setCellValue('C18', $objRichText_vice_desc)
                                    ->setCellValue('D18', $objRichText_vice_dilig);   
                        /** 
                         * Fila GERENCIA   
                         */
                            /** Texto Descripción */
                                $objRichText_ger_desc = new PHPExcel_RichText();
                                $objRichText_ger_desc->createTextRun("Gerencia o Departamento dada por la asignación del centro de costo");
                            /** Texto Diligenciamiento */
                                $objRichText_ger_dilig = new PHPExcel_RichText();
                                $objRichText_ger_dilig->createTextRun("En la columna ");
                                $objRichText_ger_dilig->createTextRun("\"SIGLA GERENCIA Y/O DPTO\" ")->getFont()->setItalic(true);
                                $objRichText_ger_dilig->createTextRun("diligenciar la sigla de la Gerencia o Departamento según el centro de costo del activo, de acuerdo con la ");
                                $objRichText_ger_dilig->createTextRun("\"Tabla_Jerarquizacion\" ")->getFont()->setItalic(true);
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B19', HelpTitles('SIGLA GERENCIA Y/O DPTO. ','*'))
                                    ->setCellValue('C19', $objRichText_ger_desc)
                                    ->setCellValue('D19', $objRichText_ger_dilig);   
                        /** 
                         * Fila MUNICIPIO   
                         */
                            /** Texto Descripción */
                                $objRichText_mun_desc = new PHPExcel_RichText();
                                $objRichText_mun_desc->createTextRun("Código de la entidad administrativa que puede agrupar una o varias localidades");
                            /** Texto Diligenciamiento */
                                $objRichText_mun_dilig = new PHPExcel_RichText();
                                $objRichText_mun_dilig->createTextRun("En la columna ");
                                $objRichText_mun_dilig->createTextRun("\"COD. MUNICIPIO\" ")->getFont()->setItalic(true);
                                $objRichText_mun_dilig->createTextRun("diligenciar el código del municipio asignado al activo de acuerdo con el auxiliar de activos fijos de Ecopetrol en SAP");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B20', HelpTitles('COD. MUNICIPIO ','*'))
                                    ->setCellValue('C20', $objRichText_mun_desc)
                                    ->setCellValue('D20', $objRichText_mun_dilig);   
                        /** 
                         * Fila NOMBRE MUNICIPIO    
                         */
                            /** Texto Descripción */
                                $objRichText_noMun_desc = new PHPExcel_RichText();
                                $objRichText_noMun_desc->createTextRun("Nombre de la entidad administrativa que puede agrupar una o varias localidades");
                            /** Texto Diligenciamiento */
                                $objRichText_noMun_dilig = new PHPExcel_RichText();
                                $objRichText_noMun_dilig->createTextRun("En la columna ");
                                $objRichText_noMun_dilig->createTextRun("\"NOMBRE MUNICIPIO\" ")->getFont()->setItalic(true);
                                $objRichText_noMun_dilig->createTextRun("diligenciar el nombre del municipio según el código de municipio asignado al activo fijo, de acuerdo con la ");
                                $objRichText_noMun_dilig->createTextRun("\"Tabla_Municipios\" ")->getFont()->setItalic(true);
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B21', HelpTitles('NOMBRE MUNICIPIO ','*'))
                                    ->setCellValue('C21', $objRichText_noMun_desc)
                                    ->setCellValue('D21', $objRichText_noMun_dilig);   
                        /** 
                         * Fila VALOR DE BAJA   
                         */
                            /** Texto Descripción */
                                $objRichText_valor_desc = new PHPExcel_RichText();
                                $objRichText_valor_desc->createTextRun("Valor de adquisición en pesos correspondiente a la baja");
                            /** Texto Diligenciamiento */
                                $objRichText_valor_dilig = new PHPExcel_RichText();
                                    $objRichText_valor_dilig->createTextRun("• Baja ");
                                    $objRichText_valor_dilig->createTextRun("total ")->getFont()->setBold(true);
                                    $objRichText_valor_dilig->createTextRun("de un ");
                                    $objRichText_valor_dilig->createTextRun("activo ")->getFont()->setBold(true);
                                    $objRichText_valor_dilig->createTextRun("(principal + subnúmeros)")->getFont()->setItalic(true);
                                    $objRichText_valor_dilig->createTextRun(": \n    - En la columna ");
                                    $objRichText_valor_dilig->createTextRun("\"VALOR DE BAJA\" ")->getFont()->setItalic(true);
                                    $objRichText_valor_dilig->createTextRun("dejar vacía \n• Baja ");
                                    $objRichText_valor_dilig->createTextRun("total ")->getFont()->setBold(true);
                                    $objRichText_valor_dilig->createTextRun("de un ");
                                    $objRichText_valor_dilig->createTextRun("subnúmero:")->getFont()->setBold(true);
                                    $objRichText_valor_dilig->createTextRun("\n    - En la columna ");
                                    $objRichText_valor_dilig->createTextRun("\"VALOR DE BAJA\" ")->getFont()->setItalic(true);
                                    $objRichText_valor_dilig->createTextRun("dejar vacía ");
                                    $objRichText_valor_dilig->createTextRun("\n• Baja ");
                                    $objRichText_valor_dilig->createTextRun("parcial ")->getFont()->setBold(true);
                                    $objRichText_valor_dilig->createTextRun("de un ");
                                    $objRichText_valor_dilig->createTextRun("activo ")->getFont()->setBold(true);
                                    $objRichText_valor_dilig->createTextRun("(principal + subnúmero)")->getFont()->setItalic(true);
                                    $objRichText_valor_dilig->createTextRun("\n    - En la columna ");
                                    $objRichText_valor_dilig->createTextRun("\"VALOR DE BAJA\" ")->getFont()->setItalic(true);
                                    $objRichText_valor_dilig->createTextRun("diligenciar el valor de adquisición en pesos correspondiente a la baja parcial \n    - Se debe asegurar que el valor restante de adquisición con el cual quedará el activo en SAP sea razonable ");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i)
                                    ->setCellValue('B22', HelpTitles('VALOR DE BAJA ','*'))
                                    ->setCellValue('C22', $objRichText_valor_desc)
                                    ->setCellValue('D22', $objRichText_valor_dilig); 
                        /** 
                         * Fila MOTIVO DE RETIRO   
                         */
                            /** Texto Descripción */
                                $objRichText_motivo_desc = new PHPExcel_RichText();
                                $objRichText_motivo_desc->createTextRun("Razón por la cual la operación procede con el retiro del activo, teniendo en cuenta el estado físico y funcionabilidad del mismo");
                            /** Texto Diligenciamiento */
                                $objRichText_motivo_dilig = new PHPExcel_RichText();
                                $objRichText_motivo_dilig->createTextRun("En la columna ");
                                $objRichText_motivo_dilig->createTextRun("\"MOTIVO DE RETIRO\" ")->getFont()->setItalic(true);
                                $objRichText_motivo_dilig->createTextRun("diligenciar según la tabla ");
                                $objRichText_motivo_dilig->createTextRun("Anexo 1.1 ")->getFont()->setBold(true);
                                $objRichText_motivo_dilig->createTextRun("y ver detalle de cada motivo del retiro en el procedimiento ");
                                $objRichText_motivo_dilig->createTextRun("\"GFI-P-030 Control Administrativo de Activos Fijos\"")->getFont()->setItalic(true);
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B23', HelpTitles('MOTIVO DE RETIRO ','*'))
                                    ->setCellValue('C23', $objRichText_motivo_desc)
                                    ->setCellValue('D23', $objRichText_motivo_dilig);   
                        /** 
                         * Fila DISPOSICIÓN FINAL  
                         */
                            /** Texto Descripción */
                                $objRichText_disp_desc = new PHPExcel_RichText();
                                $objRichText_disp_desc->createTextRun("Destino final que surtirá el activo teniendo en cuenta el mitivo de retiro");
                            /** Texto Diligenciamiento */
                                $objRichText_disp_dilig = new PHPExcel_RichText();
                                $objRichText_disp_dilig->createTextRun("En la columna ");
                                $objRichText_disp_dilig->createTextRun("\"DISPOSICIÓN FINAL PROPUESTA\" ")->getFont()->setItalic(true);
                                $objRichText_disp_dilig->createTextRun("diligenciar según la tabla ");
                                $objRichText_disp_dilig->createTextRun("Anexo 1.1 ")->getFont()->setBold(true);
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B24', HelpTitles('DISPOSICIÓN FINAL PROPUESTA ','*'))
                                    ->setCellValue('C24', $objRichText_disp_desc)
                                    ->setCellValue('D24', $objRichText_disp_dilig);  
                        /** 
                         * Fila OBSERVACIONES  
                         */
                            /** Texto Descripción */
                                $objRichText_obs_desc = new PHPExcel_RichText();
                                $objRichText_obs_desc->createTextRun("Comentarios relevantes o aclaraciones");
                            /** Texto Diligenciamiento */
                                $objRichText_obs_dilig = new PHPExcel_RichText();
                                $objRichText_obs_dilig->createTextRun("En la columna ");
                                $objRichText_obs_dilig->createTextRun("\"OBSERVACIONES\" ")->getFont()->setItalic(true);
                                $objRichText_obs_dilig->createTextRun("diligenciar los comentarios relevantes o aclaraciones  sobre los activos objeto de retiro. \nIgualmente, se puede diligenciar información complementaria sobre los motivos del retiro de los activos");
                            /** Insertar textos */
                                $objPHPExcel->setActiveSheetIndex($i) 
                                    ->setCellValue('B25', HelpTitles('OBSERVACIONES ','*'))
                                    ->setCellValue('C25', $objRichText_obs_desc)
                                    ->setCellValue('D25', $objRichText_obs_dilig);  
                     /** 
                     * Tabla Anexo 1.1  
                     */    
                        $objPHPExcel->setActiveSheetIndex($i)
                            ->setCellValue('B28', "Anexo 1.1")
                            ->setCellValue('B29', "MOTIVO DE RETIRO")
                            ->setCellValue('C29', "DISPOSICIÓN FINAL")
                            ->setCellValue('B30', "DACIÓN EN PAGO")
                            ->setCellValue('C30', "ENTREGA DEL ACTIVO")
                            ->setCellValue('B31', "DEMOLICIÓN DE CO0NSTRUCCIONES O DESMANTELAMIENTO DE FACILIDADES")
                            ->setCellValue('C31', "ENTREGA DEL ACTIVO")
                            ->setCellValue('C32', "VENTA COMO CHATARRA")
                            ->setCellValue('C33', "RESIDUO NO APROVECHABLE")
                            ->setCellValue('B34', "DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO")
                            ->setCellValue('C34', "VENTA COMO CHATARRA")
                            ->setCellValue('C35', "RESIDUO NO APROVECHABLE")
                            ->setCellValue('B36', "ESCISIÓN")
                            ->setCellValue('C36', "ENTREGA DEL ACTIVO")
                            ->setCellValue('B37', "NO SE REQUIERE PARA LA OPERACIÓN")
                            ->setCellValue('C37', "VENTA COMO ACTIVO FUNCIONAL")
                            ->setCellValue('C38', "CESIÓN SIN COSTO")
                            ->setCellValue('C39', "ABANDONO TÉCNICO")
                            ->setCellValue('C40', "VENTA COMO CHATARRA")
                            ->setCellValue('B41', "OBSOLESCENCIA O DESUSO")
                            ->setCellValue('C41', "VENTA COMO ACTIVO FUNCIONAL")
                            ->setCellValue('C42', "VENTA COMO CHATARRA")
                            ->setCellValue('C43', "RESIDUO NO APROVECHABLE")
                            ->setCellValue('C44', "NO APLICA. FINALIZACIÓN USO ACTIVO INTANGIBLE")
                            ->setCellValue('B45', "PERDIDA O HURTO")
                            ->setCellValue('C45', "NO APLICA")
                            ->setCellValue('B46', "PERMUTA O INTERCAMBIO")
                            ->setCellValue('C46', "ENTREGA DEL ACTIVO")
                            ->setCellValue('B47', "VENTA POR DIRECTRIZ DEL NEGOCIO")
                            ->setCellValue('C47', "ENTREGA DEL ACTIVO")
                            ->setCellValue('B48', "SANEAMIENTO CONTABLE")
                            ->setCellValue('C48', "NO APLICA");

                break;
            }
            $i++;
        }
    /*remueve la ultima hoja creada por defecto*/
        $objPHPExcel->removeSheetByIndex(2);
    /*PARENT'S ROWS*/
        /** CAPTURAR INFORMACION VIA POST */
            $grupo = $_POST['jsonExcel'];
            $obj = json_decode($grupo, true);
            // print_r($obj);
            // $grupo2 = $_POST['jsonExcelChilds'];
            // $obj2 = json_decode($grupo2, true);
        /** Recorrer el objeto recibido para poblar el archivo excel */
            $vShift = 7;
            for($i=0; $i<sizeof($obj); $i++){
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.($i+$vShift), $obj[$i]['tipo_baja'])
                ->setCellValue('C'.($i+$vShift), $obj[$i]['activo'])
                ->setCellValue('D'.($i+$vShift), $obj[$i]['subnumero'])
                ->setCellValue('E'.($i+$vShift), $obj[$i]['denominacion'])
                ->setCellValue('F'.($i+$vShift), $obj[$i]['inventario'])
                ->setCellValue('G'.($i+$vShift), $obj[$i]['serie'])
                ->setCellValue('H'.($i+$vShift), $obj[$i]['tag'])
                ->setCellValue('I'.($i+$vShift), $obj[$i]['ceco'])
                ->setCellValue('J'.($i+$vShift), $obj[$i]['sigla_vicepresidencia'])
                ->setCellValue('K'.($i+$vShift), $obj[$i]['sigla_gerencia'])
                ->setCellValue('L'.($i+$vShift), $obj[$i]['valor_baja'])
                ->setCellValue('M'.($i+$vShift), $obj[$i]['motivo_retiro'])
                ->setCellValue('N'.($i+$vShift), $obj[$i]['disposicion_final'])
                ->setCellValue('O'.($i+$vShift), $obj[$i]['observaciones']);
            };

            $vShiftTotal = "B".$vShift.":O".($vShift + sizeof($obj) - 1) ;
            $vShiftFormat = "L".$vShift.":L".($vShift + sizeof($obj) - 1) ;
            $vShiftGlobal = "B2:O".($vShift + sizeof($obj) - 1) ;
            // print_r($vShiftTotal);
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftTotal)
                ->applyFromArray($properties['cellBorderColorGray']);
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftFormat)
                ->getNumberFormat()
                ->setFormatCode('$ #,##0');
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftGlobal)
                ->applyFromArray($properties['cellOutlineBorderMedium']);
    /** Renombrar Hoja */
        $objPHPExcel->getSheet(0)
                    ->setTitle('Formato APR')
                    ->freezePaneByColumnAndRow(0,7);
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getSheet(1)
            ->setTitle('Ayuda');
    
    /** mensajes adicionales */
        $vRow = $vShift + sizeof($obj) + 1;
        $vShiftMessage = "B".($vShift + sizeof($obj) + 3).":O".($vShift + sizeof($obj) + 4) ;
        
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("B".($vShift + sizeof($obj) + 1), "* Campos Obligatorios");
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("B".($vShift + sizeof($obj) + 3), "Todos los derechos reservados para Ecopetrol S.A. Ninguna reproducción externa copia o transmisión digital de esta publicación puede ser hecha sin permiso escrito. Ningún párrafo de esta publicación puede ser reproducido, copiado o transmitido digitalmente sin un consentimiento escrito o de acuerdo con las leyes que regulan los derechos de autor y con base en la regulación vigente.");
        $objPHPExcel->getSheet(0)
            ->mergeCells($vShiftMessage)
            ->getStyle($vShiftMessage)
            ->getAlignment()
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ->setWrapText(true);
        $objPHPExcel->getSheet(0)
            ->getStyle($vShiftMessage)
            ->applyFromArray($properties['cellBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getRowDimension($vRow)->setRowHeight(13);

    /** AGREGAR SEGURIDAD A LA HOJA 1 CON CONTRASEÑA */

        $objPHPExcel->getActiveSheet()->getProtection()->setSheet(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setSort(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertColumns(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setInsertRows(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setFormatCells(true);
        $objPHPExcel->getActiveSheet()->getProtection()->setPassword('password');

    /** Borrar todos los archivos excel previamente generados */    
        $files = glob('xls/bajas/*'); // get all file names
        foreach($files as $file){ // iterate files
          if(is_file($file))
            unlink($file); // delete file
        };
    /** Nombrar y generar el archivo XLS */
        header('Content-Type: application/openxmlformats-officedocument.spreadsheetml.sheet');
        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $route = 'xls/bajas/GFI-F-055-';
        $datenow = date('YmdHis');
        $extension = '.xlsx';
        $name = $route . $datenow . $extension;
        $objWriter->save($name);
        $objName = array(
            'route' => $name,
            'name' => 'GFI-F-055-autorizacion-retiro-de-activos-fijos-de-operacion-'.$datenow.$extension
        );
        die( json_encode( $objName ) );
?>