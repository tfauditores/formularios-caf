<?php
    if(isset($_POST['get_option']) || isset($_GET['get_option']))
    {
        require_once('conexion.php');

        if(isset($_POST['get_option'])){
            $ceco = $_POST['get_option'];
        }
        if(isset($_GET['get_option'])){
            $ceco = $_GET['get_option'];
        }
        $query = "  SELECT nombreCeco, descripcionCeco, area, vicepresidencia, gerencia
                    FROM jerarquizacion
                    WHERE ceco='$ceco'
        ";
        $results=mysqli_query($con, $query);
        $numResults = mysqli_num_rows($results);
        if ($numResults > 0) {
            while($row=mysqli_fetch_array($results))
            {
                $data = [   'ceco' => $ceco,
                            'nombreCeco' => $row['nombreCeco'],
                            'descripcionCeco' => $row['descripcionCeco'],
                            'area' => $row['area'],
                            'vicepresidencia' => $row['vicepresidencia'],
                            'gerencia' => $row['gerencia']
                         ];
                header('Content-type: application/json');
                echo json_encode( $data );
            }
        }
        else{
            $data = [ 'result' => 'invalid data' ] ;
            header('Content-type: application/json');
            echo json_encode( $data );
        }
        exit;
    }
?>