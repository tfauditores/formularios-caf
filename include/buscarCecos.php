<?php
    require_once('conexion.php');

    $query = "  SELECT  c.ceco, c.nombre
                FROM cecos AS c
                WHERE 1
    ";
    $results=mysqli_query($con, $query);
    $numResults = mysqli_num_rows($results);
    if ($numResults > 0) {
        $data = array();
        while($row=mysqli_fetch_array($results  ))
        {
            $dataTemp = [   
                'ceco' => $row['ceco'],
                'nombre' => $row['nombre']
            ];
            array_push($data,$dataTemp);
        }
    }
    else{
        $data = [ 'result' => 'invalid query' ] ;
    }
    header('Content-type: application/json');
    http_response_code(200);
    echo json_encode( $data );
?>