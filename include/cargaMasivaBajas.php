<?php
    /*Manejo de consulta en metodo POST*/
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        /** Error reporting */
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('Europe/London');

            // var_dump($_FILES);
            $unwanted_array = array(    
                'Š', 'š', 'Ž', 'ž', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ý', 'þ', 'ÿ'
            );
            $wanted_array = array(
                'S', 's', 'Z', 'z', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'Ss', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'b', 'y'
            );
            $file = str_replace($unwanted_array, $wanted_array, $_FILES['excel']['name']);
            // $file = iconv('UTF-8','ASCII//TRANSLIT', $_FILES['excel']['name']);
            $file = preg_replace ( '/[ ]/i' , '_' , $file );
            $datenow = date('YmdHis');
            $ext = end((explode(".", $_FILES['excel']['name'])));
            
            if(!is_dir('temp/bajas')){
                if(!is_dir('temp')){
                    mkdir('temp',0755);
                }
                mkdir('temp/bajas',0755);
            }
            
            $path = 'temp/bajas/'. basename($datenow.'-'.$file);
            move_uploaded_file($_FILES['excel']['tmp_name'], $path);

        /** Include PHPExcel */
            require_once 'Classes/PHPExcel.php';
            
            switch($ext){
                case 'xls':
                case 'XLS':
                    $objReader = PHPExcel_IOFactory::createReader('Excel5');
                break;
                case 'xlsx':
                case 'XLSX':
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                break;
                default:
                    die;
            }
            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($path);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $data = [];
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                array_push($data, $rowData);
            }

            array_push($data, $path);

            /*Se envia la información en formato JSON*/
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
            // echo $file;
    }
    /*Manejo de consulta en metodo GET*/
    else if($_SERVER['REQUEST_METHOD'] === 'GET'){
        require_once('conexion.php');

        isset($_GET['activo']) ? $activo = $_GET['activo'] : $activo = '';
        isset($_GET['subnumero']) && $_GET['subnumero'] != "" ? $subnumero = $_GET['subnumero'] : $subnumero = 0;
        isset($_GET['inventario']) ? $inventario = $_GET['inventario'] : $inventario = '';

        if( strlen( trim($activo) ) > 0){
            
            $query = "SELECT a.activo, a.denominacion, a.noInventario, a.serie, a.ceco, a.sn, a.valorAdquisicionAr01
                    FROM auxiliar AS a
                    WHERE a.activo = $activo AND a.sn = $subnumero";

            $count = "SELECT COUNT(a.activo) FROM auxiliar AS a WHERE a.activo LIKE '$activo'";
            $count = mysqli_query($con, $count);
            $count = mysqli_fetch_array($count)[0] - 1;

        }
        else if( strlen( trim($inventario) ) > 0){
            
            $query = "SELECT a.activo, a.denominacion, a.noInventario, a.serie, a.ceco, a.sn, a.valorAdquisicionAr01
                    FROM auxiliar AS a
                    WHERE a.noInventario = $inventario AND a.sn = $subnumero";

            $count = "SELECT COUNT(a.noInventario) FROM auxiliar AS a WHERE a.noInventario LIKE '$inventario'";
            $count = mysqli_query($con, $count);
            $count = mysqli_fetch_array($count)[0] - 1;

        }

        $results = mysqli_query($con, $query);
        if($results){
            $numResults = mysqli_num_rows($results);
    
            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_array($results) ){
                    $dataTemp = [
                        'activo' => $row['activo'],
                        'sn' => $row['sn'],
                        'denominacion' => $row['denominacion'],
                        'inventario' => $row['noInventario'],
                        'serie' => $row['serie'],
                        'ceco' => $row['ceco'],
                        'valorAdquisicion' => $row['valorAdquisicionAr01'],
                        'countSubnumbers' => $count
                    ];
                    array_push($data,$dataTemp);
                }
            }
            else{
                $data = [ 'result' => 'invalid active' ] ;
            }
        }
        else{
            $data = [ 'result' => 'invalid active' ] ;
        }
        /*Se envia la información en formato JSON*/
        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode( $data );
    }