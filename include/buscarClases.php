<?php
    require_once('conexion.php');

    if(isset($_GET['type']) && $_GET['type'] == 'json'){

        if(isset($_GET['option']) && $_GET['option'] == 'asociadas'){
            //$query = "SELECT c.clase, c.nombreClase FROM clase_clave AS c WHERE c.asociadas = 1 ORDER BY c.nombreClase ASC";
            $query = "SELECT c.clase, c.nombreClase FROM clase_clave AS c WHERE 1 ORDER BY c.nombreClase ASC";
        }
        else{
            //$query = "SELECT c.clase, c.nombreClase, c.asociadas FROM clase_clave AS c WHERE 1 ORDER BY c.nombreClase ASC";
            $query = "SELECT c.clase, c.nombreClase FROM clase_clave AS c WHERE 1 ORDER BY c.nombreClase ASC";
        }

        $results=mysqli_query($con, $query);
        $numResults = mysqli_num_rows($results);
        if ($numResults > 0) {
            $data = array();
            while($row=mysqli_fetch_array($results  ))
            {
                $dataTemp = [   
                    'clase' => $row['clase'],
                    'nombre' => $row['nombreClase'],
                ];
                array_push($data,$dataTemp);
            }
        }
        else{
            $data = [ 'result' => 'invalid query' ] ;
        }
        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode( $data );

        exit;
    }
    else{

        if(isset($_GET['option']) && $_GET['option'] == 'asociadas'){
            // $query = "SELECT c.clase, c.nombreClase FROM clase_clave AS c WHERE c.asociadas = 1 ORDER BY c.nombreClase ASC";
            $query = "SELECT c.clase, c.nombreClase FROM clase_clave AS c WHERE 1 ORDER BY c.nombreClase ASC";
        }
        else{
            $query = "SELECT c.clase, c.nombreClase FROM clase_clave AS c WHERE 1 ORDER BY c.nombreClase ASC";
        }
        $find=mysqli_query($con, $query);
    
        while($row = mysqli_fetch_array($find))
        {
            echo "<option value=\"". $row['clase'] ."\">".$row['nombreClase']." (".$row['clase'].")</option>";
        }
        exit;

    }
?>