<?php
    function postQuery($kind, $format){
        require_once('conexion.php');

        $operacion =  $kind['operacion'];

        $queryDirecta = "   SELECT DISTINCT UNI.unidad, UNI.nombre, CAT.clase 
                            FROM `unidades` AS UNI 
                            LEFT JOIN catalogo AS CAT 
                            ON UNI.unidad = CAT.unidad
                            WHERE tipo = 'catalogo'";
        if( isset($kind['clase']) && $kind['clase']>0 ){
            $clase = $kind['clase'];
            $queryDirecta .= " AND clase = '$clase' ";
        }
        $queryDirecta .= "
                            ORDER BY UNI.unidad ASC
                        ";
        $queryAsociadas = " SELECT DISTINCT UNI.unidad, UNI.nombre
                            FROM `unidades` AS UNI
                            WHERE UNI.tipo = 'sap'
                            ORDER BY UNI.unidad ASC
                        ";

        $query =  ( $operacion=="directa") ? $queryDirecta : $queryAsociadas ;

        $find = mysqli_query($con, $query);

        if($format == 'json'){
            $numResults = mysqli_num_rows($find);
            if ($numResults > 0) {
                $data = array();
                while($row=mysqli_fetch_array($find))
                {
                    $dataTemp = [   
                        'unidad' => $row['unidad'],
                        'nombre' => $row['nombre']
                    ];
                    array_push($data,$dataTemp);
                }
            }
            else{
                $data = [ 'result' => 'invalid query' ] ;
            }
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
        }
        else{
            while($row=mysqli_fetch_array($find))
            {
                echo "<option value=\"". $row['unidad'] ."\">".  $row['nombre']. " (" .$row['unidad']. ")" ."</option>";
            }
            exit;
        }
    }
    if(isset($_POST['operacion']))
    {   
        isset($_POST['type']) ? $type = $_POST['type'] : $type = '';
        
        postQuery($_POST, $type);
    }
    if(isset($_GET['operacion']))
    {
        isset($_GET['type']) ? $type = $_GET['type'] : $type = '';

        postQuery($_GET, $type);
    }
    


?>