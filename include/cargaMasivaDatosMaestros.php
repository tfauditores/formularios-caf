<?php
    /*Manejo de consulta en metodo POST*/
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        /** Error reporting */
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('America/Bogota');

            // var_dump($_FILES);
            $unwanted_array = array(    
                'Š', 'š', 'Ž', 'ž', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ý', 'þ', 'ÿ'
            );
            $wanted_array = array(
                'S', 's', 'Z', 'z', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'Ss', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'b', 'y'
            );
            $file = str_replace($unwanted_array, $wanted_array, $_FILES['excel']['name']);
            // $file = iconv('UTF-8','ASCII//TRANSLIT', $_FILES['excel']['name']);
            $file = preg_replace ( '/[ ]/i' , '_' , $file );
            $datenow = date('YmdHis');
            $ext = end((explode(".", $_FILES['excel']['name'])));
            
            if(!is_dir('temp/datosTecnicos')){
                if(!is_dir('temp')){
                    mkdir('temp',0755);
                }
                mkdir('temp/datosTecnicos',0755);
            }
            $path = 'temp/datosTecnicos/'. basename($datenow.'-'.$file);
            move_uploaded_file($_FILES['excel']['tmp_name'], $path);
        /** Include PHPExcel */
            require_once 'Classes/PHPExcel.php';
            
            switch($ext){
                case 'xls':
                case 'XLS':
                    $objReader = PHPExcel_IOFactory::createReader('Excel5');
                break;
                case 'xlsx':
                case 'XLSX':
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                break;
                default:
                    die;
            }
            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($path);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $data = [];
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);

                // aplica formato fecha para el dato que se lee en la columna "FECHA INICIAL PARA VIDA ÚTIL REMANENTE **"
                if(isset($rowData[0][32]) && gettype($rowData[0][32]) == 'double'){
                    $rowData[0][32] = date('d/m/Y',PHPExcel_Shared_Date::ExcelToPHP($sheet->getCellByColumnAndRow(32, $row)->getValue()));
                }
                
                array_push($data, $rowData);
            }
            

            array_push($data, $path);
            /*Se envia la información en formato JSON*/
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
    }