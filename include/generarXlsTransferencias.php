<?php
    /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');
     /** Include PHPExcel */
        require_once 'Classes/PHPExcel.php';
    /** Crea un nuevo objeto PHPExcel */
        $objPHPExcel = new PHPExcel();
    /** Establecer propiedades del documento a generar*/
        $objPHPExcel->getProperties()
        ->setCompany("TF AUDITORES")
        ->setCreator("TF AUDITORES")
        ->setLastModifiedBy("TF AUDITORES")
        ->setTitle("TRANSFERENCIAS")
        ->setSubject("TRANSFERENCIAS")
        ->setDescription("FORMATO DE TRANSFERENCIAS")
        ->setKeywords("TFAUDITORES, TRANSFERENCIAS, GFI-088")
        ->setCategory("SERVICIOS");
    /** Formatos de celdas  */
        $fontVerdana = array(
            'font' => array(
                'bold' => true,
                'size' => 9.5,
                'name' => 'Verdana'
            )
        );
        $fontCalibri = array(
            'font' => array(
                'bold' => true,
                'size' => 10,
                'name' => 'Calibri'
            )
        );
        $fontCalibri2 = array(
            'font' => array(
                'bold' => false,
                'size' => 10,
                'name' => 'Calibri'
            )
        );
        $fontSize14 = array(
            'font' => array(
                'size' => 14
            )
        );
        $fontSize12 = array(
            'font' => array(
                'size' => 12
            )
        );
        $fontSize11 = array(
            'font' => array(
                'size' => 11
            )
        );
        $fontColorWhite = array(
            'font' => array(
                'color' => array('rgb' => 'ffffff')
            )
        );
        $fontColorRed = array(
            'font' => array(
                'color' => array('rgb' => 'ff0000')
            )
        );
        $fontColorGreen = array(
            'font' => array(
                'color' => array('rgb' => '004237')
            )
        );
        $fondoVerde = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'C9D200')
            )
        );
        $fondoVerdeLight = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'A9D08e')
            )
        );
        $fondoVerdeOscuro = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => '004238')
            )
        );
        $fondoGris = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'E6E6E6')
            )
        );
        $fondoGris2 = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'c3c4c6')
            )
        );
        $fondoColorPiel = array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'EEECE1')
            )
        );
        $alignCenter = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            )
        );
        $alignCenterHorizontal = array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'wrap' => true
            )
        );
        $alignCenterVertical = array(
            'alignment' => array(
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                'wrap' => true
            )
        );
        $cellBorderDouble = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $cellBorderThin = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $cellBorderMedium = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => '000000')
                ),
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => 'ffffff')
                )
            )
        );
        $cellBorderOutline = array(
            'borders' => array(
                'outline' => array(
                  'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                  'color' => array('rgb' => '000000')
                )
              )
        );
        $cellAllBorderMedium = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $cellBorderThinTop = array(
            'borders' => array(
                'top' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $cellBorderThinBottom = array(
            'borders' => array(
                'bottom' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $cellBorderColorWhite = array(
            'borders' => array(
                'allborders' => array(
                    'color' => array('rgb' => 'ffffff')
                )
            )
        );
        $cellBorderColorGreen = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '9bbb59')
                )
            )
        );
        $cellBorderColorBlack = array(
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('rgb' => '000000')
                )
            )
        );
        $cellOutlineBorderMedium = array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => array('rgb' => '000000')
                )
            )
        );

    /** PAGINA(0) DE CONTENIDO GENERADO */        
        /** Crear pagina*/
            $objPHPExcel->createSheet(0);
        /** Combinar celdas */
            $objPHPExcel->getSheet(0)->mergeCells('B2:C4'); //LOGO
            $objPHPExcel->getSheet(0)->mergeCells('D2:Q2'); //FORMATO
            $objPHPExcel->getSheet(0)->mergeCells('D3:Q3');
            $objPHPExcel->getSheet(0)->mergeCells('E4:P4'); //VERSION
            $objPHPExcel->getSheet(0)->mergeCells('C5:D5'); //FECHA
        /** Asignación de formatos a celdas y rangos */
            $objPHPExcel->getDefaultStyle()
                ->applyFromArray($cellBorderThin)
                ->applyFromArray($cellBorderColorWhite);
            $objPHPExcel->getSheet(0)
                ->getStyle("B2:Q4")
                ->applyFromArray($alignCenter)
                ->applyFromArray($cellAllBorderMedium);
            $objPHPExcel->getSheet(0)
                ->getStyle("D2:D3")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontSize12);
            $objPHPExcel->getSheet(0)
                ->getStyle("D4:Q4")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontSize11);
            $objPHPExcel->getSheet(0)
                ->getStyle("B5:Q5")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontSize11);
            $objPHPExcel->getSheet(0)
                ->getColumnDimensionByColumn('A')
                ->setWidth('1');
            $objPHPExcel->getSheet(0)
                ->getStyle("B6:Q6")
                ->applyFromArray($fondoVerdeOscuro)
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontColorWhite)
                ->applyFromArray($alignCenter);
            $objPHPExcel->getSheet(0)
                ->getStyle("G6:N6")
                ->applyFromArray($fondoVerde)
                ->applyFromArray($fontColorGreen);
            $objPHPExcel->getSheet(0)
                ->getStyle("O6:P6")
                ->applyFromArray($fondoVerdeLight)
                ->applyFromArray($fontColorGreen);

        /** Dimensiones de columnasy filas */
            $objPHPExcel->getSheet(0)
                ->getRowDimension('1')
                ->setRowHeight(5);
            $objPHPExcel->getSheet(0)
                ->getRowDimension('2')
                ->setRowHeight(21);
            $objPHPExcel->getSheet(0)
                ->getRowDimension('3')
                ->setRowHeight(35);
            $objPHPExcel->getSheet(0)
                ->getRowDimension('4')
                ->setRowHeight(21);
            $objPHPExcel->getSheet(0)
                ->getRowDimension('5')
                ->setRowHeight(21);

        /** Agregar valores de plantilla y encabezados de tabla */
            $datenow = date('d/m/Y');
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', "")
            ->setCellValue('D2', "FORMATO DE TRANSFERENCIA DE ACTIVOS FIJOS")
            ->setCellValue('D3', "GESTIÓN FINANCIERA \n DEPARTAMENTO DE INFRAESTRUCTURA Y TIERRAS - GRUPO CONTROL ACTIVOS FIJOS - GSC")
            ->setCellValue('D4', "GFI-F-088")
            ->setCellValue('E4', "Elaborado: 17/11/2017")
            ->setCellValue('Q4', "Versión: 1")
            ->setCellValue('B5', "Fecha Solicitud:")
            ->setCellValue('C5', $datenow)
            ->setCellValue('B6', "CÓDIGO \n ACTIVO FIJO *")
            ->setCellValue('C6', "DENOMINACIÓN \n ACTIVO FIJO *")
            ->setCellValue('D6', "No. INVENTARIO *")
            ->setCellValue('E6', "SERIE *")
            ->setCellValue('F6', "COD. CLASE *")
            ->setCellValue('G6', "NUEVO COD. MUNICIPIO")
            ->setCellValue('H6', "NUEVO MUNICIPIO")
            ->setCellValue('I6', "NUEVO COD. UBICACIÓN GEOGRÁFICA")
            ->setCellValue('J6', "NUEVA UBICACIÓN GEOGRÁFICA")
            ->setCellValue('K6', "NUEVO COD. CECO")
            ->setCellValue('L6', "NUEVO COD. CAMPO PLANTA")
            ->setCellValue('M6', "NUEVO CAMPO PLANTA")
            ->setCellValue('N6', "VIDA ÚTIL TOTAL (En Meses) **")
            ->setCellValue('O6', "NUEVA VICEPRESIDENCIA")
            ->setCellValue('P6', "NUEVA GERENCIA")
            ->setCellValue('Q6', "OBSERVACIONES");
            
        /** Agregar Imagen logo Ecopetrol */     
            $objDrawing = new PHPExcel_Worksheet_Drawing();
        
            $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(80);
            $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(160);

            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $logo = '../img/logoForm.jpg';
            $objDrawing->setPath($logo);
            $objDrawing->setCoordinates('B3');
            $objDrawing->setResizeProportional(false);
            $objDrawing->setOffsetX(6);
            $objDrawing->setOffsetY(50);
            $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
            $objDrawing->setWorksheet( $objPHPExcel->getSheet(0) ); 
        /** Habilitar los filtros para las columnnas de contenido */
            $objPHPExcel->getSheet(0)->setAutoFilter('B6:Q6');
        /** CAPTURAR INFORMACION VIA POST */
            $grupo = $_POST['jsonExcel'];
            $obj = json_decode($grupo, true);
        /** Recorrer el objeto recibido para poblar el archivo excel */
            $vShift = 7; //fila inicial para insertar 
            for($i=0; $i<sizeof($obj); $i++){
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.($i+$vShift), $obj[$i]['activo'])
                ->setCellValue('C'.($i+$vShift), $obj[$i]['denominacion'])
                ->setCellValue('D'.($i+$vShift), $obj[$i]['inventario'])
                ->setCellValue('E'.($i+$vShift), $obj[$i]['serie'])
                ->setCellValue('F'.($i+$vShift), $obj[$i]['clase'])
                ->setCellValue('G'.($i+$vShift), $obj[$i]['new_municipio'])
                ->setCellValue('H'.($i+$vShift), $obj[$i]['nombre_municipio'])
                ->setCellValue('I'.($i+$vShift), $obj[$i]['new_ubicacion'])
                ->setCellValue('J'.($i+$vShift), $obj[$i]['nombre_ubicacion'])
                ->setCellValue('K'.($i+$vShift), $obj[$i]['new_ceco'])
                ->setCellValue('L'.($i+$vShift), $obj[$i]['new_campo'])
                ->setCellValue('M'.($i+$vShift), $obj[$i]['nombre_campo'])
                ->setCellValue('N'.($i+$vShift), $obj[$i]['vu_meses'])
                ->setCellValue('O'.($i+$vShift), $obj[$i]['new_vicepresidencia'])
                ->setCellValue('P'.($i+$vShift), $obj[$i]['new_gerencia'])
                ->setCellValue('Q'.($i+$vShift), $obj[$i]['observaciones']);
            }
            $vShiftTotal = "B".$vShift.":Q".($vShift + sizeof($obj) - 1) ;
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftTotal)
                ->applyFromArray($cellBorderColorGreen);
            $size = 5 + sizeof( $obj ) + 1;
            $objPHPExcel->getSheet(0)
                ->getStyle( 'B5:Q'.$size ) 
                ->applyFromArray($cellBorderOutline);    
                
        /** Entregado y recibido por... */            
            $vRow = $vShift + sizeof($obj) + 1;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("B".($vRow), "ENTREGADO POR:");
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("B".($vRow+1), "REGISTRO:");
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("B".($vRow+2), "CARGO:");
            
            $objPHPExcel->getSheet(0)
                ->mergeCells("C".($vRow).":D".($vRow))
                ->mergeCells("C".($vRow+1).":D".($vRow+1))
                ->mergeCells("C".($vRow+2).":D".($vRow+2));
            $objPHPExcel->getSheet(0)
                ->getStyle("C".($vRow).":D".($vRow))
                ->applyFromArray($cellBorderThinBottom);    
            $objPHPExcel->getSheet(0)
                ->getStyle("C".($vRow).":D".($vRow+1))
                ->applyFromArray($cellBorderThinBottom);    
            $objPHPExcel->getSheet(0)
                ->getStyle("C".($vRow).":D".($vRow+2))
                ->applyFromArray($cellBorderThinBottom);    
    
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("F".($vRow), "RECIBIDO POR:");
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("F".($vRow+1), "REGISTRO:");
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("F".($vRow+2), "CARGO:");
    
            $objPHPExcel->getSheet(0)
                ->mergeCells("G".($vRow).":I".($vRow))
                ->mergeCells("G".($vRow+1).":I".($vRow+1))
                ->mergeCells("G".($vRow+2).":I".($vRow+2));
            $objPHPExcel->getSheet(0)
                ->getStyle("G".($vRow).":I".($vRow))
                ->applyFromArray($cellBorderThinBottom);    
            $objPHPExcel->getSheet(0)
                ->getStyle("G".($vRow).":I".($vRow+1))
                ->applyFromArray($cellBorderThinBottom);    
            $objPHPExcel->getSheet(0)
                ->getStyle("G".($vRow).":I".($vRow+2))
                ->applyFromArray($cellBorderThinBottom); 
            
        /** mensaje adicional */
            $vRow = $vShift + sizeof($obj) + 5;
            $vShiftMessage = "B".($vShift + sizeof($obj) + 5).":Q".($vShift + sizeof($obj) + 5) ;
            
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue("B".($vRow), "Todos los derechos reservados para Ecopetrol S.A. Ninguna reproducción externa copia o transmisión digital de esta publicación puede ser hecha sin permiso escrito. Ningún párrafo de esta publicación puede ser reproducido, copiado o transmitido digitalmente sin un consentimiento escrito o de acuerdo con las leyes que regulan los derechos de autor y con base en la regulación vigente.");
            $objPHPExcel->getSheet(0)
                ->mergeCells($vShiftMessage)
                ->getStyle($vShiftMessage)
                ->getAlignment()
                ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
                ->setWrapText(true);
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftMessage)
                ->applyFromArray($cellAllBorderMedium);
            $objPHPExcel->getSheet(0)
                ->getRowDimension($vRow)->setRowHeight(30);
        /** AutoSize de las columnas de contenido */    
            foreach(range('B6','Q6') as $columnID) {
                $objPHPExcel->getSheet(0)->getColumnDimension($columnID)
                    ->setAutoSize(true);
            }
            foreach(range('A6','C6') as $columnID) {
                $objPHPExcel->getSheet(0)->getColumnDimension('A'.$columnID)
                    ->setAutoSize(true);
            }
        /** Renombrar Hoja e inmovilizar filas superiores */
            $objPHPExcel->getSheet(0)
            ->setTitle('Transferencias')
            ->freezePaneByColumnAndRow(0, 7);
    /** PAGINA(1) DE AYUDA  */ 
        /** Eliminamos la última hoja creada por defecto */
            $objPHPExcel->removeSheetByIndex(1);
        /** Crear pagina 1 */
            $objPHPExcel->createSheet(1);
        /** Combinar celdas */
            $objPHPExcel->getSheet(1)->mergeCells('B2:B4'); //LOGO
            $objPHPExcel->getSheet(1)->mergeCells('C2:E2'); //FORMATO
            $objPHPExcel->getSheet(1)->mergeCells('C3:E3'); // gestion financiera
            $objPHPExcel->getSheet(1)->mergeCells('B6:E6'); // TITULO GRIS
            for($j=7; $j<=26; $j++){
                $cellPosition = 'C'.$j.':E'.$j;
                $objPHPExcel->getSheet(1)->mergeCells($cellPosition);
            };
        /** Asignación de formatos a celdas y rangos */
            $objPHPExcel->getSheet(1)
                ->getStyle("B2:E4")
                ->applyFromArray($alignCenter)
                ->applyFromArray($cellAllBorderMedium);
            $objPHPExcel->getSheet(1)
                ->getStyle("C2:C3")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontSize12);
            $objPHPExcel->getSheet(1)
                ->getStyle("C4:E4")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontSize11);
            $objPHPExcel->getSheet(1)
                ->getStyle("B5:E5")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fontSize11);
            $objPHPExcel->getSheet(1)
                ->getStyle("B6:E26")
                ->applyFromArray($fontCalibri2)
                ->applyFromArray($fontSize11)
                ->getAlignment()    
                ->setWrapText(true);
            $objPHPExcel->getSheet(1)
                ->getStyle("B6")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fondoColorPiel)
                ->applyFromArray($alignCenter);
            $objPHPExcel->getSheet(1)
                ->getStyle("B7:E7")
                ->applyFromArray($fontCalibri)
                ->applyFromArray($fondoVerdeOscuro)
                ->applyFromArray($fontColorWhite)
                ->applyFromArray($alignCenter);
            $objPHPExcel->getSheet(1)
                ->getStyle("B8:B26")
                ->applyFromArray($alignCenterVertical);
            $objPHPExcel->getSheet(1)
                ->getStyle("B8:E26")
                ->applyFromArray($cellBorderThin)
                ->applyFromArray($cellBorderColorBlack);
            $objPHPExcel->getSheet(1)
                ->getStyle("B8:E26")
                ->applyFromArray($cellOutlineBorderMedium);
        /** Dimensiones de columnas y filas */
            $objPHPExcel->getSheet(1)->getRowDimension('1')->setRowHeight(5);
            $objPHPExcel->getSheet(1)->getRowDimension('2')->setRowHeight(21);
            $objPHPExcel->getSheet(1)->getRowDimension('3')->setRowHeight(35);
            $objPHPExcel->getSheet(1)->getRowDimension('4')->setRowHeight(21);
            $objPHPExcel->getSheet(1)->getRowDimension('5')->setRowHeight(21);    
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('0')->setAutoSize(false);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('0')->setWidth('1');
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('1')->setAutoSize(false);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('1')->setWidth('22');
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('2')->setAutoSize(false);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('2')->setWidth('25');
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('3')->setAutoSize(false);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('3')->setWidth('60');
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('4')->setAutoSize(false);
            $objPHPExcel->getActiveSheet()->getColumnDimensionByColumn('4')->setWidth('25');
        /** Agregar Imagen logo Ecopetrol */     
            $objDrawing = new PHPExcel_Worksheet_Drawing();
        
            $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(80);
            $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(180);

            $objDrawing->setName('Logo');
            $objDrawing->setDescription('Logo');
            $logo = '../img/logoForm.jpg';
            $objDrawing->setPath($logo);
            $objDrawing->setCoordinates('B3');
            $objDrawing->setResizeProportional(false);
            $objDrawing->setOffsetX(6);
            $objDrawing->setOffsetY(50);
            $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
            $objDrawing->setWorksheet( $objPHPExcel->getSheet(1) );
        /**  Agregar valores de plantilla y encabezados de tabla */
            function HelpTitles($text1,$text2,$order=2){
                switch($order){
                    case 1:
                        $objRichText = new PHPExcel_RichText();
                        $run1 = $objRichText->createTextRun($text1);
                        $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                        $run2 = $objRichText->createTextRun($text2);
                        $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                    break;
                    case 2:
                    default:
                        $objRichText = new PHPExcel_RichText();
                        $run1 = $objRichText->createTextRun($text1." ");
                        $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                        $run2 = $objRichText->createTextRun($text2." ");
                        $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                    break;
                }
                return $objRichText;
            }
            $objPHPExcel->setActiveSheetIndex(1)
                ->setCellValue('A1', "")
                ->setCellValue('C2', "FORMATO DE TRANSFERENCIA DE ACTIVOS FIJOS")
                ->setCellValue('C3', "GESTIÓN FINANCIERA \n DEPARTAMENTO DE INFRAESTRUCTURA Y TIERRAS - GRUPO CONTROL ACTIVOS FIJOS - GSC")
                ->setCellValue('C4', "GFI-F-088")
                ->setCellValue('D4', "Elaborado: 17/11/2017")
                ->setCellValue('E4', "Versión: 3")
                ->setCellValue('B5', HelpTitles('* ','Campos Obligatorios', 1))
                ->setCellValue('B6', "INFORMACIÓN DEL FORMATO")
                ->setCellValue('B7', "CAMPO")
                ->setCellValue('C7', "DESCRIPCIÓN")
                ->setCellValue('B8', HelpTitles('CÓDIGO ACTIVO FIJO ','*'))
                ->setCellValue('C8', "Código identificación del activo en el auxiliar de activos fijos de Ecopetrol en SAP/AA.")
                ->setCellValue('B9', HelpTitles('SUBNÚMERO ','*'))
                ->setCellValue('C9', "Dígitos adicionales al código activo fijo de SAP, inicia con 0 que identifica al activo principal, y si posee capitalizaciones como mayor valor, adiciones o mejoras al activo principal, se identifica con el mismo código de activo fijo principal más el (los) dígito(s) consecutivo(s).")
                ->setCellValue('B10', HelpTitles('DENOMINACIÓN ACTIVO FIJO ','*'))
                ->setCellValue('C10', "Nombre o denominación del activo fijo.")
                ->setCellValue('B11', HelpTitles('No. INVENTARIO ','*'))
                ->setCellValue('C11', "Campo numérico que permite la identificación física para el control interno en la compañía, por medio de un sticker o placa.")
                ->setCellValue('B12', HelpTitles('SERIE ','*'))
                ->setCellValue('C12', "Número de serie del activo fijo en caso de no contar con la información escribir \"No Registra\", en caso de no aplicar el campo escribir \"No Aplica\".")
                ->setCellValue('B13', HelpTitles('COD. CLASE','*'))
                ->setCellValue('C13', "Corresponde al código de la clase que se encuentra entre el grupo seleccionado de acuerdo a la \"Tabla Catálogo de Activos Fijos - GFI-T-004\" ")
                ->setCellValue('B14', HelpTitles('NUEVO COD. MUNICIPIO','*'))
                ->setCellValue('C14', "Nuevo código de identificacion del municipío de la nueva localizacion geográfica, si aplica.  Utilizar TABLA DE MUNICIPIOS que actualmente tiene ECP.")
                ->setCellValue('B15', HelpTitles('NUEVO MUNICIPIO','*'))
                ->setCellValue('C15', "Nuevo municipío de la nueva localizacion geográfica, si aplica.  Utilizar TABLA DE MUNICIPIOS que actualmente tiene ECP.")
                ->setCellValue('B16', HelpTitles('NUEVO COD. UBICACIÓN GEOGRÁFICA','*'))
                ->setCellValue('C16', "Código SAP que permite identificar la nueva localización geográfica o un punto físico donde se encuentra instalado el activo. Utilizar TABLA DE UBICACIONES que actualmente tiene ECP.")
                ->setCellValue('B17', HelpTitles('NUEVA UBICACIÓN GEOGRÁFICA','*'))
                ->setCellValue('C17', "Nombre que permite identificar la nueva localización geográfica o un punto físico donde se encuentra instalado el activo. Utilizar TABLA DE UBICACIONES que actualmente tiene ECP.")
                ->setCellValue('B18', HelpTitles('NUEVO COD. CECO','*'))
                ->setCellValue('C18', "Nuevo código SAP de centro de costos responsable del activo, el cual asume los costos o gastos por depreciación/amortización. \n IMPORTANTE: Si su activo pertenece a una de las 36 clases de la hoja \"Clases\" y va a pasar a un centro de costo diferente a producción, es obligatorio diligenciar el campo de vida útil, dado que cambia el método de depreciación a línea recta. Si por el contrario, su activo va a pasar a un centro de costo de producción, es obligatorio diligenciar el campo planta, dado que cambia el método de depreciación a unidades de producción.")
                ->setCellValue('B19', HelpTitles('NUEVO COD. CAMPO PLANTA','*'))
                ->setCellValue('C19', "Nuevo código del campo planta donde se encuentra el activo fijo. Aplica únicamente para activos de la Vicepresidencia de Producción, por lo tanto depende del centro de costos diligenciado.")
                ->setCellValue('B20', HelpTitles('NUEVO CAMPO PLANTA','*'))
                ->setCellValue('C20', "Identificacion de la nueva Unidad Generadora de Efectivo.  Para activos de producción será el nombre del campo de producción. Segmentos diferentes de producción: \n VEX1: Exploración \n VIN2: ICP \n VRP2: Refinería \n VTL1: Transporte \n COR1: Corporativo")
                ->setCellValue('B21', 'VIDA UTIL TOTAL (En meses)')
                ->setCellValue('B21',  HelpTitles('VIDA ÚTIL TOTAL (En Meses)','**') )
                ->setCellValue('C21', "Es el dato estimado en meses totales de la Vida útil técnica del activo fijo, Ejemplo:  10 años y 6 meses.\n Para este campo se diligencia el número 126. \n Sólo se registra si el activo deprecia por línea recta.")
                ->setCellValue('B22', 'NUEVA VICEPRESIDENCIA')
                ->setCellValue('C22', "Este campo es diligenciado por la Coordinación de Activos Fijos. Nombre de la Vicepresidencia donde se encuentra el activo fijo.")
                ->setCellValue('B23', 'NUEVA GERENCIA')
                ->setCellValue('C23', "Este campo es diligenciado por la Coordinación de Activos Fijos. Nombre de la Gerencia donde se encuentra el activo fijo.")
                ->setCellValue('B24', 'OBSERVACIONES')
                ->setCellValue('C24', "Indique algún dato relevante del activo si lo amerita.")
                ->setCellValue('B25', HelpTitles('ENTREGADO POR ','*'))
                ->setCellValue('C25', "Identifique el nombre, registro y cargo del funcionario responsable de la custodia del Activo hasta la fecha de transferencia.")
                ->setCellValue('B26', HelpTitles('RECIBIDO POR ','*'))
                ->setCellValue('C26', "Identifique el nombre, registro y cargo del nuevo funcionario responsable de la custodia del Activo.");
        /** Renombrar Hoja e inmovilizar filas superiores */
            $objPHPExcel->getSheet(1)
            ->setTitle('Ayuda');
            
    /** Establecer la hoja activa, para que cuando se abra el documento se muestre primero. */
        $objPHPExcel->setActiveSheetIndex(0);
    /** Borrar todos los archivos excel previamente generados */    
        $files = glob('xls/transferencias/*'); // get all file names
        foreach($files as $file){ // iterate files
          if(is_file($file))
            unlink($file); // delete file
        }
    /** Nombrar y generar el archivo XLS */
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        //$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $route = 'xls/transferencias/transferencias-';
        $datenow = date('YmdHis');
        $extension = '.xls';
        $name = $route . $datenow . $extension;
        $objWriter->save($name);
        $objName = array(
            'route' => $name,
            'name' => 'transferencias-'.$datenow
        );
        die( json_encode( $objName ) );
?>