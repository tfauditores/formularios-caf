<?php
    error_reporting(E_ALL ^ E_WARNING); 
    require_once('conexion.php');

    class Asset extends Conexion{
        public $asset = null;
        public $sn = null;
        public $list = null;
        public $params = null;
        public $type = null;

        public function __construct($asset = 0, $sn = 0, $list = true, $params = '*', $type = 'activo'){
            $this->asset = $asset;
            $this->sn = $sn;
            $this->list = $list;
            $this->params = $params;
            $this->type = $type;
        }

        public function getAsset(){
            
            $con = $this->getConnection();

            $query = "SELECT {$this->params} FROM `auxiliar` WHERE `{$this->type}` = {$this->asset}";
            if($this->sn != 0 && $this->list == 'false'){
                $query .= " AND `sn` = {$this->sn}";
            }
            $query .= " ORDER BY sn ASC";
            $this->list == 'false' ? $query.= " LIMIT 1": "";
            // return $query;
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'invalid asset number' ] ;
            }

            return $data;

        }

        public function getExistingInvetory(){

            $con = $this->getConnection();

            // $query = "SELECT `activo`,`sn`,`noInventario`,`clase` FROM `auxiliar` AS a WHERE `noInventario` LIKE \"%{$this->asset}%\" ORDER BY sn ASC";
            $query = "SELECT `activo`,`sn`,`noInventario`,`clase` FROM `auxiliar` AS a WHERE `noInventario` = {$this->asset} ORDER BY sn ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'error' => 'invalid asset number' ] ;
            }

            return $data;
        }
    }

    class Catalogo extends Conexion{
        public $params = null;

        public function __construct($params = '*'){
            $this->params = $params;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `catalogo` AS a WHERE 1 ORDER BY `clase` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'catalogo not exist' ] ;
            }

            return $data;
        }
    }

    class Unidades extends Conexion{
        public $params = null;

        public function __construct($params = '*'){
            $this->params = $params;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `unidades` AS a WHERE 1 ORDER BY `unidad` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'unidades not exist' ] ;
            }

            return $data;
        }
    }

    class Fabricantes extends Conexion{
        public $params = null;

        public function __construct($params = '*'){
            $this->params = $params;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `fabricantes` AS a WHERE 1 ORDER BY `fabricante` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'fabricantes not exist' ] ;
            }

            return $data;
        }
    }
    
    class Ubicaciones extends Conexion{
        public $params = null;

        public function __construct($params = '*'){
            $this->params = $params;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT DISTINCT {$this->params} FROM `ubicaciones` AS a WHERE 1 ORDER BY `departamento` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);
            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    
                    // var_dump($row);
                    if(count($data) > 0){
                        $existDepartamento = false;

                        foreach($data as $dk => $d){
                            if($d['departamento'] == $row['departamento']){
                                $existDepartamento = true;
                                $existMunicipio = false;

                                foreach($d['municipios'] as $mk => $m){

                                    if($m['municipio'] == $row['municipio']){
                                        
                                        $existMunicipio = true;
                                        $existUbicacion = false;

                                        foreach($m['ubicaciones'] as $u){
                                            if($u['ubicacion'] == $row['ubicacion']){
                                                $existUbicacion = true;
                                            }
                                        }

                                        if($existUbicacion == false){
                                            $dataTemp = [
                                                'ubicacion' => $row['ubicacion'],
                                                'codigoUbicacion' => $row['codigoUbicacion']
                                            ];
                                            array_push($data[$dk]['municipios'][$mk]['ubicaciones'], $dataTemp);
                                        }

                                    }

                                }

                                if($existMunicipio == false){

                                    $dataTemp = [
                                        'municipio' => $row['municipio'],
                                        'codigoMunicipio' => $row['codigoMunicipio'],
                                        'ubicaciones' => [
                                            [
                                                'ubicacion' => $row['ubicacion'],
                                                'codigoUbicacion' => $row['codigoUbicacion']
                                            ]
                                        ]
                                    ];

                                    array_push($data[$dk]['municipios'], $dataTemp);
                                }
                            }
                        }
                        
                        if($existDepartamento == false){
                            $dataTemp = [
                                'departamento' => $row['departamento'],
                                'municipios' => [
                                    [
                                        'municipio' => $row['municipio'],
                                        'codigoMunicipio' => $row['codigoMunicipio'],
                                        'ubicaciones' => [
                                            [
                                                'ubicacion' => $row['ubicacion'],
                                                'codigoUbicacion' => $row['codigoUbicacion']
                                            ]
                                        ]
                                    ]
                                ]
                            ];
                            array_push($data, $dataTemp);
                        }
                    }
                    else{
                        $dataTemp = [
                            'departamento' => $row['departamento'],
                            'municipios' => [
                                [
                                    'municipio' => $row['municipio'],
                                    'codigoMunicipio' => $row['codigoMunicipio'],
                                    'ubicaciones' => [
                                        [
                                            'ubicacion' => $row['ubicacion'],
                                            'codigoUbicacion' => $row['codigoUbicacion']
                                        ]
                                    ]
                                ]
                            ]
                        ];
                        array_push($data, $dataTemp);
                    }
                }
            }
            else{
                $data = [ 'result' => 'ubicaciones not exist' ] ;
            }
            return $data;
        }
    }

    class Cecos extends Conexion{
        public $params = null;

        public function __construct($params = '*', $name=''){
            $this->params = $params;
            $this->name = $name;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `ceco_campo` WHERE 1 ORDER BY `nombreCeco` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'cecos not exist' ] ;
            }

            return $data;
        }
        public function getDataByName(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `ceco_campo` WHERE `ceco` LIKE '{$this->name}' ORDER BY `nombreCeco` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'error' => 'Ceco no encontrado' ] ;
            }

            return $data;
        }
    }
    
    class CampoPlanta extends Conexion{
        public $params = null;

        public function __construct($params = '*'){
            $this->params = $params;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `campo_planta` AS a WHERE 1 ORDER BY `nombre` ASC";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'campo_planta not exist' ] ;
            }

            return $data;
        }
    }
    
    class Jerarquizacion extends Conexion{
        public $params = null;
        public $value = null;

        public function __construct($params = '*', $value = 0){
            $this->params = $params;
            $this->value = $value;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `ceco_campo` WHERE `ceco` LIKE \"{$this->value}\"";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'error' => 'jerarquizacion not exist' ] ;
            }

            return $data;
        }
    }

    class Custodios extends Conexion{
        public $params = null;

        public function __construct($params = '*'){
            $this->params = $params;
        }

        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT {$this->params} FROM `personal` WHERE 1 ORDER BY `id` ASC";
            //return $query;
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'custodios not exist' ] ;
            }

            return $data;
        }
    }

    class SMM extends Conexion{
        public function getData(){
            $con = $this->getConnection();
            $query = "SELECT * FROM `smm_historico` WHERE 1 ORDER BY `ano` DESC LIMIT 1";
            $results = mysqli_query($con, $query);
            $numResults = mysqli_num_rows($results);

            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_assoc($results) ){
                    array_push($data,$row);
                }
            }
            else{
                $data = [ 'result' => 'No se encontró el salario mínimo legal vigente' ] ;
            }

            return $data;
        }
    }

    if(isset($_GET['method'])){
        switch($_GET['method']){
            case 'getAsset':
                $active = new Asset(
                    $_GET['number'],
                    $_GET['sn'],
                    $_GET['list'],
                    $_GET['params'],
                    $_GET['type']
                );
                $data = $active->getAsset();
            break;
            case 'getExistingInvetory':
                $inventory = new Asset(
                    $_GET['number']
                );
                $data = $inventory->getExistingInvetory();
            break;
            case 'getCatalogo':
                $catalogo = new Catalogo(
                    $_GET['params']
                );
                $data = $catalogo->getData();
            break;
            case 'getUnidades':
                $unidades = new Unidades(
                    $_GET['params']
                );
                $data = $unidades->getData();
            break;
            case 'getFabricantes':
                $fabricantes = new Fabricantes(
                    $_GET['params']
                );
                $data = $fabricantes->getData();
            break;
            case 'getUbicaciones':
                $ubicaciones = new Ubicaciones(
                    $_GET['params']
                );
                $data = $ubicaciones->getData();
            break;
            case 'getCecos':
                $cecos = new Cecos(
                    $_GET['params']
                );
                $data = $cecos->getData();
            break;    
            case 'getCecoByName':
                $cecos = new Cecos(
                    $_GET['params'], $_GET['name']
                );
                $data = $cecos->getDataByName();
            break;
            case 'getCampoPlanta':
                $campoPlanta = new CampoPlanta(
                    $_GET['params']
                );
                $data = $campoPlanta->getData();
            break;
            case 'getJerarquizacion':
                $jerarquizacion = new Jerarquizacion(
                    $_GET['params'],
                    $_GET['value']
                );
                $data = $jerarquizacion->getData();
            break;
            case 'getCustodios':
                $custorios = new Custodios(
                    $_GET['params']
                );
                $data = $custorios->getData();
            break;
            case 'SMM':
                $smm = new SMM();
                $data = $smm->getData();
            break;
        }
    }
    header('Content-type: application/json');
    http_response_code(200);
    echo json_encode( $data );
    
    