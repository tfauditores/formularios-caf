<?php
    if(isset($_POST['get_option']))
    {
        require_once('conexion.php');

        $activo = $_POST['get_option'];
        $subnum = isset($_POST['subnum']) ? $_POST['subnum'] : 0 ;
     
        //Cuenta el número de hijos
            $query1 = " SELECT COUNT(a.activo) 
                        FROM auxiliar AS a 
                        WHERE a.activo LIKE '$activo' ";
            $results = mysqli_query($con, $query1);
            $count = mysqli_fetch_array($results)[0] - 1;
        //
        

        $query = "  SELECT  a.activo, a.clase, a.grupo, a.denominacion, a.denominacion2, 
                            a.departamento, a.municipio, a.nombreMunicipio, 
                            a.ubicacionGeografica, a.nombreUbicacionGeografica, a.ceco, 
                            a.noInventario, a.vuMesesAr01, a.tag, a.campoPlanta, 
                            a.nombreCampoPlanta, a.unidadMedida, a.fechaCapitalizacion,
                            a.tipoOperacion, a.vuRemanenteAr01
                    FROM auxiliar AS a
                    WHERE a.activo = '$activo'
                    AND sn='$subnum'
        ";
        $results=mysqli_query($con, $query);
        $numResults = mysqli_num_rows($results);
        if ($numResults > 0) {
            while($row=mysqli_fetch_array($results  ))
            {
                $data = [   'activo' => $row['activo'],
                            'clase' => $row['clase'],
                            'grupo' => $row['grupo'],
                            'inventario' => $row['noInventario'],
                            'denominacion' => $row['denominacion'],
                            'denominacion2' => $row['denominacion2'],
                            'departamento' => $row['departamento'],
                            'municipio' => $row['municipio'],
                            'nombreMunicipio' => $row['nombreMunicipio'],
                            'ubicacionGeografica' => $row['ubicacionGeografica'],
                            'nombreUbicacionGeografica' => $row['nombreUbicacionGeografica'],
                            'campoPlanta' => $row['campoPlanta'],
                            'nombreCampoPlanta' => $row['nombreCampoPlanta'],
                            'ceco' => $row['ceco'],
                            'tag' => $row['tag'],
                            'vidaUtil' => $row['vuMesesAr01'],
                            'vidaUtilRemanente' => $row['vuRemanenteAr01'],
                            'unidadMedida' => $row['unidadMedida'],
                            'fechaCapitalizacion' => $row['fechaCapitalizacion'],
                            'tipoOperacion' => $row['tipoOperacion'],
                            'subnumeros' => $count
                         ];
                header('Content-type: application/json');
                echo json_encode( $data );
            }
        }
        else{
            
        }
        exit;
    }
?>