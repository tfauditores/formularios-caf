<?php
    // Archivo de conexión a base de datos
    require_once('conexion.php');
    // Lectura de variables POST o GET
    $reference          = isset($_REQUEST['id_referencia']) ? $_REQUEST['id_referencia'] : '';
    $numberRequest      = isset($_REQUEST['id_numero_referencia']) ? $_REQUEST['id_numero_referencia'] : '';
    $subnumberRequest   = isset($_REQUEST['id_subnumero_referencia']) ? $_REQUEST['id_subnumero_referencia'] : 0;
    $subnFlag = false;
    $count = 0;

    /* si se aplica el # de activo se trae el subnumero para verificar si tiene activos hijos*/
    if( $subnumberRequest !='0' && $subnumberRequest !='' ){
        $reference = "_key";
        $numberRequest = $numberRequest.$subnumberRequest;
    }

    //si se trata de un principal cuenta el número de hijos
    if($reference == "activo" && $subnumberRequest == 0){
        $query2 = "   SELECT COUNT(a.$reference) 
        FROM auxiliar AS a 
        WHERE a.$reference LIKE '$numberRequest' ";
        $results = mysqli_query($con, $query2);
        $count = mysqli_fetch_array($results)[0] - 1;
    
    }

    if( $reference == 'subnumeros'){
        $reference = 'activo';
        $subnFlag = true;
    }

    //consulta de tabla auxiliar
    $query = "  SELECT  a.activo, a.sn, a.noInventario, a.tipoOperacion,
                        a.departamento, a.nombreMunicipio, a.nombreCampoPlanta, a.nombreUbicacionGeografica,
                        a.custodio, a.ceco, a.claveAr01,
                        a.area, a.vicepresidencia, a.gerencia,
                        a.clase, a.nombreClase, a.fechaCapitalizacion, a.denominacion, a.denominacion2,
                        a.fabricante, a.tag, a.serie, a.modelo, a.capacidad, a.unidadMedida,
                        b.claveProd, b.claveOtros
                FROM auxiliar AS a LEFT JOIN clase_clave AS b ON a.clase = b.clase
                WHERE a.$reference LIKE '$numberRequest'";
    
    //PARA NO INCLUIR EL NUMERO PRINCIPAL
    //$query .= $subnFlag ? " AND a.sn != '0'" : " AND a.sn = $subnumberRequest" ;

    $query .= $subnFlag ? " " : " AND a.sn = $subnumberRequest" ;

    $results = mysqli_query($con, $query);
    $numResults = mysqli_num_rows($results);

    if ($numResults > 0) {
        $data = array();
        while( $row = mysqli_fetch_array($results) ){
            $dataTemp = [  
                'activo' => $row['activo'],
                'sn' => $row['sn'],
                'inventario' => $row['noInventario'],
                'operacion' => $row['tipoOperacion'],

                'departamento' => $row['departamento'],
                'nombreMunicipio' => $row['nombreMunicipio'],
                'nombreCampoPlanta' => $row['nombreCampoPlanta'],
                'nombreUbicacionGeografica' => $row['nombreUbicacionGeografica'],
                
                'area' => $row['area'],
                'vicepresidencia' => $row['vicepresidencia'],
                'gerencia' => $row['gerencia'],
                
                'custodio' => $row['custodio'],
                'ceco' => $row['ceco'],
                'claveAr01' => $row['claveAr01'],

                'clase' => $row['clase'],
                'nombreClase' => $row['nombreClase'],
                'fechaCapitalizacion' => $row['fechaCapitalizacion'],
                'denominacion' => $row['denominacion'],
                'denominacion2' => $row['denominacion2'],

                'fabricante' => $row['fabricante'],
                'tag' => $row['tag'],
                'serie' => $row['serie'],
                'modelo' => $row['modelo'],
                'capacidad' => $row['capacidad'],
                'unidadMedida' => $row['unidadMedida'],
                
                'claveProd' => $row['claveProd'],
                'claveOtros' => $row['claveOtros'],

                'subnumeros' => $count
            ];
            array_push($data, $dataTemp);
        }
        if(!$subnFlag){
            $data = $data[0];
        }
    }
    else{
        $data = [ 'result' => 'invalid active' ] ;
    }

    //Se envia la información en formato JSON
    header('Content-type: application/json');
    http_response_code(200);
    echo json_encode( $data );
    
?>