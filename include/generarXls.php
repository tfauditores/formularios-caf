<?php
    /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');
     /** Include PHPExcel */
        require_once 'Classes/PHPExcel.php';
    /** Crea un nuevo objeto PHPExcel */
        $objPHPExcel = new PHPExcel();
    /** Establecer propiedades del documento a generar*/
        $objPHPExcel->getProperties()
        ->setCompany("TF AUDITORES")
        ->setCreator("TF AUDITORES")
        ->setLastModifiedBy("TF AUDITORES")
        ->setTitle("CREACION DE DATOS MAESTROS")
        ->setSubject("CREACION DE DATOS MAESTROS")
        ->setDescription("FORMATO SOLICITUD DE CREACION DE DATOS MAESTROS")
        ->setKeywords("TFAUDITORES, DATOS MAESTROS, GFI-049")
        ->setCategory("SERVICIOS");
    /** Formatos de celdas  */
        $properties = array(
            'fontVerdana' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontVerdana2' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontCalibri' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontCalibri2' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontSize14' => array(
                'font' => array(
                    'size' => 14
                )
            ),
            'fontSize12' => array(
                'font' => array(
                    'size' => 12
                )
            ),
            'fontSize11' => array(
                'font' => array(
                    'size' => 11
                )
            ),
            'fontColorWhite' => array(
                'font' => array(
                    'color' => array('rgb' => 'ffffff')
                )
            ),
            'fontColorRed' => array(
                'font' => array(
                    'color' => array('rgb' => 'ff0000')
                )
            ),
            'fontColorGreen' => array(
                'font' => array(
                    'color' => array('rgb' => '004237')
                )
            ),
            'fondoVerde' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C9D200')
                )
            ),
            'fondoVerdeOscuro' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '004238')
                )
            ),
            'fondoGris' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E6E6E6')
                )
            ),
            'fondoGris2' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c3c4c6')
                )
            ),
            'fondoColorPiel' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'EEECE1')
                )
            ),
            'alignCenter' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterHorizontal' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterVertical' => array(
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'cellBorderDouble' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThin' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellOutlineBorderThin' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderMedium' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderMedium' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellRightBorderMedium' => array(
                'borders' => array(
                    'right' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderMediumRight' => array(
                'borders' => array(
                    'right' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinTop' => array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderColorWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellBorderColorGreen' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '9bbb59')
                    )
                )
            ),
            'cellBorderColorGray' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'bfbfbf')
                    )
                )
            ),
            'cellBorderColorBlack' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
    /** Asignación de formatos a celdas y rengos */
    /** Print Header*/
        function print_header($objPHPExcel, $sheet, $properties){
            /* Combinar celdas */
                $objPHPExcel->getSheet($sheet)->setShowGridlines(false);
                $objPHPExcel->getSheet($sheet)->mergeCells('B2:C4');
                $objPHPExcel->getSheet($sheet)->mergeCells('D2:AJ2');
                $objPHPExcel->getSheet($sheet)->mergeCells('D3:AJ3');
                $objPHPExcel->getSheet($sheet)->mergeCells('E4:AI4');
            /** Asignación de formatos a celdas y rengos */
                $objPHPExcel->getDefaultStyle()
                            ->applyFromArray($properties['cellBorderMedium'])
                            ->applyFromArray($properties['cellBorderColorWhite'])
                            ->applyFromArray($properties['fontVerdana2']);
                $objPHPExcel->getSheet($sheet)
                            ->getStyle("B2:AJ4")
                            ->applyFromArray($properties['alignCenter'])
                            ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                            ->getStyle("D2:AJ4")
                            ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                            ->getStyle("B6:AJ6")
                            ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                            ->getColumnDimensionByColumn('A')
                            ->setWidth('1');
            /** Dimensiones de columnas*/
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')
                    ->setRowHeight(24);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')
                    ->setRowHeight(35);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')
                    ->setRowHeight(24);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                        ->setCellValue('A1', "")
                        ->setCellValue('D2', "FORMATO CREACIÓN DE DATOS MAESTROS DE ACTIVOS FIJOS")
                        ->setCellValue('D3', "GESTIÓN FINANCIERA \nGSC - DEPARTAMENTO DE TIERRAS Y CONTROL ACTIVOS FIJOS")
                        ->setCellValue('D4', "GFI-F-049")
                        ->setCellValue('E4', "Elaborado: 06/12/2017")
                        ->setCellValue('AJ4', "Versión: 3")
                        ->setCellValue('B6', "Fecha Solicitud:");
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
                
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(100);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(300);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(50);
                $objDrawing->setOffsetY(20);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        }
        function print_header2($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->setShowGridlines(false);
                $objPHPExcel->getSheet($sheet)->mergeCells('B2:B4');
                $objPHPExcel->getSheet($sheet)->mergeCells('C2:E2');
                $objPHPExcel->getSheet($sheet)->mergeCells('C3:E3');
            /** Asignación de formatos a celdas y rengos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['cellBorderColorWhite'])
                    ->applyFromArray($properties['fontVerdana2']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B2:E4")
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("C2:E4")
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B8:E8")
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimensionByColumn('A')
                    ->setWidth('1');
            /** Dimensiones de columnas*/
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')
                    ->setRowHeight(5);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')
                    ->setRowHeight(24);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')
                    ->setRowHeight(35);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('4')
                    ->setRowHeight(24);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('B')
                    ->setWidth(38);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('C')
                    ->setWidth(30);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('D')
                    ->setWidth(104);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('E')
                    ->setWidth(30);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('C2', "FORMATO CREACIÓN DE DATOS MAESTROS DE ACTIVOS FIJOS")
                    ->setCellValue('C3', "GESTIÓN FINANCIERA\n GSC - DEPARTAMENTO DE TIERRAS Y CONTROL ACTIVOS FIJOS")
                    ->setCellValue('C4', "GFI-F-049")
                    ->setCellValue('D4', "Elaborado: 06/12/2017")
                    ->setCellValue('E4', "Versión: 3");
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
            
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(130);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(350);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B2');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(15);
                $objDrawing->setOffsetY(5);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
        $i = 0;
        while($i < 2){
            /** Crear pagina*/
                $objPHPExcel->createSheet($i);
            switch ($i){
                case 0:
                    print_header($objPHPExcel, $i, $properties);
                    /** Combinar celdas */
                        $objPHPExcel->getActiveSheet()->mergeCells('AA6:AB6');
                        $objPHPExcel->getActiveSheet()->mergeCells('AC6:AD6');
                        $objPHPExcel->getActiveSheet()->mergeCells('AE6:AI6');
                    /** Asignación de formatos a celdas y rengos */
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B7:AJ7")
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontVerdana'])
                            ->applyFromArray($properties['fontColorWhite'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("AI7")
                            ->applyFromArray($properties['fondoVerde'])
                            ->applyFromArray($properties['fontVerdana'])
                            ->applyFromArray($properties['fontColorGreen'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("AA6:AI6")
                            ->applyFromArray($properties['fondoGris'])
                            ->applyFromArray($properties['fontVerdana'])
                            ->applyFromArray($properties['fontColorGreen'])
                            ->applyFromArray($properties['alignCenter']);
                    /** Dimensiones de columnas*/
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('1')
                            ->setRowHeight(5);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('2')
                            ->setRowHeight(24);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('3')
                            ->setRowHeight(35);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('4')
                            ->setRowHeight(24);
                        $objPHPExcel->getSheet($i)
                            ->getRowDimension('6')
                            ->setRowHeight(30);
                        $objPHPExcel->getActiveSheet()
                            ->getColumnDimension('A')
                            ->setWidth(1);
                    
                    /** Agregar valores de plantilla y encabezados de tabla */
                        $datenow = date('d/m/Y');
                
                        $objPHPExcel->setActiveSheetIndex($i)
                        ->setCellValue('C6', $datenow)
                        ->setCellValue('AA6', "APLICA PARA ACTIVOS DE VDP")
                        ->setCellValue('AC6', "APLICA PARA CREACIÓN DE VEHICULOS")
                        ->setCellValue('AE6', "APLICA PARA GESTIÓN DE CREACIÓN Y TRASLADOS EN SIMULTÁNEO")
                        ->setCellValue('B7', "TIPO DE OPERACIÓN *")
                        ->setCellValue('C7', "TIPO DE CREACIÓN *")
                        ->setCellValue('D7', "CÓDIGO ACTIVO FIJO")
                        ->setCellValue('E7', "SUBNÚMERO")
                        ->setCellValue('F7', "COD. CLASE *")
                        ->setCellValue('G7', "DENOMINACIÓN ACTIVO FIJO*")
                        ->setCellValue('H7', "DENOMINACIÓN COMPLEMENTARIA *")
                        ->setCellValue('I7', "No. INVENTARIO *")
                        ->setCellValue('J7', "FABRICANTE *")
                        ->setCellValue('K7', "MODELO *")
                        ->setCellValue('L7', "CAPACIDAD *")
                        ->setCellValue('M7', "UNIDAD DE MEDIDA *")
                        ->setCellValue('N7', "SERIE *")
                        ->setCellValue('O7', "TAG *")
                        ->setCellValue('P7', "REGISTRO CUSTODIO *")
                        ->setCellValue('Q7', "NOMBRE CUSTODIO *")
                        ->setCellValue('R7', "COD. MUNICIPIO *")
                        ->setCellValue('S7', "NOMBRE MUNICIPIO *")
                        ->setCellValue('T7', "COD. UBICACIÓN GEOGRÁFICA *")
                        ->setCellValue('U7', "NOMBRE UBICACIÓN GEOGRÁFICA *")
                        ->setCellValue('V7', "COD. CECO *")
                        ->setCellValue('W7', "VICEPRESIDENCIA *")
                        ->setCellValue('X7', "GERENCIA *")
                        ->setCellValue('Y7', "VIDA ÚTIL TOTAL (En Meses) *")
                        ->setCellValue('Z7', "COSTO DEL ACTIVO *")
                        ->setCellValue('AA7', "COD. CAMPO PLANTA *")
                        ->setCellValue('AB7', "NOMBRE CAMPO PLANTA *")
                        ->setCellValue('AC7', "MATRICULA DE VEHICULOS *")
                        ->setCellValue('AD7', "AÑO MODELO DEL VEHICULO *")
                        ->setCellValue('AE7', "ACTIVO ORIGEN **")
                        ->setCellValue('AF7', "SN ORIGEN **")
                        ->setCellValue('AG7', "FECHA CAPITALIZACIÓN ORIGEN **")
                        ->setCellValue('AH7', "VALOR DE ADQUISICIÓN A TRASLADAR **")
                        ->setCellValue('AI7', "MOTIVO DE TRASLADO **")
                        ->setCellValue('AJ7', "OBSERVACIONES");
                    /** AutoSize de las columnas de contenido */ 
                        for ($column = 'B'; $column != 'AJ'; $column++) {
                            $objPHPExcel->getSheet($i)->getColumnDimension($column)
                                ->setAutoSize(true);
                        }
                    /** Habilitar los filtros para las columnnas de contenido */
                        $objPHPExcel->getSheet($i)->setAutoFilter('B7:AI8');
                break;
                case 1:
                    print_header2($objPHPExcel, $i, $properties);
                    /** Combinar celdas */
                        $objPHPExcel->getSheet($i)->mergeCells('B6:E6');
                        $objPHPExcel->getSheet($i)->mergeCells('B7:E7');
                        $objPHPExcel->getSheet($i)->mergeCells('B8:E8');
                        $objPHPExcel->getSheet($i)->mergeCells('C9:D9');
                        
                        for($j=10; $j<=43; $j++){
                            $cellPosition = 'C'.$j.':D'.$j;
                            $objPHPExcel->getSheet($i)->mergeCells($cellPosition);
                        };
                    /** Asignación de formatos a celdas y rengos */
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B6:E43")
                            ->applyFromArray($properties['fontCalibri2'])
                            ->applyFromArray($properties['fontSize11'])
                            ->getAlignment()
                            ->setWrapText(true);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8")
                            ->applyFromArray($properties['fontCalibri'])
                            ->applyFromArray($properties['fondoColorPiel'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B9:E9")
                            ->applyFromArray($properties['fontCalibri'])
                            ->applyFromArray($properties['fondoVerdeOscuro'])
                            ->applyFromArray($properties['fontColorWhite'])
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8:E43")
                            ->applyFromArray($properties['cellBorderThin'])
                            ->applyFromArray($properties['cellBorderColorBlack']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B8:E43")
                            ->applyFromArray($properties['cellOutlineBorderMedium']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("E10:E43")
                            ->applyFromArray($properties['alignCenter']);
                        $objPHPExcel->getSheet($i)
                            ->getStyle("B10:D43")
                            ->applyFromArray($properties['alignCenterVertical']);
                            
    
                    /** Agregar valores de plantilla y encabezados de tabla */
                        function HelpTitles($text1,$text2,$order=2){
                            switch($order){
                                case 1:
                                    $objRichText = new PHPExcel_RichText();
                                    $run1 = $objRichText->createTextRun($text1);
                                    $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                    $run2 = $objRichText->createTextRun($text2);
                                    $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                break;
                                case 2:
                                default:
                                    $objRichText = new PHPExcel_RichText();
                                    $run1 = $objRichText->createTextRun($text1);
                                    $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                    $run2 = $objRichText->createTextRun($text2);
                                    $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                break;
                            }
                            return $objRichText;
                        }
    
                        $objPHPExcel->setActiveSheetIndex($i)
                            ->setCellValue('B6', HelpTitles('* ','Campos Obligatorios',1) )
                            ->setCellValue('B7', HelpTitles('** ','Aplica para un traslados.',1) )
                            ->setCellValue('B8', "INFORMACIÓN DEL FORMATO (Debe estar acorde con lo especificado en el Catálogo de activos fijos vigente GFI-T-004)")
                            ->setCellValue('B9', "CAMPO")
                            ->setCellValue('C9', "DESCRIPCIÓN")
                            ->setCellValue('E9', "Para Operación DIRECTA Ver \"Tabla Catálogo de Activos Fijos - GFI-T-004\"")
                            ->setCellValue('B10', HelpTitles('TIPO DE OPERACIÓN ','*'))
                            ->setCellValue('C10', "Campo de selección, las opciones validas son: DIRECTA o ASOCIADA.\n El tipo de operación ASOCIADA corresponde a los campos operados por el socio del contrato de asociación, de lo contrario será operación DIRECTA.")
                            ->setCellValue('B11', HelpTitles('TIPO DE CREACIÓN ','*'))
                            ->setCellValue('C11', "Campo de selección, las opciones validas son: \nPRINCIPAL (activo fijo principal) o \nSUBNUMERO (adición, mejora o mayor valor a un activo fijo principal).")
                            ->setCellValue('B12', "CÓDIGO ACTIVO FIJO")
                            ->setCellValue('C12', "Código automático generado por SAP/AA para la identificación del Activo Fijo - Asignado por la Coordinación de Activos Fijos. Para solicitud de creación de activos fijos principales se debe dejar en blanco. En caso que requiera la creación de un subnúmero es obligatorio registrar el código del Activo Fijo Principal.")
                            ->setCellValue('B13', "SUBNÚMERO")
                            ->setCellValue('C13', "Código automático generado por SAP/AA para la identificación de la adición, mejora o mayor valor del activo fijo asignado por la Coordinación de Activos Fijos.")
                            ->setCellValue('B14', HelpTitles('COD. CLASE ','*'))
                            ->setCellValue('C14', "Corresponde al código de la clase que se encuentra entre el grupo seleccionado de acuerdo a la \"Tabla Catálogo de Activos Fijos - GFI-T-004\" En el caso que la Creación sea de un SUBNÚMERO debe dejarse en blanco.")
                            ->setCellValue('E14', "X")
                            ->setCellValue('B15', HelpTitles('DENOMINACIÓN ACTIVO FIJO ','*'))
                            ->setCellValue('C15', "Nombre o denominación del activo fijo objeto de creación.\n En los casos de creación de subnúmeros, se debe ingresar el concepto del bien. (Adición, mejora, mayor valor) más la descripción o concepto de los mismos.")
                            ->setCellValue('E15', "X")
                            ->setCellValue('B16', HelpTitles('DENOMINACIÓN COMPLEMENTARIA ','*'))
                            ->setCellValue('C16', "1.  Descripción adicional que permite complementar la denominación del activo.  No aplica para operación asociada. \n2.  Para la creación de Terrenos o Servidumbres se debe indicar la cédula catastral.")
                            ->setCellValue('E16', "X")
                            ->setCellValue('B17', HelpTitles('No. INVENTARIO ','*'))
                            ->setCellValue('C17', "1. Campo numérico que permite la identificación física para el control interno en la compañía, por medio de un sticker o placa. No aplica para activos de operación Asociada.\n2. Para la creación de Terrenos o Servidumbres se debe indicar el Folio de Matricula inmobiliaria.\n3. En caso de activos que son operados por terceros, se debe diligenciar \"No Registra\"")
                            ->setCellValue('B18', HelpTitles('FABRICANTE ','*'))
                            ->setCellValue('C18', "1.  Fabricante o marca del activo fijo objeto de creación. \n2.  Para la creación de Terrenos o Servidumbres se debe indicar el número de la Escritura pública más la fecha de la misma.")
                            ->setCellValue('B19', HelpTitles('MODELO ','*'))
                            ->setCellValue('C19', "1.  Modelo del activo fijo objeto de creación. \n2.  Para la creación de Terrenos o Servidumbres se debe indicar el nombre de la Notaria mas la ciudad de registro.")
                            ->setCellValue('B20', HelpTitles('CAPACIDAD ','*'))
                            ->setCellValue('C20', "Capacidad del activo fijo , debe estar de acuerdo a la dimensión o unidad de medida del activo fijo. ")
                            ->setCellValue('B21', HelpTitles('UNIDAD DE MEDIDA ','*'))
                            ->setCellValue('C21', "Es una determinada magnitud física, definida y adoptada por convención. \nPara operación asociada, se sugiere tomar como guía la \"Tabla Catálogo de Activos Fijos - GFI-T-004\", en caso de ser un activo no catalogado, usar las unidades de medida que acepta SAP.")
                            ->setCellValue('E21', "X")
                            ->setCellValue('B22', HelpTitles('SERIE ', '*'))
                            ->setCellValue('C22', "Número de serie del activo objeto de creación.")
                            ->setCellValue('B23', HelpTitles('TAG ','*'))
                            ->setCellValue('C23', "1. Código asignado  al activo por el area de mantenimiento. No aplica para operación asociada. \n2. Para los terrenos y servidumbres indicar el código SIG (Código único del predio que esta asociado a la base de datos geográfica y alfanumérica).")
                            ->setCellValue('B24', HelpTitles('REGISTRO CUSTODIO ','*'))
                            ->setCellValue('C24', "1.  Registro del funcionario de Ecopetrol responsable de la custodia del Activo Fijo. \n2.  Para operación asociada corresponde al registro del gerente responsable del activo ante la asociación.")
                            ->setCellValue('B25', HelpTitles('NOMBRE CUSTODIO ','*'))
                            ->setCellValue('C25', "Nombre del funcionario de Ecopetrol responsable de la custodia del activo fijo.")
                            ->setCellValue('B26', HelpTitles('COD. MUNICIPIO ','*'))
                            ->setCellValue('C26', "Código SAP del Municipio donde se encuentra el activo objeto de creación.")
                            ->setCellValue('B27', HelpTitles('NOMBRE MUNICIPIO ','*'))
                            ->setCellValue('C27', "Nombre del Municipio donde se encuentra el activo objeto de creación.")
                            ->setCellValue('B28', HelpTitles('COD. UBICACIÓN GEOGRÁFICA ','*'))
                            ->setCellValue('C28', "Código SAP que Permite identificar la localización o un punto físico donde se encuentra instalado el activo.")
                            ->setCellValue('B29', HelpTitles('NOMBRE UBICACIÓN GEOGRÁFICA ','*'))
                            ->setCellValue('C29', "Nombre que permite identificar la localización o un punto físico donde se encuentra instalado el activo.")
                            ->setCellValue('B30', HelpTitles('COD. CECO ','*'))
                            ->setCellValue('C30', "Centro de Costos responsable del activo, el cual asume los costos o gastos por depreciación/amortización.")
                            ->setCellValue('B31', HelpTitles('VICEPRESIDENCIA ','*'))
                            ->setCellValue('C31', "Nombre de la Vicepresidencia responsable del activo fijo. Depende del centro de costos asignado.")
                            ->setCellValue('B32', HelpTitles('GERENCIA ','*'))
                            ->setCellValue('C32', "Nombre de la Gerencia responsable del activo fijo. Depende del centro de costos asignado.")
                            ->setCellValue('B33', HelpTitles('VIDA ÚTIL TOTAL (En Meses) ','*'))
                            ->setCellValue('C33', "Es el dato estimado en meses totales de la Vida útil técnica del activo fijo, Ejemplo:  10 años y 6 meses. \nPara este campo se diligencia el número 126.")
                            ->setCellValue('B34', HelpTitles('COSTO DEL ACTIVO ','*'))
                            ->setCellValue('C34', "Costo razonable a capitalizar del activo fijo objeto de creación. Corresponde a un valor entero. \nSi la creación corresponde por motivo de un traslado, este valor debe ser igual que el registrado en la columna VALOR TRASLADO")
                            ->setCellValue('B35', HelpTitles('COD. CAMPO PLANTA ','*'))
                            ->setCellValue('C35', "Código del campo planta donde se encuentra el activo objeto de creación. Aplica únicamente para activos de la Vicepresidencia de Producción. En caso de no aplicar el campo escribir \"No Aplica\".")
                            ->setCellValue('B36', HelpTitles('NOMBRE CAMPO PLANTA ','*'))
                            ->setCellValue('C36', "Nombre del campo planta donde se encuentra el activo objeto de creación. Aplica únicamente para activos de la Vicepresidencia de Producción. En caso de no aplicar el campo escribir \"No Aplica\".")
                            ->setCellValue('B37', HelpTitles('MATRICULA DE VEHICULOS ','*'))
                            ->setCellValue('C37', "Número de Matricula del vehículo objeto de creación. En caso de no aplicar el campo escribir \"No Aplica\".")
                            ->setCellValue('B38', HelpTitles('AÑO MODELO DEL VEHICULO ','*'))
                            ->setCellValue('C38', "Año del modelo del vehículo. En caso de no aplicar el campo escribir \"No Aplica\".")
                            ->setCellValue('B39', HelpTitles('ACTIVO ORIGEN ','**'))
                            ->setCellValue('C39', "Código SAP del cual se trasladarán los valores y su vida útil a un activo destino.")
                            ->setCellValue('B40', HelpTitles('SN ORIGEN ','**'))
                            ->setCellValue('C40', "Subnúmero SAP del cual se trasladarán los valores y su vida útil a un activo destino.")
                            ->setCellValue('B41', HelpTitles('FECHA CAPITALIZACIÓN ORIGEN  ','**'))
                            ->setCellValue('C41', "Fecha de entrada en operación del activo fijo (Siempre será el último día calendario del mes)")
                            ->setCellValue('B42', HelpTitles('VALOR DE ADQUISICIÓN A TRASLADAR ','*'))
                            ->setCellValue('C42', "Valor de adquisición en pesos a trasladar del Activo Origen al Activo Destino.")
                            ->setCellValue('B43', HelpTitles('MOTIVO DEL TRASLADO ','*'))
                            ->setCellValue('C43', "Registre información sobre el motivo o la razón para efectuar el traslado. Los posibles motivos son: Inventario, Desagregación Asociadas, Corrección Capitalización");
                    /** height of cell */
                        for( $row = 10; $row < 44; $row++ ){
                            $objPHPExcel->getSheet($i)
                                ->getRowDimension($row)
                                ->setRowHeight(45);
                        }
                break;
            }
            
            $i++;
        }
        /*remueve la ultima hoja creada por defecto*/
            $objPHPExcel->removeSheetByIndex(2);
    /** CAPTURAR INFORMACION VIA POST */
        $grupo = $_POST['jsonExcel'];
        $obj = json_decode($grupo, true);
        // print_r($obj);
    /** Recorrer el objeto recibido para poblar el archivo excel */
        $vShift = 8;
        for($i=0; $i<sizeof($obj); $i++){
            $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('B'.($i+$vShift), $obj[$i]['operacion'])
            ->setCellValue('C'.($i+$vShift), $obj[$i]['creacion'])
            ->setCellValue('D'.($i+$vShift), $obj[$i]['activo'])
            ->setCellValue('E'.($i+$vShift), $obj[$i]['sn'])
            ->setCellValue('F'.($i+$vShift), $obj[$i]['codClase'])
            ->setCellValue('G'.($i+$vShift), $obj[$i]['denominacion'])
            ->setCellValue('H'.($i+$vShift), $obj[$i]['denominacion2'])
            ->setCellValue('I'.($i+$vShift), $obj[$i]['inventario'])
            ->setCellValue('J'.($i+$vShift), $obj[$i]['fabricante'])
            ->setCellValue('K'.($i+$vShift), $obj[$i]['modelo'])
            ->setCellValue('L'.($i+$vShift), $obj[$i]['capacidad'])
            ->setCellValue('M'.($i+$vShift), $obj[$i]['unidadMedida'])
            ->setCellValue('N'.($i+$vShift), $obj[$i]['serie'])
            ->setCellValue('O'.($i+$vShift), $obj[$i]['tag'])
            ->setCellValue('P'.($i+$vShift), $obj[$i]['regCustodio'])
            ->setCellValue('Q'.($i+$vShift), $obj[$i]['custodio'])
            ->setCellValue('R'.($i+$vShift), $obj[$i]['codMunicipio'])
            ->setCellValue('S'.($i+$vShift), $obj[$i]['municipio'])
            ->setCellValue('T'.($i+$vShift), $obj[$i]['codUbicacion'])
            ->setCellValue('U'.($i+$vShift), $obj[$i]['ubicacion'])
            ->setCellValue('V'.($i+$vShift), $obj[$i]['codCeco'])
            ->setCellValue('W'.($i+$vShift), $obj[$i]['vicepresidencia'])
            ->setCellValue('X'.($i+$vShift), $obj[$i]['gerencia'])
            ->setCellValue('Y'.($i+$vShift), $obj[$i]['vidaUtilTotal'])
            ->setCellValue('Z'.($i+$vShift), $obj[$i]['costo'])
            ->setCellValue('AA'.($i+$vShift), $obj[$i]['codCampoPlanta'])
            ->setCellValue('AB'.($i+$vShift), $obj[$i]['campoPlanta'])
            ->setCellValue('AC'.($i+$vShift), $obj[$i]['matriculaVehiculo'])
            ->setCellValue('AD'.($i+$vShift), $obj[$i]['modeloVehiculo'])
            ->setCellValue('AE'.($i+$vShift), $obj[$i]['activoOrigen'])
            ->setCellValue('AF'.($i+$vShift), $obj[$i]['snOrigen'])
            ->setCellValue('AG'.($i+$vShift), $obj[$i]['fechaCapOrigen'])
            ->setCellValue('AH'.($i+$vShift), $obj[$i]['valorTraslado'])
            ->setCellValue('AI'.($i+$vShift), $obj[$i]['motivoTraslado'])
            ->setCellValue('AJ'.($i+$vShift), $obj[$i]['observaciones']);
        }
        
        $vShiftTotal = "B".$vShift.":AJ".($vShift + sizeof($obj) - 1) ;
        $objPHPExcel->getSheet(0)
            ->getStyle($vShiftTotal)
            ->applyFromArray($properties['cellBorderColorGreen']);
        
        $vShiftGlobal = "B2:AJ".($vShift + sizeof($obj) - 1) ;
        $objPHPExcel->getSheet(0)
            ->getStyle($vShiftGlobal)
            ->applyFromArray($properties['cellOutlineBorderMedium']);
        
        $vShiftPartial = "AA7:AB".($vShift + sizeof($obj) - 1);
        $vShiftPartial2 = "AC7:AD".($vShift + sizeof($obj) - 1);
        $vShiftPartial3 = "AE7:AI".($vShift + sizeof($obj) - 1);
        $vShiftPartial4 = "AJ7:AJ".($vShift + sizeof($obj) - 1);
        $objPHPExcel->getSheet(0)
            ->getStyle("AA6:AJ6")
            ->applyFromArray($properties['cellBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getStyle("$vShiftPartial")
            ->applyFromArray($properties['cellOutlineBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getStyle("$vShiftPartial2")
            ->applyFromArray($properties['cellOutlineBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getStyle("$vShiftPartial3")
            ->applyFromArray($properties['cellOutlineBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getStyle("$vShiftPartial4")
            ->applyFromArray($properties['cellOutlineBorderMedium']);
    /** mensaje adicional */
        $vShiftMessage = "B".($vShift + sizeof($obj) + 3).":AJ".($vShift + sizeof($obj) + 3) ;
        $vRow = $vShift + sizeof($obj) + 3;
        
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("B".($vShift + sizeof($obj) + 1), "* Campos Obligatorios")
            ->setCellValue("B".($vShift + sizeof($obj) + 2), "** Aplica para traslados")
            ->setCellValue("B".($vShift + sizeof($obj) + 3), "Todos los derechos reservados para Ecopetrol S.A. Ninguna reproducción externa copia o transmisión digital de esta publicación puede ser hecha sin permiso escrito. Ningún párrafo de esta publicación puede ser reproducido, copiado o transmitido digitalmente sin un consentimiento escrito o de acuerdo con las leyes que regulan los derechos de autor y con base en la regulación vigente.");
        $objPHPExcel->getSheet(0)
            ->mergeCells($vShiftMessage)
            ->getStyle($vShiftMessage)
            ->getAlignment()
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ->setWrapText(true);
        $objPHPExcel->getSheet(0)
            ->getStyle($vShiftMessage)
            ->applyFromArray($properties['cellBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getRowDimension($vRow)->setRowHeight(30);
    /** Renombrar Hoja */
        $objPHPExcel->getActiveSheet()->setTitle('datosMaestros');

        $objPHPExcel->getSheet(1)
                    ->setTitle('Ayuda');
    /** Establecer la hoja activa, para que cuando se abra el documento se muestre primero. */
        $objPHPExcel->setActiveSheetIndex(0);
    /** AutoSize de las columnas de contenido */
        for ($column = 'B'; $column != 'AJ'; $column++) {
            $objPHPExcel->getSheet(0)->getColumnDimension($column)
                ->setAutoSize(true);
        }
    /** Habilitar los filtros para las columnnas de contenido */
        $objPHPExcel->getActiveSheet()->setAutoFilter('B7:AJ7');

    /** Borrar todos los archivos excel previamente generados */    
        $files = glob('xls/*'); // get all file names
        foreach($files as $file){ // iterate files
        if(is_file($file))
            unlink($file); // delete file
        };
    /** Nombrar y generar el archivo XLS */
        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $route = 'xls/GFI-F-049-';
        $datenow = date('YmdHis');
        $extension = '.xlsx';
        $name = $route . $datenow . $extension;
        $objWriter->save($name);
        $objName = array(
            'route' => $name,
            'name' => 'GFI-F-049-FORMATO CREACIÓN DE DATOS MAESTROS DE ACTIVOS FIJOS '.$datenow.$extension
        );
        die( json_encode( $objName ) );
?>