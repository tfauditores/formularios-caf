<?php
    require_once('conexion.php');

    /*Manejo de consulta en metodo GET*/
    if($_SERVER['REQUEST_METHOD'] === 'GET'){
        
        /* se obtiene la referencia que varia entre # de inventario y # de activo*/
        $reference = $_GET['id_referencia'];
        $numberRequest = $_GET['id_numero_referencia'];
        $subnumbersList = false;

        
        if($reference == 'subnumeros'){
            $reference = 'activo';
            $subnumbersList = true;
        }

        /*consulta de tabla auxiliar*/

        $query = "SELECT a.departamento, a.nombreMunicipio, a.nombreUbicacionGeografica, a.nombreCeco, a.ceco, a.clase, a.area, a.siglaVicepresidencia, a.siglaGerencia, a.activo, a.sn, a.noInventario, a.denominacion, a.denominacion2, a.fabricante, a.modelo, a.serie, a.tag, a.ubicacionGeografica, a.municipio, a.fechaCapitalizacion2, a.fechaCapitalizacion, a.valorContableNetoAr01, a.valorAdquisicionAr01, a.depreciacionAcumuladaAr01, a.tipoOperacion, a.custodio
                FROM auxiliar AS a
                WHERE a.$reference LIKE '$numberRequest'";
        
        if(isset($_GET['id_subnumero_referencia'])){
            $query.= "AND a.sn LIKE ".$_GET['id_subnumero_referencia']." ORDER BY (a.sn+0) ASC";
        }

        $results = mysqli_query($con, $query);
        $numResults = mysqli_num_rows($results);
        // preg_replace("/\x{00a0}/u", "", $row['denominacion2'])

        $count = "SELECT COUNT(a.$reference) FROM auxiliar AS a WHERE a.$reference LIKE '$numberRequest'";
        $count = mysqli_query($con, $count);
        $count = mysqli_fetch_array($count)[0] - 1;

        if ($numResults > 0) {
            $data = array();
            while( $row = mysqli_fetch_array($results) ){
                $dataTemp = [
                    'departamento' => $row['departamento'],
                    'municipio' => $row['nombreMunicipio'],
                    'ubicacion_geografica' => $row['nombreUbicacionGeografica'],
                    'nombre_ceco' => $row['nombreCeco'],
                    'numero_ceco' => $row['ceco'],
                    'clase' => $row['clase'],
                    'area' => $row['area'],
                    'vicepresidencia' => $row['siglaVicepresidencia'],
                    'gerencia' => $row['siglaGerencia'],
                    'activo' => $row['activo'],
                    'sn' => $row['sn'],
                    'inventario' => $row['noInventario'],
                    'denominacion' => $row['denominacion'],
                    'denominacion2' => $row['denominacion2'],
                    'marca_fabricante' => $row['fabricante'],
                    'modelo' => $row['modelo'],
                    'serie' => $row['serie'],
                    'tag' => $row['tag'],
                    'valor_adquisicion' => $row['valorAdquisicionAr01'],
                    'cod_municipio' => $row['municipio'],
                    'cod_ubicacion_geografica' => $row['ubicacionGeografica'],
                    'fecha_capitalizacion' => $row['fechaCapitalizacion'],
                    'fecha_capitalizacion2' => $row['fechaCapitalizacion2'],
                    'valor_contable_neto' => $row['valorContableNetoAr01'],
                    'valor_depreciacion' => $row['depreciacionAcumuladaAr01'],
                    'tipo_operacion' => $row['tipoOperacion'],
                    'custodio' => $row['custodio'],
                    'conteo_subnumeros_activo' => $count
                ];
                array_push($data,$dataTemp);
            }
            
            if(!$subnumbersList){
                $data = $data[0];
            }
        }
        else{
            $data = [ 'result' => 'invalid active' ] ;
        }
        /*Se envia la información en formato JSON*/
        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode( $data );
    }
    /*Manejo de consulta en metodo POST*/
    else if($_SERVER['REQUEST_METHOD'] === 'POST'){
        echo "hello post";
    }