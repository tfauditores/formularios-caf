<?php
    define('CHARSET','UTF-8');
    /*Manejo de consulta en metodo POST*/
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        /** Error reporting */
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('Europe/London');

            // var_dump($_FILES);
            $unwanted_array = array(    
                'Š', 'š', 'Ž', 'ž', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ý', 'þ', 'ÿ'
            );
            $wanted_array = array(
                'S', 's', 'Z', 'z', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'Ss', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'b', 'y'
            );
            $file = str_replace($unwanted_array, $wanted_array, $_FILES['excel']['name']);
            // $file = iconv('UTF-8','ASCII//TRANSLIT', $_FILES['excel']['name']);
            $file = preg_replace ( '/[ ]/i' , '_' , $file );
            $datenow = date('YmdHis');

            $ext = end((explode(".", $_FILES['excel']['name'])));
            
            if(!is_dir('temp/creaciones')){
                if(!is_dir('temp')){
                    mkdir('temp',0755);
                }
                mkdir('temp/creaciones',0755);
            }
            
            $path = utf8_decode('temp/creaciones/'. basename($datenow.'-'.$file));
            move_uploaded_file($_FILES['excel']['tmp_name'], $path);

        /** Include PHPExcel */
            require_once 'Classes/PHPExcel.php';
            
            switch($ext){
                case 'xls':
                case 'XLS':
                    $objReader = PHPExcel_IOFactory::createReader('Excel5');
                break;
                case 'xlsx':
                case 'XLSX':
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                break;
                default:
                    die;
            }
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($path);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $data = [];
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                array_push($data, $rowData);
            }

            array_push($data, $path);


            /*Se envia la información en formato JSON*/
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
            // echo $file;
    }
    /*Manejo de consulta en metodo GET*/
    else if($_SERVER['REQUEST_METHOD'] === 'GET'){
        require_once('conexion.php');

        if(isset($_GET['type'])){
            $type = $_GET['type'];
            switch($type){
                case 'ubicacion':
                    $municipio = $_GET['municipio'];
                    $ubicacion = $_GET['ubicacion'];
                    $query = "SELECT DISTINCT `departamento`, `codMun`, `municipio`, `codUbi`, `ubicacion` FROM `ubicaciones` WHERE `codMun` LIKE \"$municipio\" AND `codUbi` LIKE \"$ubicacion\"";

                break;
            }

            $results = mysqli_query($con, $query);
            if($results){
                $numResults = mysqli_num_rows($results);
        
                if ($numResults > 0) {

                    $data = array();
                    while( $row = mysqli_fetch_array($results) ){
                        switch($type){
                            case 'ubicacion':
                                $dataTemp = [
                                    'departamento' => $row['departamento'],
                                    'codMun' => $row['codMun'],
                                    'municipio' => $row['municipio'],
                                    'codUbi' => $row['codUbi'],
                                    'ubicacion' => $row['ubicacion']
                                ];
                            break;
                        }
                        array_push($data,$dataTemp);
                    }
                }
                else{
                    $data = [ 'result' => 'invalid data' ] ;
                }
            }
            else{
                $data = [ 'result' => 'invalid format' ] ;
            }
            /*Se envia la información en formato JSON*/
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
        }
    }