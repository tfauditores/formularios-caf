<?php
    /*Manejo de consulta en metodo POST*/
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        /** Error reporting */
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('Europe/London');

            // var_dump($_FILES);
            $file = $_FILES['excel']['tmp_name'];
        /** Include PHPExcel */
            require_once 'Classes/PHPExcel.php';

            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($file);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $data = [];
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                array_push($data, $rowData);
            }

            /*Se envia la información en formato JSON*/
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
    }
    /*Manejo de consulta en metodo GET*/
    else if($_SERVER['REQUEST_METHOD'] === 'GET'){
        require_once('conexion.php');

        $activo_origen = $_GET['activo_origen'];
        $sn_origen = $_GET['sn_origen'];
        $activo_destino = $_GET['activo_destino'];
        $sn_destino = $_GET['sn_destino'];
        
        $query = "SELECT a.activo, a.sn, a.denominacion, a.noInventario, a.fechaCapitalizacion, a.valorAdquisicionAr01
        FROM auxiliar AS a WHERE a.activo = $activo_origen AND a.sn = $sn_origen
        UNION
        SELECT a.activo, a.sn, a.denominacion, a.noInventario, a.fechaCapitalizacion, a.valorAdquisicionAr01
        FROM auxiliar AS a WHERE a.activo = $activo_destino AND a.sn = $sn_destino";

        $results = mysqli_query($con, $query);
        if($results){
            $numResults = mysqli_num_rows($results);
    
            if ($numResults > 0) {
                $data = array();
                while( $row = mysqli_fetch_array($results) ){
                    $date = date_create( $row['fechaCapitalizacion'] );
                    $dataTemp = [
                        'activo' => $row['activo'],
                        'sn' => $row['sn'],
                        'denominacion' => $row['denominacion'],
                        'inventario' => $row['noInventario'],
                        'denominacion' => $row['denominacion'],
                        'fecha_capitalizacion' => date_format( $date, "d/m/Y" ),
                        'valor_adquisicion' => $row['valorAdquisicionAr01']
                    ];
                    array_push($data,$dataTemp);
                }
            }
            else{
                $data = [ 'result' => 'invalid active' ] ;
            }
        }
        else{
            $data = [ 'result' => 'invalid format' ] ;
        }
        /*Se envia la información en formato JSON*/
        // header('Content-type: application/json');
        http_response_code(200);
        echo json_encode( $data );
    }