<?php
    require_once 'Classes/PHPExcel.php';
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<title>Cargar xls</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="../img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<header></header>
	<form action="" method="POST" enctype="multipart/form-data" name="form1">
		<h2>Carga masiva de Transferencias</h2>
		<p>Cargue el formato 88 correctamente diligenciado</p>
		<input type="file" name="archivo" id="archivo"> <br>
		<input type="submit" value="Actualizar">		<br>
    </form>	

    <table>
	<?php
		if(isset($_FILES['archivo'])){
            $allowedExtensions = array("xls", "xlsx");

            $file_name = $_FILES['archivo']['name'];
            $temp_name  = $_FILES['archivo']['tmp_name'];
            $file_type = $_FILES['archivo']['type'];
        /** VERIFICAR SI PESO EXCEDE 5MB */
            if($_FILES['archivo']['size']>=5242880)//5mb
            {
                $fileSize = $_FILES['archivo']['size'];
                //return false;
                die("El archivo es muy pesado");
            }
        /** VERIFICAR SI EXTENSION DEL ARCHIVO ES PERMITIDA */
            $path_parts = pathinfo($file_name);
                
            //$dirname = $path_parts['dirname'];
            //$basename = $path_parts['basename'];
            //$filename = $path_parts['filename'];
            $ext = $path_parts['extension'];
            if(!in_array($ext,$allowedExtensions)) {
                //return false;
                die("La extensión $ext del archivo $file_name no es permitida para el cargue masivo");
            }

        /** MOVER ARCHIVO AL SERVIDOR */
			$server_file = "xls/masivaTransferencia.xls";
			if(is_uploaded_file($temp_name)){
				copy($temp_name, $server_file);
				echo "<strong>Archivo Cargado Correctamente </strong><br><br>";
            }
        /** LEER ARCHIVO CARGADO USANDO PHPEXCEL */
            require_once ('Classes/PHPExcel/IOFactory.php');
            //$objPHPExcel = PHPExcel_IOFactory::load('xls/masivaTransferencia.xls');

            $objReader = PHPExcel_IOFactory::createReader('Excel5');
            //$objReader = PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);

            $server_file = 'xls/masivaTransferencia.xls';
            $objPHPExcel = $objReader->load($server_file);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

        /** VERIFICAR SI EL ARCHIVO XLS CARGADO CORRESPONDE CON EL FORMATO GFI-F-088 */
            $title = $objPHPExcel->getProperties()->getTitle() ;
            $description = $objPHPExcel->getProperties()->getDescription() ;
            if ( $title != "TRANSFERENCIAS" || $description != "FORMATO DE TRANSFERENCIAS" ){
                die("EL ARCHIVO CARGADO NO CORRESPONDE CON EL FORMATO GFI-F-088");
            }

        /** GUARDAR LAS FILAS DEL EXCEL EN UN ARREGLO */
            $objHoja     = $objPHPExcel -> getSheet(0)->toArray(null);
        /** VERIFICAR QUE EL ARCHIVO TENGA REGISTROS */
            if( count($objHoja) > 12 ){
                foreach ($objHoja as $iIndice => $objCelda) {
                    if($iIndice>=5){
                        if($objCelda[1]=="" && $objCelda[3]=="" ){
                            break;
                        }
                        echo '
                            <tr>
                                <td>'.$objCelda[0].'</td>
                                <td>'.$objCelda[1].'</td>
                                <td>'.$objCelda[2].'</td>
                                <td>'.$objCelda[3].'</td>
                                <td>'.$objCelda[4].'</td>
                                <td>'.$objCelda[5].'</td>
                                <td>'.$objCelda[6].'</td>
                                <td>'.$objCelda[7].'</td>
                                <td>'.$objCelda[8].'</td>
                                <td>'.$objCelda[9].'</td>
                                <td>'.$objCelda[10].'</td>
                                <td>'.$objCelda[11].'</td>
                                <td>'.$objCelda[12].'</td>
                                <td>'.$objCelda[13].'</td>
                                <td>'.$objCelda[14].'</td>
                                <td>'.$objCelda[15].'</td>
                                <td>'.$objCelda[16].'</td>
                            </tr>';
                        }
                }
            }
            else{
                echo "No se encontró información en el archivo";
            }
        }

	?>
	</table>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#success">Exitosos</a></li>
        <li><a data-toggle="tab" href="#error">Errores</a></li>
    </ul>

    <div class="tab-content">
        <div id="success" class="tab-pane fade in active">
            <h3>Exitosos</h3>
            <p>Listado de activos que pasaron las pruebas y validaciones</p>
        </div>
        <div id="error" class="tab-pane fade">
            <h3>No se encontraron errores</h3>
        </div>
    </div>

    <script>
        $("form").submit(function(){

        });
    </script>
</body>
</html>