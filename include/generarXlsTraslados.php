<?php
    /** Error reporting */
        error_reporting(E_ALL);
        ini_set('display_errors', TRUE);
        ini_set('display_startup_errors', TRUE);
        date_default_timezone_set('Europe/London');
        /** Include PHPExcel */
        require_once 'Classes/PHPExcel.php';
    /** Crea un nuevo objeto PHPExcel */
        $objPHPExcel = new PHPExcel();
    /** Establecer propiedades del documento a generar*/
        $objPHPExcel->getProperties()
            ->setCompany("TF AUDITORES")
            ->setCreator("TF AUDITORES")
            ->setLastModifiedBy("TF AUDITORES")
            ->setTitle("TRASLADO DE ACTIVOS")
            ->setSubject("TRASLADO DE ACTIVOS")
            ->setDescription("FORMATO DE TRASLADO DE VALORES DE ACTIVOS FIJOS")
            ->setKeywords("TFAUDITORES, TRASLADOS DE ACTIVOS, GFI-F-103")
            ->setCategory("SERVICIOS");
    /** Formatos de celdas  */
        $properties = array(

            'fontVerdana' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontVerdana2' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 9.5,
                    'name' => 'Verdana'
                )
            ),
            'fontCalibri' => array(
                'font' => array(
                    'bold' => true,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontCalibri2' => array(
                'font' => array(
                    'bold' => false,
                    'size' => 10,
                    'name' => 'Calibri'
                )
            ),
            'fontSize14' => array(
                'font' => array(
                    'size' => 14
                )
            ),
            'fontSize12' => array(
                'font' => array(
                    'size' => 12
                )
            ),
            'fontSize11' => array(
                'font' => array(
                    'size' => 11
                )
            ),
            'fontColorWhite' => array(
                'font' => array(
                    'color' => array('rgb' => 'ffffff')
                )
            ),
            'fontColorRed' => array(
                'font' => array(
                    'color' => array('rgb' => 'ff0000')
                )
            ),
            'fontColorGreen' => array(
                'font' => array(
                    'color' => array('rgb' => '004237')
                )
            ),
            'fondoVerde' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'C9D200')
                )
            ),
            'fondoVerdeOscuro' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => '004238')
                )
            ),
            'fondoGris' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'E6E6E6')
                )
            ),
            'fondoColorPiel' => array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'EEECE1')
                )
            ),
            'alignCenter' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterHorizontal' => array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'wrap' => true
                )
            ),
            'alignCenterVertical' => array(
                'alignment' => array(
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                    'wrap' => true
                )
            ),
            'cellBorderDouble' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_DOUBLE,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThin' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellOutlineBorderThin' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderMedium' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderMedium' => array(
                'borders' => array(
                    'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellOutlineBorderMediumRight' => array(
                'borders' => array(
                    'right' => array(
                        'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderThinTop' => array(
                'borders' => array(
                    'top' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            ),
            'cellBorderColorWhite' => array(
                'borders' => array(
                    'allborders' => array(
                        'color' => array('rgb' => 'ffffff')
                    )
                )
            ),
            'cellBorderColorGreen' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '9bbb59')
                    )
                )
            ),
            'cellBorderColorGray' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => 'bfbfbf')
                    )
                )
            ),
            'cellBorderColorBlack' => array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('rgb' => '000000')
                    )
                )
            )
        );
    
                // 'color' => array('rgb' => '9bbb59')
        $i = 0;
        
    /** Print Header*/
        function print_header($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->mergeCells('B1:C3');
                $objPHPExcel->getSheet($sheet)->mergeCells('D1:O1');
                $objPHPExcel->getSheet($sheet)->mergeCells('D2:O2');
                $objPHPExcel->getSheet($sheet)->mergeCells('E3:N3');
            /** Asignación de formatos a celdas y rengos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['cellBorderColorWhite'])
                    ->applyFromArray($properties['fontVerdana2']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B1:O3")
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("D1:O3")
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B5:O5")
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimensionByColumn('A')
                    ->setWidth('1');
            /** Dimensiones de columnas*/
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')
                    ->setRowHeight(24);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')
                    ->setRowHeight(35);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')
                    ->setRowHeight(24);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('D1', "FORMATO DE TRASLADO DE VALORES DE ACTIVOS FIJOS")
                    ->setCellValue('D2', "GESTIÓN FINANCIERA\n GSC - DEPARTAMENTO DE INFRAESTRUCTURA Y TIERRAS - GRUPO CONTROL ACTIVOS FIJOS")
                    ->setCellValue('D3', "GFI-F-103")
                    ->setCellValue('E3', "Elaborado: 17/11/2017")
                    ->setCellValue('O3', "Versión: 1")
                    ->setCellValue('B5', "Fecha Solicitud:");
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
            
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(100);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(235);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B1');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(20);
                $objDrawing->setOffsetY(20);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
        function print_header2($objPHPExcel, $sheet, $properties){
            /** Combinar celdas */
                $objPHPExcel->getSheet($sheet)->mergeCells('B1:B3');
                $objPHPExcel->getSheet($sheet)->mergeCells('C1:E1');
                $objPHPExcel->getSheet($sheet)->mergeCells('C2:E2');
            /** Asignación de formatos a celdas y rengos */
                $objPHPExcel->getDefaultStyle()
                    ->applyFromArray($properties['cellBorderMedium'])
                    ->applyFromArray($properties['cellBorderColorWhite'])
                    ->applyFromArray($properties['fontVerdana2']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B1:E3")
                    ->applyFromArray($properties['alignCenter'])
                    ->applyFromArray($properties['cellBorderMedium']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("C1:E3")
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getStyle("B6:E6")
                    ->applyFromArray($properties['fontVerdana']);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimensionByColumn('A')
                    ->setWidth('1');
            /** Dimensiones de columnas*/
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('1')
                    ->setRowHeight(24);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('2')
                    ->setRowHeight(35);
                $objPHPExcel->getSheet($sheet)
                    ->getRowDimension('3')
                    ->setRowHeight(24);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('B')
                    ->setWidth(38);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('C')
                    ->setWidth(30);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('D')
                    ->setWidth(104);
                $objPHPExcel->getSheet($sheet)
                    ->getColumnDimension('E')
                    ->setWidth(30);
            /** Agregar valores de plantilla y encabezados de tabla */
                $objPHPExcel->setActiveSheetIndex($sheet)
                    ->setCellValue('A1', "")
                    ->setCellValue('C1', "FORMATO DE TRASLADO DE VALORES DE ACTIVOS FIJOS")
                    ->setCellValue('C2', "GESTIÓN FINANCIERA\n GSC - DEPARTAMENTO DE INFRAESTRUCTURA Y TIERRAS - GRUPO CONTROL ACTIVOS FIJOS")
                    ->setCellValue('C3', "GFI-F-103")
                    ->setCellValue('D3', "Elaborado: 17/11/2017")
                    ->setCellValue('E3', "Versión: 1");
            /** Agregar Imagen */
                $objDrawing = new PHPExcel_Worksheet_Drawing();
            
                $image_height_pt = PHPExcel_Shared_Drawing::pixelsToPoints(130);
                $image_width_pt = PHPExcel_Shared_Drawing::pixelsToPoints(350);

                $objDrawing->setName('Logo');
                $objDrawing->setDescription('Logo');
                $logo = '../img/logoForm.jpg';
                $objDrawing->setPath($logo);
                $objDrawing->setCoordinates('B1');
                $objDrawing->setResizeProportional(false);
                $objDrawing->setOffsetX(15);
                $objDrawing->setOffsetY(5);
                $objDrawing->setWidthAndHeight($image_width_pt , $image_height_pt);
                $objDrawing->setWorksheet( $objPHPExcel->getSheet($sheet) ); 
        };
    while($i < 2){
        /** Crear pagina*/
            $objPHPExcel->createSheet($i);

        switch($i){
            case 0:
                print_header($objPHPExcel, $i, $properties);
                /** Combinar celdas */
                    $objPHPExcel->getSheet($i)->mergeCells('B6:G6');
                    $objPHPExcel->getSheet($i)->mergeCells('H6:N6');
                /** Asignación de formatos a celdas y rangos */
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B6:N6")
                        ->applyFromArray($properties['fondoGris'])
                        ->applyFromArray($properties['fontVerdana'])
                        ->applyFromArray($properties['fontColorGreen'])
                        ->applyFromArray($properties['alignCenter'])
                        ->applyFromArray($properties['cellBorderMedium']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B7:O7")
                        ->applyFromArray($properties['fondoVerdeOscuro'])
                        ->applyFromArray($properties['fontVerdana'])
                        ->applyFromArray($properties['fontColorWhite'])
                        ->applyFromArray($properties['alignCenter'])
                        ->applyFromArray($properties['cellBorderThinWhite']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("M7")
                        ->applyFromArray($properties['fondoVerde'])
                        ->applyFromArray($properties['fontColorGreen']);

                /** Agregar valores de plantilla y encabezados de tabla */
                    $datenow = date('d/m/Y');

                    $objPHPExcel->setActiveSheetIndex($i)
                    ->setCellValue('C5', $datenow)
                    ->setCellValue('B6', "CÓDIGO SAP ORIGEN")
                    ->setCellValue('H6', "CÓDIGO SAP DESTINO")
                    ->setCellValue('B7', "CÓDIGO\n ACTIVO FIJO  *")
                    ->setCellValue('C7', "SUBNÚMERO *")
                    ->setCellValue('D7', "DENOMINACIÓN\n ACTIVO FIJO *")
                    ->setCellValue('E7', "No. INVENTARIO *")
                    ->setCellValue('F7', "FECHA\n CAPITALIZACIÓN *")
                    ->setCellValue('G7', "VALOR\n ADQUISICIÓN *")
                    ->setCellValue('H7', "CÓDIGO\n ACTIVO FIJO *")
                    ->setCellValue('I7', "SUBNÚMERO *")
                    ->setCellValue('J7', "DENOMINACIÓN\n ACTIVO FIJO *")
                    ->setCellValue('K7', "No. INVENTARIO *")
                    ->setCellValue('L7', "FECHA\n CAPITALIZACIÓN *")
                    ->setCellValue('M7', "VALOR DE ADQUISICIÓN\n A TRASLADAR *")
                    ->setCellValue('N7', "MOTIVO DEL TRASLADO *")
                    ->setCellValue('O7', "OBSERVACIONES");
                /** AutoSize de las columnas de contenido */    
                    foreach(range('B','O') as $columnID) {
                        $objPHPExcel->getSheet($i)->getColumnDimension($columnID)
                            ->setAutoSize(true);
                    };
                /** Habilitar los filtros para las columnnas de contenido */
                    $objPHPExcel->getSheet($i)->setAutoFilter('B7:O7');
            break;
            case 1:
                print_header2($objPHPExcel, $i, $properties);
                /** Combinar celdas */
                    $objPHPExcel->getSheet($i)->mergeCells('B6:E6');
                    $objPHPExcel->getSheet($i)->mergeCells('B7:E7');
                    $objPHPExcel->getSheet($i)->mergeCells('C8:E8');
                    $objPHPExcel->getSheet($i)->mergeCells('B9:E9');
                    $objPHPExcel->getSheet($i)->mergeCells('B16:E16');

                    for($j=10; $j<=15; $j++){
                        $cellPosition = 'C'.$j.':E'.$j;
                        $objPHPExcel->getSheet($i)->mergeCells($cellPosition);
                    };
                    for($j=17; $j<=24; $j++){
                        $cellPosition = 'C'.$j.':E'.$j;
                        $objPHPExcel->getSheet($i)->mergeCells($cellPosition);
                    };
                /** Asignación de formatos a celdas y rengos */
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B6:E24")
                        ->applyFromArray($properties['fontCalibri2'])
                        ->applyFromArray($properties['fontSize11'])
                        ->getAlignment()
                        ->setWrapText(true);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B7")
                        ->applyFromArray($properties['fontCalibri'])
                        ->applyFromArray($properties['fondoColorPiel'])
                        ->applyFromArray($properties['alignCenter']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B8:E8")
                        ->applyFromArray($properties['fontCalibri'])
                        ->applyFromArray($properties['fondoVerdeOscuro'])
                        ->applyFromArray($properties['fontColorWhite'])
                        ->applyFromArray($properties['alignCenter']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B9")
                        ->applyFromArray($properties['fontCalibri'])
                        ->applyFromArray($properties['fondoGris'])
                        ->applyFromArray($properties['alignCenter']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B16")
                        ->applyFromArray($properties['fontCalibri'])
                        ->applyFromArray($properties['fondoGris'])
                        ->applyFromArray($properties['alignCenter']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B7:E24")
                        ->applyFromArray($properties['cellBorderThin'])
                        ->applyFromArray($properties['cellBorderColorBlack']);
                    $objPHPExcel->getSheet($i)
                        ->getStyle("B7:E24")
                        ->applyFromArray($properties['cellOutlineBorderMedium']);
                        

                /** Agregar valores de plantilla y encabezados de tabla */
                    function HelpTitles($text1,$text2,$order=2){
                        switch($order){
                            case 1:
                                $objRichText = new PHPExcel_RichText();
                                $run1 = $objRichText->createTextRun($text1);
                                $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                                $run2 = $objRichText->createTextRun($text2);
                                $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                            break;
                            case 2:
                            default:
                                $objRichText = new PHPExcel_RichText();
                                $run1 = $objRichText->createTextRun($text1);
                                $run1->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_BLACK ) );
                                $run2 = $objRichText->createTextRun($text2);
                                $run2->getFont()->setColor( new PHPExcel_Style_Color( PHPExcel_Style_Color::COLOR_RED ) );
                            break;
                        }
                        return $objRichText;
                    }

                    $objPHPExcel->setActiveSheetIndex($i)
                        ->setCellValue('B6', HelpTitles('* ','Campos Obligatorios',1) )
                        ->setCellValue('B7', "INFORMACIÓN DEL FORMATO")
                        ->setCellValue('B8', "CAMPO")
                        ->setCellValue('C8', "DESCRIPCIÓN")
                        ->setCellValue('B9', "CÓDIGO SAP ORIGEN (Activo de donde salen los valores a trasladar)")
                        ->setCellValue('B10', HelpTitles('CÓDIGO ACTIVO FIJO ','*'))
                        ->setCellValue('C10', "Código identificación del activo origen del auxiliar de activos fijos de Ecopetrol en SAP/AA, de donde salen los valores a trasladar.")
                        ->setCellValue('B11', HelpTitles('SUBNÚMERO ','*'))
                        ->setCellValue('C11', "Dígitos adicionales al código activo fijo de SAP, inicia con 0 que identifica al activo principal, y si posee capitalizaciones como mayor valor, adiciones o mejoras al activo principal, se identifica con el mismo código de activo fijo principal más el (los) dígito(s) consecutivo(s).")
                        ->setCellValue('B12', HelpTitles('DENOMINACIÓN ACTIVO FIJO ','*'))
                        ->setCellValue('C12', "Nombre o denominación del activo fijo objeto del traslado de valores.")
                        ->setCellValue('B13', HelpTitles('No. INVENTARIO ','*'))
                        ->setCellValue('C13', "Campo numérico que permite la identificación física para el control interno en la compañía, por medio de un sticker o placa del activo fijo origen.")
                        ->setCellValue('B14', HelpTitles('FECHA CAPITALIZACIÓN ','*'))
                        ->setCellValue('C14', "Fecha de capitalización del activo fijo origen.")
                        ->setCellValue('B15', HelpTitles('VALOR ADQUISICIÓN ','*'))
                        ->setCellValue('C15', "Valor de adquisición del activo fijo objeto del traslado de valores.")
                        ->setCellValue('B16', "CÓDIGO SAP DESTINO (Activo que recibe los valores a trasladar)")
                        ->setCellValue('B17', HelpTitles('CÓDIGO ACTIVO FIJO ','*'))
                        ->setCellValue('C17', "Código identificación del activo destino del auxiliar de activos fijos de Ecopetrol en SAP/AA, que recibe los valores a trasladar.")
                        ->setCellValue('B18', HelpTitles('SUBNÚMERO ','*'))
                        ->setCellValue('C18', "Dígitos adicionales al código activo fijo de SAP, inicia con 0 que identifica al activo principal, y si posee capitalizaciones como mayor valor, adiciones o mejoras al activo principal, se identifica con el mismo código de activo fijo principal más el (los) dígito(s) consecutivo(s).")
                        ->setCellValue('B19', HelpTitles('DENOMINACIÓN ACTIVO FIJO ','*'))
                        ->setCellValue('C19', "Nombre o denominación del activo fijo que recibe el traslado de valores.")
                        ->setCellValue('B20', HelpTitles('No. INVENTARIO ','*'))
                        ->setCellValue('C20', "Campo numérico que permite la identificación física para el control interno en la compañía, por medio de un sticker o placa del activo fijo destino.")
                        ->setCellValue('B21', HelpTitles('FECHA CAPITALIZACIÓN ','*'))
                        ->setCellValue('C21', "Fecha de capitalización del activo fijo destino.")
                        ->setCellValue('B22', HelpTitles('VALOR DE ADQUISICIÓN A TRASLADAR ','*'))
                        ->setCellValue('C22', "Valor de adquisición en pesos a trasladar del Activo Origen al Activo Destino.")
                        ->setCellValue('B23', HelpTitles('MOTIVO DEL TRASLADO ','*'))
                        ->setCellValue('C23', "Registre información sobre el motivo o la razón para efectuar el traslado. Los posibles motivos son: Inventario, Desagregación Asociadas, Corrección Capitalización")
                        ->setCellValue('B24', "OBSERVACIONES")
                        ->setCellValue('C24', "Indique algún dato relevante del activo si lo amerita.");
                /** height of cell */
                    $objPHPExcel->getSheet($i)
                        ->getRowDimension('11')
                        ->setRowHeight(30);
                    $objPHPExcel->getSheet($i)
                        ->getRowDimension('18')
                        ->setRowHeight(30);
            break;
        }
            
        $i++;
    };
    /*remueve la ultima hoja creada por defecto*/
        $objPHPExcel->removeSheetByIndex(2);
    /*PARENT'S ROWS*/
        /** CAPTURAR INFORMACION VIA POST */
            $grupo = $_POST['jsonExcel'];
            $obj = json_decode($grupo, true);
            // print_r($obj);
            // $grupo2 = $_POST['jsonExcelChilds'];
            // $obj2 = json_decode($grupo2, true);
        /** Recorrer el objeto recibido para poblar el archivo excel */
            $vShift = 8;
            for($i=0; $i<sizeof($obj); $i++){
                $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('B'.($i+$vShift), $obj[$i]['origen_numero_activo'])
                ->setCellValue('C'.($i+$vShift), $obj[$i]['origen_subnumero_activo'])
                ->setCellValue('D'.($i+$vShift), $obj[$i]['origen_denominacion'])
                ->setCellValue('E'.($i+$vShift), $obj[$i]['origen_numero_inventario'])
                ->setCellValue('F'.($i+$vShift), $obj[$i]['origen_fecha_capitalizacion'])
                ->setCellValue('G'.($i+$vShift), (int)$obj[$i]['origen_valor_adquisicion'])
                ->setCellValue('H'.($i+$vShift), $obj[$i]['destino_numero_activo'])
                ->setCellValue('I'.($i+$vShift), $obj[$i]['destino_subnumero_activo'])
                ->setCellValue('J'.($i+$vShift), $obj[$i]['destino_denominacion'])
                ->setCellValue('K'.($i+$vShift), $obj[$i]['destino_numero_inventario'])
                ->setCellValue('L'.($i+$vShift), $obj[$i]['destino_fecha_capitalizacion'])
                ->setCellValue('M'.($i+$vShift), (int)$obj[$i]['destino_valor_traslado'])
                ->setCellValue('N'.($i+$vShift), $obj[$i]['destino_motivo_traslado'])
                ->setCellValue('O'.($i+$vShift), $obj[$i]['observaciones']);
            };
            $vShiftTotal = "B".$vShift.":O".($vShift + sizeof($obj) - 1) ;
            $vShiftFormat = "G".$vShift.":G".($vShift + sizeof($obj) - 1) ;
            $vShiftFormat2 = "M".$vShift.":M".($vShift + sizeof($obj) - 1) ;
            $vShiftGlobal = "B1:O".($vShift + sizeof($obj) - 1) ;
            
            // print_r($vShiftTotal);
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftTotal)
                ->applyFromArray($properties['cellBorderColorGray']);
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftFormat)
                ->getNumberFormat()
                ->setFormatCode('$ #,##0');
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftFormat2)
                ->getNumberFormat()
                ->setFormatCode('$ #,##0');
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftGlobal)
                ->applyFromArray($properties['cellOutlineBorderMedium']);
            //right border
            $vShiftBorder = "G7:G".($vShift + sizeof($obj) - 1);
            $objPHPExcel->getSheet(0)
                ->getStyle($vShiftBorder)
                ->applyFromArray($properties['cellOutlineBorderMediumRight']);
    /** Renombrar Hoja */
        $objPHPExcel->getSheet(0)
                    ->setTitle('Formato Traslado')
                    ->freezePaneByColumnAndRow(0,8);
        $objPHPExcel->setActiveSheetIndex(0);

        $objPHPExcel->getSheet(1)
            ->setTitle('Ayuda');
    
    /** mensaje adicional */
        $vShiftMessage = "B".($vShift + sizeof($obj) + 1).":O".($vShift + sizeof($obj) + 1) ;
        $vRow = $vShift + sizeof($obj) + 1;
        
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue("B".($vShift + sizeof($obj) + 1), "Todos los derechos reservados para Ecopetrol S.A. Ninguna reproducción externa copia o transmisión digital de esta publicación puede ser hecha sin permiso escrito. Ningún párrafo de esta publicación puede ser reproducido, copiado o transmitido digitalmente sin un consentimiento escrito o de acuerdo con las leyes que regulan los derechos de autor y con base en la regulación vigente.");
        $objPHPExcel->getSheet(0)
            ->mergeCells($vShiftMessage)
            ->getStyle($vShiftMessage)
            ->getAlignment()
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER)
            ->setWrapText(true);
        $objPHPExcel->getSheet(0)
            ->getStyle($vShiftMessage)
            ->applyFromArray($properties['cellBorderMedium']);
        $objPHPExcel->getSheet(0)
            ->getRowDimension($vRow)->setRowHeight(30);

    /** Borrar todos los archivos excel previamente generados */    
        $files = glob('xls/bajas/*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
            unlink($file); // delete file
        };
    /** Nombrar y generar el archivo XLS */
        // $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $route = 'xls/bajas/GFI-F-103-';
        $datenow = date('YmdHis');
        $extension = '.xlsx';
        $name = $route . $datenow . $extension;
        $objWriter->save($name);
        $objName = array(
            'route' => $name,
            'name' => 'GFI-F-103-FORMATO DE TRASLADO DE VALORES DE ACTIVOS FIJOS '.$datenow
        );
        die( json_encode( $objName ) );
?>