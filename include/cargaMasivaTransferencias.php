<?php
    /*Manejo de consulta en metodo POST*/
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        /** Error reporting */
            error_reporting(E_ALL);
            ini_set('display_errors', TRUE);
            ini_set('display_startup_errors', TRUE);
            date_default_timezone_set('Europe/London');

            // var_dump($_FILES);
            $unwanted_array = array(    
                'Š', 'š', 'Ž', 'ž', 'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'Þ', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ð', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ý', 'þ', 'ÿ'
            );
            $wanted_array = array(
                'S', 's', 'Z', 'z', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 'B', 'Ss', 'a', 'a', 'a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'o', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'y', 'b', 'y'
            );
            $file = str_replace($unwanted_array, $wanted_array, $_FILES['excel']['name']);
            // $file = iconv('UTF-8','ASCII//TRANSLIT', $_FILES['excel']['name']);
            $file = preg_replace ( '/[ ]/i' , '_' , $file );
            $datenow = date('YmdHis');
            $ext = end((explode(".", $_FILES['excel']['name'])));
            
            if(!is_dir('temp/transferencias')){
                if(!is_dir('temp')){
                    mkdir('temp',0755);
                }
                mkdir('temp/transferencias',0755);
            }

            $path = 'temp/transferencias/'. basename($datenow.'-'.$file);
            move_uploaded_file($_FILES['excel']['tmp_name'], $path);
        /** Include PHPExcel */
            require_once 'Classes/PHPExcel.php';
            
            switch($ext){
                case 'xls':
                    $objReader = PHPExcel_IOFactory::createReader('Excel5');
                break;
                case 'xlsx':
                    $objReader = PHPExcel_IOFactory::createReader('Excel2007');
                break;
                default:
                    die;
            }
            $objReader->setReadDataOnly(true);

            $objPHPExcel = $objReader->load($path);
            $sheet = $objPHPExcel->getSheet(0); 
            $highestRow = $sheet->getHighestRow(); 
            $highestColumn = $sheet->getHighestColumn();

            $data = [];
            for ($row = 1; $row <= $highestRow; $row++){ 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                array_push($data, $rowData);
            }

            array_push($data, $path);

            /*Se envia la información en formato JSON*/
            header('Content-type: application/json');
            http_response_code(200);
            echo json_encode( $data );
    }
    /*Manejo de consulta en metodo GET*/
    else if($_SERVER['REQUEST_METHOD'] === 'GET'){
        require_once('conexion.php');

        if(isset($_GET['congruence'])){

            $municipio = $_GET['cod_municipio'];
            $ubicacionGeografica = $_GET['cod_ubicacion_geografica'];
            $ceco = $_GET['cod_ceco'];
            $campoPlanta = $_GET['cod_campo_planta'];

            if( $municipio == '' || $ubicacionGeografica == '' || $ceco == '' || $campoPlanta == '' )
            {
                $data = ['value' => 'Incongruence'];
            }
            else
            {

                $query = "SELECT DISTINCT u.codMun, u.municipio, u.codCam, 
                u.campoPlanta, u.codUbi, u.ubicacion, c.ceco 
                FROM ubicaciones AS u 
                LEFT JOIN ceco_campo AS c 
                ON c.codCam LIKE CONCAT('%',u.codCam,'%') 
                WHERE u.codCam LIKE CONCAT('%','$campoPlanta','%') 
                AND u.codMun LIKE CONCAT('%','$municipio','%') 
                AND u.codUbi LIKE CONCAT('%','$ubicacionGeografica','%') 
                AND c.ceco LIKE CONCAT('%','$ceco','%')";

                $results = mysqli_query($con, $query);
                $numResults = mysqli_num_rows($results);
    
                if($numResults > 0){
                    while( $row = mysqli_fetch_array($results) ){
                        $data = [
                            'value' => 'Congruence',
                            'new_municipio' => $row['municipio'],
                            'new_ubicacion' => $row['ubicacion'],
                            'new_campo_planta' => $row['campoPlanta']
                        ];
                    }
                }
                else{
                    $data = ['value' => 'Incongruence'];
                }

            }

        }
        else if(isset($_GET['individual'])){
            
            $table = $_GET['table'];
            $column = $_GET['column'];
            $value = $_GET['value'];

            $query = "SELECT COUNT(*) FROM $table WHERE $column LIKE '%$value%'";

            $results = mysqli_query($con, $query);
            if($results){
                $numResults = mysqli_num_rows($results);
        
                if ($numResults > 0) {
                    while( $row = mysqli_fetch_array($results) ){
                        $data = ['count' => $row['COUNT(*)'] ];
                    }
                }
            }
            
        }
        else{

            isset($_GET['activo']) ? $activo = $_GET['activo'] : $activo = '';
            isset($_GET['inventario']) ? $inventario = $_GET['inventario'] : $inventario = '';
    
            if( strlen( trim($activo) ) > 0){
                
                $query = "SELECT a.activo, a.denominacion, a.noInventario, a.serie, a.clase, a.municipio, a.nombreMunicipio, a.ubicacionGeografica, a.nombreUbicacionGeografica, a.ceco, a.campoPlanta, a.nombreCampoPlanta, a.siglaVicepresidencia, a.vicepresidencia, a.siglaGerencia, a.gerencia, c.claveProd, c.claveOtros
                FROM auxiliar AS a LEFT JOIN clase_clave AS c ON a.clase = c.clase 
                WHERE a.activo = $activo AND a.sn = 0";
    
            }
            else if( strlen( trim($inventario) ) > 0){
                
                $query = "SELECT a.activo, a.denominacion, a.noInventario, a.serie, a.clase, a.municipio, a.nombreMunicipio, a.ubicacionGeografica, a.nombreUbicacionGeografica, a.ceco, a.campoPlanta, a.nombreCampoPlanta, a.siglaVicepresidencia, a.vicepresidencia, a.siglaGerencia, a.gerencia, c.claveProd, c.claveOtros
                FROM auxiliar AS a LEFT JOIN clase_clave AS c ON a.clase = c.clase 
                WHERE a.noInventario = $inventario AND a.sn = 0";
    
            }
    
            $results = mysqli_query($con, $query);
            if($results){
                $numResults = mysqli_num_rows($results);
        
                if ($numResults > 0) {
                    $data = array();
                    while( $row = mysqli_fetch_array($results) ){
                        $dataTemp = [
                            'activo' => $row['activo'],
                            'denominacion' => $row['denominacion'],
                            'inventario' => $row['noInventario'],
                            'serie' => $row['serie'],
                            'clase' => $row['clase'],
                            'cod_municipio' => $row['municipio'],
                            'nombre_municipio' => $row['nombreMunicipio'],
                            'cod_ubicacion_geografica' => $row['ubicacionGeografica'],
                            'ubicacion_geografica' => $row['nombreUbicacionGeografica'],
                            'ceco' => $row['ceco'],
                            'cod_campo_planta' => $row['campoPlanta'],
                            'campo_planta' => $row['nombreCampoPlanta'],
                            'sigla_vicepresidencia' => $row['siglaVicepresidencia'],
                            'vicepresidencia' => $row['vicepresidencia'],
                            'sigla_gerencia' => $row['siglaGerencia'],
                            'gerencia' => $row['gerencia'],
                            'clave_produccion' => $row['claveProd'],
                            'clave_otros' => $row['claveOtros']
                        ];
                        array_push($data,$dataTemp);
                    }
                }
                else{
                    $data = [ 'result' => 'invalid active' ] ;
                }
            }
            else{
                $data = [ 'result' => 'invalid format' ] ;
            }
            
        }
        /*Se envia la información en formato JSON*/
        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode( $data );
    }