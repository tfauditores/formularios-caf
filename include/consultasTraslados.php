<?php
    require_once('conexion.php');

    /*Manejo de consulta en metodo GET*/
    if($_SERVER['REQUEST_METHOD'] === 'GET'){
        $numero_activo = $_GET['id_numero_activo'];

        $query = "SELECT a.activo, a.sn, a.denominacion, a.fechaCapitalizacion, a.valorAdquisicionAr01, a.ceco, a.claveAr01, a.vuAnosAr01, a.vuMesesAr01, a.vuPeriodosAr01, a.vuRemanenteAr01, a.valorContableNetoAr01, a.bloqueado, a.noInventario
                FROM auxiliar AS a
                WHERE a.activo LIKE '$numero_activo'" ;
        
        $results = mysqli_query($con, $query);
        $numResults = mysqli_num_rows($results);

        if($numResults){
            $count = "SELECT COUNT(a.activo) FROM auxiliar AS a WHERE a.activo LIKE '$numero_activo'";
    
            $count = mysqli_query($con, $count);
            $count = mysqli_fetch_array($count)[0];
    
            $data = [];
            while( $row = mysqli_fetch_array($results) ){
    
                $date = date('d/m/Y',  strtotime(  str_replace('/', '-',  $row['fechaCapitalizacion']) )  );
    
                $dataTemp = [
                    'numero_activo' => $row['activo'],
                    'sn' => $row['sn'],
                    'denominacion' => $row['denominacion'],
                    'fecha_capitalizacion' => $date,
                    'valor_adquisicion' => $row['valorAdquisicionAr01'],
                    'ceco' => $row['ceco'],
                    'clave_amortizacion' => $row['claveAr01'],
                    'vida_util_anios' => $row['vuAnosAr01'],
                    'vida_util_meses' => $row['vuPeriodosAr01'],
                    'vida_util_total' => $row['vuMesesAr01'],
                    'vida_util_remanente' => $row['vuRemanenteAr01'],
                    'valor_contable_neto' => $row['valorContableNetoAr01'],
                    'bloqueado' => $row['bloqueado'],
                    'numero_inventario' => $row['noInventario'],
                    'subnumeros' => $count - 1
                ];

                array_push($data,$dataTemp);
            }
        }
        else{
            $data = [ 'result' => 'invalid active' ] ;
        }

        header('Content-type: application/json');
        http_response_code(200);
        echo json_encode( $data );
    }
    /*Manejo de consulta en metodo POST*/
    else if($_SERVER['REQUEST_METHOD'] === 'POST'){
        echo "hello post";
    }