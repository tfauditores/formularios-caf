<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    // session_start();
    // var_dump(session_id());
    // echo $_GET['asd'];
?>
<!DOCTYPE html>
<html lang="es_CO">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Gestión de bajas auxiliar</title>
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
    <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-darkness.css">
    <link rel="stylesheet" href="css/altas.css">
    <link rel="stylesheet" href="css/generic.css">
    <link rel="stylesheet" type="text/css" href="css/masive.css">
    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <main class="main-content">
        <section class="module">
            <div class="container big-container">
                <form action="" method="post" class="withdrawal-form" id="formatoBajas">
                    <div class="row text-left nav-view">
                        <label class="btn btn-styled btn-single fix-margin active">
                            Baja Manual
                        </label>
                        <label class="btn btn-styled2 btn-multiple fix-margin">
                            Baja Múltiple
                            <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xls">
                        </label>
                        <a href="formats/GFI-F-055-formato-autorizacion-para-retiro-de-activos-fijos-de-operacion.xlsx" download>
                            <div class="btn-excel">
                                <span>FORMATO F-055</span>
                                <img src="img/excel.png" alt=""/>
                            </div>
                        </a>
                    </div>
                    <div class="single-view">
                        <div class="row fields-group">
                            <div class="col-lg-12 mb20">
                                <label class="label-selector">
                                    TIPO DE BAJA:
                                </label>
                                <label class="label-selector">
                                    <input name="tipo_baja" type="radio" value="baja_total" checked> <i></i>
                                    <span>BAJA TOTAL <sup>2</sup></span>
                                </label>
                                <label class="label-selector">
                                    <input name="tipo_baja" type="radio" value="baja_parcial"> <i></i>
                                    <span>BAJA PARCIAL <sup>3</sup> <!--<sup>*</sup>--></span>
                                </label>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-section clearfix show-subnumber hide-subnumber">
                                    <div class="block">
                                        <label>NÚMERO DE ACTIVO: <sup>1</sup></label>
                                        <input type="text" name="activo" class="form-control" autofocus>
                                    </div>
                                    <div class="block subnumber-block">
                                        <span class="subnumber">Subnúmero:</span>
                                        <input type="number" name="subnumero" class="form-control" placeholder="SUBNÚMERO" value="0" min="0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-section">
                                    <label>NÚMERO DE INVENTARIO (PLACA): <sup>1</sup></label>
                                    <input type="text" name="inventario" class="form-control">
                                </div> 
                            </div>
                            <div class="col-lg-3 col-md-3 sigma">
                                <label>&Sigma; VALOR CONTABLE NETO <span class="tag"></span></label>
                                <span class="value"></span>
                            </div>
                            <div class="col-lg-6 col-md-6 withdrawal-partial">
                                <div class="block col-lg-4 col-md-4">
                                    <div class="form-section">
                                        <label>VALOR ADQUISICION*:</label>
                                        <input type="hidden" name="number_adq_total" class="hidden-form-control">
                                        <input type="text" name="valor_adquisicion" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="block col-lg-4 col-md-4">
                                    <div class="form-section">
                                        <label>BAJA PARCIAL (BP):</label>
                                        <input type="hidden" name="number_baja_total" class="hidden-form-control">
                                        <input type="text" name="number_baja_parcial" class="form-control">
                                    </div>
                                </div>
                                <div class="block col-lg-4 col-md-4">
                                    <div class="form-section">
                                        <label><span data-toggle="tooltip" data-placement="top" title="SALDO">(V.Adq)</span> - <span data-toggle="tooltip" data-placement="top" title="BAJA PARCIAL">BP</span></label>
                                        <span class="withdrawal-result"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row fields-group">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-section">
                                    <label>MOTIVO DEL RETIRO: <sup>4</sup></label>
                                    <select name="motivo_retiro" class="form-control">
                                        <option value="">SELECCIONE...</option>
                                        <option value="DACIÓN EN PAGO">DACIÓN EN PAGO</option>
                                        <option value="DEMOLICIÓN DE CONSTRUCCIÓN">DEMOLICIÓN DE CONSTRUCCIÓN</option>
                                        <option value="DESMANTELAMIENTO DE FACILIDADES">DESMANTELAMIENTO DE FACILIDADES</option>
                                        <option value="DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO ">DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO </option>
                                        <option value="ESCISIÓN">ESCISIÓN</option>
                                        <option value="NO SE REQUIERE PARA LA OPERACIÓN">NO SE REQUIERE PARA LA OPERACIÓN</option>
                                        <option value="OBSOLESCENCIA O DESUSO">OBSOLESCENCIA O DESUSO</option>
                                        <option value="PÉRDIDA O HURTO">PÉRDIDA O HURTO</option>
                                        <option value="PÉRDIDA O HURTO EN PODER DE TERCEROS">PÉRDIDA O HURTO EN PODER DE TERCEROS</option>
                                        <option value="PERMUTA O CAMBIO">PERMUTA O CAMBIO</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-section">
                                    <label>DISPOSICIÓN FINAL PROPUESTA: <sup>4</sup></label>
                                    <select name="disposicion_final" class="form-control">
                                        <option value="">SELECCIONE...</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label>OBSERVACIONES:</label>
                                <textarea name="observaciones" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="row show-characteristics">
                            <span>Ver características <i class="ion ion-eye"></i></span>
                        </div>
                        <div class="slide-info row">

                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DEPARTAMENTO:</label>
                                    <span data-name="departamento"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CIUDAD:</label>
                                    <span data-name="municipio"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>UBICACIÓN GEOGRÁFICA:</label>
                                    <span data-name="ubicacion_geografica"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CECO (CENTRO DE COSTOS):</label>
                                    <span data-name="nombre_ceco"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>NÚMERO DE CECO (CENTRO DE COSTOS):</label>
                                    <span data-name="numero_ceco"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>ÁREA:</label>
                                    <span data-name="area"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>VICEPRESIDENCIA:</label>
                                    <span data-name="vicepresidencia"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>GERENCIA:</label>
                                    <span data-name="gerencia"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DENOMINACIÓN:</label>
                                    <span data-name="denominacion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DENOMINACIÓN 2:</label>
                                    <span data-name="denominacion_2"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>MARCA DEL FABRICANTE:</label>
                                    <span data-name="marca_fabricante"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>MODELO:</label>
                                    <span data-name="modelo"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>SERIE:</label>
                                    <span data-name="serie"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>TAG:</label>
                                    <span data-name="tag"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>VALOR DE ADQUISICIÓN:</label>
                                    <span data-name="valor_adquisicion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>VALOR CONTABLE NETO:</label>
                                    <span data-name="valor_contable_neto"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 hidden">
                                <div class="form-section">
                                    <label>CÓDIGO DE MUNICIPIO:</label>
                                    <span data-name="cod_municipio"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 hidden">
                                <div class="form-section">
                                    <label>CÓDIGO DE UBICACIÓN GEOGRÁFICA:</label>
                                    <span data-name="cod_ubicacion_geografica"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>FECHA DE CAPITALIZACIÓN:</label>
                                    <span data-name="fecha_capitalizacion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>TIPO DE OPERACIÓN:</label>
                                    <span data-name="tipo_operacion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CUSTODIO:</label>
                                    <span data-name="custodio"></span>
                                </div>
                            </div>
                            
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 text-center">
                                <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button>
                                <button class="btn btn-styled2 btn-end" type="button">FINALIZAR SOLICITUD</button>
                            </div>
                        </div>
                    </div>
                    <div class="multiple-view">
                        <div class="status-block">
                            <nav class="nav-tabs" data-tab="multiple">
                                <ul>
                                    <li class="active" data-type="success">Activos agregados <i>0</i></li>
                                    <li data-type="error">Activos con errores <i>0</i></li>
                                </ul>
                            </nav>
                            <div class="tabs" data-tab="multiple">
                                <div class="tab active">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-secondary button2 btn-end">Finalizar solicitud</button>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                    </div>
                                    <div class="error-multiple">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="inline-table">
                    <table class="table table-striped table-responsive table-bajas table-data">
                        <thead class="thead-default">
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Tipo de baja</th>
                                <th># de activo</th>
                                <th>SN</th>
                                <th>Denominación</th>
                                <th># de inventario</th>
                                <th>Serie</th>
                                <th>Tag</th>
                                <th>CeCo</th>
                                <th>Sigla Vicepresidencia</th>
                                <th>Sigla Gerencia</th>
                                <th>Valor de baja</th>
                                <th>Motivo del retiro</th>
                                <th>Disposición final</th>
                                <th>Observación</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row disclaimer">
                    <div class="col-lg-12 mv20">
                        <p>
                            Valor neto corresponde a la fecha de diligenciamiento del formato. Puede ser superior al valor real de baja, dado que la fecha de aprobación y ejecución en el sistema es poterior a la fecha de diligenciamiento de la solicitud
                            <br/><sup>1</sup> Ingrese número de activo o número de inventario para hacer la consulta. 
                            <br/><sup>2</sup> La BAJA TOTAL aplica solamente para activos principales (los subnúmeros serán incluidos). 
                            <br/><sup>3</sup> En BAJA PARCIAL es posible solicitar la baja del valor total o parcial de adquisición de un subnúmero.
                            <br/><sup>4</sup> Los motivos de baja y las respectivas disposiciones corresponden con el formato GIF-P-030 Procedimiento Administrativo Activos Fijos.
                        </p>
                    </div>
                </div>                
            </div>
        </section>
    </main>
    <footer class="main-footer">
        <div class="col-lg-12 supportedBrowsers">Aplicación probada y soportada en los navegadores:</div>
        <div class="col-lg-12 browserIcons">
            <span title="Descargar Chrome" class="browser">
                <a href="https://www.google.com/chrome/" target="_blank">CHROME</a>
            </span>
            <span title="Descargar Firefox" class="browser firefox">
                <a href="https://www.mozilla.org/es-ES/firefox/" target="_blank">FIREFOX</a>
            </span>
            <span title="Abrir en Edge" class="browser edge">
                <a href="microsoft-edge:https://ecopetrol.sharepoint.com/sites/CAF/SitePages/solicitud-de-bajas.aspx"> EDGE</a>
            </span>
            <span title="Descargar Opera" class="browser opera">
                <a href="https://www.opera.com/es-419/download" target="_blank">OPERA</a>
            </span>
        </div>
    </footer>
    <div class="thankyou-message">
        <div class="table">
            <div class="table-cell">
                <h2 class="title">GRACIAS POR REALIZAR LA SOLICITUD, <br>PRONTO ATENDEREMOS SU REQUERIMIENTO</h2>
            </div>
        </div>
    </div>
    <div id="wait">
        <div>
            <img src='img/demo_wait.gif' width="64" height="64" />
            <br>Cargando...
        </div>
    </div>
    <script src="vendor/jquery/jquery-1.12.4.js"></script>
    <script src="vendor/jqueryui/jquery-ui.js"></script>
    <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
    <script src="js/lib/jquery.priceformat.min.js"></script>
    <script src="js/lib/jquery.validate.min.js"></script>
    <script src="js/bajas.js"></script>
</body>
</html>