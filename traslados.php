<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    // session_start();
    // var_dump(session_id());
?>
<!DOCTYPE html>
<html lang="es_CO">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Traslados auxiliar</title>
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
    <link rel="stylesheet" href="vendor/jqueryui/jqueryui-1-11-2-theme-base.css">
    <link rel="stylesheet" href="css/altas.css">
    <link rel="stylesheet" href="css/generic.css">
    <link rel="stylesheet" href="css/masive.css">

    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <main class="main-content">
        <section class="module">
            <div class="container big-container">
                <form action="" method="post" class="withdrawal-form" id="formatoTraslados">
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <label class="btn btn-styled btn-multiple fix-margin">
                                Bajas multiples
                                <input type="file" name="excel-file" class="upload-control" accept=".xlsx">
                            </label>
                        </div>
                    </div> -->
                    <div class="single-view">
                        <p class="red">* Ingrese número de activo o número de inventario para hacer la consulta</p>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 activo_origen">
                                <div class="mb20">
                                    <h2 class="title">ACTIVO ORIGEN</h2>
                                </div>
                                <div class="fields-group">
                                    <div class="form-section clearfix">
                                        <div class="block">
                                            <label>NÚMERO DE ACTIVO:</label>
                                            <input type="text" name="id_numero_activo" class="form-control">
                                        </div>
                                        <div class="block subnumber-block">
                                            <span class="subnumber">Subnúmero:</span>
                                            <input type="number" name="id_subnumero_activo" class="form-control" placeholder="SUBNÚMERO" value="0" min="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-section">
                                            <label>VALOR DE ADQUISICIÓN:</label>
                                            <input type="text" name="valor_adquisicion" class="form-control" disabled>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-section">
                                            <label>VALOR DISPONIBLE PARA TRASLADAR:</label>
                                            <span data-name="valor_disponible"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row show-characteristics abs">
                                    <span>Ver características <i class="ion ion-eye"></i></span>
                                </div>
                                <div class="row-slide-info">
                                    <div class="slide-info dont-show">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>FECHA DE CAPITALIZACIÓN:</label>
                                                <span data-name="fecha_capitalizacion"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>DENOMINACIÓN:</label>
                                                <span data-name="denominacion"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL AÑOS:</label>
                                                <span data-name="vida_util_a"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL MESES:</label>
                                                <span data-name="vida_util_m"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL TOTAL:</label>
                                                <span data-name="vida_util_t"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>CLAVE DE AMORTIZACIÓN:</label>
                                                <span data-name="clave_amortizacion"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>CECO:</label>
                                                <span data-name="ceco"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL REMANENTE:</label>
                                                <span data-name="vida_util_remanente"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>VALOR CONTABLE NETO:</label>
                                                <span data-name="valor_contable_neto"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>No INVENTARIO:</label>
                                                <span data-name="numero_inventario"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 activo_destino">
                                <div class="mb20">
                                    <h2 class="title">ACTIVO DESTINO</h2>
                                </div>
                                <div class="fields-group">
                                    <div class="form-section clearfix">
                                        <div class="block">
                                            <label>NÚMERO DE ACTIVO:</label>
                                            <input type="text" name="id_numero_activo_destino" class="form-control">
                                        </div>
                                        <div class="block subnumber-block">
                                            <span class="subnumber">Subnúmero:</span>
                                            <input type="number" name="id_subnumero_activo_destino" class="form-control" placeholder="SUBNÚMERO" value="0" min="0">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-section">
                                            <label>MOTIVO DEL TRASLADO:</label>
                                            <select name="motivo_traslado" class="form-control">
                                                <option value="">SELECCIONE...</option>
                                                <option value="INVENTARIO">INVENTARIO</option>
                                                <option value="DESAGREGACIÓN ASOCIADAS">DESAGREGACIÓN ASOCIADAS</option>
                                                <option value="CORRECCIÓN DE CAPITALIZACIÓN">CORRECCIÓN DE CAPITALIZACIÓN</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-md-6">
                                        <div class="form-section">
                                            <label>VALOR DE TRASLADO:</label>
                                            <input type="text" name="valor_traslado" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row-slide-info">
                                    <div class="slide-info dont-show">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>FECHA DE CAPITALIZACIÓN:</label>
                                                <span data-name="fecha_capitalizacion_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>DENOMINACIÓN:</label>
                                                <span data-name="denominacion_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL AÑOS:</label>
                                                <span data-name="vida_util_a_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL MESES:</label>
                                                <span data-name="vida_util_m_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL TOTAL:</label>
                                                <span data-name="vida_util_t_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>CLAVE DE AMORTIZACIÓN:</label>
                                                <span data-name="clave_amortizacion_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>CECO:</label>
                                                <span data-name="ceco_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>VIDA ÚTIL REMANENTE:</label>
                                                <span data-name="vida_util_remanente_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-section">
                                                <label>VALOR DE ADQUISICIÓN:</label>
                                                <span data-name="valor_adquisicion_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>VALOR CONTABLE NETO:</label>
                                                <span data-name="valor_contable_neto_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>BLOQUEADO:</label>
                                                <span data-name="bloqueado_destino"></span>
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 hidden">
                                            <div class="form-section">
                                                <label>No INVENTARIO:</label>
                                                <span data-name="numero_inventario_destino"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 observaciones">
                                <div class="form-section clearfix">
                                    <label>OBSERVACIONES:</label>
                                    <textarea name="observaciones" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 text-center">
                                <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button>
                                <button class="btn btn-styled2 btn-end" type="button">FINALIZAR SOLICITUD</button>
                            </div>
                        </div>
                    </div>
                    <div class="multiple-view">
                        <div class="status-block">
                            <nav class="nav-tabs" data-tab="multiple">
                                <ul>
                                    <li class="active" data-type="success">Existosos <i>0</i></li>
                                    <li data-type="error">Errores <i>0</i></li>
                                </ul>
                            </nav>
                            <div class="tabs" data-tab="multiple">
                                <div class="tab active">
                                    <div class="cover-table">
                                        <table class="success-multiple traslados">
                                            <thead>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <th colspan="6">CÓDIGO SAP ORIGEN</th>
                                                    <th colspan="7">CÓDIGO SAP DESTINO</th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                                <tr>
                                                    <th>Fila</th>
                                                    <th>Código activo fijo</th>
                                                    <th>Subnúmero</th>
                                                    <th>Denominación</th>
                                                    <th>No. Inventario</th>
                                                    <th>Fecha de capitalización</th>
                                                    <th>Valor adquisición</th>
                                                    <th>Código activo fijo</th>
                                                    <th>Subnúmero</th>
                                                    <th>Denominación</th>
                                                    <th>No. Inventario</th>
                                                    <th>Fecha de capitalización</th>
                                                    <th>Valor adquisición a trasladar</th>
                                                    <th>Motivo del traslado</th>
                                                    <th>Observaciones</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                    <div class="text-center">
                                        <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="error-multiple">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- <div class="ctrls-table">
                    <span class="ion ion-android-apps" data-toggle="tooltip" title="Ver otras características"></span>
                </div> -->
                <div class="inline-table type2">
                    <table class="table table-striped table-responsive table-traslados">
                        <thead class="thead-default">
                            <tr class="border-top">
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th colspan="4" class="green border-right">ORIGEN</th>
                                <th colspan="4" class="green">TRASLADO</th>
                                <th>&nbsp;</th>
                                <th class="hide-column">&nbsp;</th>
                            </tr>
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Número de activo</th>
                                <th>SN</th>
                                <th>Fecha de capitalización</th>
                                <th class="hide-column separate-column">Número de inventario</th>
                                <th class="hide-column separate-column">Denominación</th>
                                <th class="hide-column separate-column">Vida útil años</th>
                                <th class="hide-column separate-column">Vida útil meses</th>
                                <th class="hide-column separate-column">Clave de amorización</th>
                                <th class="hide-column separate-column">Ceco</th>
                                <th class="border-right">Valor de adquisición</th>
                                <th>Número de activo</th>
                                <th>SN</th>
                                <th>Fecha de capitalización</th>
                                <th class="hide-column separate-column">Número de inventario</th>
                                <th class="hide-column separate-column">Denominación</th>
                                <th class="hide-column separate-column">Vida útil años</th>
                                <th class="hide-column separate-column">Vida útil meses</th>
                                <th class="hide-column separate-column">Clave de amorización</th>
                                <th class="hide-column separate-column">Ceco</th>
                                <th>Valor de traslado</th>
                                <th>Motivo del traslado</th>
                                <th class="hide-column">Observaciones</th>
                                <th class="always-hide-column">Valor contable neto origen</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-lg-12 show-list">
                        <label data-modal="traslados">Ver listado de traslados <i class="ion ion-android-list" data-modal="bajas"><sup>10</sup></i></label>
                    </div>
                </div>
            </div>
        </section>
        <div class="modalbox" data-modal="traslados">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="ctrls-table">
                            <span class="ion ion-android-apps" data-toggle="tooltip" title="Ver otras características"></span>
                        </div>
                        <div class="cover-table">
                            <table class="table table-striped table-responsive table-traslados">
                                <thead class="thead-default">
                                <tr class="border-top">
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th colspan="4" class="green border-right">ORIGEN</th>
                                    <th colspan="4" class="green">TRASLADO</th>
                                    <th>&nbsp;</th>
                                    <th class="hide-column">&nbsp;</th>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <th>&nbsp;</th>
                                    <th>Número de activo</th>
                                    <th>SN</th>
                                    <th>Fecha de capitalización</th>
                                    <th class="hide-column separate-column">Número de inventario</th>
                                    <th class="hide-column separate-column">Denominación</th>
                                    <th class="hide-column separate-column">Vida útil años</th>
                                    <th class="hide-column separate-column">Vida útil meses</th>
                                    <th class="hide-column separate-column">Clave de amorización</th>
                                    <th class="hide-column separate-column">Ceco</th>
                                    <th class="border-right">Valor de adquisición</th>
                                    <th>Número de activo</th>
                                    <th>SN</th>
                                    <th>Fecha de capitalización</th>
                                    <th class="hide-column separate-column">Número de inventario</th>
                                    <th class="hide-column separate-column">Denominación</th>
                                    <th class="hide-column separate-column">Vida útil años</th>
                                    <th class="hide-column separate-column">Vida útil meses</th>
                                    <th class="hide-column separate-column">Clave de amorización</th>
                                    <th class="hide-column separate-column">Ceco</th>
                                    <th>Valor de traslado</th>
                                    <th>Motivo del traslado</th>
                                    <th class="hide-column">Observaciones</th>
                                    <th class="always-hide-column">Valor contable neto origen</th>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modalbox" data-modal="subnumbers">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table"></div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <footer class="main-footer">

    </footer>
    <div id="wait">
        <div>
            <img src='img/demo_wait.gif' width="64" height="64" />
            <br>Cargando...
        </div>
    </div>
    <script src="vendor/jquery/jquery-1.12.4.js"></script>
    <script src="vendor/jqueryui/jquery-ui.js"></script>
    <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
    <script src="js/lib/jquery.priceformat.min.js"></script>
    <script src="js/lib/jquery.validate.min.js"></script>
    <script src="js/functions-traslados.js"></script>
    <script src="js/data-load-traslados.js"></script>
</body>
</html>