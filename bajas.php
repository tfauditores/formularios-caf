<?php
	/** Check origin */
	require_once 'include/checkOrigin.php';
    // session_start();
    // var_dump(session_id());
    // echo $_GET['asd'];
?>
<!DOCTYPE html>
<html lang="es_CO">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Bajas auxiliar</title>
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap3-3-7.min.css">
    <link rel="stylesheet" href="css/altas.css">
    <link rel="stylesheet" href="css/generic.css">
    <link rel="stylesheet" type="text/css" href="css/masive.css">
    <link href="img/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
    <main class="main-content">
        <section class="module">
            <div class="container big-container">
                <form action="" method="post" class="withdrawal-form" id="formatoBajas">
                    <div class="row text-left nav-view">
                        <label class="btn btn-styled btn-single fix-margin active">
                            Baja sencilla
                        </label>
                        <label class="btn btn-styled2 btn-multiple fix-margin">
                            Bajas multiples
                            <input type="file" name="excel-file" class="upload-control" accept=".xlsx,.xls">
                        </label>
                        <a href="formats/GFI-F-055-FORMATO DE AUTORIZACIÓN PARA RETIRO DE ACTIVOS FIJOS DE LA OPERACIÓN.XLSX" download>
                            <!-- <label class="btn btn-styled btn-format fix-margin">
                                Formato <i class="icon ion-document-text"></i>
                            </label> -->
                            <div class="btn-excel">
                                <span>FORMATO F-055</span>
                                <img src="img/excel.png" alt=""/>
                            </div>
                        </a>
                    </div>
                    <div class="single-view">
                        <div class="row fields-group">
                            <div class="col-lg-12 mb20">
                                <label class="label-selector">
                                    TIPO DE BAJA:
                                </label>
                                <label class="label-selector">
                                    <input name="tipo_baja" type="radio" value="baja_total" checked> <i></i>
                                    <span>BAJA TOTAL **</span>
                                </label>
                                <label class="label-selector">
                                    <input name="tipo_baja" type="radio" value="baja_parcial"> <i></i>
                                    <span>BAJA PARCIAL *** <!--<sup>*</sup>--></span>
                                </label>
                                <!-- <div class="btn btn-styled btn-multiple mt00">Bajas multiples</div> -->
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-section clearfix show-subnumber hide-subnumber">
                                    <div class="block">
                                        <label>NÚMERO DE ACTIVO: *</label>
                                        <input type="text" name="id_numero_activo" class="form-control">
                                    </div>
                                    <div class="block subnumber-block">
                                        <span class="subnumber">Subnúmero:</span>
                                        <input type="number" name="id_subnumero_activo" class="form-control" placeholder="SUBNÚMERO" value="0" min="0">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-section">
                                    <label>NÚMERO DE INVENTARIO (PLACA): *</label>
                                    <input type="text" name="id_numero_inventario" class="form-control">
                                </div> 
                            </div>
                            <div class="col-lg-6 col-md-6 withdrawal-partial">
                                <div class="block col-lg-4 col-md-4">
                                    <div class="form-section">
                                        <label>VALOR DE ADQUISICIÓN (VA):</label>
                                        <input type="text" name="valor_adquisicion" class="form-control" disabled>
                                    </div>
                                </div>
                                <div class="block col-lg-4 col-md-4">
                                    <div class="form-section">
                                        <label>BAJA PARCIAL (BP):</label>
                                        <input type="hidden" name="number_baja_total" class="hidden-form-control">
                                        <input type="text" name="number_baja_parcial" class="form-control">
                                    </div>
                                </div>
                                <div class="block col-lg-4 col-md-4">
                                    <div class="form-section">
                                        <label><span data-toggle="tooltip" data-placement="top" title="VALOR DE ADQUISICIÓN">VA</span> - <span data-toggle="tooltip" data-placement="top" title="BAJA PARCIAL">BP</span></label>
                                        <span class="withdrawal-result"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row fields-group">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-section">
                                    <label>MOTIVO DEL RETIRO:</label>
                                    <select name="motivo_retiro" class="form-control">
                                        <option value="SELECCIONE...">SELECCIONE...</option>
                                        <option value="OBSOLESCENCIA O DESUSO">OBSOLESCENCIA O DESUSO</option>
                                        <option value="DEMOLICIÓN DE CONSTRUCCIÓN">DEMOLICIÓN DE CONSTRUCCIÓN</option>
                                        <option value="DESMANTELAMIENTO DE FACILIDADES">DESMANTELAMIENTO DE FACILIDADES</option>
                                        <option value="DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO">DESTRUCCIÓN FORTUITA, FUERZA MAYOR O IMPREVISTO</option>
                                        <option value="PÉRDIDA O HURTO">PÉRDIDA O HURTO</option>
                                        <option value="PÉRDIDA O HURTO EN PODER DE TERCEROS">PÉRDIDA O HURTO EN PODER DE TERCEROS</option>
                                        <option value="ESCISIÓN">ESCISIÓN</option>
                                        <option value="PERMUTA O CAMBIO">PERMUTA O CAMBIO</option>
                                        <option value="DACIÓN EN PAGO">DACIÓN EN PAGO</option>
                                        <option value="NO SE REQUIERE PARA LA OPERACIÓN">NO SE REQUIERE PARA LA OPERACIÓN</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-section">
                                    <label>DISPOSICIÓN FINAL PROPUESTA:</label>
                                    <select name="disposicion_final" class="form-control">
                                        <option value="SELECCIONE...">SELECCIONE...</option>
                                        <option value="CESIÓN SIN COSTO">CESIÓN SIN COSTO</option>
                                        <option value="VENTA ACTIVO FUNCIONAL">VENTA ACTIVO FUNCIONAL</option>
                                        <option value="VENTA POR CHATARRA">VENTA POR CHATARRA</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <label>OBSERVACIONES:</label>
                                <textarea name="observaciones" class="form-control"></textarea>
                            </div>
                            <!-- <div class="col-lg-12">
                                <p class="disclaimer"><span>*</span> El valor de depreciación y el valor contable neto puede variar mes por mes, debido al proceso de depreciación mensual.</p>
                            </div> -->
                        </div>
                        <div class="row show-characteristics">
                            <span>Ver características <i class="ion ion-eye"></i></span>
                        </div>
                        <div class="slide-info row">

                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DEPARTAMENTO:</label>
                                    <span data-name="departamento"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CIUDAD:</label>
                                    <span data-name="municipio"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>UBICACIÓN GEOGRÁFICA:</label>
                                    <span data-name="ubicacion_geografica"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CECO (CENTRO DE COSTOS):</label>
                                    <span data-name="nombre_ceco"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>NÚMERO DE CECO (CENTRO DE COSTOS):</label>
                                    <span data-name="numero_ceco"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>ÁREA:</label>
                                    <span data-name="area"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>VICEPRESIDENCIA:</label>
                                    <span data-name="vicepresidencia"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>GERENCIA:</label>
                                    <span data-name="gerencia"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DENOMINACIÓN:</label>
                                    <span data-name="denominacion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>DENOMINACIÓN 2:</label>
                                    <span data-name="denominacion_2"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>MARCA DEL FABRICANTE:</label>
                                    <span data-name="marca_fabricante"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>MODELO:</label>
                                    <span data-name="modelo"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>SERIE:</label>
                                    <span data-name="serie"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>TAG:</label>
                                    <span data-name="tag"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>VALOR DE ADQUISICIÓN:</label>
                                    <span data-name="valor_adquisicion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>VALOR CONTABLE NETO:</label>
                                    <span data-name="valor_contable_neto"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 hidden">
                                <div class="form-section">
                                    <label>CÓDIGO DE MUNICIPIO:</label>
                                    <span data-name="cod_municipio"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 hidden">
                                <div class="form-section">
                                    <label>CÓDIGO DE UBICACIÓN GEOGRÁFICA:</label>
                                    <span data-name="cod_ubicacion_geografica"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>FECHA DE CAPITALIZACIÓN:</label>
                                    <span data-name="fecha_capitalizacion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>TIPO DE OPERACIÓN:</label>
                                    <span data-name="tipo_operacion"></span>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-section">
                                    <label>CUSTODIO:</label>
                                    <span data-name="custodio"></span>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row disclaimer">
                            <div class="col-lg-12 mv20">
                                <p>* Ingrese número de activo o número de inventario para hacer la consulta. ** La baja total es para activos principales, en baja parcial solo se puede dar de baja hasta el 75% del valor de adquisición del activo. *** En baja parcial es posible asignarle todo el valor de adquisición a un subnúmero para hacerle una baja total</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 text-center">
                                <button class="btn btn-styled btn-add" type="button">AGREGAR A SOLICITUD</button>
                                <button class="btn btn-styled2 btn-end" type="button">FINALIZAR SOLICITUD</button>
                            </div>
                        </div>
                    </div>
                    <div class="multiple-view">
                        <div class="status-block">
                            <nav class="nav-tabs" data-tab="multiple">
                                <ul>
                                    <li class="active" data-type="success">Activos agregados <i>0</i></li>
                                    <li data-type="error">Activos con errores <i>0</i></li>
                                </ul>
                            </nav>
                            <div class="tabs" data-tab="multiple">
                                <div class="tab active">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-secondary button2 btn-end">Finalizar solicitud</button>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div class="text-center">
                                        <button type="button" class="btn btn-secondary button2 btn-error">Descargar errores</button>
                                    </div>
                                    <div class="error-multiple">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- <div class="ctrls-table">
                    <span class="ion ion-android-apps" data-toggle="tooltip" title="Ver otras características"></span>
                </div> -->
                <div class="inline-table">
                    <table class="table table-striped table-responsive table-bajas table-data">
                        <thead class="thead-default">
                            <tr>
                                <th>&nbsp;</th>
                                <th>&nbsp;</th>
                                <th>Número de inventario</th>
                                <th>Número de activo</th>
                                <th>SN</th>
                                <th class="always-hide-column">Departamento</th>
                                <th class="always-hide-column">Municipio</th>
                                <th class="always-hide-column">Ubicación geográfica</th>
                                <th class="always-hide-column">Centro de costos</th>
                                <th class="hide-column"># Centro de costos</th>
                                <th class="always-hide-column">Área</th>
                                <th class="always-hide-column">Vicepresidencia</th>
                                <th class="always-hide-column">Gerencia</th>
                                <th>Denominación</th>
                                <th class="always-hide-column">Marca del fabricante</th>
                                <th class="always-hide-column">Modelo</th>
                                <th class="always-hide-column">Serie</th>
                                <th class="always-hide-column">Tag</th>
                                <th>Motivo del retiro</th>
                                <th>Disposición final</th>
                                <th class="hide-column">Valor de adquisición</th>
                                <th>Valor de baja</th>
                                <th>Tipo de baja</th>
                                <th class="always-hide-column">Código de municipio</th>
                                <th class="always-hide-column">Código de ubicación geográfica</th>
                                <th class="always-hide-column">Denominación 2</th>
                                <th>Observaciones</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <!-- <div class="row">
                    <div class="col-lg-12 show-list">
                        <label data-modal="bajas">Ver listado de bajas <i class="ion ion-android-list" data-modal="bajas"><sup>10</sup></i></label>
                    </div>
                </div> -->
            </div>
        </section>
        <div class="modalbox" data-modal="subnumbers">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="cover-table"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modalbox" data-modal="bajas">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="ctrls-table">
                            <span class="ion ion-android-apps" data-toggle="tooltip" title="Ver otras características"></span>
                        </div>
                        <div class="cover-table">
                            <table class="table table-striped table-responsive table-bajas table-data">
                                <thead class="thead-default">
                                    <tr>
                                        <th>&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th>Número de inventario</th>
                                        <th>Número de activo</th>
                                        <th>SN</th>
                                        <th class="always-hide-column">Departamento</th>
                                        <th class="always-hide-column">Municipio</th>
                                        <th class="always-hide-column">Ubicación geográfica</th>
                                        <th class="always-hide-column">Centro de costos</th>
                                        <th class="hide-column"># Centro de costos</th>
                                        <th class="always-hide-column">Área</th>
                                        <th class="always-hide-column">Vicepresidencia</th>
                                        <th class="always-hide-column">Gerencia</th>
                                        <th>Denominación</th>
                                        <th class="always-hide-column">Marca del fabricante</th>
                                        <th class="always-hide-column">Modelo</th>
                                        <th class="always-hide-column">Serie</th>
                                        <th class="always-hide-column">Tag</th>
                                        <th>Motivo del retiro</th>
                                        <th>Disposición final</th>
                                        <th class="hide-column">Valor de adquisición</th>
                                        <th>Valor de baja</th>
                                        <th>Tipo de baja</th>
                                        <th class="always-hide-column">Código de municipio</th>
                                        <th class="always-hide-column">Código de ubicación geográfica</th>
                                        <th class="always-hide-column">Denominación 2</th>
                                        <th class="hide-column">Observaciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="modalbox" data-modal="multiples">
            <div class="table">
                <div class="table-cell">
                    <div class="content">
                        <span class="ion ion-android-cancel close-modal"></span>
                        <div class="masive_data">
                            <h2 class="title">Carga masiva de datos</h2>
                            <p>Por favor seleccione el tipo de baja que quiere realizar y acontinuación ingrese los números de activos seguidos del sn y el valor de la baja separados por coma (,) espacios o tabulaciones según sea el caso separando cada registro por enter o punto y coma (;)</p>
                            <div class="form-section text-center">
                                <label class="label-selector">
                                    <input name="tipo_baja_multiple" type="radio" value="total" checked> <i></i>
                                    <span>BAJA TOTAL</span>
                                </label>
                                <label class="label-selector">
                                    <input name="tipo_baja_multiple" type="radio" value="parcial"> <i></i>
                                    <span>BAJA PARCIAL</span>
                                </label>
                            </div>
                            <div class="cover_row_masive_data">
                                <table class="rows_masive_data">
                                    <thead>
                                        <tr>
                                            <th>NO. INVENTARIO</th>
                                            <th>ACTIVO</th>
                                            <th>SN</th>
                                            <th>VALOR DE BAJA</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><textarea type="text" name="multipleInventario" class="form-control"></textarea></td>
                                            <td><textarea type="text" name="multipleActive" class="form-control"></textarea></td>
                                            <td><textarea type="text" name="multipleSN" class="form-control"></textarea></td>
                                            <td><textarea type="text" name="multipleLow" class="form-control"></textarea></td>
                                            <td><i class="ion ion-trash-b"></i></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="newRow-multiple"><span class="ion ion-plus-circled"></span> Nuevo registro</div>
                            </div>
                            <div class="form-section text-center">
                                <button class="btn btn-styled btn-add-multiple">Continuar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </main>
    <footer class="main-footer">

    </footer>
    <div class="tankyou-message">
        <div class="table">
            <div class="table-cell">
                <h2 class="title">GRACIAS POR REALIZAR LA SOLICITUD, <br>PRONTO ATENDEREMOS SU REQUERIMIENTO</h2>
            </div>
        </div>
    </div>
    <div id="wait">
        <div>
            <img src='img/demo_wait.gif' width="64" height="64" />
            <br>Cargando...
        </div>
    </div>
    <script src="vendor/jquery/jquery-1.12.4.js"></script>
    <script src="vendor/jqueryui/jquery-ui.js"></script>
    <script src="vendor/bootstrap/bootstrap3-3-7.min.js"></script>
    <script src="js/lib/jquery.priceformat.min.js"></script>
    <script src="js/lib/jquery.validate.min.js"></script>
    <script src="js/functions-bajas.js"></script>
    <script src="js/data-load-bajas.js"></script>
</body>
</html>